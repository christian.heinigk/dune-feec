# .. cmake_module::
#
#    This modules content is executed whenever a module required or suggests dune-grid!
#

dune_require_cxx_standard(MODULE "dune-feec" VERSION 14)
if (NOT CXX_MAX_SUPPORTED_STANDARD LESS 17)
  add_compile_options(-DDEC_HAS_CXX17=1)
endif()

find_package(SuiteSparse OPTIONAL_COMPONENTS UMFPACK)
include(AddSuiteSparseFlags)

find_package(Eigen3 REQUIRED 3.2)
set(DEC_HAS_EIGEN true)

include(add_feec_executable)
