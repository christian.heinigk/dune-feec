macro(target_enable_backend_pkg _TARGET_ _SCOPE_)

  add_library(mtl4lib INTERFACE)

  find_package(MTL REQUIRED)
  target_include_directories(mtl4lib INTERFACE ${MTL_INCLUDE_DIRS})
  target_link_libraries(mtl4lib INTERFACE ${MTL_LIBRARIES})
  target_compile_options(mtl4lib INTERFACE ${MTL_LIBRARIES})

  message(STATUS "  Found MTL, version: ${MTL_VERSION}.${MTL_MINOR_VERSION}")

  target_link_libraries(${_TARGET_} ${_SCOPE_} mtl4lib)

endmacro(target_enable_backend_pkg)
