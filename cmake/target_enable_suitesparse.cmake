macro(target_enable_suitesparse _TARGET_ _SCOPE_)
  find_package(SuiteSparse OPTIONAL_COMPONENTS ${ARGN})

  option(ENABLE_SUITESPARSE
         "Enable Library-collection SuiteSparse" ${SuiteSparse_FOUND})

  if (ENABLE_SUITESPARSE)
    add_library(suitesparselib INTERFACE)
    target_include_directories(suitesparselib INTERFACE ${SuiteSparse_INCLUDE_DIRS})
    target_link_libraries(suitesparselib INTERFACE ${SuiteSparse_LIBRARIES})

    message(STATUS "SuiteSparse_LIBRARIES = ${SuiteSparse_LIBRARIES}")

    target_compile_definitions(suitesparselib INTERFACE "DEC_HAS_SUITESPARSE=1")
    target_link_libraries(${_TARGET_} ${_SCOPE_} suitesparselib)
    message(STATUS "  Found SuiteSparse")
  endif (ENABLE_SUITESPARSE)
endmacro(target_enable_suitesparse)
