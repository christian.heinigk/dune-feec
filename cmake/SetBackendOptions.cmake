# inspired by: http://blog.bethcodes.com/cmake-tips-tricks-drop-down-list

set(LINEAR_ALGEBRA_BACKEND "Default" CACHE STRING "The backend for sparse matrix and vector operations (Eigen/PETSc/MTL/Default)")
set_property(CACHE LINEAR_ALGEBRA_BACKEND PROPERTY STRINGS Eigen PETSc MTL Default)


if (NOT (DEFINED BACKEND_LAST))
  set(BACKEND_LAST "NotAnOption" CACHE STRING "last backend loaded")
  mark_as_advanced(FORCE BACKEND_LAST)
endif()

if (NOT (${LINEAR_ALGEBRA_BACKEND} MATCHES ${BACKEND_LAST}))
  unset(LIBRARY_INCLUDES CACHE)
  set(BACKEND_LAST ${LINEAR_ALGEBRA_BACKEND} CACHE STRING "Updating backend project configuration option" FORCE)
endif()


if (${LINEAR_ALGEBRA_BACKEND} MATCHES "Eigen")
  set(LIBRARY_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/Vendor/Option1")
endif()

if (${LINEAR_ALGEBRA_BACKEND} MATCHES "PETSc")
  set(LIBRARY_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/Vendor/Option2")
endif()

if (${LINEAR_ALGEBRA_BACKEND} MATCHES "MTL")
  set(LIBRARY_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/Vendor/Option1")
endif()

if (${LINEAR_ALGEBRA_BACKEND} MATCHES "Default")
  set(LIBRARY_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/Vendor/Option1")
endif()


macro(target_enable_backend _TARGET_ _SCOPE_)
  if (NOT BACKEND_FOUND)
    include(Backend${LINEAR_ALGEBRA_BACKEND}_config)

    # macro should be provided by package
    target_enable_backend_pkg(${_TARGET_} ${_SCOPE_})

    string(TOUPPER "${LINEAR_ALGEBRA_BACKEND}" BACKEND_STR)
    target_compile_definitions(${_TARGET_} ${_SCOPE_} "DEC_HAS_${BACKEND_STR}=1")

    set(DEC_HAS_${BACKEND_STR} ON)
    set(BACKEND_FOUND ON)
  endif()
endmacro(target_enable_backend)
