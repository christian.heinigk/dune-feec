macro(target_add_sources _TARGET_ _SCOPE_)
  set(sources "")
  foreach(source ${ARGN})
    list(APPEND sources ${CMAKE_CURRENT_SOURCE_DIR}/${source})
  endforeach()

  target_sources(${_TARGET_} ${_SCOPE_} ${sources})
endmacro(target_add_sources)
