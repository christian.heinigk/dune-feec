macro(target_enable_gsl _TARGET_ _SCOPE_)
  find_path(GSL_INC_DIR NAMES gsl_assert gsl_util gsl_algorithm
    PATHS
      ${CMAKE_SOURCE_DIR}/external/GSL
    HINTS
      ${GSL_DIR}/include ${GSL_ROOT}/include
      $ENV{GSL_DIR}/include $ENV{GSL_ROOT}/include
    PATH_SUFFIXES gsl)

  if (GSL_INC_DIR AND EXISTS ${GSL_INC_DIR})
    get_filename_component(GSL_INC_DIR ${GSL_INC_DIR} DIRECTORY) # strip of the last ./gsl subdirectory
  else ()
    # if no GSL library was found, try to download a version
    option(DOWNLOAD_GSL "Try to download the Guideline Support Library library" OFF)

    if (DOWNLOAD_GSL)
      set(GSL_URL "https://github.com/Microsoft/GSL.git")
      message(STATUS "  Downloading GSL library from ${GSL_URL} ...")
      find_package(Git)
      if (Git_FOUND)
        file(MAKE_DIRECTORY ${CMAKE_SOURCE_DIR}/external)
        execute_process(COMMAND
          ${GIT_EXECUTABLE} clone --quiet --single-branch ${GSL_URL} ${CMAKE_SOURCE_DIR}/external/GSL)

        set(GSL_INC_DIR ${CMAKE_SOURCE_DIR}/external/GSL)
      else ()
        message(FATAL_ERROR "No GIT found for downloading GSL")
      endif()
    endif (DOWNLOAD_GSL)
  endif ()

  if (GSL_INC_DIR AND EXISTS ${GSL_INC_DIR})
    add_library(gsllib INTERFACE)
    target_include_directories(gsllib INTERFACE ${GSL_INC_DIR})
    target_compile_definitions(gsllib INTERFACE "DEC_HAS_GSL=1")
    target_link_libraries(${_TARGET_} ${_SCOPE_} gsllib)
    unset(GSL_INC_DIR CACHE)
  else ()
#     message(FATAL_ERROR "GSL library needed. Install it manually or set option DOWNLOAD_GSL!")
  endif ()
endmacro(target_enable_gsl)
