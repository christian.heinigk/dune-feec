macro(target_enable_backend_pkg _TARGET_ _SCOPE_)
  if (NOT EXISTS ${CMAKE_SOURCE_DIR}/external/eigen)
    find_package(Eigen3)
  endif ()

  if (EIGEN3_FOUND)
    message(STATUS "  Found Eigen3, version: ${EIGEN3_VERSION}")
  else (EIGEN3_FOUND)
    # if no eigen library was found, try to download a version
    option(DOWNLOAD_EIGEN3 "Try to download the eigen3 library" OFF)

    if (DOWNLOAD_EIGEN3)
      set(EIGEN3_URL "https://bitbucket.org/eigen/eigen/")
      set(EIGEN3_VERSION "3.3.1")
      message(STATUS "  Downloading Eigen3 (${EIGEN3_VERSION}) library from ${EIGEN3_URL} ...")

      find_package(UnixCommands)
      if (TAR)
        # http://bitbucket.org/eigen/eigen/get/3.3.1.tar.gz, NOTE: this is a pseudo url, does not work!
        file(DOWNLOAD ${EIGEN3_URL}/get/${EIGEN3_VERSION}.tar.gz /tmp/eigen3.tar.gz LOG DOWNLOAD_LOG)
        message(STATUS "${DOWNLOAD_LOG}")
        file(MAKE_DIRECTORY ${CMAKE_SOURCE_DIR}/external/eigen)
        execute_process(COMMAND
          ${TAR} -xzf /tmp/eigen3.tar.gz --strip-components=1 -C ${CMAKE_SOURCE_DIR}/external/eigen)
      else (TAR)
        find_package(Hg)
        if (Hg_FOUND)
          file(MAKE_DIRECTORY ${CMAKE_SOURCE_DIR}/external)
          execute_process(COMMAND
            ${HG_EXECUTABLE} clone --branch ${EIGEN3_VERSION} ${EIGEN3_URL} ${CMAKE_SOURCE_DIR}/external/eigen)
        endif (Hg_FOUND)
      endif (TAR)
    endif (DOWNLOAD_EIGEN3)
    set(EIGEN3_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/external/eigen)
  endif (EIGEN3_FOUND)

  if (EIGEN3_INCLUDE_DIR AND EXISTS ${EIGEN3_INCLUDE_DIR})
    add_library(eigenlib INTERFACE)
    target_include_directories(eigenlib INTERFACE ${EIGEN3_INCLUDE_DIR})
    target_compile_options_optional(eigenlib INTERFACE "-Wno-deprecated-declarations")

    target_link_libraries(${_TARGET_} ${_SCOPE_} eigenlib)
  endif ()
endmacro(target_enable_backend_pkg)
