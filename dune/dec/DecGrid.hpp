#pragma once

#include "halfedgegrid/Grid.hpp"

namespace Dec
{
  template <class GridImp>
  using DecGrid = HalfEdgeGrid<GridImp>;

} // end namespace Dec
