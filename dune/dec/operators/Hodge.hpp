#pragma once

#include <cstddef>
#include <type_traits>

#include "Operator.hpp"

namespace Dec
{
  namespace tag
  {
    template <int K> struct primal : public std::integral_constant<int,K> {};
    template <int K> struct dual : public std::integral_constant<int,K> {};

  } // end namespace tag

  template <class GridView, class Domain>
  class Hodge
      : public Operator<Hodge<GridView, Domain>, GridView, Domain::value, Domain::value>
  {
    friend class OperatorAccessor;
    using Super = Operator<Hodge<GridView, Domain>, GridView, Domain::value, Domain::value>;

  public:

    /// Constructor
    Hodge(GridView const& gv)
      : Super(gv)
    {}

    using Super::gridView;


  protected: // Implementation of operator methods:

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
    {
      impl(A, e, factor, Domain{});
    }

    template <class Inserter, class Entity, int K>
    void impl(Inserter& A, Entity const& e, float_type factor, tag::primal<K>) const
    {
      std::size_t i = e.index();
      A(i,i) << factor * gridView().dual_volume(e) / std::max(1.e-10, gridView().volume(e));
    }

    template <class Inserter, class Entity, int K>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor, tag::dual<K>) const
    {
      std::size_t i = e.index();
      A(i,i) << factor * gridView().volume(e) / std::max(1.e-10, gridView().dual_volume(e));
    }
  };

} // end namespace Dec
