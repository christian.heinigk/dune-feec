#pragma once

#include <cmath>

#include <dune/geometry/referenceelements.hh>

#include <dune/dec/Operator.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/utility/FunctorVector.hpp>
#include <dune/dec/utility/MaxVector.hpp>

namespace Dec
{
  /**
    * \addtogroup operators
    * @{
    **/

  template <class GridView, int K> class LaplaceRR;
  template <class GridView, int K> class LaplaceGD;

  /// Laplace-deRham Operator: (-1)^(n*k+1) (*d*d + d*d*)
  template <class GridView, int K>
  class LaplaceDeRham
      : public Operator<LaplaceDeRham<GridView, K>, GridView, K, K>
  {
    friend class OperatorAccessor;
    using Super = Operator<LaplaceDeRham<GridView, K>, GridView, K, K>;

    static_assert( 0 <= K && K <= GridView::dimension, "K out of range [0,dim]" );

  public:

    /// Constructor
    LaplaceDeRham(GridView const& gv, float_type factor = 1)
      : Super(gv)
      , laplaceRR(gv)
      , laplaceGD(gv)
      , scale(factor * std::pow(-1, GridView::dimension*K + 1))
    {}

  protected: // Implementation of operator methods:

    auto nzrows_impl() const
    {
      return max_vector(laplaceRR.nzrows(), laplaceGD.nzrows());
    }

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
    {
      laplaceRR.assembleRow(A, e, factor*scale);
      laplaceGD.assembleRow(A, e, factor*scale);
    }

  private:

    LaplaceRR<GridView,K> laplaceRR;
    LaplaceGD<GridView,K> laplaceGD;
    float_type scale;
  };

  template <class GridView>
  using LaplaceBeltrami = LaplaceDeRham<GridView,0>;


  /// RotRot-Laplace: (*d*d)
  template <class GridView, int K>
  class LaplaceRR
      : public Operator<LaplaceRR<GridView, K>, GridView, K, K>
  {
    friend class OperatorAccessor;
    template <class,int> friend class LaplaceDeRham;

    using Super = Operator<LaplaceRR<GridView, K>, GridView, K, K>;

    constexpr int sign(int p, int n) const { return p*(n-p) % 2 == 0 ? 1 : -1; }

  public:

    static constexpr int dim = GridView::dimension;

    /// Constructor
    LaplaceRR(GridView const& gv)
      : Super(gv)
    {}

    using Super::gridView;

  protected: // Implementation of operator methods:

//     auto nzrows_impl() const
//     {
//       return nzrows_impl(int_<K>);
//     }
//
//     template <int d>
//     auto nzrows_impl(int_t<d>) const
//     {
//       return make_vector([this](std::size_t i)
//       {
//         auto const& elem = Dune::ReferenceElements<float_type, d+1>::simplex();
//         EntityIndex<dim-d, dim> e{i};
//         auto const& T = entities(e, Codim_<dim-d-1>);
//         return T.size()*(elem.size(1) - 1) + 1;
//       });
//     }
//
//     auto nzrows_impl(int_t<dim>) const
//     {
//       return make_vector([](std::size_t) { return 0u; });
//     }

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
    {
      assembleRow_impl(A, e, factor, int_<K>);
    }

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& v, float_type factor, int_t<0>) const
    {
      auto const& indexSet = gridView().indexSet();
      std::size_t v_i = indexSet.index(v);
      double dual_volume_v = gridView().dual_volume(v);

      for (auto const& e : edges(v)) {
        if (!indexSet.owns(e))
          continue;
        double coeff = factor * gridView().dual_volume(e) / this->regularize(gridView().volume(e) * dual_volume_v);

        for (auto const& v_ : vertices(e)) {
          std::size_t v_j = indexSet.index(v_);
          A(v_i,v_j) << (v_j == v_i ? -1 : 1) * coeff;
        }
      } // end for(e)
    }

    template <class Inserter, class Entity, int d>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor, int_t<d>) const
    {
      auto const& indexSet = gridView().indexSet();
      std::size_t e_i = indexSet.index(e);

      // assemble Laplace^RR operator
      double coeff0 = sign(d,dim) * factor * gridView().volume(e) / this->regularize(gridView().dual_volume(e));

      for (auto const& T : entities(e, Codim_<Entity::codimension-1>)) {
        if (!indexSet.owns(T))
          continue;
        double coeff =  T.sign(e) * coeff0 * gridView().dual_volume(T) / this->regularize(gridView().volume(T));

        for (auto const& e_ : entities(T, Codim_<Entity::codimension>)) {
          std::size_t e_j = indexSet.index(e_);
          A(e_i,e_j) << e_.sign(T) * coeff;
        }
      }
    }

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& /*A*/, Entity const& /*e*/, float_type /*factor*/, int_t<dim>) const
    {
      /* do nothing */
    }
  };


  /// GradDiv-Laplace: (d*d*)
  template <class GridView, int K>
  class LaplaceGD
      : public Operator<LaplaceGD<GridView, K>, GridView, K, K>
  {
    friend class OperatorAccessor;
    template <class,int> friend class LaplaceDeRham;

    using Super = Operator<LaplaceGD<GridView, K>, GridView, K, K>;

    constexpr int sign(int p, int n) const { return p*(n-p) % 2 == 0 ? 1 : -1; }

  public:

    static constexpr int dim = GridView::dimension;

    /// Constructor
    LaplaceGD(GridView const& gv)
      : Super(gv)
    {}

    using Super::gridView;

  protected: // Implementation of operator methods:

//     auto nzrows() const
//     {
//       return nzrows_impl(int_<K>);
//     }
//
//     auto nzrows_impl(int_t<0>) const
//     {
//       return make_vector([](std::size_t) { return 0u; });
//     }
//
//     template <int d>
//     auto nzrows_impl(int_t<d>) const
//     {
//       return make_vector([this](std::size_t i)
//       {
//         auto const& elem = Dune::ReferenceElements<float_type, d>::simplex();
//         EntityIndex<dim-d, dim> e{i};
//
//         std::size_t sum = 1;
//         for (auto const& v : this->gridView().template entities<dim-d+1>(e))
//           sum += this->gridView().template entities<dim-d>(v).size();
//
//         return sum - elem.size(1);
//       });
//     }

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
    {
      assembleRow_impl(A, e, factor, int_<K>);
    }

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& /*A*/, Entity const& /*e*/, float_type /*factor*/, int_t<0>) const
    {
      /* do nothing */
    }

    template <class Inserter, class Entity, int d>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor, int_t<d>) const
    {
      auto const& indexSet = gridView().indexSet();
      std::size_t e_i = indexSet.index(e);

      // assemble Laplace^GD operator
      for (auto const& v : entities(e, Codim_<dim-d+1>)) {
        if (!indexSet.owns(v))
          continue;
        double coeff = sign(dim-d+1,dim) * factor * v.sign(e) * gridView().volume(v) / this->regularize(gridView().dual_volume(v));

        for (auto const& e_ : entities(v, Codim_<dim-d>)) {
          std::size_t e_j = indexSet.index(e_);
          A(e_i,e_j) << (v.codimension == dim ? -1 : 1) * e_.sign(v) * coeff * gridView().dual_volume(e_) / this->regularize(gridView().volume(e_));
        }
      }
    }

  };

  /** @} */

} // end namespace Dec
