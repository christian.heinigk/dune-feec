#pragma once

#include <cmath>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include <dune/dec/Operator.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/utility/FunctorVector.hpp>

namespace Dec
{
  /**
    * \addtogroup operators
    * @{
    **/

  template <class GridView>
  class FEMCenterWeight
  {
    static constexpr int K = 0;

    using FeSpace = Dune::Functions::PQ1NodalBasis<GridView>;

  public:

    /// Constructor
    FEMCenterWeight(GridView const& gv)
      : gv_(gv)
      , feSpace_(gv)
    {}

    template <class Vector>
    void assemble_vec(Vector& vec, float_type factor = 1) const
    {
      vec.resize(gv_.size(GridView::dimension));

      auto localView = feSpace_.localView();
      auto localIndexSet = feSpace_.localIndexSet();

      static constexpr int Size = GridView::dimension+1;
      SmallVector<float_type, Size> elementVector;

//       auto const& indexSet = grid_.leafGridView().indexSet();
      for (auto const& element : elements(gv_)) {
        localView.bind(element);
        localIndexSet.bind(localView);

        set_to_zero(elementVector);

        auto geometry = element.geometry();
        auto const term = geometry.center() - geometry.barycenter();
//         auto const term = grid_.geometry_.template center<0>(indexSet.index(element)) - element.geometry().center();

        auto const& localBasis = localView.tree().finiteElement().localBasis();

        auto const& quad = Dune::QuadratureRules<double, GridView::dimension>::rule(element.type(), 1);
        for (size_t iq = 0; iq < quad.size(); ++iq) {
          // Position of the current quadrature point in the reference element
          auto const& quadPos = quad[iq].position();

          // The transposed inverse Jacobian of the map from the reference element to the element
          auto const jacobian = geometry.jacobianInverseTransposed(quadPos);

          // The multiplicative factor in the integral transformation formula
          double const scaling = factor * geometry.integrationElement(quadPos) * quad[iq].weight();

          // The gradients of the shape functions on the reference element
          std::vector<Dune::FieldMatrix<double,1,GridView::dimension> > referenceGradients;
          localBasis.evaluateJacobian(quadPos, referenceGradients);

          // Compute the shape function gradients on the real element
          std::vector<Dune::FieldVector<double,GridView::dimensionworld> > gradients(referenceGradients.size());

          for (std::size_t i = 0; i < gradients.size(); ++i)
            jacobian.mv(referenceGradients[i][0], gradients[i]);

          for (int i = 0; i < Size; ++i) {
            int const local_i = localView.tree().localIndex(i);
            elementVector[local_i] += gradients[i].dot(term) * scaling;
          }
        }

        for (int i = 0; i < Size; ++i) {
          // The global index of the i−th vertex of the element
          auto const row = localIndexSet.index(i);
          vec[row] += elementVector[i];
        }
      }
    }

  private:

    GridView gv_;
    FeSpace feSpace_;
  };

  /** @} */

} // end namespace Dec
