#pragma once

#include <dune/dec/Operator.hpp>

namespace Dec
{
  /**
    * \addtogroup operators
    * @{
    **/

  /// Scale an operator by a scalar factor
  template <class Model, class GridView, int N, int M>
  class ScaleOperator
      : public Operator<ScaleOperator<Model,GridView,N,M>, GridView, N, M>
  {
    friend class OperatorAccessor;
    using Super = Operator<ScaleOperator<Model,GridView,N,M>, GridView, N, M>;


  public:

    /// Constructor, stores a copy to the operator `op`
    ScaleOperator(Model const& op, float_type scale)
      : Super(op.grid)
      , op_(op)
      , scale_(scale)
    {}


  protected: // Implementation details:

    auto nzrows_impl() const
    {
      return op_.nzrows();
    }

    template <class Inserter>
    void assemble_impl(Inserter& A, float_type factor) const
    {
      op_.assemble(A, factor*scale_);
    }


  private: // Member variables:

    Model op_;
    float_type scale_;
  };

  /// \brief A generator method for \ref ScaleOperator, i.e. multiplication from
  /// right with a scalar. \relates ScaleOperator
  template <class Model, class GridView, int N, int M>
  ScaleOperator<Model, GridView, N, M>
  operator*(Operator<Model,GridView,N,M> const& lhs, float_type scale)
  {
    return {lhs.derived(), scale};
  }

  /// \brief A generator method for \ref ScaleOperator, i.e. multiplication from
  /// left with a scalar. \relates ScaleOperator
  template <class Model, class GridView, int N, int M>
  ScaleOperator<Model, GridView, N, M>
  operator*(float_type scale, Operator<Model,GridView,N,M> const& rhs)
  {
    return {rhs.derived(), scale};
  }

  /** @} */

} // end namespace Dec
