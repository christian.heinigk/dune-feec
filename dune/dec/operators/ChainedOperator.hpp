#pragma once

#include <dune/dec/Operator.hpp>

namespace Dec
{
  /**
    * \addtogroup operators
    * @{
    **/

  /// Adding more than one operator to a matrix, i.e. chaining by `plus`.
  template <class M1, class M2, class GridView, int N, int M>
  class ChainedOperator
      : public Operator<ChainedOperator<M1,M2,GridView,N,M>, GridView, N, M>
  {
    friend class OperatorAccessor;
    using Super = Operator<ChainedOperator<M1,M2,GridView,N,M>, GridView, N, M>;


  public:

    /// Constructor, stores copies of the two sub-operators `op1` and `op2`.
    ChainedOperator(M1 const& op1, M2 const& op2)
      : Super(op1.gridView())
      , op1_(op1)
      , op2_(op2)
    {}


  protected: // Implementation details:

    auto nzrows_impl() const
    {
      return max_vector(op1_.nzrows(), op2_.nzrows());
    }

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
    {
      op1_.assembleRow(A, e, factor);
      op2_.assembleRow(A, e, factor);
    }


  private: // Member variables:

    M1 op1_;
    M2 op2_;
  };


  /// A generator method for \ref ChainedOperator, \relates ChainedOperator
  template <class Model1, class Model2, class GridView, int N, int M>
  ChainedOperator<Model1, Model2, GridView, N, M>
  operator+(Operator<Model1,GridView,N,M> const& lhs, Operator<Model2,GridView,N,M> const& rhs)
  {
    return {lhs.derived(), rhs.derived()};
  }

  /** @} */

} // end namespace Dec
