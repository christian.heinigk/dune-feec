#pragma once

#include <cstddef>

#include <dune/dec/Operator.hpp>

namespace Dec
{
  /**
    * \addtogroup operators
    * @{
    **/

  /// Operator representing a (scaled) identity
  template <class GridView, int K>
  class Identity
      : public Operator<Identity<GridView, K>, GridView, K, K>
  {
    friend class OperatorAccessor;
    using Super = Operator<Identity<GridView, K>, GridView, K, K>;

    static_assert( 0 <= K && K <= GridView::dimension, "K out of range [0,dim]" );

  public:

    /// Constructor, stores a reference to the `gv`.
    Identity(GridView const& gv, float_type value = 1)
      : Super(gv)
      , value_(value)
    {}

  protected: // Implementation of operator methods:

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
    {
      std::size_t i = e.index();
      A(i,i) << factor*value_;
    }

  private:

    float_type value_;
  };

  /** @} */

} // end namespace Dec
