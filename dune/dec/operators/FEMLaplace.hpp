#pragma once

#include <cmath>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include <dune/dec/Operator.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/utility/FunctorVector.hpp>

namespace Dec
{
  /**
    * \addtogroup operators
    * @{
    **/

  /// An operator representing a Lagrange-P1 FEM laplace on simplicial elements.
  template <class GridView>
  class FEMLaplace
      : public Operator<FEMLaplace<GridView>, GridView, 0, 0>
  {
    static constexpr int K = 0;

    friend class OperatorAccessor;
    using Super = Operator<FEMLaplace<GridView>, GridView, K, K>;

    using FeSpace = Dune::Functions::PQ1NodalBasis<GridView>;

  public:

    /// Constructor, stores a reference to `greid`.
    FEMLaplace(GridView const& gv)
      : Super(gv)
      , feSpace_(gv)
    {}

  protected: // Implementation of operator methods:

//     auto nzrows_impl() const
//     {
//       return make_vector([this](std::size_t v_i) {
//         auto const& edges = this->grid_.edges<Grid::dimension>(v_i);
//         return std::accumulate(edges.begin(), edges.end(), [this](auto const e) {
//           return this->grid_.vertices(e).size();
//         }) + 1;
//       });
//     }

    using Super::gridView;

    template <class Matrix>
    void assemble_impl(Matrix& A, float_type factor) const
    {
      auto localView = feSpace_.localView();
      auto localIndexSet = feSpace_.localIndexSet();

      static constexpr int Size = GridView::dimension+1;
      SmallMatrix<float_type, Size, Size> elementMatrix;

      for (auto const& element : elements(gridView())) {
        localView.bind(element);
        localIndexSet.bind(localView);

        set_to_zero(elementMatrix);

        auto geometry = element.geometry();
        auto const& localBasis = localView.tree().finiteElement().localBasis();

        auto const& quad = Dune::QuadratureRules<double, GridView::dimension>::rule(element.type(), 1);
        for (std::size_t iq = 0; iq < quad.size(); ++iq) {
          // Position of the current quadrature point in the reference element
          auto const& quadPos = quad[iq].position();

          // The transposed inverse Jacobian of the map from the reference element to the element
          auto const jacobian = geometry.jacobianInverseTransposed(quadPos);

          // The multiplicative factor in the integral transformation formula
          double const scaling = factor * geometry.integrationElement(quadPos) * quad[iq].weight();

          // The gradients of the shape functions on the reference element
          std::vector<Dune::FieldMatrix<double,1,GridView::dimension> > referenceGradients;
          localBasis.evaluateJacobian(quadPos, referenceGradients);

          // Compute the shape function gradients on the real element
          std::vector<Dune::FieldVector<double,GridView::dimensionworld> > gradients(referenceGradients.size());

          for (std::size_t i = 0; i < gradients.size(); ++i)
            jacobian.mv(referenceGradients[i][0], gradients[i]);

          for (int i = 0; i < Size; ++i) {
            int const local_i = localView.tree().localIndex(i);
            for (int j = 0; j < Size; ++j) {
              int const local_j = localView.tree().localIndex(j);
              elementMatrix(local_i,local_j) += gradients[i].dot(gradients[j]) * scaling;
            }
          }
        }

        for (int i = 0; i < Size; ++i) {
          // The global index of the i−th vertex of the element
          auto const row = localIndexSet.index(i);
          for (int j = 0; j < Size; ++j) {
            // The global index of the j−th vertex of the element
            auto const col = localIndexSet.index(j);
            A(row, col) << elementMatrix(i, j);
          }
        }
      }
    }

  private:

    FeSpace feSpace_;
  };

  /** @} */

} // end namespace Dec
