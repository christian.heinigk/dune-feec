#pragma once

#include <algorithm>
#include <iterator>
#include <numeric>
#include <vector>

#include <dune/common/deprecated.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/GridTransfer.hpp>
#include <dune/dec/common/Dim.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/utility/TupleUtility.hpp>

#include "Entity.hpp"
#include "GeometryCache.hpp"
#include "GridView.hpp"
#include "GridFamily.hpp"
#include "HalfEdge.hpp"
#include "IndexSet.hpp"
#include "GeometrySynchronization.hpp"

namespace Dec
{

  template <class GridBase>
  class HalfEdgeGrid
      : public Dune::GridDefaultImplementation<GridBase::dimension, GridBase::dimensionworld, Dec::float_type, HalfEdgeGridFamily<GridBase>>
  {
    using Self = HalfEdgeGrid;

  public:

    static constexpr int dimension = GridBase::dimension;
    static constexpr int dimensionworld = GridBase::dimensionworld;

    static_assert( dimension == 2, "Surface grids required!" );

    using ctype = float_type;
    using IndexType = typename HalfEdge::IndexType;

    using Geometry = GeometryCache<HalfEdgeGridFamily<GridBase>>;
    using GlobalCoordinate = typename Geometry::GlobalCoordinate;
    using GridView = HalfEdgeGridView<Self>;
    using IndexSet = HalfEdgeIndexSet<dimension>;
    using LeafGridView = GridView;
    using LeafIndexSet = IndexSet;
    using LevelGridView = GridView;
    using LevelIndexSet = IndexSet;
    using Traits = typename HalfEdgeGridFamily<GridBase>::Traits;
    using Transfer = GridTransfer<dimension>;

    template <int cd>
    struct Codim
    {
      using Entity = HalfEdgeEntity<cd, dimension, Self>;
      using EntitySeed = Dec::EntitySeed<cd, Self>;
    };

    template <int cd>
    using Entity_t = typename Codim<cd>::Entity;


  public:

    /// Constructor, stores a reference to `grid`.
    HalfEdgeGrid(GridBase& grid, bool store_intermediate = false)
      : gridBase_(grid)
    {
      if (store_intermediate) {
        indexSets_.emplace_back(grid.levelGridView(0));
        geometries_.emplace_back(grid.levelGridView(0), grid.levelIndexSet(0), indexSets_.back());
        synchGeometry(leafGridView(), geometries_.back());
        levels_.push_back(0);

        for (int l = 1; l <= grid.maxLevel(); ++l) {
          GradTransferFactory<GridBase> factory(gridBase_);
          indexSets_.emplace_back(grid.levelGridView(l));
          geometries_.emplace_back(grid.levelGridView(l), grid.levelIndexSet(l), indexSets_.back());
          synchGeometry(leafGridView(), geometries_.back());

          transfer_.emplace_back(factory.create());
          levels_.push_back(l);
        }
      } else {
        indexSets_.emplace_back(grid.leafGridView());
        geometries_.emplace_back(grid.leafGridView(), grid.leafIndexSet(), indexSets_.back());
        synchGeometry(leafGridView(), geometries_.back());
        levels_.push_back(grid.maxLevel());
      }
    }

    /// Returns maximum level defined in this grid.
    int maxLevel() const { return indexSets_.size()-1; }

    /// Returns number of leaf entities of a given codim.
    IndexType size(int codim) const
    {
      return leafIndexSet().size(codim);
    }

    /// Returns number of level entities of a given codim.
    IndexType size(int level, int codim) const
    {
      return levelIndexSet(level).size(codim);
    }

    /// Returns number of leaf entities per geometry type.
    IndexType size(Dune::GeometryType type) const
    {
      return leafIndexSet().size(type);
    }

    /// Returns number of level entities per geometry type.
    IndexType size(int level, Dune::GeometryType type) const
    {
      return levelIndexSet(level).size(type);
    }


    /// Return const reference to the grids leaf index set. NOTE: deprecated
    IndexSet const& DUNE_DEPRECATED indexSet() const { return indexSets_.back(); }

    /// Return const reference to the grids leaf index set.
    IndexSet const& leafIndexSet() const { return indexSets_.back(); }

    /// Return const reference to the grids level index set.
    IndexSet const& levelIndexSet(int level) const
    {
      assert( level >= -1 && level < int(indexSets_.size()) );
      return level == -1 ? leafIndexSet() : indexSets_[level];
    }


    /// View for this grid. NOTE: deprecated
    GridView DUNE_DEPRECATED gridView() const { return {*this, -1}; }

    /// View for the leaf grid.
    GridView leafGridView() const { return {*this, -1}; }

    /// View for a grid level.
    GridView levelGridView(int level) const { return {*this, level}; }


    /// \brief Refine the grid refCount times using the default refinement rule.
    /// Reinitializes the half-edge structure after refinement.
    void globalRefine(int refCount = 1, bool store_intermediate = false)
    {
      int refStep = store_intermediate ? 1 : refCount;

      for (int i = 0; i < refCount; i += refStep) {
        GradTransferFactory<GridBase> factory(gridBase_);
        gridBase_.globalRefine(refStep);
        indexSets_.emplace_back(gridBase_.leafGridView());
        geometries_.emplace_back(gridBase_.leafGridView(), gridBase_.leafIndexSet(), indexSets_.back());
        synchGeometry(leafGridView(), geometries_.back());

        if (store_intermediate)
          transfer_.emplace_back(factory.create());
        levels_.push_back(gridBase_.maxLevel());
      }
    }

    /// Returns the entity associated with the half-edge index `he`
    template <int cd>
    Entity_t<cd> entity(IndexType he) const
    {
      return {leafIndexSet(), coordinates(), he};
    }

    /// Obtain Entity from EntitySeed.
    template <class EntitySeed>
    Entity_t<EntitySeed::codimension> entity(EntitySeed const& seed) const
    {
      return {leafIndexSet(), coordinates(), seed.he_};
    }

    /// return const reference to a collective communication object.
    auto const& comm() const { return gridBase_.comm(); }


  public: // access to geometry information

    /// Return grid width
    float_type width() const
    {
      float_type h = 0;
      for (auto const& e : edges(leafGridView()))
        h = std::max(h, volume(e));

      return h;
    }

    /// Return the Geomery-Cache for the leaf-level
    Geometry const& leafGeometry() const { return geometries_.back(); }

    /// Return the Geomery-Cache for the given `level`
    Geometry const& levelGeometry(int level) const
    {
      assert( level >= -1 && level < int(geometries_.size()) );
      return level == -1 ? leafGeometry() : geometries_[level];
    }

    /// Returns a vector of vertex coordinates
    auto const& coordinates() const
    {
      return leafGeometry().coordinates();
    }

    /// Returns the (circum-)center of the `entity`.
    template <class Entity>
    GlobalCoordinate const& center(Entity const& entity) const
    {
      return leafGeometry().template center<Entity::codimension>(entity.index());
    }

    /// Returns the volume of the `entity`.
    template <class Entity>
    float_type volume(Entity const& entity) const
    {
      return leafGeometry().template volume<Entity::codimension>(entity.index());
    }

    /// Returns the dual volume of the `entity`.
    template <class Entity>
    float_type dual_volume(Entity const& entity) const
    {
      return leafGeometry().template dual_volume<Entity::codimension>(entity.index());
    }


  public:

    GridBase&       base()       { return gridBase_; }
    GridBase const& base() const { return gridBase_; }

    int baseLevel(int l) const
    {
      return l >= 0 ? levels_[l] : base().maxLevel();
    }

    std::size_t memory() const
    {
      std::size_t mem = 0;

      auto add_memory = [](std::size_t m, auto const& e) { return m + e.memory(); };
      mem = std::accumulate(indexSets_.begin(), indexSets_.end(), mem, add_memory) + sizeof(indexSets_);
      mem = std::accumulate(geometries_.begin(), geometries_.end(), mem, add_memory) + sizeof(geometries_);
      mem = std::accumulate(transfer_.begin(), transfer_.end(), mem, add_memory) + sizeof(transfer_);

      return mem + levels_.capacity()*sizeof(int) + sizeof(levels_);
    }

  private:

    void synchGeometry(GridView const& gv, Geometry& geo)
    {
      GhostSynchHandle<GridView, Geometry> synch(gv, geo);
      gv.communicate(synch, Dune::All_All_Interface, Dune::ForwardCommunication);
    }


  public: // member variables

    GridBase& gridBase_;    //< reference to the underlying grid

    std::vector<IndexSet> indexSets_; //< the stored index-sets for all levels
    std::vector<Geometry> geometries_; //< the cached geometries for all levels
    std::vector<Transfer> transfer_; //< grid transfer operators (interpolation/restriction)

    std::vector<int> levels_;
  };


} // end namespace Dec
