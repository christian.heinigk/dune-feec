#pragma once

#include <array>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>


#include <dune/dec/EntityData.hpp>
#include <dune/dec/ranges/IteratorRange.hpp>
#include <dune/dec/ranges/Map.hpp>
#include <dune/dec/utility/TupleUtility.hpp>

#include "HalfEdge.hpp"

namespace Dec
{

  template <int dim>
  class HalfEdgeIndexSet
  {
  public: // public member types

    static constexpr int dimension = dim;

    using IndexType = typename HalfEdge::IndexType;
    using Types = std::array<Dune::GeometryType, 1>;

  private: // private member types

    using RefElements = Dune::ReferenceElements<float_type, dim>;

  public: // constructors:

    /// Constructor, calls \ref init with given `griView`
    template <class BaseGridView>
    HalfEdgeIndexSet(BaseGridView const& gridView)
      : data_(gridView)
    {
      init(gridView);
    }

    HalfEdgeIndexSet(HalfEdgeIndexSet const&) = delete;
    HalfEdgeIndexSet(HalfEdgeIndexSet&&) = default;

    HalfEdgeIndexSet& operator=(HalfEdgeIndexSet const&) = delete;
    HalfEdgeIndexSet& operator=(HalfEdgeIndexSet&&) = default;


  public: // initialization of data structures:

    /// Create half-edge structure based on the given Dune `gridView`.
    template <class BaseGridView>
    void init(BaseGridView const& gridView);


  private: // implementation details:

    // add a chain of boundary half-edges, starting with a new half-edge with index `he`
    // at the opposite of `next(he_start)`, `he_last` is the last inserted element of the chain.
    IndexType add_boundary_chain(IndexType he, IndexType he_start, IndexType he_last);


  public:

    /// Map entity to index.
    template <class Entity>
    IndexType index(Entity const& e) const { return e.index(); }

    /// Map a subentity to an index.
    template <class Entity>
    IndexType subIndex(Entity const& e, int i, int codim) const
    {
      switch (codim) {
        case 0:
          return subIndex<0>(e, i); break;
        case 1:
          return subIndex<1>(e, i); break;
        default:
          return subIndex<2>(e, i); break;
      }
      return 0;
    }

    /// Map a subentity to an index.
    template <int codim, class Entity>
    IndexType subIndex(Entity const& e, int i) const
    {
      auto range = indices<codim>(e);
      auto it = range.begin();
      std::advance(it, i);

      return *it;
    }

    /// Returns the number of entities of given geoemtry-type
    IndexType size(Dune::GeometryType type) const
    {
      return size(dimension - type.dim());
    }

    /// Returns the number of entities of given `codim`
    IndexType size(int codim) const
    {
      switch (dimension - codim) {
        case 0:
          return vertices_.size(); break;
        case 1:
          return edges_.size(); break;
        case 2:
          return faces_.size(); break;
        default:
          error_exit("Unknown codimension");
      }
      return 0;
    }

    /// Returns the half-edge with index `he`
    HalfEdge const& halfEdge(IndexType he) const
    {
      assert( he < half_edges_.size() );
      return half_edges_[he];
    }

    /// Returns the index of an entity associated with the half-edge index `he`
    template <int cd>
    IndexType indexOfHalfEdge(IndexType he) const
    {
      return indexOfHalfEdge(halfEdge(he), Codim_<cd>);
    }

    /// Returns the half-edge index of an entity of codimension `cd` with index `i`
    template <int cd>
    IndexType halfEdgeOfIndex(IndexType i) const
    {
      return halfEdgeOfIndex(i, Codim_<cd>);
    }


  private: // implementation details:

    IndexType indexOfHalfEdge(HalfEdge const& he, Codim_t<2>) const { return he.vertex_; }
    IndexType indexOfHalfEdge(HalfEdge const& he, Codim_t<1>) const { return he.edge_; }
    IndexType indexOfHalfEdge(HalfEdge const& he, Codim_t<0>) const { return he.face_; }

    IndexType halfEdgeOfIndex(IndexType i, Codim_t<2>) const { assert( i < vertices_.size() ); return vertices_[i]; }
    IndexType halfEdgeOfIndex(IndexType i, Codim_t<1>) const { assert( i < edges_.size() ); return edges_[i]; }
    IndexType halfEdgeOfIndex(IndexType i, Codim_t<0>) const { assert( i < faces_.size() ); return faces_[i]; }


  public:

    template <int codim, int cd, REQUIRES(codim != cd)>
    auto halfEdgeIndices(IndexType idx) const
    {
      assert( idx != invalid );
      return halfEdgeRange(halfEdgeOfIndex<codim>(idx), Codim_<codim>, Codim_<cd>);
    }

    template <int codim, int cd, REQUIRES(codim == cd)>
    auto halfEdgeIndices(IndexType idx) const { return std::array<IndexType,1>{{halfEdgeOfIndex(idx)}}; }

    template <int cd, class Entity, REQUIRES(Entity::codimension != cd)>
    auto halfEdgeIndices(Entity const& e) const
    {
      return halfEdgeIndices<Entity::codimension, cd>(index(e));
    }

    template <int cd, class Entity, REQUIRES(Entity::codimension == cd)>
    auto halfEdgeIndices(Entity const& e) const { return std::array<IndexType,1>{{e.he_}}; }


    /// Returns a range to the adjacent entities of codim `cd` of entity with codim `codim` and index `idx`.
    template <int codim, int cd, REQUIRES(codim != cd) >
    auto indices(IndexType idx) const
    {
      auto op = [this](IndexType he) { return this->template indexOfHalfEdge<cd>(he); };
      return ranges::map(op, halfEdgeIndices<codim,cd>(idx));
    }

    template <int codim, int cd, REQUIRES(codim == cd) >
    auto indices(IndexType idx) const { return std::array<IndexType,1>{{idx}}; }


    /// Returns a range to the adjacent entities of codim `cd` of entity `e`
    template <int cd, class Entity>
    auto indices(Entity const& e) const
    {
      return indices<Entity::codimension, cd>(index(e));
    }


    /// Returns a range of facets adjacent to tneity `e`
    template <class Entity>
    auto facetIndices(Entity const& e) const { return indices<Entity::codimension+1>(e); }

    /// Returns a range of faces adjacent to entity `e`
    template <class Entity>
    auto faceIndices(Entity const& e) const { return indices<0>(e); }

    /// Returns a range of edges adjacent to entity `e`
    template <class Entity>
    auto edgeIndices(Entity const& e) const { return indices<1>(e); }

    /// Returns a range of vertices adjacent to entity `e`
    template <class Entity>
    auto vertexIndices(Entity const& e) const { return indices<2>(e); }


  private: // implementation details:

    // return range of vertices of edge `e`
    auto halfEdgeRange(IndexType he_idx, Codim_t<1> /*e*/, Codim_t<2>) const
    {
      auto const& he = halfEdge(he_idx);

      return ranges::make_range(he.previous_, he.next_,
        [this](IndexType& he, auto const&) { he = this->halfEdge(he).next_; }, operation::id{}, 2);
    }

    // returns a range of vertices of face `f`
    auto halfEdgeRange(IndexType he_idx, Codim_t<0> /*f*/, Codim_t<2>) const
    {
      auto const& he = halfEdge(he_idx);

      return ranges::make_cyclic_range(he.previous_, he.previous_,
        [this](IndexType& he) { he = this->halfEdge(he).next_; }, operation::id{}, dimension+1);
    }

    // Returns a range of edges of vertex `v`
    auto halfEdgeRange(IndexType he_idx, Codim_t<2> /*v*/, Codim_t<1>) const
    {
      return ranges::make_cyclic_range(he_idx, he_idx,
        [this](IndexType& he) { he = this->halfEdge(he).next(*this).opposite_; }, operation::id{});
    }

    // Returns a range of edges of face `f`
    auto halfEdgeRange(IndexType he_idx, Codim_t<0> /*f*/, Codim_t<1>) const
    {
      return ranges::make_cyclic_range(he_idx, he_idx,
        [this](IndexType& he) { he = this->halfEdge(he).next_; }, operation::id{}, dimension+1);
    }

    // Returns a range of faces of vertex `v`
    auto halfEdgeRange(IndexType he_idx, Codim_t<2> /*v*/, Codim_t<0>) const
    {
      return ranges::make_cyclic_range(he_idx, he_idx,
        [this](IndexType& he) {
          auto face_he = this->halfEdge(he).next(*this).opposite_;
          while (this->halfEdge(face_he).outside())
            face_he = this->halfEdge(face_he).next(*this).opposite_;
          he = face_he;
        }, operation::id{});
    }

    // Returns a range of faces of edge `e`
    auto halfEdgeRange(IndexType he_idx, Codim_t<1> /*e*/, Codim_t<0>) const
    {
      auto const& he = halfEdge(he_idx);
      std::vector<IndexType> faces = {halfEdgeOfIndex<0>(he.face_)};

      auto neighbour = he.opposite(*this).face_;
      if (neighbour != invalid)
        faces.push_back(halfEdgeOfIndex<0>(neighbour));
      return faces;
    }


  public:

    /// Obtain all geometry types of entities in domain.
    // returns something like std::array<Dune::GeometryType, 1>
    Types types(int codim) const
    {
      return {{ Dune::GeometryType(Dune::GeometryType::simplex, dimension-codim) }};
    }

    /// Returns true if the given entity is contained in the current gridView
    template <class Entity>
    bool contains(Entity const& e) const
    {
      return (e.indexSet_ == this) && (e.he_ < half_edges_.size());
    }

    template <class Entity, int cd = Entity::codimension>
    bool owns(Entity const& e) const
    {
      return data_.owns(e);
    }

    template <int cd>
    bool owns(IndexType i) const
    {
      return data_.template owns<cd>(i);
    }

    // check orientation of elements, maybe correct it.
    void check();

    // reorient a face `f`
    void reorient(IndexType f)
    {
      IndexType idx = faces_[f];

      std::array<IndexType, dimension+1> vertices;
      for (int j = 0; j < dimension+1; ++j) {
        auto const& he = half_edges_[idx];
        vertices[j] = he.vertex_;
        idx = he.next_;
      }

      for (int j = 0; j < dimension+1; ++j) {
        auto& he = half_edges_[idx];
        std::swap(he.next_, he.previous_);
        he.vertex_ = vertices[(j + dimension)%(dimension+1)];
        idx = he.previous_;
      }
    }

    std::size_t memory() const
    {
      std::size_t mem = sizeof(invalid);
      mem += vertices_.capacity()*sizeof(IndexType) + sizeof(vertices_);
      mem += edges_.capacity()*sizeof(IndexType) + sizeof(edges_);
      mem += faces_.capacity()*sizeof(IndexType) + sizeof(faces_);
      mem += half_edges_.capacity()*sizeof(HalfEdge) + sizeof(half_edges_);
      mem += data_.memory();

      return mem;
    }

  private:

    const IndexType invalid = HalfEdge::invalid;


  public: // private:

    std::vector<IndexType> vertices_; //< half-edge index of vertices
    std::vector<IndexType> edges_;    //< half-edge index of edges
    std::vector<IndexType> faces_;    //< half-edge index of faces

    std::vector<HalfEdge> half_edges_;  //< half-edges

    EntityData<dim> data_;
  };


  // Implementation:


  template <int dim>
    template <class BaseGridView>
  void HalfEdgeIndexSet<dim>::init(BaseGridView const& gridView)
  {
    vertices_.clear();
    edges_.clear();
    faces_.clear();
    half_edges_.clear();

    // provide enough space in index containers
    vertices_.resize(gridView.size(dimension), invalid);
    edges_.resize(gridView.size(dimension-1), invalid);
    faces_.resize(gridView.size(0), invalid);
    half_edges_.resize(gridView.size(dimension-1) * 2);

    auto const& indexSet = gridView.indexSet();
    IndexType he = 0;
    for (auto const& elem : elements(gridView)) {
      auto const& refElem = RefElements::general(elem.type());

      int num_edges = refElem.size(1);

      IndexType face = indexSet.index(elem); // extract face-index
      for (int i = 0; i < num_edges; ++i) {
        HalfEdge h;
        h.next_ = he + (i+num_edges-1)%num_edges;
        h.previous_ = he + (i+1)%num_edges;

        // local vertex index, the edge points to
        int pos = refElem.subEntity(i, 1, i%2 == 0 ? 1 : 0, 2);

        IndexType vertex = indexSet.subIndex(elem, pos, 2); // extract vertex index
        h.vertex_ = vertex;
        vertices_[vertex] = he+i;

        IndexType edge = indexSet.subIndex(elem, i, 1); // extract edge-index
        h.edge_ = edge;
        h.face_ = face;

        // set opposite half_edge, if edge is assigned an half-edge already.
        if (edges_[edge] != invalid) {
          HalfEdge& h_opp = half_edges_[edges_[edge]];
          h.opposite_ = edges_[edge];
          h_opp.opposite_ = he+i;
        }
        edges_[edge] = he+i; // assign new half-edge to edge

        // assign half-edge to face that belongs to 0th facet
        if (i == 0)
          faces_[face] = he+i;

        half_edges_[he+i] = h;
      }

      he += num_edges;
    }

    check(); // correct orientation

    // correct boundary edges.
    for (std::size_t i = 0; i < edges_.size(); ++i) {
      auto const& half_edge = half_edges_[edges_[i]];
      if (half_edge.opposite_ == invalid) // boundary edge
        he = add_boundary_chain(he, half_edge.previous_, invalid);
    }
  }


#define INDEX_TYPE typename HalfEdgeIndexSet<dim>::IndexType
  template <int dim>
  INDEX_TYPE HalfEdgeIndexSet<dim>::add_boundary_chain(INDEX_TYPE he,
                                                        INDEX_TYPE he_start,
                                                        INDEX_TYPE he_last)
  {
    IndexType he_next = he_start;
    IndexType he_prev = invalid;
    // find next boundary segment
    he_prev = he_start;
    do {
      he_start = half_edges_[he_next].next_;
      he_next = half_edges_[he_start].opposite_;
    }
    while (he_next != invalid && half_edges_[he_next].face_ != invalid);

    if (he_next == invalid) {
      // create a new half-edge at the boundary
      HalfEdge h_boundary;

      h_boundary.opposite_ = he_start;
      h_boundary.vertex_ = half_edges_[he_prev].vertex_;
      h_boundary.edge_ = half_edges_[he_start].edge_;
      h_boundary.face_ = invalid;
      half_edges_[he] = h_boundary;
      half_edges_[he_start].opposite_ = he;

      if (he_last != invalid) {
        half_edges_[he].next_ = he_last;
        half_edges_[he_last].previous_ = he;
      }

      return add_boundary_chain(he+1, he_start, he);
    }
    else {
      // update existing half-edge
      half_edges_[he_next].next_ = he_last;
      half_edges_[he_last].previous_ = he_next;
      return he;
    }
  }
#undef INDEX_TYPE


  template <int dim>
  void HalfEdgeIndexSet<dim>::check()
  {
    std::vector<bool> oriented(size(0), false);
    oriented[0] = true;
    for (IndexType T1 = 0; T1 < size(0); ++T1) {
      IndexType i = halfEdgeOfIndex<0>(T1);
      for (int n_i = 0; n_i < dimension+1; ++n_i) {
        auto const& he = halfEdge(i);
        if (he.opposite_ == invalid || halfEdge(he.opposite_).outside()) {
          i = he.next_;
          continue;
        }

        auto const& he_opp = halfEdge(he.opposite_);

        IndexType T2 = he_opp.face_; // no boundary edge

        if (oriented[T1] == oriented[T2]) {
          i = he.next_;
          continue;
        }

        // assume T1 oriented and T2 not oriented
        if (he.vertex_ == he_opp.vertex_) { // faces are differently oriented
          msg("found wrong orientation in elements ",T1,", ",T2);
          if (oriented[T1] && !oriented[T2])
            reorient(T1);
          else
            reorient(T2);
        }

        oriented[T2] = true;

        i = he.next_;
      }
    }

    msg("check finished!");
  }

} // end namespace Dec
