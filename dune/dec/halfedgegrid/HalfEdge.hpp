#pragma once

#include <cstddef>

#include <dune/dec/common/Output.hpp>

namespace Dec
{
  struct HalfEdge
  {
    using IndexType = unsigned long int;

    // an index representing unset entities or neighbours
    static constexpr IndexType invalid = IndexType(-1);


  public:

    template <class IndexSet>
    HalfEdge const& next(IndexSet const& indexSet) const
    {
      return indexSet.halfEdge(next_);
    }

    template <class IndexSet>
    HalfEdge const& previous(IndexSet const& indexSet) const
    {
      return indexSet.halfEdge(previous_);
    }

    template <class IndexSet>
    HalfEdge const& opposite(IndexSet const& indexSet) const
    {
      return indexSet.halfEdge(opposite_);
    }


    template <class Grid>
    auto const& vertex(Grid const& grid) const
    {
      return grid.entity<2>(vertex_);
    }

    template <class Grid>
    auto const& edge(Grid const& grid) const
    {
      return grid.entity<1>(edge_);
    }

    template <class Grid>
    auto const& face(Grid const& grid) const
    {
      return grid.entity<0>(face_);
    }

    bool outside() const
    {
      return face_ == invalid;
    }

    void print() const
    {
      msg("[n=",next_,", p=",previous_,", o=",opposite_,", v=",vertex_,", e=",edge_,", f=",face_,"]");
    }

    std::size_t memory() const
    {
      return 6*sizeof(IndexType);
    }

  public:

    // half edges
    IndexType next_ = invalid;
    IndexType previous_ = invalid;
    IndexType opposite_ = invalid;

    // entities
    IndexType vertex_ = invalid; // target of the half edge
    IndexType edge_ = invalid;   // edge that the half edge is part of
    IndexType face_ = invalid;   // left element of the half edge
  };

} // end namespace Dec
