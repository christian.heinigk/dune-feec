#pragma once

#include <iterator>

#include <dune/grid/common/rangegenerators.hh>

#include "Entity.hpp"

namespace Dec
{
  template <class GridType>
  class HalfEdgeGridView
  {
  public:
    using Grid = GridType;

    static constexpr int dimension = Grid::dimension;
    static constexpr int dimensionworld = Grid::dimensionworld;

    using IndexType = typename Grid::IndexType;
    using IndexSet = typename Grid::IndexSet;
    using Geometry = typename Grid::Geometry;

    using GlobalCoordinate = typename Geometry::GlobalCoordinate;

    using ctype = float_type;

    template <int cd>
    struct Codim
    {
      using Entity = HalfEdgeEntity<cd, dimension, Grid>;

      template <Dune::PartitionIteratorType pit = Dune::All_Partition>
      struct Iterator
          : public std::iterator<std::random_access_iterator_tag, Entity>
      {
        using value_type = Entity;
        using difference_type = std::ptrdiff_t;
        using Reference = Entity;

        template <Dune::PartitionIteratorType p>
        using pit_t = std::integral_constant<Dune::PartitionIteratorType, p>;

        Iterator(HalfEdgeGridView const& gv, IndexType index, IndexType end)
          : gv_(gv)
          , index_(index)
          , end_(end)
        {
          if (pit != Dune::All_Partition) {
            while (index_ != end_ && !Dune::partitionSet<pit>().contains(partitionType()))
              ++index_;
          }
        }

        Iterator& operator++()    { increment(pit_t<pit>{}); return *this; }
        Iterator  operator++(int) { Iterator tmp(*this); ++(*this); return tmp; }

        bool operator==(Iterator const& other) const { return index_ == other.index_; }
        bool operator!=(Iterator const& other) const { return !(*this == other); }

        std::shared_ptr<Entity> operator->() const
        {
          return std::make_shared<Entity>(gv_.indexSet(), gv_.coordinates(), he());
        }
        Entity operator*() const { return {gv_.indexSet(), gv_.coordinates(), he()}; }

      private:

        void increment(pit_t<Dune::All_Partition>)
        {
          ++index_;
        }

        template <Dune::PartitionIteratorType p>
        void increment(pit_t<p>)
        {
          ++index_;
          while (index_ != end_ && !Dune::partitionSet<p>().contains(partitionType()))
            ++index_;
        }

        IndexType he() const { return gv_.indexSet().template halfEdgeOfIndex<cd>(index_); }
        HalfEdge const& halfEdge() const { return gv_.indexSet().halfEdge(he()); }

        Dune::PartitionType partitionType() const
        {
          return gv_.data().template partitionType<cd>(index_);
        }

      private:
        HalfEdgeGridView gv_;
        IndexType index_;
        IndexType end_;
      };

      template <Dune::PartitionIteratorType pit>
      struct Partition
      {
        using Iterator = Codim::Iterator<pit>;
      };
    };


  public:

    /// Constructor, stores reference to `grid`
    HalfEdgeGridView(Grid const& grid, int level)
      : grid_(grid)
      , level_(level)
    {}

    HalfEdgeGridView(HalfEdgeGridView const& gv)
      : grid_(gv.grid_)
      , level_(gv.level_)
    {}

    HalfEdgeGridView(HalfEdgeGridView&& gv)
      : grid_(gv.grid_)
      , level_(gv.level_)
    {}

  public:

    HalfEdgeGridView& operator=(HalfEdgeGridView const& that) { level_ = that.level_; return *this; }
    HalfEdgeGridView& operator=(HalfEdgeGridView&& that) { level_ = that.level_; return *this; }

    /// Obtain a const reference to the underlying grid.
    Grid const& grid() const
    {
      return grid_;
    }

    /// Obtain the index set
    IndexSet const& indexSet() const
    {
      return grid_.levelIndexSet(level_);
    }

    /// Obtain number of entities in a given codimension
    IndexType size(int codim) const
    {
      return grid_.size(level_, codim);
    }

    /// Obtain number of entities with a given geometry type.
    IndexType size(Dune::GeometryType type) const
    {
      return grid_.size(level_, type);
    }

    //// Return size of the overlap region for a given codim on the grid view.
    int overlapSize(int codim) const
    {
      return base().overlapSize(codim);
    }

    /// Return size of the ghost region for a given codim on the grid view.
    int ghostSize(int codim) const
    {
      return base().ghostSize(codim);
    }

    /// Obtain begin iterator for this view.
    template <int cd, Dune::PartitionIteratorType pit = Dune::All_Partition>
    typename Codim<cd>::template Partition<pit>::Iterator begin() const
    {
      return {*this, 0u, size(cd)};
    }

    /// Obtain end iterator for this view.
    template <int cd, Dune::PartitionIteratorType pit = Dune::All_Partition>
    typename Codim<cd>::template Partition<pit>::Iterator end() const
    {
      return {*this, size(cd), size(cd)};
    }

    int level() const { return level_; }

    auto const& comm() const { return grid_.comm(); }

    /// Communicate data on this view
    template <class DataHandleImp, class T>
    void communicate(Dune::CommDataHandleIF<DataHandleImp, T>& data,
                     Dune::InterfaceType iftype,
                     Dune::CommunicationDirection dir) const
    {
      base().communicate(data, iftype, dir);
    }

  public: // access to geometry information

    /// Return the Geomery-Cache for the current level
    Geometry const& geometry() const { return grid_.levelGeometry(level_); }

    /// Returns a vector of vertex coordinates
    auto const& coordinates() const
    {
      return geometry().coordinates();
    }

    /// Returns the (circum-)center of the `entity`.
    template <class Entity>
    GlobalCoordinate const& center(Entity const& entity) const
    {
      return geometry().template center<Entity::codimension>(entity.index());
    }

    /// Returns the volume of the `entity`.
    template <class Entity>
    float_type volume(Entity const& entity) const
    {
      return geometry().template volume<Entity::codimension>(entity.index());
    }

    /// Returns the dual volume of the `entity`.
    template <class Entity>
    float_type dual_volume(Entity const& entity) const
    {
      return geometry().template dual_volume<Entity::codimension>(entity.index());
    }

    template <class BaseEntity>
    typename Grid::template Entity_t<BaseEntity::codimension> entity(BaseEntity const& e) const
    {
      IndexType idx = base().indexSet().index(e);
      IndexType he = indexSet().template halfEdgeOfIndex<BaseEntity::codimension>(idx);

      return {indexSet(), coordinates(), he};
    }

    auto const& data() const
    {
      return indexSet().data_;
    }

  public:

    auto base() const
    {
      int l = baseLevel();
      return grid_.base().levelGridView(l);
    }

    int baseLevel() const
    {
      return grid_.baseLevel(level_);
    }

    std::size_t memory() const
    {
      return sizeof(int);
    }


  private:

    Grid const& grid_;
    int level_;
  };


  using Dune::entities;
  using Dune::elements;
  using Dune::facets;
  using Dune::edges;
  using Dune::vertices;

} // end namespace Dec
