#pragma once

#include <memory>

#include <dune/common/fvector.hh>
#include <dune/geometry/affinegeometry.hh>
#include <dune/geometry/referenceelements.hh>

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/LocalView.hpp>
#include <dune/dec/common/Dim.hpp>
#include <dune/dec/common/FieldMatVec.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/ranges/Store.hpp>

namespace Dec
{
  template <int mydim, int coorddim, class Grid>
  class CircumcenterGeometry
      // : public Dune::GeometryDefaultImplementation<mydim, coorddim, Grid, CircumcenterGeometry>
  {
  public:

    /// Dimension of the geometry
    static constexpr int dimension = Grid::dimension;
    static constexpr int mydimension = mydim;
    static constexpr int codimension = dimension - mydim;
    static constexpr int dimensionworld = coorddim;

    using Entity = typename Grid::template Codim<codimension>::Entity;
    using IndexSet = typename Grid::IndexSet;
    using ctype = float_type;

    /// Type for local coordinate vector
    using LocalCoordinate = Dune::FieldVector<float_type, mydimension>;

    /// Type for coordinate vector in world space
    using GlobalCoordinate = Dune::FieldVector<float_type, dimensionworld>;

    using DuneGeometry = Dune::AffineGeometry<float_type, mydimension, dimensionworld>;
    using JacobianTransposed = typename DuneGeometry::JacobianTransposed;
    using JacobianInverseTransposed = typename DuneGeometry::JacobianInverseTransposed;


  private:

    /// type of reference element
    using ReferenceElement = Dune::ReferenceElement<float_type, mydimension>;
    using ReferenceElements = Dune::ReferenceElements<float_type, mydimension>;


  public:

    /// Constructor, stores reference to `entity`, `indexSet` and `coordinates`.
    CircumcenterGeometry(Entity const& entity,
                         IndexSet const& indexSet,
                         std::vector<GlobalCoordinate> const& coordinates)
      : entity_(entity)
      , indexSet_(indexSet)
      , coordinates_(coordinates)
    {}


    /// Return the center of this entity
    GlobalCoordinate center() const
    {
      return center(entity_, Dim_<mydimension>);
    }

    /// Return the barycenter of this entity
    /// NOTE: Redirects to a Dune::AffineGeometry implementation
    GlobalCoordinate barycenter() const
    {
      return duneGeometry().center();
    }

    /// Return the volume of this entity
    float_type volume() const
    {
      return volume(entity_, Dim_<mydimension>);
    }

    /// Return the dualvolume of this entity
    float_type dual_volume() const
    {
      return dual_volume(entity_, Codim_<codimension>);
    }

    /// Always true: this is an affine geometry
    bool affine() const { return true; }

    /// Obtain the type of the reference element
    Dune::GeometryType type() const { return refElem_.type(); }

    /// Obtain number of corners of the corresponding reference element
    int corners() const { return refElem_.size(mydimension); }

    /// \brief Obtain coordinates of the i-th corner.
    /// NOTE: Not efficient, use ranges instead, \see coordinates.
    GlobalCoordinate const& corner(int i) const
    {
      auto vertices = indexSet_.vertexIndices(entity_);
      auto it = vertices.begin();
      std::advance(it, i);
      return coordinates_[*it];
    }

    /// A view to the coordinates of the entity corners.
    auto coordinates() const
    {
      return local_view(coordinates_, indexSet_.vertexIndices(entity_));
    }

    /// \brief Return the factor appearing in the integral transformation formula
    /// NOTE: Redirects to a Dune::AffineGeometry implementation
    float_type integrationElement(LocalCoordinate const& local) const
    {
      return duneGeometry().integrationElement(local);
    }

    /// \brief Obtain the transposed of the Jacobian.
    /// NOTE: Redirects to a Dune::AffineGeometry implementation
    JacobianTransposed const& jacobianTransposed(LocalCoordinate const& local) const
    {
      return duneGeometry().jacobianTransposed(local);
    }

    /// \brief Obtain the transposed of the Jacobian's inverse.
    /// NOTE: Redirects to a Dune::AffineGeometry implementation
    JacobianInverseTransposed const& jacobianInverseTransposed(LocalCoordinate const& local) const
    {
      return duneGeometry().jacobianInverseTransposed(local);
    }

    /// \brief Return a vector pointing in the direction of the oriented edge.
    /// NOTE: Available for edge-entities only.
    GlobalCoordinate vector() const
    {
      static_assert( mydimension == 1, "An edge-vector is defined for edges only!" );

      auto coords = coordinates();
      auto v0 = coords.begin();
      auto v1 = std::next(v0);

      return (*v1) - (*v0);
    }

    /// \brief Return a vector pointing in the direction of the oriented dual_edge, i.e.
    /// pointing from center to center of the ancident cells.
    /// NOTE: Available for edge-entities only.
    GlobalCoordinate dual_vector() const
    {
      static_assert( mydimension == 1, "An dual-edge-vector is defined for edges only!" );

      auto f = faces(entity_);
      auto f0_it = f.begin();
      auto f1_it = std::next(f0_it);

      return (*f1_it).geometry().center() - (*f1_it).geometry().center();
    }


  private: // specializations for concrete dimensions

    DuneGeometry const& duneGeometry() const
    {
      if (!duneGeometry_)
        duneGeometry_.reset(new DuneGeometry(type(), store(coordinates())));
      return *duneGeometry_;
    }

    // vertex coordinate
    template <class EntityType>
    GlobalCoordinate center(EntityType const& entity, Dim_t<0>) const
    {
      assert( EntityType::mydimension == 0 );
      assert( entity.index() < coordinates_.size() );
      return coordinates_[entity.index()];
    }

    // Center of an edge
    template <class EntityType>
    GlobalCoordinate center(EntityType const& entity, Dim_t<1>) const
    {
      assert( EntityType::mydimension == 1 );
      auto vertices = indexSet_.vertexIndices(entity);
      auto v0 = vertices.begin();
      auto v1 = std::next(v0);
      return 0.5*(coordinates_[*v0] + coordinates_[*v1]);
    }

    // Center of a triangle, TODO: use more efficient formula based on angles
    template <class EntityType>
    GlobalCoordinate center(EntityType const& entity, Dim_t<2>) const
    {
      assert( EntityType::mydimension == 2 );
      auto vertices = indexSet_.vertexIndices(entity);
      auto v0 = vertices.begin();
      auto v1 = std::next(v0);
      auto v2 = std::next(v1);

      GlobalCoordinate e01 = coordinates_[*v0] - coordinates_[*v1];
      GlobalCoordinate e02 = coordinates_[*v0] - coordinates_[*v2];
      GlobalCoordinate e21 = coordinates_[*v2] - coordinates_[*v1];

      auto sqr_e01 = unary_dot(e01);
      auto sqr_e02 = unary_dot(e02);

      auto D2 = std::abs(sqr_e01*sqr_e02 - sqr(dot(e01, e02)));
      assert_msg( D2 > tol_, "D2:=",D2," < tol:=",tol_ );

      auto a1 = -sqr_e02 * dot(e01, e21) / (2*D2);
      auto a2 =  sqr_e01 * dot(e02, e21) / (2*D2);
      return coordinates_[*v0] + a1*e01 + a2*e02;
    }


    // volume of a vertex := 1
    template <class EntityType>
    float_type volume(EntityType const& /*entity*/, Dim_t<0>) const
    {
      return 1;
    }

    // Length of an edge
    template <class EntityType>
    float_type volume(EntityType const& entity, Dim_t<1>) const
    {
      auto vertices = indexSet_.vertexIndices(entity);
      auto v0 = vertices.begin();
      auto v1 = std::next(v0);

      auto edge = coordinates_[*v0] - coordinates_[*v1];
      return edge.two_norm();
    }

    // Volume of a triangle
    template <class EntityType, int d>
    float_type volume(EntityType const& /*entity*/, Dim_t<d>) const
    {
      return simplex_volume(coordinates(), Dim_<d>);
    }


    // dual-volume of a face = volume of a vertex := 1
    template <class EntityType>
    float_type dual_volume(EntityType const& /*entity*/, Codim_t<0>) const
    {
      return 1;
    }

    // Length of the dual edge
    template <class EntityType>
    float_type dual_volume(EntityType const& entity, Codim_t<1>) const
    {
      auto c = center(entity, Dim_<1>);

      float_type dual_vol = 0;
      for (auto const& f : faces(entity)) // faces adjacent to the edge
      {
        // ignore contributions from ghost entities
        if (f.partitionType() == Dune::GhostEntity)
          continue;

        auto edge = center(f, Dim_<2>) - c;
        dual_vol += edge.two_norm();
      }

      assert( std::isfinite(dual_vol) );
      return dual_vol;
    }

    // Volume of the dual-cell, calculated recursivly
    template <class EntityType, int cd>
    float_type dual_volume(EntityType const& entity, Codim_t<cd>) const
    {
      std::vector<GlobalCoordinate> corners(cd+1);
      return fill_dual_volume(entity, corners, 0, Codim_<cd>, Codim_<cd>);
    }


    std::size_t memory() const
    {
      return sizeof(float_type) + sizeof(duneGeometry_);
    }

  private: // implementation details:

    template <class EntityType, class Container, int cd0, int cd>
    float_type fill_dual_volume(EntityType const& entity, Container& corners, std::size_t n, Codim_t<cd0>, Codim_t<cd>) const
    {
      static_assert( cd > 0, "" );
      static_assert( EntityType::codimension == cd, "" );
      corners[n] = center(entity, Dim_<EntityType::mydimension>);

      float_type dual_vol = 0;
      for (auto const& e : entities(entity, Codim_<cd-1>))
      {
        // ignore contributions from ghost entities
        if (e.partitionType() == Dune::GhostEntity)
          continue;

        auto sub_vol = fill_dual_volume(e, corners, n+1, Codim_<cd0>, Codim_<cd-1>);
        dual_vol += sub_vol;
      }

      assert( std::isfinite(dual_vol) );
      return dual_vol;
    }

    template <class EntityType, class Container, int cd0>
    float_type fill_dual_volume(EntityType const& entity, Container& corners, std::size_t n, Codim_t<cd0>, Codim_t<0>) const
    {
      corners[n] = center(entity, Dim_<EntityType::mydimension>);
      assert( n == corners.size()-1 );
      return simplex_volume(corners, Dim_<cd0>);
    }

    // Return the volume of a simplex with corners `corners` and dimension `d`.
    template <class Container, int d>
    float_type simplex_volume(Container const& corners, Dim_t<d>) const
    {
      assert( std::distance(corners.begin(), corners.end()) == d+1 );
      auto it = corners.begin();
      auto const& v0 = *it;
      ++it;

      FixMat<float_type, dimensionworld, d> A;
      for (std::size_t j = 0; it != corners.end(); ++it, ++j) {
        auto const& vertex = *it;
        for (std::size_t i = 0; i < dimensionworld; ++i)
          A(i,j) = vertex[i] - v0[i];
      }

      auto vol = std::sqrt( std::abs(det(trans(A) * A)) ) / factorial(d);
      assert( std::isfinite(vol) );
      return vol;
    }


  private:

    Entity const& entity_;      //< a reference to the entity, the geometry is related to.
    IndexSet const& indexSet_;  //< a reference to the global indexSet
    std::vector<GlobalCoordinate> const& coordinates_; //< a reference to the vector of all grid coordinates

    ReferenceElement const& refElem_ = ReferenceElements::simplex(); //< the corresponding reference element
    float_type const tol_ = std::numeric_limits<float_type>::epsilon();

    mutable std::shared_ptr<DuneGeometry> duneGeometry_; //< a geometry pointer, initialized only if needed, by using \ref duneGeometry().
  };


} // end namespace Dec
