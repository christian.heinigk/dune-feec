#pragma once

#include <cstddef>

#include <dune/grid/common/entityseed.hh>

#include "HalfEdge.hpp"

namespace Dec
{
  template <int codim, class GridImp>
  struct EntitySeed
  {
    static constexpr int dimension = GridImp::dimension;
    static constexpr int codimension = codim;
    static constexpr int mydimension = dimension-codim;

    using IndexType = typename HalfEdge::IndexType;

    constexpr bool isValid() const { return true; }

    IndexType he_; //< half-edge index representing this entity
  };

} // end namespace Dec
