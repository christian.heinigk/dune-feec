#pragma once

#include <dune/grid/common/datahandleif.hh>
#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/mapper.hh>
#include <dune/grid/common/partitionset.hh>
#include <dune/common/binaryfunctions.hh>
#include <dune/common/parallel/mpihelper.hh>

namespace Dec
{
  template <class GridView, class GeometryCache>
  class GhostSynchHandle
      : public Dune::CommDataHandleIF<GhostSynchHandle<GridView, GeometryCache>, double>
  {
    using IndexType = typename HalfEdge::IndexType;

  public:
    GhostSynchHandle(GridView const& gv, GeometryCache& cache)
      : gv_(gv)
      , cache_(cache)
    {}

    bool contains(int /*dim*/, int codim) const
    {
      return codim > 0;
    }

    bool fixedSize(int /*dim*/, int /*codim*/) const
    {
      return true;
    }

    template <class BaseEntity>
    std::size_t size(BaseEntity const& /*e_*/) const
    {
      return 1;
    }

    template <class MessageBuffer, class BaseEntity>
    void gather(MessageBuffer& buff, BaseEntity const& e_) const
    {
      auto e = gv_.entity(e_);
      buff.write(gv_.dual_volume(e));
//       msg("send entity<",BaseEntity::codimension,">(",e.index(),").dual_volume = ",gv_.dual_volume(e));
    }

    template <class MessageBuffer, class BaseEntity>
    void scatter(MessageBuffer& buff, BaseEntity const& e_, std::size_t /*n*/)
    {
      auto e = gv_.entity(e_);
      float_type dual_volume = 0.0;
      buff.read(dual_volume);
//       msg("receive entity<",BaseEntity::codimension,">(",e.index(),").dual_volume = ",dual_volume);

      // ONLY boundary entities
      if (e_.partitionType() == Dune::BorderEntity)
        cache_.dual_volumes_[BaseEntity::codimension][e.index()] += dual_volume;
      else if (e_.partitionType() == Dune::GhostEntity)
        cache_.dual_volumes_[BaseEntity::codimension][e.index()] = dual_volume;
//       else
//         msg("!!! received on ",e_.partitionType());
    }

  private:
    GridView gv_;
    GeometryCache& cache_;
  };

} // end namespace Dec
