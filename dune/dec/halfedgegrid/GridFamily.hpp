#pragma once

#include <dune/common/parallel/communication.hh>

#include <dune/grid/common/defaultgridview.hh>
#include <dune/grid/common/entity.hh>
#include <dune/grid/common/geometry.hh>

#include <dune/dec/Dec.hpp>

#include "Entity.hpp"
#include "EntitySeed.hpp"
#include "Geometry.hpp"
#include "GeometryCache.hpp"
#include "GridView.hpp"
#include "HalfEdge.hpp"
#include "IndexSet.hpp"


namespace Dec
{
  template <class GridBase>
  class HalfEdgeGrid;


  template<int,Dune::PartitionIteratorType,class> class LevelIteratorImp {};
  template<class> class LeafIntersectionImp {};
  template<class> class LevelIntersectionImp {};
  template<class> class LeafIntersectionIteratorImp {};
  template<class> class LevelIntersectionIteratorImp {};
  template<class> class HierarchicIteratorImp {};
  template<int,Dune::PartitionIteratorType,class> class LeafIteratorImp {};




  template <class GridBase>
  struct HalfEdgeGridFamily
  {
    using GridImp = HalfEdgeGrid<GridBase>;
    using GridViewImp = HalfEdgeGridView<GridImp>;
    using IndexSetImp = HalfEdgeIndexSet<GridBase::dimension>;

    using IndexType = typename HalfEdge::IndexType;

    static constexpr int dimension = GridBase::dimension;
    static constexpr int dimensionworld = GridBase::dimensionworld;

    using Traits = Dune::GridTraits<
      GridBase::dimension,
      GridBase::dimensionworld,
      HalfEdgeGrid<GridBase>,
      CircumcenterGeometry,
      HalfEdgeEntity,
        LevelIteratorImp,
        LeafIntersectionImp,
        LevelIntersectionImp,
        LeafIntersectionIteratorImp,
        LevelIntersectionIteratorImp,
        HierarchicIteratorImp,
        LeafIteratorImp,
      IndexSetImp,
      IndexSetImp,
        IndexSetImp, // GlobalIdSet
        std::size_t,
        IndexSetImp, // LocalIdSet
        std::size_t,
      typename GridBase::Traits::CollectiveCommunication,
      Dune::DefaultLevelGridViewTraits,
      Dune::DefaultLeafGridViewTraits,
      EntitySeed,
      CircumcenterGeometry
      >;
  };

} // end namespace Dec
