#pragma once

#include <array>
#include <vector>

#include <dune/dec/common/Dim.hpp>
#include <dune/dec/common/Index.hpp>
#include <dune/dec/common/StaticLoops.hpp>

#include "IndexSet.hpp"

namespace Dec
{
  template <class GridFamily>
  class GeometryCache
  {
    template <class> friend class HalfEdgeGrid;
    friend class WeightedTriangulation;
    template <class GridView, class GeometryCache> friend class GhostSynchHandle;

  public:

    using Grid = typename GridFamily::GridImp;

    static constexpr int dim = GridFamily::dimension;
    static constexpr int dow = GridFamily::dimensionworld;

    using IndexSet = HalfEdgeIndexSet<dim>;
    using IndexType = typename IndexSet::IndexType;
    using GlobalCoordinate = Dune::FieldVector<float_type, Grid::dimensionworld>;

    template <int cd>
    struct Codim
    {
      using Entity = HalfEdgeEntity<cd, dim, Grid>;
    };

    template <int cd>
    using Entity_t = typename Codim<cd>::Entity;

  public:

    /// Constructor that initializes the geometric data.
    template <class GridViewBase, class IndexSetBase, class IndexSet>
    GeometryCache(GridViewBase const& gridViewBase,
                  IndexSetBase const& indexSetBase,
                  IndexSet const& indexSet)
      : centers_{}
      , volumes_{}
      , dual_volumes_{}
    {
      // store vertex coordinates in a vector
      auto& coords = std::get<0>(centers_);
      coords.resize(indexSetBase.size(dim));
      for (auto const& v : vertices(gridViewBase))
        coords[indexSetBase.index(v)] = v.geometry().corner(0);

      init_center(indexSet);
      init_volume(indexSet);
      init_dual_volume(indexSet);

      // TODO: synchronize dual_volume on ghost entities
      // TODO: sum up dual_volumes on border entities
    }


    /// Returns a vector of grid vertex coordinates
    std::vector<GlobalCoordinate> const& coordinates() const { return std::get<0>(centers_); }

    /// Returns the coordinate of the i'th grid vertices
    GlobalCoordinate const& coordinate(std::size_t i) const { return coordinates()[i]; }


  public: // Methods for calculating the center and volume:

    template <int codim>
    GlobalCoordinate const& center(std::size_t i) const
    {
      return center_impl(i, Dim_<dim-codim>);
    }

    /// Return the volume of the i'th entity with codim `codim`
    template <int codim>
    float_type volume(std::size_t i) const
    {
      return volume_impl(i, Dim_<dim-codim>);
    }

    /// Return the dual volume of the i'th entity with codim `codim`
    template <int codim>
    float_type dual_volume(std::size_t i) const
    {
      return dual_volume_impl(i, Codim_<codim>);
    }


  private: // Implementation details:

    GlobalCoordinate const& center_impl(std::size_t i, Dim_t<0>) const { return coordinate(i); }

    template <int d>
    GlobalCoordinate const& center_impl(std::size_t i, Dim_t<d>) const
    {
      assert( i < std::get<d>(centers_).size() );
      return std::get<d>(centers_)[i];
    }


    float_type volume_impl(std::size_t /*i*/, Dim_t<0>) const { return 1; }

    template <int d>
    float_type volume_impl(std::size_t i, Dim_t<d>) const
    {
      assert( i < std::get<d>(volumes_).size() );
      return std::get<d>(volumes_)[i];
    }


    float_type dual_volume_impl(std::size_t /*i*/, Codim_t<0>) const { return 1; }

    template <int cd>
    float_type dual_volume_impl(std::size_t i, Codim_t<cd>) const
    {
      assert( i < std::get<cd>(dual_volumes_).size() );
      return std::get<cd>(dual_volumes_)[i];
    }


    std::size_t memory() const
    {
      std::size_t mem = 0;
      for (auto const& c : centers_)
        mem += c.capacity()*sizeof(GlobalCoordinate);

      for (auto const& v : volumes_)
        mem += v.capacity()*sizeof(float_type);

      for (auto const& dv : dual_volumes_)
        mem += dv.capacity()*sizeof(float_type);

      return mem + sizeof(centers_) + sizeof(volumes_) + sizeof(dual_volumes_);
    }

  private:

    // Calculate circumcenter for all entities and store in `this->centers_`.
    template <class IndexSet>
    void init_center(IndexSet const& indexSet)
    {
      forEach(range_<1,dim+1>, [&indexSet,this](auto const d)
      {
        std::get<d>(this->centers_).resize(indexSet.size(dim-d));
        for (std::size_t i = 0; i < indexSet.size(dim-d); ++i) {
          IndexType he = indexSet.template halfEdgeOfIndex<dim-d>(i);
          Entity_t<dim-d> entity{indexSet, this->coordinates(), he};
          std::get<d>(this->centers_)[i] = entity.geometry().center();
        }
      });
    }

    // Calculate volume for all entities and store in `this->volumes_`.
    template <class IndexSet>
    void init_volume(IndexSet const& indexSet)
    {
      forEach(range_<1,dim+1>, [&indexSet,this](auto const d)
      {
        std::get<d>(this->volumes_).resize(indexSet.size(dim-d));
        for (std::size_t i = 0; i < indexSet.size(dim-d); ++i) {
          IndexType he = indexSet.template halfEdgeOfIndex<dim-d>(i);
          Entity_t<dim-d> entity{indexSet, this->coordinates(), he};
          std::get<d>(this->volumes_)[i] = entity.geometry().volume();
        }
      });
    }

    // Calculate dual volume for all entities and store in `this->dual_volumes_`.
    template <class IndexSet>
    void init_dual_volume(IndexSet const& indexSet)
    {
      forEach(range_<1,dim+1>, [&indexSet,this](auto const cd)
      {
        std::get<cd>(this->dual_volumes_).resize(indexSet.size(cd));
        for (std::size_t i = 0; i < indexSet.size(cd); ++i) {
          IndexType he = indexSet.template halfEdgeOfIndex<cd>(i);
          Entity_t<cd> entity{indexSet, this->coordinates(), he};
          std::get<cd>(this->dual_volumes_)[i] = entity.geometry().dual_volume();
        }
      });
    }


  private:
    std::array<std::vector<GlobalCoordinate>, dim+1> centers_;    // center coordinates of dim d
    std::array<std::vector<float_type>, dim+1> volumes_;      // element volumes of dim d
    std::array<std::vector<float_type>, dim+1> dual_volumes_; // element dual-volumes of codim cd
  };

} // end namespace Dec
