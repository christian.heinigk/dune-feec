#pragma once

#include <dune/geometry/affinegeometry.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/entity.hh>

#include "HalfEdge.hpp"
#include "EntitySeed.hpp"
#include "Geometry.hpp"

#include <dune/dec/ranges/Map.hpp>

namespace Dec
{
  template <int codim, int dim, class Grid>
  class HalfEdgeEntity
      // : public Dune::EntityDefaultImplementation<codim, dim, Grid, HalfEdgeEntity>
  {
    using Self = HalfEdgeEntity;

  public:
    static constexpr int dimension = dim;
    static constexpr int codimension = codim;
    static constexpr int mydimension = dim-codim;
    static constexpr int dimensionworld = Grid::dimensionworld;

    using IndexSet = typename Grid::IndexSet;
    using IndexType = typename IndexSet::IndexType;
    using Geometry = CircumcenterGeometry<mydimension, dimensionworld, Grid>;
    using RefElements = Dune::ReferenceElements<Dec::float_type, mydimension>;

    using GlobalCoordinate = typename Geometry::GlobalCoordinate;

    template <class> friend class HalfEdgeGrid;
    template <int> friend class HalfEdgeIndexSet;

  public:

    HalfEdgeEntity() = default;

    /// Constructor, stores a pointer to `indexSet` and `coordinates`.
    HalfEdgeEntity(IndexSet const& indexSet, std::vector<GlobalCoordinate> const& coordinates, IndexType he)
      : indexSet_(&indexSet)
      , coordinates_(&coordinates)
      , he_(he)
    {}

    HalfEdgeEntity(HalfEdgeEntity const&) = default;
    HalfEdgeEntity(HalfEdgeEntity&&) = default;

    HalfEdgeEntity& operator=(HalfEdgeEntity const& that) = default;
    HalfEdgeEntity& operator=(HalfEdgeEntity&& that) = default;


  public:

    /// Partition type of this entity
    Dune::PartitionType partitionType() const
    {
      return indexSet_->data_.template partitionType<codimension>(index());
    }

    //TODO: add level() and isLeaf()

    /// Returns the index of this entity
    IndexType index() const
    {
      return indexSet_->template indexOfHalfEdge<codimension>(he_);
    }

    /// Returns the half-edge related to this entity
    HalfEdge const& halfEdge() const
    {
      return indexSet_->halfEdge(he_);
    }

    /// Returns a new geometry object
    Geometry geometry() const
    {
      return {*this, *indexSet_, *coordinates_};
    }

    /// Return the geometry-type related to this entity
    Dune::GeometryType type() const { return {Dune::GeometryType::simplex, mydimension}; }

    /// An entity-seed is just the index of the entity
    EntitySeed<codimension, Grid> seed() const { return {he_}; }

    /// Compares two entities for equality.
    bool operator==(HalfEdgeEntity const& that) const
    {
      return index() == that.index();
    }

    /// Compares two entities for inequality.
    bool operator!=(const HalfEdgeEntity& that) const
    {
      return !(*this == that);
    }

    /// Return the number of subEntities of codimension cc.
    unsigned int subEntities(unsigned int cc) const
    {
      return RefElements::simplex().size(cc - codimension);
    }

    /// \brief Obtain a subentity or superentity, `cd` in [0, dimension]
    /// Not efficient, use ranges instead. \see entities, \see facets, \see vertices, \see edges, and \see faces.
    template <int cd>
    HalfEdgeEntity<cd, dimension, Grid> subEntity(int i) const
    {
      auto indices = indexSet_->template halfEdgeIndices<cd>(*this);
      auto it = indices.begin();
      std::advance(it, i);
      return {*indexSet_, *coordinates_, *it};
    }

    /// Returns true, if entity has intersections with boundary
    bool hasBoundaryIntersections() const
    {
      return hasBoundaryIntersections(Dim_<mydimension>);
      return false;
    }

  private: // implementation details:

    bool hasBoundaryIntersections(Dim_t<0>) const
    {
      auto edges_ = edges();
      return std::any_of(edges_.begin(), edges_.end(), [](auto const& e) { return e.hasBoundaryIntersections(); });
    }

    bool hasBoundaryIntersections(Dim_t<1>) const
    {
      return indexSet_->halfEdge(halfEdge().opposite_).outside();
    }

    bool hasBoundaryIntersections(Dim_t<2>) const
    {
      auto edges_ = edges();
      return std::any_of(edges_.begin(), edges_.end(), [](auto const& e) { return e.hasBoundaryIntersections(); });
    }


  public:

    /// Returns the sign of the orientation relative to `that`.
    /**
     * The sign is defined by
     * ```
     * sign(v,e) = { +1 ... e points to v,
     *               -1 ... e points away from v,
     *                0 ... v not connected to e }
     * sign(e,f) = { +1 ... f is on the left side of e,
     *               -1 ... f is on the right side of e,
     *                0 ... otherwise }
     * v ... vertex, e ... edge, f ... face
     * ```
     **/
    template <int c>
    int sign(HalfEdgeEntity<c,dimension,Grid> const& that) const
    {
      assert_msg(std::abs(c - codimension) == 1, "sign defined only for facets of entity");

      switch (that.mydimension) {
        case 0: // `that` is vertex
          return that.index() == halfEdge().vertex_ ? 1 : -1; break; // `this` is edge
        case 1: // `that` is edge
          return that.sign(*this); break; // `this` is vertex or face
        case 2: // `that` is face
          return that.index() == halfEdge().face_ ? 1 : -1; break; // `this` is edge
        default:
          error_exit("Unsupported codimension of `that` entity.");
      }
      return 0;
    }

    /// Returns a range to the adjacent entities of codim `cd` of this entity
    template <int cd>
    auto entities() const
    {
      auto op = [this](IndexType idx) -> HalfEdgeEntity<cd, dimension, Grid> {
        return {*this->indexSet_, *this->coordinates_, this->indexSet_->template halfEdgeOfIndex<cd>(idx)};
      };

      return ranges::map(op, indexSet_->template indices<cd>(*this));
    }

    /// Returns a range of facets adjacent to this entity
    auto facets() const { return entities<codimension+1>(); }

    /// Returns a range of faces adjacent to this entity
    auto faces() const { return entities<0>(); }

    /// Returns a range of edges adjacent to this entity
    auto edges() const { return entities<1>(); }

    /// Returns a range of vertices adjacent to this entity
    auto vertices() const { return entities<2>(); }

    // provide range-generators in Dec namespace
    template <int cd>
    friend auto entities(HalfEdgeEntity const& entity, Codim_t<cd>) { return entity.entities<cd>(); }
    friend auto facets(HalfEdgeEntity const& entity) { return entity.facets(); }
    friend auto vertices(HalfEdgeEntity const& entity) { return entity.vertices(); }
    friend auto edges(HalfEdgeEntity const& entity) { return entity.edges(); }
    friend auto faces(HalfEdgeEntity const& entity) { return entity.faces(); }

    std::size_t memory() const
    {
      return sizeof(IndexType) + sizeof(indexSet_) + sizeof(coordinates_);
    }

  // private:
  public:

    /// a pointer to the global indexSet
    IndexSet const* indexSet_ = nullptr;

    /// a pointer to the vector of all grid coordinates
    std::vector<GlobalCoordinate> const* coordinates_ = nullptr;

    /// an half-edge index representing this entity
    IndexType he_ = HalfEdge::invalid;
  };

} // end namespace Dec
