#pragma once

#include <dune/common/parallel/collectivecommunication.hh>

#include <dune/grid/common/entity.hh>
#include <dune/grid/common/geometry.hh>

namespace Dec
{

  // Forward Declarations

  template <int dim, int dow>
  class DecGrid;

  template <int Codim, int Dim, class GridImp>
  class DecEntity;

  template <int codim, class GridImp>
  struct DecEntitySeed;

  template <int mydim, int cdim, class GridImp>
  class DecCachedGeometry;

  template <class GridImp>
  class DecIndexSet;

  template <class GridImp>
  class DecIdSet;

  class GeometryTypeRange;

  template <class GridImp>
  class DecGridView;

  template <int cd, class GridImp>
  struct DecEntityRange;


  template <int dim, int dimworld>
  struct DecGridViewTraits
  {
    using GridImp = DecGrid<dim, dimworld>;
    using GridViewImp = DecGridView<GridImp>;

    using Grid = GridImp;
    using IndexSetImp = DecIndexSet<GridImp>;
    using IndexSet = Dune::IndexSet<GridImp, IndexSetImp, size_t, GeometryTypeRange>;

    ///@{ dummy typedefs
    using Intersection = int;
    using IntersectionIterator = int;
    using CollectiveCommunication = Dune::CollectiveCommunication<Dune::No_Comm>;
    ///@}

    template <int cd>
    struct Codim
    {
      using Iterator = typename DecEntityRange<cd, GridImp>::iterator;
      using Entity = typename Grid::Traits::template Codim<cd>::Entity;
      using Geometry = typename Grid::template Codim<cd>::Geometry;
      using LocalGeometry = Geometry;
    };

    static constexpr bool conforming = true;
  };



  /// \brief A collection of types related to \ref DecGrid.
  template <int dim, int dimworld>
  struct DecGridFamily
  {
    using GridImp = DecGrid<dim, dimworld>;

    using ctype = float_type;

    static constexpr int dimension = dim;
    static constexpr int dimensionworld = dimworld;

    using IndexSetImp = DecIndexSet<GridImp>;
    using IdSetImp = DecIdSet<GridImp>;
    using GridViewImp = DecGridView<GridImp>;

    struct Traits
    {
      using Grid = GridImp;

      ///@{ dummy typedefs
      using LeafIntersection = int;
      using LevelIntersection = LeafIntersection;
      using LeafIntersectionIterator = int;
      using LevelIntersectionIterator = LeafIntersectionIterator;
      using HierarchicIterator = int;
      using CollectiveCommunication = Dune::CollectiveCommunication<Dune::No_Comm>;
      ///@}

      template <int cd>
      struct Codim
      {
        using Geometry   = Dune::Geometry<dim-cd, dimworld, const GridImp, DecCachedGeometry>;
        using GeometryImp = DecCachedGeometry<dim-cd, dimworld, const GridImp>;
        using LocalGeometry = Geometry;

        using Entity = Dune::Entity<cd, dim, const GridImp, DecEntity>;
        using EntityImp = DecEntity<cd, dim, const GridImp>;

        using EntitySeed = Dune::EntitySeed<const GridImp, DecEntitySeed<cd, const GridImp>>;
        using EntitySeedImp = DecEntitySeed<cd, const GridImp>;

        using LeafIterator = typename DecEntityRange<cd, const GridImp>::iterator;
        using LevelIterator = LeafIterator;
      };

      using LeafGridView = Dune::GridView<DecGridViewTraits<dim, dimworld>>;
      using LevelGridView = LeafGridView;

      using LeafIndexSet = Dune::IndexSet<GridImp, IndexSetImp, size_t, GeometryTypeRange>;
      using LevelIndexSet = LeafIndexSet;

      using GlobalIdSet = Dune::IdSet<GridImp, IdSetImp, size_t>;
      using LocalIdSet = GlobalIdSet;
    };
  };

}
