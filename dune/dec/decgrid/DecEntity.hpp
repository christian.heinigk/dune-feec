#pragma once

#include <vector>

#include <dune/geometry/referenceelements.hh>

#include "DecEntityData.hpp"
#include "DecEntitySeed.hpp"
#include "DecGeometry.hpp"
#include "common/Logical.hpp"

namespace Dec
{

  template <int cd, int dim, class GridImp>
  class DecEntity
      : public Dune::EntityDefaultImplementation<cd, dim, GridImp, DecEntity>
  {
    using ctype = typename GridImp::ctype;

    template <class> friend class DecIndexSet;
    template <int, int> friend class DecGrid;

  public:
    static constexpr int codimension = cd;
    static constexpr int dimension = dim;
    static constexpr int mydimension = dim-cd;

    static_assert(dimension == GridImp::dimension, "DecEntity::dimension != grid-dimension");

  public: // constructors

    DecEntity() {}

    DecEntity(GridImp const* gridImp, size_t index)
      : gridImp_(gridImp)
      , index_(index)
    {
      data_ = &gridImp->template subEntityData<codimension>(index);
      refElem_ = &Dune::ReferenceElements<ctype, mydimension>::general(type());
    }


  public:

    template <int cc>
    using Codim = typename GridImp::Traits::template Codim<cc>;

    using Geometry = typename Codim<codimension>::Geometry;
    using EntitySeed = typename Codim<codimension>::EntitySeed;

    /// level of this element, NOTE: only leaf-level = macro-level implemented
    int level() const { return 0; }

    /// Return the entity type identifier
    Dune::GeometryType type() const { return data_->type.geometryType(); }

    /// geometry of this entity
    Geometry geometry() const
    {
      using GeometryImp = typename Codim<codimension>::GeometryImp; // DecGeometry
      return Geometry( GeometryImp(*data_, gridImp_->nodes, index_) );
    }

    /// Get the seed corresponding to this entity
    EntitySeed seed() const
    {
      using EntitySeedImp = typename Codim<codimension>::EntitySeedImp; // DecEntitySeed
      return EntitySeed( EntitySeedImp{index_} );
    }

    /// Return the number of subEntities of codimension cc.
    unsigned int subEntities(unsigned int cc) const
    {
      if (cc >= codimension)
        return refElem_->size(cc - codimension); // codim of subEntity relative to codim of entity

      switch (dimension-cc) {
        case 0:
          return subEntitiesImpl(int_t<0>{});
          break;
        case 1:
          return subEntitiesImpl(int_t<1>{});
          break;
        case dimension:
          return subEntitiesImpl(int_t<dimension>{});
          break;
        default:
          error_exit("Codim cc out of range [0,dim]");
      }
      return 0u;
    }

    /// Provide access to sub entity i of given codimension.
    template <int cc> // cc = codim(entity, grid)
    typename Codim<cc>::Entity subEntity(int i) const
    {
      static_assert( 0 <= cc && cc <= dim, "Co-dimension cc out of range [0,dim]");
      return subEntityImpl<cc>(i, bool_< cc == cd >);
    }


  private: // implementation details

    // vertices of entity
    int subEntitiesImpl(int_t<0>) const
    {
      return gridImp_->template subEntityData<codimension>(index_).corners();
    }

    template <int sub_dim>
    int subEntitiesImpl(int_t<sub_dim>) const
    {
      if (mydimension == sub_dim)
        return 1;

      auto const& rel = gridImp_->template entityRelation<mydimension, sub_dim>();
      assert( index_ < rel.size() );
      return int( rel[index_].size() );
    }


    // specialization for the case cc == cd
    template <int cc>
    typename Codim<cc>::Entity subEntityImpl(int i, bool_t<true>) const
    {
      return *this;
    }

    template <int cc>
    typename Codim<cc>::Entity subEntityImpl(int i, bool_t<false>) const
    {
      using EntityImp = typename Codim<cc>::EntityImp; // DecEntity

      size_t sub_index = gridImp_->template subIndex<codimension, cc>(index_, i);
      return { EntityImp(gridImp_, sub_index) };
    }


  private:

    const GridImp* gridImp_ = nullptr;
    size_t index_;

    EntityData const* data_ = nullptr;
    Dune::ReferenceElement<ctype, mydimension> const* refElem_ = nullptr;
  };


#if DEC_HAS_DUAL_ENTITY
  // I've started to implement a dual entity structure, representing a chain of simplices

  template <int cd, int dim, class GridImp>
  class DecDualEntity
      : public Dune::EntityDefaultImplementation<cd, dim, GridImp, DecEntity>
  {
    using ctype = typename GridImp::ctype;

    template <class> friend class DecIndexSet;
    template <int, int> friend class DecGrid;

    template <int cc>
    using Codim = typename GridImp::Traits::template Codim<cc>;

    using EntityImp = typename Codim<cd>::EntityImp;

    static constexpr int primal_codimension = cd;
    static constexpr int primal_dimension = dim;
    static constexpr int primal_mydimension = dim-cd;

  public:
    static constexpr int codimension = dim-cd;
    static constexpr int dimension = dim;
    static constexpr int mydimension = cd;

    static_assert(dimension == GridImp::dimension, "DecDualEntity::dimension != grid-dimension");

  public: // constructors

    DecDualEntity() {}

    DecDualEntity(EntityImp const& primalEntity)
      : primalEntity_(primalEntity)
      , gridImp_(primalEntity.gridImp_)
      , index_(primalEntity.index_)
    {
      init(int_t<primal_codimension>{});
    }

    EntityImp const& primal() const
    {
      return primalEntity_;
    }

  public:

    using Geometry = typename Codim<codimension>::Geometry;
    using EntitySeed = typename Codim<codimension>::EntitySeed;

    int level() const { return 0; }

    /// Return the entity type identifier
    Chain<Dune::GeometryType> type() const { return data_->type.geometryType(); }

    /// geometry of this entity
    Chain<Geometry> geometry() const
    {
      assert( initialized_ );

      using GeometryImp = typename Codim<codimension>::GeometryImp; // DecGeometry

      Chain<Geometry> chain;
      chain.reserve(dataChain_.size());
      for (auto const& data : dataChain_)
        chain.push_back( Geometry( GeometryImp(data, nodes_) );

      return chain;
    }

  private:

    template <int codim>
    void init(int_t<codim>)
    {
      error_exit("DualEntity not implemented for codimension ", codim);
    }

    // dual of cell
    void init(int_t<0>)
    {
      nodes_.reserve(1);
      nodes_.push_back( primalEntity_.geometry().center() );

      dataChain_.reserve(1);
      dataChain_.push_back( EntityData{Geometry::VERTEX, {0}} );
      initialized_ = true;
    }

    // dual of edge (2d) / face (3d)
    void init(int_t<1>)
    {
      nodes_.reserve(primalEntity_.subEntities(0) + 1);

      nodes_.push_back( primalEntity_.geometry().center() );
      nodes_.push_back( primalEntity_.subEntity<0>(0).geometry().center() );
      nodes_.push_back( primalEntity_.subEntity<0>(1).geometry().center() );

      // TODO: check orientation
      dataChain_.reserve(primalEntity_.subEntities(0));
      dataChain_.push_back( EntityData{Geometry::EDGE2, {1,0}} );
      dataChain_.push_back( EntityData{Geometry::EDGE2, {0,2}} );
      initialized_ = true;
    }

    // dual of vertex (2d) / edge (3d)
    void init(int_t<2>)
    {
      nodes_.reserve(primalEntity_.subEntities(0) + primalEntity_.subEntities(1) + 1);

      nodes_.push_back( primalEntity_.geometry().center() );
      forEach(range_<0,2>, [this](auto const C) {
        for (int i = 0; i < this->primalEntity_.subEntities(C); ++i)
          this->nodes_.push_back( this->primalEntity_.template subEntity<C>(i).geometry().center() );
      });

      // TODO: check orientation
      dataChain_.reserve(2*primalEntity_.subEntities(0));
      size_t shift0 = 1, shift1 = primalEntity_.subEntities(0)+1;
      for (int i = 0; i < this->primalEntity_.subEntities(0); ++i) {
        dataChain_.push_back( EntityData{Geometry::TRIANGLE3, {0, shift1, shift0}} );
        shift1++;
        if (shift1 > nodes_.size())
          shift1 = primalEntity_.subEntities(0)+1;
        dataChain_.push_back( EntityData{Geometry::TRIANGLE3, {0, shift0, shift1}} );
        shift0++;
      }
      initialized_ = true;
    }

    // dual of vertex (3d)
    void init(int_t<3>)
    {
      nodes_.reserve(primalEntity_.subEntities(0) + primalEntity_.subEntities(1) + primalEntity_.subEntities(2) + 1);

      nodes_.push_back( primalEntity_.geometry().center() );
      forEach(range_<0,3>, [this](auto const C) {
        for (int i = 0; i < this->primalEntity_.subEntities(C); ++i)
          this->nodes_.push_back( this->primalEntity_.template subEntity<C>(i).geometry().center() );
      });

      error_exit("dual-dataChain_ not yet implemented for codim=3 entities.");
      initialized_ = true;
    }

  private:
    Chain<EntityData> dataChain_;
    std::vector<GlobalCoordinates> nodes_;
    bool initialized_ = false;
  };

#endif

} // namespace Dune
