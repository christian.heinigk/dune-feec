#pragma once

#include <vector>

#include "DecEntityData.hpp"

namespace Dec
{
  // A range wrapper that transforms entityData into entities on dereferencing.
  template <int cd, class GridImp>
  struct DecEntityRange
  {
    using EntityDataRange = std::vector<EntityData>;

    class iterator
    {
      using Entity = typename GridImp::Traits::template Codim<cd>::Entity;
      using EntityImp = typename GridImp::Traits::template Codim<cd>::EntityImp;
//       using EntityImp = DecEntity<cd, GridImp::dimension, const GridImp>;

    public:
      using value_type = Entity;
      using difference_type = size_t;

    public:
      iterator(GridImp const& gridImp, size_t index)
        : gridImp_(&gridImp)
        , index_(index)
      {}

      iterator& operator++()    { ++index_; return *this; }
      iterator  operator++(int) { iterator tmp(*this); ++(*this); return tmp; }
      iterator& operator--()    { --index_; return *this; }
      iterator  operator--(int) { iterator tmp(*this); --(*this); return tmp; }

      iterator& operator+=(difference_type rhs) { index_ += rhs; return *this; }
      iterator& operator-=(difference_type rhs) { index_ -= rhs; return *this; }

      difference_type operator-(iterator const& rhs) const { return index_ - rhs.index_; }

      iterator operator+(difference_type rhs) const { return iterator(*gridImp_, index_ + rhs); }
      iterator operator-(difference_type rhs) const { return iterator(*gridImp_, index_ - rhs); }

      friend iterator operator+(difference_type lhs, const iterator& rhs) {return iterator(rhs.gridImp_, lhs + rhs.index_);}
      friend iterator operator-(difference_type lhs, const iterator& rhs) {return iterator(rhs.gridImp_, lhs - rhs.index_);}

      bool operator==(iterator const& other) const { return index_ == other.index_; }
      bool operator!=(iterator const& other) const { return !(*this == other); }
      bool operator> (iterator const& other) const { return index_ > other.index_; }
      bool operator< (iterator const& other) const { return index_ < other.index_; }
      bool operator>=(iterator const& other) const { return index_ >= other.index_; }
      bool operator<=(iterator const& other) const { return index_ <= other.index_; }

      Entity const& operator*() const { update(); return entity_; }
      Entity const* operator->() const { update(); return &entity_; }
      Entity const& operator[](difference_type idx) { index_ += idx; update(); return entity_; }

    protected:
      void update() const
      {
        entity_ = Entity(EntityImp(gridImp_, index_));
      }

    private:
      GridImp const* gridImp_;
      size_t index_;

      mutable Entity entity_;
    };

    DecEntityRange(GridImp const& gridImp)
      : gridImp_(gridImp)
      , range_(gridImp.template subEntityData<cd>())
    {}

    iterator begin() const { return iterator(gridImp_, 0); }
    iterator end() const { return iterator(gridImp_, range_.size()); }

  private:
    GridImp const& gridImp_;
    EntityDataRange const& range_;
  };

} // end namespace Dec


#if 0
namespace std
{
  template <int cd, class GridImp>
  struct iterator_traits< typename DecEntityRange<cd, GridImp>::iterator >
  {
    using iterator = typename DecEntityRange<cd, GridImp>::iterator;
    using value_type = typename iterator::value_type;

    using difference_type = size_t;
    using pointer = value_type*;
    using reference = value_type&;
    using iterator_category = std::random_access_iterator_tag;
  };

} // end namespace std
#endif

