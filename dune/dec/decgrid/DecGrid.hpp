#pragma once

#include <cassert>
#include <string>
#include <vector>

#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/entity.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/common/gridfactory.hh>

#include "Dec.hpp"
#include "common/StaticLoops.hpp"
#include "utility/CapacityVector.hpp"
#include "utility/MatrixTuple.hpp"
#include "utility/Timer.hpp"

#include "DecGridFamily.hpp"
#include "DecEntity.hpp"
#include "DecEntitySeed.hpp"
#include "DecGridView.hpp"
#include "DecIndexSet.hpp"

namespace Dec
{
  namespace aux
  {
    // Upper bound for nr. of sub-/supEntities
    template <int Dim, int SubDim> struct GirdCapacity : index_t<9> {};
    template <> struct GirdCapacity<0,0> : index_t<1> {}; // vertex
    template <> struct GirdCapacity<1,0> : index_t<2> {}; // vertices of edge
    template <> struct GirdCapacity<2,0> : index_t<4> {}; // vertices of cell
    template <> struct GirdCapacity<1,1> : index_t<1> {}; // edge
    template <> struct GirdCapacity<2,1> : index_t<4> {}; // edges of cell
    template <> struct GirdCapacity<1,2> : index_t<2> {}; // cell neighbours of edge
    template <> struct GirdCapacity<2,2> : index_t<1> {}; // cell

  } // end namespace aux


  /// \brief A simple grid data structure.
  /**
   * The grid describes its elements by an \ref EntityData container, that contains indices of
   * vertices representing its geometry. In case of curved elements, additionally higher order
   * Geometry-DOFs are stored, that represent nodes on the edges of the elements.
   **/
  template <int dim, int dow>
  class DecGrid
      : public Dune::GridDefaultImplementation<dim, dow, float_type, DecGridFamily<dim, dow>>
  {
    using Self = DecGrid;

    static_assert( 2 <= dim && dim <= 2, "dim out of range [2,2]"); // TODO: extend to 3d grids
    static_assert( dim <= dow,           "dow must be >= dim");

    /// \brief A vector of entity indices representing subEntities or supEntities.
    /**
     * The first dimension characterizes the entity we want to extract the subEntities from. The
     * second dimension is the dimension of the subEntities/supEntities.
     * EntityRelations are thus matrices of size N^{size(dim - DIM) x size(dim - SubDim)}
     **/
    template <int Dim, int SubDim>
    class EntityRelation
        : public std::vector< CapacityVector<size_t, aux::GirdCapacity<Dim, SubDim>::value> > {};

    friend class Dune::GridFactory<Self>;
    friend class Dune::GridFactoryInterface<Self>;

    template <int, int, class> friend class DecEntity;
    template <int, int, class> friend class DecGeometry;
    template <class> friend class DecIndexSet;
    template <class Grid> friend struct DecGridAccess;

  public:
    using ctype = float_type;
    using GlobalCoordinates = Dune::FieldVector<ctype, dow>;

    using GridFamily = DecGridFamily<dim, dow>;
    using Traits = typename GridFamily::Traits;

    template <int codim>
    using Codim = typename Traits::template Codim<codim>;

  public: // constructors

    DecGrid()
      : indexSet_(*this)
      , idSet_(*this)
    {}

    DecGrid(DecGrid const&) = delete;
    DecGrid(DecGrid&&) = default;

    std::vector<GlobalCoordinates> const& coordinates() const
    {
      return nodes;
    }

  public: // methods for entity relations

    // Return the vector of entity-data representing an entities of codim 'cd', e.g.
    // all vertices for cd=dim, and all cells for cd=0
    template <int cd>
    std::vector<EntityData> const& subEntityData() const
    {
      return subEntityDataImpl(int_t<dim - cd>{});
    }

    // Return the ith entity-data representing an entities of codim 'cd', e.g.
    // a vertex for cd=dim, and a cell for cd=0
    template <int cd>
    EntityData const& subEntityData(size_t i) const
    {
      std::vector<EntityData> const& entityData = subEntityData<cd>();
      return entityData[i];
    }


  private: // implementation details

    std::vector<EntityData> const& subEntityDataImpl(int_t<0>)   const { return vertices; }
    std::vector<EntityData> const& subEntityDataImpl(int_t<1>)   const { return edges; }
    std::vector<EntityData> const& subEntityDataImpl(int_t<dim>) const { return cells; }

    template <int d>
    std::vector<EntityData> const& subEntityDataImpl(int_t<d>) const
    {
      assert_msg("subEntities for given dimension d not available!");
      return vertices;
    }

    template <int dim0, int dim1>
    EntityRelation<dim0, dim1>& entityRelation()
    {
      return std::get<dim1>(std::get<dim0>(entityRelation_));
    }

    template <int dim0, int dim1>
    EntityRelation<dim0, dim1> const& entityRelation() const
    {
      return std::get<dim1>(std::get<dim0>(entityRelation_));
    }


  public: // methods for subEntities

    /// Number of entities E > e, with codim(e, grid) = Entity::codim, codim(E, grid) = ccodim
    template <int ccodim, class Entity>
    int subEntities(Entity const& e) const
    {
      using EntityImp = typename Codim<Entity::codimension>::EntityImp;
      EntityImp const& entityImp = this->getRealImplementation(e);
      return entityImp.subEntitiesImpl(int_t<dim - ccodim>{});
    }

    /// Obtain the i'th entity E_i > e, with codim(e, grid) = Entity::codim, codim(e, E_i) = ccodim
    template <int ccodim, class Entity>
    typename Codim<ccodim>::Entity
    subEntity(Entity const& e, int i) const
    {
      using Entity_ = typename Codim<ccodim>::Entity;
      using EntityImp_ = typename Codim<ccodim>::EntityImp;
      return Entity_( EntityImp_(this, subIndex<ccodim>(e, i)) );
    }

    /// Obtain the i'th entity E_i > e, with codim(e, grid) = codim, codim(E_i, grid) = ccodim
    template <int codim, int ccodim>
    size_t subIndex(size_t index, int i) const
    {
      return indexSet_.subIndexImpl(index, i, int_t<dim - codim>{}, int_t<dim - ccodim>{});
    }

    /// Obtain the i'th entity E_i > e, with codim(e, grid) = Entity::codim, codim(E_i, grid) = ccodim
    template <int ccodim, class Entity>
    size_t subIndex(Entity const& e, int i) const
    {
      using EntityImp = typename Codim<Entity::codimension>::EntityImp;
      EntityImp const& entityImp = this->getRealImplementation(e);
      return indexSet_.subIndexImpl(entityImp.index_, i, int_t<dim - Entity::codimension>{}, int_t<dim - ccodim>{});
    }


  public: // methods inherited from DecGridBase

    /// @name Size methods
    //@{
    /// Return maximum level defined in this grid.
    int maxLevel() const
    {
      return 0;
    }

    /// Return number of grid entities of a given codim on a given level
    int size(int level, int codim) const
    {
      assert( level == 0 );
      return size(codim);
    }

    /// Return number of leaf entities of a given codim
    int size(int codim) const
    {
      switch (dim - codim) {
        case 0:
          return vertices.size();
          break;
        case 1:
          return edges.size();
          break;
        case dim:
          return cells.size();
          break;
        default:
          error_exit("Codim not implemented!");
          return 0;
      }
    }

    /// Return number of entities per level and geometry type
    int size(int level, Dune::GeometryType type) const
    {
      assert( level == 0 );
      return size(dim - type.dim());
    }

    /// Return number of leaf entities per geometry type
    int size(Dune::GeometryType type) const
    {
      return size(dim - type.dim());
    }
    //@}


    /// @name Access to index and id sets
    //@{
    /// Return const reference to the grids global id set
    typename Traits::GlobalIdSet const& globalIdSet() const
    {
      return idSet_;
    }

    /// Return const reference to the grids local id set
    typename Traits::LocalIdSet const& localIdSet() const
    {
      return idSet_;
    }

    /// Return const reference to the grids level index set for level level
    typename Traits::LevelIndexSet const& levelIndexSet(int level) const
    {
      assert( level == 0 );
      return indexSet_;
    }

    /// Return const reference to the grids leaf index set
    typename Traits::LeafIndexSet const& leafIndexSet() const
    {
      return indexSet_;
    }
    //@}


    /// Obtain Entity from EntitySeed.
    template <class EntitySeed>
    typename Codim<EntitySeed::codimension>::Entity
    entity(EntitySeed const& seed) const
    {
      using EntityImp = typename Codim<EntitySeed::codimension>::EntityImp;
      using EntitySeedImp = typename Codim<EntitySeed::codimension>::EntitySeedImp;
      EntitySeedImp const& seedImp = this->getRealImplementation(seed);
      return EntityImp( this, seedImp.index );
    }

  private:
    std::vector<GlobalCoordinates> nodes;
    std::vector<EntityData>        cells;
//     std::vector<EntityData> faces;
    std::vector<EntityData>        edges;
    std::vector<EntityData>     vertices;

    DecIndexSet<Self> indexSet_;
    DecIdSet<Self>       idSet_;

    typename MatrixTuple<int, dim+1, dim+1, EntityRelation>::type entityRelation_;
  };


} // end namespace Dec
