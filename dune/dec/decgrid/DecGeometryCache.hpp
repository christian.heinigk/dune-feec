#pragma once

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include "DecGeometry.hpp"

namespace Dec
{

  template <int Dim, int WorldDim, class GridImp>
  class DecGeometryCache;


  template <class T, int Dim, int WorldDim>
  class GeometryData
  {
    using GlobalCoordinate = Dune::FieldVector<T, WorldDim>;

    template <int, int, class> friend class DecGeometryCache;
    template <int, int> friend class DecGridFactory;

  public:
    GlobalCoordinate center_;
    T volume_ = 0;
    T dual_volume_ = 0;
  };




  template <int Dim, int WorldDim, class GridImp>
  class DecGeometryCache
  {
    using Grid = typename std::remove_const<GridImp>::type;
    using ctype = typename Grid::ctype;
    using Data = GeometryData<ctype,Dim,WorldDim>;
    using GlobalCoordinate = typename Data::GlobalCoordinate;

    template <int, int> friend class DecGridFactory;

  public:

    /// Obtain the centroid of the mapping's image
    GlobalCoordinate const& center(size_t i) const
    {
      assert( initialized );
      return data_[i].center_;
    }

    /// Obtain the volume of the element
    ctype volume(size_t i) const
    {
      assert( initialized );
      return data_[i].volume_;
    }

    ctype dual_volume(size_t i) const
    {
      assert( initialized );
      return data_[i].dual_volume_;
    }

    std::vector<Data> data_;
    bool initialized = false;
  };

} // namespace Dune
