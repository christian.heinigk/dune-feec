#pragma once

#include <vector>

#include "GeometryType.hpp"

namespace Dec
{
  /// \brief Container for all relevant data to construct an entity.
  /**
   * The data container tries to minimize the amount of data.
   **/
  struct EntityData
  {
    Geometry               type;
    std::vector<size_t> indices;
    
    size_t const& operator[](size_t i) const { return indices[i]; }
    size_t&       operator[](size_t i)       { return indices[i]; }

    // Return the number of corners
    unsigned int corners() const
    {
      return type.corners();
    }
  };

} // end namespace Dec
