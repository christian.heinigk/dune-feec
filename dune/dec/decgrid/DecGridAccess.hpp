#pragma once

namespace Dec
{
  // forward declaration
  template <int dim, int dow>
  class DecGrid;


  template <class Grid>
  struct DecGridAccess;

  template <int dim, int dow>
  struct DecGridAccess< DecGrid<dim, dow> >
  {
    using Grid = DecGrid<dim, dow>;

    template <int codim>
    struct Codim
    {
      using Geometry = typename Grid::template Codim<codim>::Geometry;
      using GeometryImp = typename Grid::template Codim<codim>::GeometryImp;

      using Entity = typename Grid::template Codim<codim>::Entity;
      using EntityImp = typename Grid::template Codim<codim>::EntityImp;

      using EntitySeed = typename Grid::template Codim<codim>::EntitySeed;
      using EntitySeedImp = typename Grid::template Codim<codim>::EntitySeedImp;
    };

    template <class Geometry>
    static typename Codim<Geometry::codimension>::GeometryImp const&
    geometry(Geometry const& g)
    {
      return geometry<Geometry::codimension>(g);
    }

    template <int codim>
    static typename Codim<codim>::GeometryImp const&
    geometry(typename Codim<codim>::Geometry const& g)
    {
      return Grid::getRealImplementation(g);
    }

    template <class Entity>
    static typename Codim<Entity::codimension>::EntityImp const&
    entity(Entity const& e)
    {
      return entity<Entity::codimension>(e);
    }

    template <int codim>
    static typename Codim<codim>::EntityImp const&
    entity(typename Codim<codim>::Entity const& e)
    {
      return Grid::getRealImplementation(e);
    }

    template <class EntitySeed>
    static typename Codim<EntitySeed::codimension>::EntitySeedImp const&
    seed(EntitySeed const& e)
    {
      return seed<EntitySeed::codimension>(e);
    }

    template <int codim>
    static typename Codim<codim>::EntitySeedImp const&
    seed(typename Codim<codim>::EntitySeed const& e)
    {
      return Grid::getRealImplementation(e);
    }

  };


  template <int dim, int dow>
  struct DecGridAccess< const DecGrid<dim, dow> >
      : public DecGridAccess< DecGrid<dim, dow> > {};


  // some free functions

  /// Obtain the unerlying implementation of an base class entity
  template <int cd, int dim, class GridImp, template<int,int,class> class EntityImp>
  EntityImp<cd, dim, GridImp> const& access(Dune::Entity<cd, dim, GridImp, EntityImp> const& e)
  {
    return DecGridAccess<GridImp>::entity(e);
  }


  /// Obtain the unerlying implementation of an base class entity-seed
  template <class GridImp, class EntitySeedImp>
  EntitySeedImp const& access(Dune::EntitySeed<GridImp, EntitySeedImp> const& e)
  {
    return DecGridAccess<GridImp>::seed(e);
  }


  /// Obtain the unerlying implementation of an base class geometry
  template <int mydim, int cdim, class GridImp, template<int,int,class> class GeometryImp>
  GeometryImp<mydim, cdim, GridImp> const& access(Dune::Geometry<mydim, cdim, GridImp, GeometryImp> const& g)
  {
    return DecGridAccess<GridImp>::geometry(g);
  }

} // end namespace Dec
