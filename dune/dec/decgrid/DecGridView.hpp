#pragma once

#include <type_traits>
#include <vector>

#include "DecEntityRange.hpp"

namespace Dec
{
  // forward declaration
  template <int dim, int dow>
  struct DecGridViewTraits;


  template <class GridImp>
  class DecGridView
  {
  public:
    using Traits = DecGridViewTraits<GridImp::dimension, GridImp::dimensionworld>;
    using Grid = typename Traits::Grid;
    using IndexSet = typename Traits::IndexSet;

    template <int cd>
    using Codim = typename Traits::template Codim<cd>;

    template <int cd>
    using EntityRange = DecEntityRange<cd, Grid>;

  public:
    DecGridView(Grid const& grid)
      : grid_(&grid)
    {}

    Grid const& grid() const
    {
      return *grid_;
    }

    /// Obtain the index set.
    IndexSet const& indexSet() const
    {
      return grid().leafIndexSet();
    }

    /// Obtain number of entities in a given codimension.
    int size(int codim) const
    {
      return grid().size(codim);
    }

    /// Obtain number of entities with a given geometry type.
    int size(Dune::GeometryType const& type) const
    {
      return grid().size(type);
    }

    /// Obtain begin iterator for this view.
    template <int cd>
    typename EntityRange<cd>::iterator begin() const
    {
      EntityRange<cd> range(grid());
      return range.begin();
    }

    /// Obtain end iterator for this view
    template <int cd>
    typename EntityRange<cd>::iterator end() const
    {
      EntityRange<cd> range(grid());
      return range.end();
    }

  private:
    Grid const* grid_;
  };

} // end namespace Dec
