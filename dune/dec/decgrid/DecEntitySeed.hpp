#pragma once

#include <type_traits>

namespace Dec
{
  template <int codim, class GridImp>
  struct DecEntitySeed
  {
    using Grid = typename std::remove_const<GridImp>::type;

    static constexpr int dimension = Grid::dimension - codim;
    static constexpr int codimension = codim;

    // store just the index of the entity
    size_t index;
  };

} // end namespace Dec
