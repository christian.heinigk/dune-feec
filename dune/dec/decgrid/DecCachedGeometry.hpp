#pragma once

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include "DecEntityData.hpp"
#include "DecGeometryCache.hpp"
#include "LagrangeBasis.hpp"
#include "LocalView.hpp"
#include "Output.hpp"

namespace Dune
{
  // External Forward Declarations

  template< class ctype, int dim >
  class ReferenceElement;

  template< class ctype, int dim >
  struct ReferenceElements;

} // end namespace Dune

namespace Dec
{
  template <int Dim, int WorldDim, class GridImp>
  class DecCachedGeometry
  {
    using Grid = typename std::remove_const<GridImp>::type;
    using Traits = typename Grid::Traits;

  public:
    using ctype = typename Grid::ctype;

    /** \brief Dimension of the geometry */
    static constexpr int mydimension = Dim;

    /** \brief Dimension of the world space */
    static constexpr int coorddimension = WorldDim;

    /** \brief Type for local coordinate vector */
    using LocalCoordinate = Dune::FieldVector<ctype, mydimension>;

    /** \brief Type for coordinate vector in world space */
    using GlobalCoordinate = Dune::FieldVector<ctype, coorddimension>;

    /** \brief Type for the transposed Jacobian matrix */
    using JacobianTransposed = Dune::FieldMatrix<ctype, mydimension, coorddimension>;

    /** \brief Type for the transposed inverse Jacobian matrix */
    using JacobianInverseTransposed = Dune::FieldMatrix<ctype, coorddimension, mydimension>;

    using CoordView = VertexView<ctype, coorddimension>;

    using Basis = LagrangeVirtualBasis<CoordView, ctype, mydimension>;
    using BasisFactory = LagrangeBasisFactory<CoordView, ctype, mydimension>;

    using NotCached = DecGeometry<Dim, WorldDim, GridImp>;

    template <int, int> friend class DecGridFactory;

  private:
    /// Type of reference element
    using ReferenceElement = Dune::ReferenceElement<ctype, mydimension>;

    /// Type of reference element factory
    using ReferenceElements = Dune::ReferenceElements<ctype, mydimension>;

  public:

    /// Create geometry from \ref EntityData and vector of all coordinates
    DecCachedGeometry(EntityData const& data, std::vector<GlobalCoordinate> const& nodes, size_t index)
      : data_(data)
      , nodes_(nodes)
      , index_(index)
      , refElement_(&ReferenceElements::general(data.type.geometryType()))
      , basis_(BasisFactory::create(data.type))
    {
      // provide a constant integration element
      if (affine())
        integrationElement_ = integrationElement( refElement().position(0,0) );
    }

    /// Return true for flat simple elements.
    bool affine() const { return data_.type.isAffine(); }

    /// Obtain the type of the reference element.
    Dune::GeometryType type() const { return refElement().type(); }

    /// Obtain number of corners of the corresponding reference element.
    int corners() const { return refElement().size(mydimension); }

    /// Obtain coordinates of the i-th corner
    GlobalCoordinate corner(int i) const
    {
      return global( refElement().position(i, mydimension) );
    }

    /// Obtain the centroid of the mapping's image
    GlobalCoordinate const& center() const
    {
      return cache().center(index_);
    }


    GlobalCoordinate global(LocalCoordinate const& local) const
    {
      return basis().eval(nodes(), local);
    }

    /// Obtain local coordinates from global position, NOTE: not implemented
    LocalCoordinate local(GlobalCoordinate const& global) const
    {
      LocalCoordinate local;
      error_exit("Not implemented!");
      return local;
    }

    /// Obtain the integration element
    ctype integrationElement(LocalCoordinate const& local) const
    {
      return affine() ? integrationElement_ : Math::gramian( jacobianTransposed(local) );
    }

    /// Obtain the volume of the element
    ctype volume() const
    {
      return cache().volume(index_);
    }

    /// Obtain the volume of the element
    ctype dual_volume() const
    {
      return cache().dual_volume(index_);
    }

    /// Obtain the transposed of the Jacobian
    JacobianTransposed jacobianTransposed(LocalCoordinate const& local) const
    {
      return basis().evalJacobian(nodes(), local);
    }

    /// Obtain the transposed of the Jacobian's inverse. NOTE: not implemented
    JacobianInverseTransposed jacobianInverseTransposed(LocalCoordinate const& local) const
    {
      JacobianInverseTransposed jacInvTrans;
      error_exit("Not implemented!");
      return jacInvTrans;
    }

    friend const ReferenceElement &referenceElement(DecCachedGeometry const& geometry)
    {
      return geometry.refElement();
    }

    static DecGeometryCache<Dim, WorldDim, GridImp>& cache()
    {
      static DecGeometryCache<Dim, WorldDim, GridImp> c;
      return c;
    }

  protected:

    CoordView nodes() const
    {
      return {nodes_, data_.indices};
    }

    Basis const& basis() const
    {
      assert( basis_ != nullptr );
      return *basis_;
    }

    ReferenceElement const& refElement() const
    {
      assert( refElement_ != nullptr );
      return *refElement_;
    }

  private:

    EntityData const& data_;
    std::vector<GlobalCoordinate> const& nodes_;
    size_t index_;
    ReferenceElement const* refElement_ = nullptr;
    std::shared_ptr<Basis> basis_;

    /// the center
    ctype integrationElement_ = 0.0;
  };

} // namespace Dune
