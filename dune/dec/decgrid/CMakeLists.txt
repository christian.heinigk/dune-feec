install(FILES
  DecCachedGeometry.hpp
  DecEntity.hpp
  DecEntityData.hpp
  DecEntityRange.hpp
  DecEntitySeed.hpp
  DecGeometry.hpp
  DecGeometryCache.hpp
  DecGrid.hpp
  DegGridAccess.hpp
  DecGridFactory.hpp
  DecGridFamily.hpp
  DecIndexSet.hpp
  GeometryType.hpp
  DESTINATION include/dune/dec/decgrid/
)
