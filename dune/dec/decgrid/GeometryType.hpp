#pragma once

#include <dune/geometry/type.hh>

#include "common/Math.hpp"

namespace Dec
{
  struct Geometry
  {
    enum Type : unsigned int {
      VERTEX    = 0,
      LINE2     = 1<<0,
      LINE3     = 1<<2,
      TRIANGLE3 = 1<<3,
      TRIANGLE6 = 1<<4,
      QUAD4     = 1<<5,
      QUAD8     = 1<<6,
      QUAD9     = 1<<7,
      OTHER     = 1<<8,

      DIM1 = LINE2 | LINE3,
      DIM2 = TRIANGLE3 | TRIANGLE6 | QUAD4 | QUAD8 | QUAD9,

      SIMPLEX = VERTEX | LINE2 | LINE3 | TRIANGLE3 | TRIANGLE6,
      QUAD    = VERTEX | LINE2 | LINE3 | QUAD4 | QUAD8 | QUAD9,

      FLAT   = VERTEX | LINE2 | TRIANGLE3 | QUAD4,
      CURVED = LINE3 | TRIANGLE6 | QUAD8 | QUAD9
    };

    Type type;

    /// Returns the dimension of the geometry
    constexpr unsigned int dim() const { return type == VERTEX ? 0 : (type & DIM1) ? 1 : (type & DIM2) ? 2 : 3; }

    /// Returns true, if geometry is a flat element.
    constexpr bool isFlat() const { return type & FLAT; }

    /// Returns true, if geometry is a curved element.
    constexpr bool isCurved() const { return type & CURVED; }

    /// Return true, if geometry is a simplex.
    constexpr bool isSimplex() const { return type & SIMPLEX; }

    /// Returns true, if geometry is a quadrangele.
    constexpr bool isQuad() const { return type & QUAD; }

    /// Return true, if affine geometry, i.e. flat simplex, \see isFlat and \see isSimplex.
    constexpr bool isAffine() const { return isSimplex() && isFlat(); }

    /// Returns nr of corners of the geometry, i.e. \ref dim + 1 if \ref isSimplex, or 2^dim.
    constexpr unsigned int corners() const { using std::pow; return isSimplex() ? dim() + 1 : pow(2u, dim()); }

    /// Returns a BasicType of \ref Dune::GeometryType.
    constexpr Dune::GeometryType::BasicType basicType() const
    {
      return isSimplex() ? Dune::GeometryType::simplex : Dune::GeometryType::cube;
    }

    /// Construct a \ref Dune::GeometryType from a \ref basicType and \ref dim.
    Dune::GeometryType geometryType() const
    {
      return {basicType(), dim()};
    }

    static Type flat(Dune::GeometryType t, int dim)
    {
      return t.isSimplex() ? (dim == 0 ? VERTEX : dim == 1 ? LINE2 : TRIANGLE3)
             /* quad */    : (dim == 0 ? VERTEX : dim == 1 ? LINE2 : QUAD4);
    }

    static Type curved(Dune::GeometryType t, int dim)
    {
      return t.isSimplex() ? (dim == 0 ? VERTEX : dim == 1 ? LINE3 : TRIANGLE6)
             /* quad */    : (dim == 0 ? VERTEX : dim == 1 ? LINE3 : QUAD8);
    }

    static Type simplex(int n)
    {
      return n == 1 ? VERTEX : n == 2 ? LINE2 : n == 3 ? TRIANGLE3 : OTHER;
    }
  };

} // end namespace Dec
