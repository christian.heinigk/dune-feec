#pragma once

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include "DecEntityData.hpp"
#include "LocalView.hpp"
#include "common/FieldMatVec.hpp"
#include "common/Output.hpp"

namespace Dune
{
  // External Forward Declarations

  template< class ctype, int dim >
  class ReferenceElement;

  template< class ctype, int dim >
  struct ReferenceElements;

} // end namespace Dune

namespace Dec
{
  template <int Dim, int WorldDim, class GridImp>
  class DecGeometry
  {
    using Grid = typename std::remove_const<GridImp>::type;
    using Traits = typename Grid::Traits;

  public:
    using ctype = typename Grid::ctype;

    /** \brief Dimension of the geometry */
    static constexpr int mydimension = Dim;

    static constexpr int codimension = Grid::dimension - Dim;

    /** \brief Dimension of the world space */
    static constexpr int coorddimension = WorldDim;

    /** \brief Type for local coordinate vector */
    using LocalCoordinate = Dune::FieldVector<ctype, mydimension>;

    /** \brief Type for coordinate vector in world space */
    using GlobalCoordinate = Dune::FieldVector<ctype, coorddimension>;

    /** \brief Type for the transposed Jacobian matrix */
    using JacobianTransposed = Dune::FieldMatrix<ctype, mydimension, coorddimension>;

    /** \brief Type for the transposed inverse Jacobian matrix */
    using JacobianInverseTransposed = Dune::FieldMatrix<ctype, coorddimension, mydimension>;

    using CoordView = VertexView<ctype, coorddimension>;

  private:
    /// Type of reference element
    using ReferenceElement = Dune::ReferenceElement<ctype, mydimension>;

    /// Type of reference element factory
    using ReferenceElements = Dune::ReferenceElements<ctype, mydimension>;

  public:

    /// Create geometry from \ref EntityData and vector of all coordinates
    DecGeometry(EntityData const& data, Grid const& grid, size_t index)
      : data_(data)
      , nodes_(grid.nodes)
      , refElement_(&ReferenceElements::general(data.type.geometryType()))
    {
      // TODO: Volumen und Umkreismittelpunkt für Simplices berechnen
      volume_ = 0;
      dual_volume_ = 0;
      integrationElement_ = volume_;
      center_ = global( refElement().position(0,0) ); // barycenter, TODO: replace with circumcenter
    }

    DecGeometry(std::vector<GlobalCoordinate> const& nodes, Grid const& grid)
      : data_{Geometry::simplex(nodes.size())}
      , nodes_(nodes)
      , refElement_(&ReferenceElements::general(data_.type.geometryType()))
      , basis_(BasisFactory::create(data_.type))
    {
      auto const& quad = Dune::QuadratureRules<ctype, mydimension>::rule(data_.type.geometryType(), 1);

      integrationElement_ = integrationElement( refElement().position(0,0) );

      volume_ = 0;
      for (auto const& qp : quad)
        volume_ += qp.weight() * integrationElement_;
    }



    /// Return true for flat simple elements.
    bool affine() const { return data_.type.isAffine(); }

    /// Obtain the type of the reference element.
    Dune::GeometryType type() const { return refElement().type(); }

    /// Obtain number of corners of the corresponding reference element.
    int corners() const { return data_.corners(); }

    /// Obtain coordinates of the i-th corner
    GlobalCoordinate corner(int i) const
    {
      return nodes()[i];
    }

    /// Obtain the centroid of the mapping's image
    GlobalCoordinate center() const
    {
      return center_;
    }

    // Linear barycentric interpolation of coordinates
    GlobalCoordinate global(LocalCoordinate const& local) const
    {
      GlobalCoordinate coord( local[0] * corner(0) );
      for (int i = 1; i < corners(); ++i)
        coord += local[i] * corner(i);
      return coord;
    }

    /// Obtain local coordinates from global position, NOTE: not implemented
    LocalCoordinate local(GlobalCoordinate const& global) const
    {
      LocalCoordinate local;
      error_exit("local(): Not implemented!");
      return local;
    }

    /// Obtain the integration element
    ctype integrationElement(LocalCoordinate const& local) const
    {
      return integrationElement_;
    }

    /// Obtain the volume of the element
    ctype volume() const
    {
      error_exit("volume(): Not implemented!");
      return volume_;
    }

    /// Obtain (signed) dual volume of the element
    ctype dual_volume() const
    {
      error_exit("dual_volume(): Not implemented!");
      return dual_volume_;
    }

    /// Obtain the transposed of the Jacobian
    JacobianTransposed jacobianTransposed(LocalCoordinate const& local) const
    {
      error_exit("jacobianTransposed(): Not implemented!");
      JacobianTransposed jacTrans;
      return jacTrans;
    }

    /// Obtain the transposed of the Jacobian's inverse. NOTE: not implemented
    JacobianInverseTransposed jacobianInverseTransposed(LocalCoordinate const& local) const
    {
      error_exit("jacobianInverseTransposed(): Not implemented!");
      JacobianInverseTransposed jacInvTrans;
      return jacInvTrans;
    }

    friend const ReferenceElement &referenceElement(DecGeometry const& geometry)
    {
      return geometry.refElement();
    }

  protected:

    CoordView nodes() const
    {
      return {nodes_, data_.indices};
    }

    ReferenceElement const& refElement() const
    {
      assert( refElement_ != nullptr );
      return *refElement_;
    }

  public:

    void init_dual_volume(Grid const& grid, size_t index)
    {
      std::vector<GlobalCoordinate> dual_nodes(codimension + 1);
      using EntityImp = typename Grid::template Codim<codimension>::EntityImp;
      typename Grid::template Codim<codimension>::Entity entity( EntityImp(&grid, index) );

      dual_volume_ = calc_dual_volume(entity, grid, dual_nodes);
      // NOTE: instead of using global coordinates we could use a sub-quadrature with local coordinates of the centers
    }

    template <class Entity>
    ctype calc_dual_volume(Entity const& entity, Grid const& grid, std::vector<GlobalCoordinate>& dual_nodes) const
    {
      static constexpr int cc = Entity::codimension;

      dual_nodes[Grid::dimension - cc] = entity.geometry().center();
      ctype dual_vol = 0;
      if (cc == 0) {
        if (dual_nodes.size() == 1)
          return 1;

        DecGeometry<Grid::dimension-cc, WorldDim, GridImp> geo(dual_nodes, grid);
        return geo.volume();
      } else {
        static constexpr int cc_next = cc > 0 ? cc-1 : 0;
        int const nNeighbours = grid.template subEntities<cc_next>(entity);
        for (int j = 0; j < nNeighbours; ++j) {
          auto e = grid.template subEntity<cc_next>(entity, j);
          dual_vol += calc_dual_volume(e, grid, dual_nodes);
        }
      }

      return dual_vol;
    }

  private:

    EntityData const& data_;
    std::vector<GlobalCoordinate> const& nodes_;
    ReferenceElement const* refElement_ = nullptr;

    /// the center
    ctype integrationElement_ = 0.0;
    ctype volume_ = 0.0;
    ctype dual_volume_ = 0.0;
    GlobalCoordinate center_;
  };


  /// Specialization for Geometry of Vertex
  template <int WorldDim, class GridImp>
  class DecGeometry<0, WorldDim, GridImp>
  {
    using Grid = typename std::remove_const<GridImp>::type;
    using Traits = typename Grid::Traits;

  public:
    using ctype = typename Grid::ctype;

    /** \brief Dimension of the geometry */
    static constexpr int mydimension = 0;

    /** \brief Dimension of the world space */
    static constexpr int coorddimension = WorldDim;

    /** \brief Type for local coordinate vector */
    using LocalCoordinate = Dune::FieldVector<ctype, mydimension>;

    /** \brief Type for coordinate vector in world space */
    using GlobalCoordinate = Dune::FieldVector<ctype, coorddimension>;

    /** \brief Type for the transposed Jacobian matrix */
    using JacobianTransposed = Dune::FieldMatrix<ctype, mydimension, coorddimension>;

    /** \brief Type for the transposed inverse Jacobian matrix */
    using JacobianInverseTransposed = Dune::FieldMatrix<ctype, coorddimension, mydimension>;

  private:
    /// Type of reference element
    typedef Dune::ReferenceElement<ctype, mydimension> ReferenceElement;

    /// Type of reference element factory
    typedef Dune::ReferenceElements<ctype, mydimension> ReferenceElements;

  public:

    /// Create geometry from \ref EntityData and vector of all coordinates
    DecGeometry(EntityData const& data, std::vector<GlobalCoordinate> const& nodes)
      : node_(nodes[data.indices[0]])
    {
      // TODO: dual Volumen für Vertices berechnen
      dual_volume_ = 0;
    }

    /// Return true for flat simple elements.
    bool affine() const { return true; }

    /// Obtain the type of the reference element.
    Dune::GeometryType type() const { return {0u}; }

    /// Obtain number of corners of the corresponding reference element.
    int corners() const { return 1; }

    /// Obtain coordinates of the i-th corner
    GlobalCoordinate const& corner(int /*i*/) const
    {
      return node_;
    }

    /// Obtain the centroid of the mapping's image
    GlobalCoordinate const& center() const
    {
      return node_;
    }

    GlobalCoordinate const& global(LocalCoordinate const& /*local*/) const
    {
      return node_;
    }

    /// Obtain local coordinates from global position, NOTE: not implemented
    LocalCoordinate local(GlobalCoordinate const& /*global*/) const
    {
      return {1.0};
    }

    /// Obtain the integration element
    ctype integrationElement(LocalCoordinate const& local) const
    {
      return 1.0;
    }

    /// Obtain the volume of the element
    ctype volume() const
    {
      return 1.0;
    }

    /// Obtain (signed) dual volume of the element
    ctype dual_volume() const
    {
      error_exit("dual_volume(): Not implemented!");
      return dual_volume_;
    }

    /// Obtain the transposed of the Jacobian
    JacobianTransposed jacobianTransposed(LocalCoordinate const& local) const
    {
      error_exit("jacobianTransposed(): Not implemented!");
      JacobianTransposed jacTrans;
      return jacTrans;
    }

    /// Obtain the transposed of the Jacobian's inverse. NOTE: not implemented
    JacobianInverseTransposed jacobianInverseTransposed(LocalCoordinate const& local) const
    {
      error_exit("jacobianInverseTransposed(): Not implemented!");
      JacobianInverseTransposed jacInvTrans;
      return jacInvTrans;
    }

    friend const ReferenceElement &referenceElement(DecGeometry const& /*geometry*/)
    {
      return ReferenceElements::simplex();
    }

  private:

    GlobalCoordinate const& node_;

    /// the center
    ctype dual_volume_ = 0.0;
  };

} // namespace Dune
