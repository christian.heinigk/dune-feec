#pragma once

#include <map>
#include <memory>
#include <utility>
#include <vector>

#include <dune/common/fvector.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/gridfactory.hh>

#include "DecGrid.hpp"
#include "common/Output.hpp"
#include "utility/SortedPair.hpp"

namespace Dune
{
  template <class GridImp, class = Dec::Void_t<> >
  struct Cached
      : public std::false_type {};

  template <class GridImp>
  struct Cached<GridImp, Dec::Void_t<typename GridImp::template Codim<0>::GeometryImp::NotCached>>
      : public std::true_type {};



  /// \brief A factory class for \ref DecGrid
  /**
   * Basically the factory adds the vertices and cells to a vector, by
   * creating \ref EntityData containers for each cell. Finalize constructs
   * entity relations like subEntity and supEntity that can be used in the
   * \ref DecIndexSet directly.
   **/
  template <int dim, int dow>
  class GridFactory<Dec::DecGrid<dim,dow>>
      : public GridFactoryInterface<Dec::DecGrid<dim,dow>>
  {
    using Grid = Dec::DecGrid<dim,dow>;

    /// Type used by the grid for coordinates
    using ctype = typename Grid::ctype;

    using GlobalCoordinates = FieldVector<ctype, dow>;

  public:

    /// Default constructor
    GridFactory()
      : grid_( new Grid )
    {}

    /// Destructor. Calls the destructor of the underlying grid,
    /// only if the grid construction was not finished.
    ~GridFactory()
    {
      if (!finished)
        delete grid_;
    }

    /// Insert a vertex into the coarse grid
    virtual void insertVertex(const FieldVector<ctype,dow>& pos) override
    {
      nodes.push_back(pos);
    }

    /// Insert an element into the coarse grid.
    virtual void insertElement(const GeometryType& type,
                               const std::vector<unsigned int>& vertices) override
    {
      std::vector<size_t> indices(vertices.begin(), vertices.end());
      cells.push_back({Dec::Geometry::flat(type, dim), indices});
    }

    /// Insert a parametrized element into the coarse grid.
    virtual void insertElement(const GeometryType& type,
                               const std::vector<unsigned int>& vertices,
                               const std::shared_ptr<VirtualFunction<FieldVector<ctype,dim>,
                                                                     FieldVector<ctype,dow> > >& elementParametrization) override
    {
      auto const& refElem = Dune::ReferenceElements<ctype, dim>::general(type);

      // make a copy of the indices to add more for the higher order nodes
      std::vector<size_t> indices(vertices.begin(), vertices.end());

      // add inner nodes for lagrange basis
      for (int i = 0; i < refElem.size(dim-1); ++i) { // edges
        auto const& local = refElem.position(i, dim-1); // barycenter of edges
        size_t idx = nodes.size();

        FieldVector<ctype,dow> global;
        elementParametrization->evaluate(local, global);

        nodes.push_back(global);
        indices.push_back(idx);
      }

      cells.push_back({Dec::Geometry::curved(type, dim), indices});
    }

    virtual void insertBoundarySegment(const std::vector<unsigned int>& vertices) override
    {
      Dec::msg("Read boundary segments: ",vertices.size());
    }

    virtual void insertBoundarySegment(const std::vector<unsigned int>& vertices,
                                       const std::shared_ptr<BoundarySegment<dim,dow> >& boundarySegment) override
    {
      Dec::msg("Read boundary segments2: ",vertices.size());
    }

    /// Finalize grid creation and hand over the grid.
    virtual Grid* createGrid() override
    {
      createBegin();

      initVertices();
      initEdges();
      initCells();

      assert( Cached<Grid>::value );
      initCache(Dec::Bool<Cached<Grid>::value>{});

      finished = true;
      return grid_;
    }


  private:

    // Delete unused nodes and sort indices of vertices, so that first the
    // corners are indexed, second the higher order nodes.
    void createBegin()
    {
      std::vector<short> used(nodes.size(), 0);

      for (auto const& c : cells) {
        size_t i = 0;
        for (; i < c.corners(); ++i)
          used[c.indices[i]] = 1;
        for (; i < c.indices.size(); ++i)
          used[c.indices[i]] = 2;
      }

      std::vector<size_t> idx_map(nodes.size());
      grid_->nodes.reserve(nodes.size());

      numVertices = 0;
      size_t j = 0;

      // at first the primary vertices
      for (size_t i = 0; i < nodes.size(); ++i) {
        if (used[i] == 1) {
          grid_->nodes.push_back(nodes[i]);
          idx_map[i] = j++;
          numVertices++;
        }
      }

      // then the higher order vertices
      for (size_t i = 0; i < nodes.size(); ++i) {
        if (used[i] > 1) {
          grid_->nodes.push_back(nodes[i]);
          idx_map[i] = j++;
        }
      }

      // rearange indices in cells
      for (auto& c : cells)
        for (auto& idx : c.indices)
          idx = idx_map[idx];
    }

    // Create vertex entities
    void initVertices()
    {
      // construct vertices from nodes
      grid_->vertices.resize(numVertices);
      for (unsigned int i = 0; i < numVertices; ++i)
        grid_->vertices[i] = {Dec::Geometry::VERTEX, {i}};
    }

    // Create edge entities
    void initEdges()
    {
      auto& edges_of_vertex = grid_->template entityRelation<0,1>();
      auto& cells_of_edge = grid_->template entityRelation<1,dim>();
      auto& edges_of_cell = grid_->template entityRelation<dim,1>();

      // use Euler polyeder formula to calculate nr. of edges
      // use characteristic = 0 to get an upper bound
      size_t const numEdges = numVertices + cells.size();
      grid_->edges.reserve(numEdges);

      edges_of_vertex.resize(numVertices);
      cells_of_edge.resize(numEdges);
      edges_of_cell.resize(cells.size());

      std::map<Dec::SortedPair<size_t>, size_t> visited; // edge -> edge_index
      using iterator = typename std::map<Dec::SortedPair<size_t>, size_t>::iterator;

      for (size_t c_index = 0; c_index < cells.size(); ++c_index) {
        auto const& c = cells[c_index];
        auto const& refElem = Dune::ReferenceElements<ctype, dim>::general(c.type.geometryType());

        iterator it;
        bool inserted = false;
        for (int i = 0; i < refElem.size(dim-1); ++i) {
          int idx0 = refElem.subEntity(i, dim-1, 0, dim);
          int idx1 = refElem.subEntity(i, dim-1, 1, dim);

          // insert edges with orientation, from lower to higher global index
          Dec::SortedPair<size_t> e = {c.indices[idx0], c.indices[idx1]};
          std::tie(it, inserted) = visited.insert({e, 0});

          if (inserted) {
            size_t edge_idx = grid_->edges.size();
            if (c.type.isFlat())
              grid_->edges.push_back({Dec::Geometry::LINE2, {e.first, e.second}});
            else
              grid_->edges.push_back({Dec::Geometry::LINE3, {e.first, e.second, c.indices[refElem.size(dim-1) + i]}});

            it->second = edge_idx;

            edges_of_vertex[e.first].push_back(edge_idx);
            edges_of_vertex[e.second].push_back(edge_idx);
          }

          edges_of_cell[c_index].push_back(it->second);
          if (cells_of_edge.size() <= it->second)
            cells_of_edge.resize(cells_of_edge.size() + 10);
          cells_of_edge[it->second].push_back(c_index);
        }
      }

      Dec::msg("#vertices = ",numVertices,", #cells = ",cells.size(),", #edges = ",grid_->edges.size());
      int characteristic = int(numVertices) + int(cells.size()) - int(grid_->edges.size());
      Dec::msg("characteristic = ",characteristic);
    }

#if 0
    // Create face entities
    void initFaces()
    {
      auto& faces_of_vertex = grid_->template entityRelation<0,2>();
      auto& cells_of_face = grid_->template entityRelation<2,dim>();
      auto& faces_of_cell = grid_->template entityRelation<dim,2>();

      // user Euler polyeder formula to calculate nr. of faces
      // use characteristic = 0 to get an approx. upper bound
      size_t const numFaces = grid_->edges.size() + cells.size() - numVertices;
      grid_->faces.reserve(numFaces);

      faces_of_vertex.resize(numVertices);
      cells_of_face.resize(numFaces);
      faces_of_cell.resize(cells.size());

      std::map<std::vector<size_t>, size_t> visited; // face -> face_index
      using iterator = typename std::map<std::vector<size_t>, size_t>::iterator;

      for (auto const& c : cells) {
        auto const& refElem = Dune::ReferenceElements<ctype, dim>::general(c.type.geometryType());

        iterator it;
        bool inserted = false;
        for (int i = 0; i < refElem.size(dim-1); ++i) {
          std::vector<size_t> f;
          for (int ii = 0; ii < refElem.size(i, dim-1, dim); ++ii)
            f.push_back( c.indices[refElem.subEntity(i, dim-1, ii, dim)] );

          std::tie(it, inserted) = visited.insert({sort(f), 0});

          if (inserted) {
            size_t face_idx = grid_->faces.size();
            if (c.type.isFlat())
              grid_->faces.push_back({Geometry::TRIANGLE3, f});
            else
              error_exit("Not yet implemented!");
//            grid_->faces.push_back({Geometry::TRIANGLE6, {e.first, e.second, c.indices[refElem.size(dim-1) + i]}});

            it->second = face_idx;

            faces_of_vertex[e.first].push_back(face_idx);
            faces_of_vertex[e.second].push_back(face_idx);
          }

          faces_of_cell[c.index].push_back(it->second);
          if (cells_of_face.size() <= it->second)
            cells_of_face.resize(cells_of_edge.size() + 10);
          cells_of_face[it->second].push_back(c.index);
        }
      }

      Dec::msg("#vertices = ",numVertices,", #cells = ",cells.size(),", #edges = ",grid_->edges.size(),", #faces = ",grid_->faces.size());
      int characteristic = int(numVertices) - int(cells.size()) - int(grid_->edges.size()) + int(grid_->faces.size());
      Dec::msg("characteristic = ",characteristic);
    }
#endif

    // Create vertex-to-cell relations
    void initCells()
    {
      auto& cells_of_vertex = grid_->template entityRelation<0,dim>();
      cells_of_vertex.resize(numVertices);

      for (size_t c_index = 0; c_index < cells.size(); ++c_index) {
        auto const& c = cells[c_index];
        for (auto const& idx : c.indices)
          cells_of_vertex[idx].push_back(c_index);
      }

      std::swap(grid_->nodes, nodes);
      std::swap(grid_->cells, cells);
    }


    void initCache(Dec::Bool<false>) { assert( false ); }

    void initCache(Dec::Bool<true>)
    {
      forEach(Dec::range_<0,dim+1>, [this](auto const cd) {
        using GeoImp = typename Grid::template Codim<cd>::GeometryImp;
        using Geo = typename GeoImp::NotCached;

        auto const& data = this->grid_->template subEntityData<cd>();
        auto& cache = GeoImp::cache().data_;

        for (size_t i = 0; i < data.size(); ++i) {
          Geo geometry(data[i], *this->grid_, i);
          cache[i].center_ = geometry.center();
          cache[i].volume_ = geometry.volume();

          if (cd == 0) { // cell
            using EntityImp = typename Grid::template Codim<cd>::EntityImp;
            typename Grid::template Codim<cd>::Entity entity( EntityImp(this->grid_, i) );
            int corners = this->grid_->template subEntities<dim>(entity);

            using VertexImp = typename Grid::template Codim<dim>::GeometryImp;
            auto& vertexCache = VertexImp::cache().data_;
            for (int j = 0; j < corners; ++j) {
              size_t index = this->grid_->template subIndex<dim>(entity, j);
              vertexCache[index].dual_volume_ += cache[i].volume_ / corners;
            }

            cache[i].dual_volume_ = 1;
          } else if (cd != dim) { // vertex->dual_volume already assigned
            geometry.init_dual_volume(*this->grid_, i);
            cache[i].dual_volume_ = geometry.dual_volume();
          }
        }

        GeoImp::cache().initialized = true;
      });
    }

  private:
    // Pointer to the grid being built
    Grid* grid_;

    // Creation was finished and grid handed over to receiver.
    bool finished = false;

    // buffers for the mesh data
    std::vector<GlobalCoordinates> nodes;
    std::vector<Dec::EntityData> cells;

    size_t numVertices = 0;
  };

} // end namespace Dune
