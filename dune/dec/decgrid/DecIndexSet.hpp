#pragma once

#include <type_traits>
#include <vector>

#include <dune/geometry/type.hh>
#include <dune/grid/common/indexidset.hh>

#include "common/Output.hpp"
#include "common/Index.hpp"

namespace Dec
{
  /// \brief A wrapper for EntityData vectors
  /**
   * Provides an iterator range that gives an iterator that constructs a GeometryType
   * on dereferencing the iterator.
   **/
  struct GeometryTypeRange
  {
    using EntityDataRange = std::vector<EntityData>;
    using EntityDataIterator = typename EntityDataRange::const_iterator;

    class iterator
        : public std::iterator<std::input_iterator_tag, Dune::GeometryType>
    {
    public:
      explicit iterator(EntityDataIterator it) : it_(it) {}

      iterator& operator++() { ++it_; return *this; }
      iterator operator++(int) { iterator tmp(*this); ++(*this); return tmp; }

      bool operator==(iterator other) const { return it_ == other.it_; }
      bool operator!=(iterator other) const { return !(*this == other); }

      /// Construct the GeometryType from a \ref Geometry::BaseType and a dimension.
      Dune::GeometryType operator*() const { return it_->type.geometryType(); }

    private:
      EntityDataIterator it_;
    };

    GeometryTypeRange(EntityDataRange const& range) : range_(range) {}

    iterator begin() const { return iterator(range_.begin()); }
    iterator end() const { return iterator(range_.end()); }

  private:
    EntityDataRange const& range_;
  };


  /// \brief Implement a \ref IndexSet for \ref DecGrid.
  /**
   * The indices in the grid are already assigned and thus the indices are extracted from
   * the EntityData directly.
   **/
  template <class GridImp>
  class DecIndexSet
      : public Dune::IndexSet<GridImp, DecIndexSet<GridImp>, size_t, GeometryTypeRange>
  {
    using Grid = typename std::remove_const<GridImp>::type;
    using Traits = typename Grid::Traits;

    template <int dim, int dow> friend class DecGrid;

  public:
    template <int cc>
    using Codim = typename Traits::template Codim<cc>;

    /// Dimension of the grid (maximum allowed codimension)
    static constexpr int dim = Grid::dimension;

  public:

    /// Constructor takes a grid reference.
    DecIndexSet(Grid const& grid)
      : grid_(grid)
    {}


  public:

    /// Map entity to index.
    template <int cc>
    size_t index(typename Codim<cc>::Entity const& e) const
    {
      typename Codim<cc>::EntityImp const& e_ = grid_.getRealImplementation(e);
      return e_.index_;
    }

    /// Map entity to index.
    template <class Entity>
    size_t index(Entity const& e) const
    {
      return index<Entity::codim>(e);
    }

    /// Map a subentity to an index.
    template <int cc>
    size_t subIndex(typename Codim<cc>::Entity const& e, int i, unsigned int ccodim) const
    {
      typename Codim<cc>::EntityImp const& e_ = grid_.getRealImplementation(e);

      switch (dim-ccodim) {
        case 0:
          return subIndexImpl(e_.index_, i, int_t<dim-cc>{}, int_t<0>{}); // vertex
          break;
        case 1:
          return subIndexImpl(e_.index_, i, int_t<dim-cc>{}, int_t<1>{}); // edge
          break;
        case dim:
          return subIndexImpl(e_.index_, i, int_t<dim-cc>{}, int_t<dim>{}); // cell
          break;
        default:
          error_exit("Codim out of range [0,dim]");
      }
    }

    /// Map a subentity to an index.
    template <class Entity>
    size_t subIndex(Entity const& e, int i, unsigned int ccodim) const
    {
      return subIndex<Entity::codim>(e, i, ccodim);
    }

  private: // implementation defails

    // index of i'th vertex of cell
    size_t subIndexImpl(size_t index, int i, int_t<dim>, int_t<0>) const
    {
      return grid_.cells[index].indices[i];
    }

    // index of i'th vertex of edge
    size_t subIndexImpl(size_t index, int i, int_t<1>, int_t<0>) const
    {
      return grid_.edges[index].indices[i];
    }

    template <int entity_dim, int sub_dim>
    size_t subIndexImpl(size_t index, int i, int_t<entity_dim>, int_t<sub_dim>) const
    {
      if (entity_dim == sub_dim)
        return index;

      auto const& rel = grid_.template entityRelation<entity_dim, sub_dim>();
      return rel[index][i];
    }


  public:

    /// Iterator range over Const reference to a vector of geometry types.
    /// Uses a EntityData wrapper, that creates the GeometryType on the fly.
    GeometryTypeRange types(int codim) const
    {
      switch (dim-codim) {
        case 0:
          return GeometryTypeRange(grid_.vertices);
          break;
        case 1:
          return GeometryTypeRange(grid_.edges);
          break;
        case dim:
          return GeometryTypeRange(grid_.cells);
          break;
        default:
          error_exit("Codim out of range [0,dim]");
      }
    }

    /// Return total number of entities of given geometry type in entity set.
    size_t size(Dune::GeometryType type) const
    {
      return size(dim - type.dim());
    }

    /// Return total number of entities of given codim in the entity set.
    size_t size(int codim) const
    {
      switch (dim-codim) {
        case 0:
          return grid_.vertices.size();
          break;
        case 1:
          return grid_.edges.size();
          break;
        case dim:
          return grid_.cells.size();
          break;
        default:
          error_exit("Codim out of range [0,dim]");
      }
    }

    /// Return true if the given entity is contained in the grid. Tests only of entitys index is
    /// within the entity-range.
    template <class Entity>
    bool contains(Entity const& e) const
    {
      typename Codim<Entity::codimension>::EntityImp const& e_ = grid_.getRealImplementation(e);
      return e_.index_ < size(Entity::codim);
    }

  private:
    Grid const& grid_;
  };


  // ---------------------------------------------------------------------------


  /// \brief Class that provides unique ids for entities of a grid.
  /**
   * Since no grid changes are allowed, the id is the same as the entity index.
   * \see DecIndexSet for the access of the entity indices.
   **/
  template <class GridImp>
  class DecIdSet
      : public Dune::IdSet<GridImp, DecIdSet<GridImp>, size_t>
  {
    using Grid = typename std::remove_const<GridImp>::type;
    using Traits = typename Grid::Traits;

  public:
    template <int cc>
    using Codim = typename Traits::template Codim<cc>;

    /// Dimension of the grid (maximum allowed codimension)
    static constexpr int dim = Grid::dimension;

  public:
    /// Constructor takes a grid reference and stores its indexSet as reference.
    DecIdSet(Grid const& grid)
      : indexSet_(grid.leafIndexSet())
    {}

    /// Get id of an entity of codim cc.
    template <int cc>
    size_t id(typename Codim<cc>::Entity const& e) const
    {
      return indexSet_.index(e);
    }

    /// Get id of an entity of codim cc.
    template <class Entity>
    size_t id(Entity const& e) const
    {
      return indexSet_.index(e);
    }

    /// Get id of subentity i of co-dimension codim of a cell (entity of co-dimension 0).
    size_t subId(typename Codim<0>::Entity const& e, int i, unsigned int codim) const
    {
      return indexSet_.subIndex(e, i, codim);
    }

  private:
    typename Traits::LeafIndexSet const& indexSet_;
  };

} // end namespace Dec
