#pragma once

#include <type_traits>
#include <vector>

#include <dune/grid/common/gridfactory.hh>

namespace Dune
{
  /// \brief Create grid from a vector of points and elements
  template <class GridType>
  class CoordsGridFactory
  {
  public:
    static constexpr int dim = GridType::dimension;
    static constexpr int dow = GridType::dimensionworld;

    static_assert( dim <= 3, "CoordsGridFactory implemented for dim <= 3 only!" );

    using ctype = typename GridType::ctype;
    using WorldVector = FieldVector<ctype, dow>;

    /// \brief Construct the grid from a container of element-index vectors \tparam Elements
    /// and a vector of global coordinates \tparam Coordinates.
    template <class Elements, class Coordinates>
    static GridType* createGrid(Elements const& elements,
                                Coordinates const& coordinates)
    {
      static_assert( std::is_same<typename Elements::value_type, std::vector<unsigned int>>::value, "" );
      static_assert( std::is_same<typename Coordinates::value_type, WorldVector>::value, "" );

      // The grid factory
      GridFactory<GridType> factory;

      for (auto const& vertex : coordinates)
        factory.insertVertex(vertex);

      for (auto const& elem : elements) {
        factory.insertElement(geometryTypeFromVertexCount(dim, elem.size()), elem);
      }

      // Create the grid and hand it to the calling method
      return factory.createGrid();
    }

  };

}  // end namespace Dune
