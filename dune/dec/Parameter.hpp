#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "external/json11/json11.hpp"

namespace Dec
{
  using Parameter = json11::Json;

  inline bool read(std::string const& filename, Parameter& json)
  {
    std::string in;
    std::fstream fs;
    fs.open(filename, std::ios::in);
    if (fs.is_open()) {
      for (std::string line; std::getline(fs, line); ) {
          in += line;
      }
      in += "\n";
      fs.close();
    } else {
      std::cout << "json file '" << filename << "' could not be read!\n";
    }

    std::string err;
    json = json11::Json::parse(in, err);
    if (!err.empty())
      std::cout << "JSon-Fehler: " << err << "\n";

    return err.empty();
  }

} // end namespace Dec
