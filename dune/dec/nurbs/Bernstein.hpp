#pragma once

#include <fstream>
#include <iostream>
#include <string>

#include <dune/common/fvector.hh>

#include <Eigen/Dense>

#include <dune/dec/common/Get.hpp>
#include <dune/dec/common/Logical.hpp>
#include <dune/dec/common/Size.hpp>
#include <dune/dec/common/StaticLoops.hpp>
#include <dune/dec/common/FieldMatVec.hpp>

namespace Dec
{
  template <int N>
  using Coord = Dune::FieldVector<double, N>;

  namespace aux
  {
    template <class T>
    struct PlainObjectImpl
    {
      using type = T;
    };

    template <class XprType, int BlockRows, int BlockCols, bool InnerPanel>
    struct PlainObjectImpl<Eigen::Block<XprType, BlockRows, BlockCols, InnerPanel>>
    {
      using type = Eigen::Matrix<Decay_t<typename XprType::Scalar>, BlockRows, BlockCols>;
    };

    template <class Derived>
    struct PlainObjectImpl<Eigen::MatrixBase<Derived>>
    {
      using type = typename Derived::PlainObject;
    };

    template <class Derived>
    struct PlainObjectImpl<Eigen::ArrayBase<Derived>>
    {
      using type = typename Derived::PlainObject;
    };
  }

  template <class T>
  using PlainObject = typename aux::PlainObjectImpl<T>::type;


  namespace aux
  {
    template <int i, int n, bool valid>
    class BernsteinImpl;
  }

  template <int i, int n>
  using Bernstein = aux::BernsteinImpl<i, n, (i>=0 && i <= n)>;

  namespace aux
  {
    template <int i, int n, bool valid /* true */>
    class BernsteinImpl
    {
      template <class T>
      static T eval(T u)
      {
        return (1 - u)*Bernstein<i, n-1>::eval(u) + u*Bernstein<i-1, n-1>::eval(u);
      }
    };

    template <int i, int n>
    class BernsteinImpl<i, n, false> // i < 0 || i > n
    {
      template <class T>
      static T eval(T /*u*/)
      {
        return T(0);
      }
    };


    template <>
    class BernsteinImpl<0, 0, true>
    {
      template <class T>
      static T eval(T /*u*/) { return T(1); }
    };

    template <>
    class BernsteinImpl<0, 1, true>
    {
      template <class T>
      static T eval(T u) { return 1 - u; }
    };

    template <>
    class BernsteinImpl<1, 1, true>
    {
      template <class T>
      static T eval(T u) { return u; }
    };

  } // end namespace aux


  template <int i, int n>
  class BernsteinDiff
  {
    template <class T>
    static T eval(T u)
    {
      return n*(Bernstein<i-1, n-1>::eval(u) - Bernstein<i, n-1>::eval(u));
    }
  };

  // compute the value of a Bernstein polynomial
  template <class T>
  T bernstein(int i, int n, T u)
  {
    if (i < 0 || i > n)
      return T(0);

    std::vector<T> temp(n+1, T(0));
    temp[n-i] = 1;

    T u1 = 1 - u;
    for (int k = 1; k <= n; ++k)
      for (int j = n; j >= k; --j)
        temp[j] = u1*temp[j] + u*temp[j-1];

    return temp[n];
  }

  namespace test
  {
    static constexpr int n_max = 10;

    // nonegativity: B_{i,n}(u) >= 0, for all i,n and 0<=u<=1
    bool bernstein_nonnegativity(double tol);

    // partition od unity: sum_i=0^n B_{i,n}(u) = 1, for all 0<=u<=1
    bool bernstein_partition_of_unity(double tol);

    // symmetry: for any n, the set {B_{i,n}(u)} is symmetric with respect to u=1/2
    bool bernstein_symmetry(double tol);

    // B_{i,n}(u) attains exactly one maximum ion the interval [0,1], that is, at u=i/n
    bool bernstein_maximum(double h);

    // recursive definition: B_{i,n}(u) = (1-u)*B_{i,n-1}(u) + u*B_{i-1,n-1}(u)
    bool bernstein_recursive_definition(double tol);

  } // end namespace test


  // compute all nth-degree Bernstein polynomials
  template <class T>
  std::vector<T> all_bernstein(std::size_t n, T u)
  {
    std::vector<T> B(n);
    B[0] = 1;
    T u1 = 1 - u;
    for (std::size_t j = 1; j < n; ++j) {
      T saved = 0;
      for (std::size_t k = 0; k < j; ++k) {
        T temp = B[k];
        B[k] = saved + u1*temp;
        saved = u*temp;
      }
      B[j] = saved;
    }

    return B;
  }


  // compute point on Bezier curve
  template <class Points, class T>
  typename Points::value_type point_on_bezier_curve(Points const& P, T u)
  {
    std::size_t n = P.size();

    auto B = all_bernstein(n, u);
    auto C = B[0] * P[0];
    for (std::size_t k = 1; k < n; ++k)
      C += B[k] * P[k];

    return C;
  }


  // compute derivative of Bezier curve
  template <class Points, class T>
  typename Points::value_type derivative_on_bezier_curve(Points const& P, T u)
  {
    Expects( P.size() > 0 );
    std::size_t n = P.size()-1;

    auto B = all_bernstein(n, u);
    auto C = B[0] * (P[1] - P[0]);
    for (std::size_t k = 1; k < n; ++k)
      C += B[k] * (P[k+1] - P[k]);

    return C*n;
  }


  // compute point on a Bezier curve using deCasteljau algorithm
  template <class Points, class T,
            int N = Size<typename Points::value_type>>
  Coord<N> deCasteljau(Points const& P, T u)
  {
    std::size_t n = P.size();

    PlainObject<Points> Q(P);

    for (std::size_t k = 1; k < n; ++k)
      for (std::size_t i = 0; i < n-k; ++i)
        Q[i] = (1-u)*Q[i] + u*Q[i+1];
    return Q[0];
  }

  template <class Points>
  void plot(Points const& P, std::string filename, std::size_t resolution = 100)
  {
    Expects( resolution > 1 );

    std::ofstream out(filename, std::ios_base::out);
    out << "# number points: " << resolution << "\n";
    out << "# X Y\n";

    double h = 1.0/(resolution-1);
    for (std::size_t i = 0; i < resolution; ++i) {
      auto C = deCasteljau(P, i*h);
      for (auto const& c_i : C)
        out << ' ' << c_i;
      out << '\n';
    }
    out << "\n\n";
    out << "# number points: " << P.size() << "\n";
    out << "# X Y\n";
    for (auto const& p : P) {
      for (auto const& p_i : p)
        out << ' ' << p_i;
      out << '\n';
    }
  }


  template <int N>
  Coord<N-1> restrict(Coord<N> const& Pw)
  {
    using T = typename Coord<N>::value_type;
    Coord<N-1> P;
    auto w = Pw[N-1] != T(0) ? Pw[N-1] : T(1);
    for (int j = 0; j < N-1; ++j)
      P[j] = Pw[j]/w;
    return P;
  }

  template <int N, class T>
  Coord<N+1> project(Coord<N> const& P, T w)
  {
    Coord<N+1> Pw;
    for (int j = 0; j < N; ++j)
      Pw[j] = w*P[j];
    Pw[N] = w;
    return Pw;
  }

  // TODO: use wrapper to produce higher-dimensional point automatically
  template <class Points, class Weights,
            int N = Size<typename Points::value_type>>
  std::vector<Coord<N+1>> project(Points const& P, Weights const& weights)
  {
    Expects( P.size() == weights.size() );

    std::vector<Coord<N+1>> Pw(P.size());
    for (std::size_t i = 0; i < std::size_t(P.size()); ++i)
      Pw[i] = project(P[i], weights[i]);
    return Pw;
  }


  // compute point on rational Bezier curve, by projection into N+1 dimension space
  template <class Points, class Weights, class T>
  auto deCasteljau(Points const& P, Weights const& weights, T u)
  {
    return restrict( deCasteljau(project(P,weights), u) );
  }

  template <class Points, class Weights>
  void plot(Points const& P, Weights const& weights, std::string filename, std::size_t resolution = 100)
  {
    Expects( resolution > 1 );

    std::ofstream out(filename, std::ios_base::out);
    out << "# number points: " << resolution << "\n";
    out << "# X Y\n";

    double h = 1.0/(resolution-1);
    for (std::size_t i = 0; i < resolution; ++i) {
      auto C = deCasteljau(P, weights, i*h);
      for (auto const& c_i : C)
        out << ' ' << c_i;
      out << '\n';
    }
    out << "\n\n";
    out << "# number points: " << P.size() << "\n";
    out << "# X Y\n";
    for (auto const& p : P) {
      for (auto const& p_i : p)
        out << ' ' << p_i;
      out << '\n';
    }
  }

  namespace test
  {
    // compare point_on_bezier_curve() with deCasteljau()
    bool point_on_bezier_curve_decasteljau(double tol);

    // n=1 then C(u)=(1-u)*P0 + u*P1
    bool decasteljau_line(double tol);

    // Endpoint interpolation: C(0) = P0 and C(1) = Pn, and
    // If wi = 1 for all i, then R_{i,n}(u) = B_{i,n}(u) for all i
    bool decasteljau_rational(double tol);

    // Points = {(1,0),(1,1),(0,1)}, weights = {1,1,2} => circle
    bool decasteljau_circle(double tol);

  } // end namespace test


  // evaluate point on Bezier surface given by parameters u and v in [0,1]
  template <class PointsArray, class T,
            int N = Size<typename PointsArray::value_type>>
  Coord<N> deCasteljau2(PointsArray const& P, T u, T v) // PointsArray = Eigen::Matrix<...>
  {
    std::size_t n = P.cols();
    std::size_t m = P.rows();

    std::vector<Coord<N>> Q(std::max(n,m));

    if (n <= m) {
      for (std::size_t j = 0; j < m; ++j)
        Q[j] = deCasteljau(P.row(j), u);
      return deCasteljau(Q, v);
    } else {
      for (std::size_t i = 0; i < n; ++i)
        Q[i] = deCasteljau(P.col(i), v);
      return deCasteljau(Q, u);
    }
  }

  template <class PointsArray>
  void plot2(PointsArray const& P, std::string filename, std::size_t resolution = 100)
  {
    Expects( resolution > 1 );

    std::ofstream out(filename, std::ios_base::out);
    out << "# number points: " << resolution*resolution << "\n";
    out << "# X Y Z\n";

    double h = 1.0/(resolution-1);
    for (std::size_t i = 0; i < resolution; ++i) {
      for (std::size_t j = 0; j < resolution; ++j) {
        auto C = deCasteljau2(P, i*h, j*h);
        for (auto const& c_i : C)
          out << ' ' << c_i;
        out << '\n';
      }
      out << '\n';
    }
  }


  // rational Bezier surface evaluation
  template <class PointsArray, class WeightsArray, class T,
            int N = Size<typename PointsArray::value_type>>
  Coord<N> deCasteljau2(PointsArray const& P, WeightsArray const& weights, T u, T v) // PointsArray = Eigen::Matrix<...>
  {
    Expects( P.size() == weights.size() );

    std::size_t n = P.cols();
    std::size_t m = P.rows();

    std::vector<Coord<N+1>> Q(std::max(n,m));

    if (n <= m) {
      for (std::size_t j = 0; j < m; ++j)
        Q[j] = deCasteljau(project(P.row(j), weights.row(j)), u);
      return restrict( deCasteljau(Q, v) );
    } else {
      for (std::size_t i = 0; i < n; ++i)
        Q[i] = deCasteljau(project(P.col(i), weights.col(i)), v);
      return restrict( deCasteljau(Q, u) );
    }
  }

  namespace test
  {
    // If wi = 1 for all i, then R_{i,n}(u) = B_{i,n}(u) for all i
    bool decasteljau2_rational(double tol);

  } // end namespace test


  // binary search for the fitting interval
  template <class T, class KnotVector>
  std::size_t findSpan(std::size_t n, std::size_t p, T u, KnotVector const& U)
  {
//     assert( U.size() == n+2 );
    if (u == U[n+1])
      return n; // special case

    std::size_t low = p, high = n+1;
    std::size_t mid = (low + high)/2;
    while (u < U[mid] || u >= U[mid+1]) {
      if (u < U[mid])
        high = mid;
      else
        low = mid;
      mid = (low + high)/2;
    }
    return mid;
  }


  template <class T, class KnotVector>
  std::vector<T> basisFunctions(int i, T u, std::size_t p, KnotVector const& U)
  {
    std::vector<T> N(p+1), left(p+1), right(p+1);
    N[0] = 1;
    for (std::size_t j = 1; j <= p; ++j) {
      left[j] = u - U[i+1-j];
      right[j] = U[i+j] - u;

      T saved = 0;
      for (std::size_t r = 0; r < j; ++r) {
        T temp = N[r] / (right[r+1] + left[j-r]);
        N[r] = saved + right[r+1]*temp;
        saved = left[j-r]*temp;
      }
      N[j] = saved;
    }
  }




} // end namespace Dec
