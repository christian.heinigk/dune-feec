#include "Bernstein.hpp"

#include <Eigen/Dense>

namespace Dec {
namespace test {

// nonegativity: B_{i,n}(u) >= 0, for all i,n and 0<=u<=1
bool bernstein_nonnegativity(double tol)
{
  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};

  for (int n = 0; n <= n_max; ++n) {
    for (int i = -1; i <= n; ++i) {
      for (auto u : U) {
        if (bernstein(i,n,u) < -tol)
          return false;
      }
    }
  }
  return true;
}

// partition od unity: sum_i=0^n B_{i,n}(u) = 1, for all 0<=u<=1
bool bernstein_partition_of_unity(double tol)
{
  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};

  for (auto u : U) {
    double sum = 0.0;
    for (int i = 0; i <= n_max; ++i) {
      sum += bernstein(i,n_max,u);
    }
    if (std::abs(sum - 1.0) > tol)
      return false;
  }
  return true;
}

// symmetry: for any n, the set {B_{i,n}(u)} is symmetric with respect to u=1/2
bool bernstein_symmetry(double tol)
{
  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};

  for (int n = 0; n <= n_max; ++n) {
    if (std::abs(bernstein(0,n,0.0) - 1.0) > tol ||
        std::abs(bernstein(n,n,1.0) - 1.0) > tol)
      return false;

    for (int i = -1; i <= n; ++i) {
      for (auto u : U) {
        if (std::abs(bernstein(i,n,u) - bernstein(i,n,1 - u)) > tol)
          return false;
      }
    }
  }
  return true;
}

// B_{i,n}(u) attains exactly one maximum ion the interval [0,1], that is, at u=i/n
bool bernstein_maximum(double h = 1.e-10)
{
  for (int n = 0; n <= n_max; ++n) {
    for (int i = -1; i <= n; ++i) {
      double u = i/double(n);
      auto f = bernstein(i,n,u);
      if (f <= bernstein(i,n,u - h) || f <= bernstein(i,n,u + h))
        return false;
    }
  }
  return true;
}

// recursive definition: B_{i,n}(u) = (1-u)*B_{i,n-1}(u) + u*B_{i-1,n-1}(u)
bool bernstein_recursive_definition(double tol)
{
  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};

  for (int n = 0; n <= n_max; ++n) {
    for (int i = 0; i <= n; ++i) {
      for (auto u : U) {
        if (std::abs( bernstein(i,n,u) - ((1-u)*bernstein(i,n-1,u) + u*bernstein(i-1,n-1,u)) ) > tol)
          return false;
      }
    }
  }
  return true;
}

bool point_on_bezier_curve_decasteljau(double tol)
{
  Coord<3> P0 = {0.0, 1.0, -2.0};
  Coord<3> P1 = {-2.0, 3.0, 1.0};
  Coord<3> P2 = {-1.0, 2.0, -1.0};

  std::vector<Coord<3>> points;
  points.push_back(P0);
  points.push_back(P1);
  points.push_back(P2);

  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};
  for (auto u : U) {
    if (distance(deCasteljau(points, u), point_on_bezier_curve(points, u)) > tol)
      return false;
  }
  return true;
}

bool decasteljau_line(double tol)
{
  Coord<3> P0 = {0.0, 1.0, -2.0};
  Coord<3> P1 = {-2.0, 3.0, 1.0};

  std::vector<Coord<3>> points;
  points.push_back(P0);
  points.push_back(P1);

  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};
  for (auto u : U) {
    if (distance(deCasteljau(points, u), (1-u)*P0 + u*P1) > tol)
      return false;
  }
  return true;
}


bool decasteljau_rational(double tol)
{
  Coord<3> P0 = {0.0, 1.0, -2.0};
  Coord<3> P1 = {-2.0, 3.0, 1.0};
  Coord<3> P2 = {-1.0, 2.0, -1.0};

  std::vector<Coord<3>> points;
  points.push_back(P0);
  points.push_back(P1);
  points.push_back(P2);

  std::vector<double> weights(points.size(), 1.0);
  std::vector<double> weights2(points.size(), 2.0);

  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};
  for (auto u : U) {
    if (distance(deCasteljau(points, u), deCasteljau(points, weights, u)) > tol)
      return false;
  }

  if (distance(P0, deCasteljau(points, weights2, 0.0)) > tol)
    return false;

  if (distance(P2, deCasteljau(points, weights2, 1.0)) > tol)
    return false;

  return true;
}


bool decasteljau_circle(double tol)
{
  Coord<2> P0 = {1.0, 0.0};
  Coord<2> P1 = {1.0, 1.0};
  Coord<2> P2 = {0.0, 1.0};

  std::vector<Coord<2>> points;
  points.push_back(P0);
  points.push_back(P1);
  points.push_back(P2);

  std::vector<double> weights = {1.0, 1.0, 2.0};

  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};
  for (auto u : U) {
    if (std::abs(two_norm(deCasteljau(points, u)) - 1.0) > tol)
      return false;
  }

  return true;
}



bool decasteljau2_rational(double tol)
{
  Coord<3> P00 = {0.0, 0.0, 1.0};
  Coord<3> P10 = {1.0, 0.0, 1.5};
  Coord<3> P20 = {2.0, 0.0, 0.6};

  Coord<3> P01 = {-0.2, 1.0, 0.5};
  Coord<3> P11 = {1.3, 1.0, 1.0};
  Coord<3> P21 = {2.1, 1.0, 0.1};

  Coord<3> P02 = {0.2, 2.0, 0.25};
  Coord<3> P12 = {1.0, 2.0, 0.75};
  Coord<3> P22 = {1.9, 2.0, -0.15};

  Eigen::Matrix<Coord<3>, 3, 3> points;
  points <<  P00, P10, P20,
             P01, P11, P21,
             P02, P12, P22;

  Eigen::Matrix<double, 3, 3> weights = Eigen::MatrixXd::Constant(3, 3, 1.0);

  auto U = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};
  auto V = {0.0, 1.0/M_PI, 1.0/std::sqrt(2.0), 1.0};

  for (auto u : U) {
    for (auto v : V) {
      if (distance(deCasteljau2(points, u, v), deCasteljau2(points, weights, u, v)) > tol)
        return false;
    }
  }
  return true;
}

} // end namespace test
} // end namespace Dec
