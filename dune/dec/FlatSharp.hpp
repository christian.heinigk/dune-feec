#pragma once

#include "LinearAlgebra.hpp"
#include "ranges/Store.hpp"

namespace Dec
{
  // Implementation details:
  namespace aux
  {
    template <int K, int dim>
    struct Flat {};

    template <int K, int dim>
    struct Sharp {};

    template <int K, int dim>
    struct Interpol;


    template <int dim>
    struct Flat<0, dim>
    {
      template <class GridView, class Vector>
      static Vector impl(GridView const& /*gv*/, Vector const& field, int_t<0>) // K = 0
      {
        static_assert( dim == GridView::dimension, "" );
        return {field};
      }
    };


    template <int dim>
    struct Flat<dim, dim>
    {
      // values at the vertices --> integral value over cells
      template <class GridView, class Vector>
      static Vector impl(GridView const& gv, Vector const& field, int_t<0>, valid)
      {
        Vector form{gv, dim, field.name()};

        for (auto const& e : elements(gv))
        {
          Value_t<Vector> value{};
          int n = 0;
          for (auto const& v : vertices(e)) {
            value += field[v.index()];
            ++n;
          }

          form[e.index()] = value*gv.volume(e)/n;
        }

        return form;
      }

      // values at the cell centers --> integral value over cells
      template <class GridView, class Vector>
      static Vector impl(GridView const& gv, Vector const& field, int_t<dim>, requires_t<(dim>0)>)
      {
        Vector form{field};
        for (auto const& e : elements(gv))
        {
          form[e.index()] *= gv.volume(e);
        }

        return form;
      }

      template <class GridView, class Vector, int k>
      static auto impl(GridView const& gv, Vector const& field, int_t<k>)
      {
        static_assert( dim == GridView::dimension, "" );
        return Flat::impl(gv, field, int_<k>, valid_);
      }
    };


    template <int dim>
    struct Flat<1, dim>
    {
      // define the transformation for vector fields living on the vertices
      template <class GridView, class WorldVector = typename GridView::GlobalCoordinate>
      static DOFVector<float_type> impl(GridView const& gv, DOFVector<WorldVector> const& field, int_t<0>)
      {
        static_assert( dim == GridView::dimension, "" );

        DOFVector<float_type> form{gv, 1, field.name()};

        auto const& is = gv.indexSet();
        for (auto const& e : edges(gv))
        {
          auto vert = is.vertexIndices(e);
          auto v0 = vert.begin();
          auto v1 = std::next(v0);

          WorldVector vec = 0.5*(field[*v0] + field[*v1]);

          form[e.index()] = vec.dot(e.geometry().vector());
        }
        return form;
      }
    };


    // ---------------------------------------------------------------------------------------------


    template <int dim>
    struct Sharp<0, dim>
    {
      // sharp a 0-form
      template <class GridView, class Vector>
      static Vector impl(GridView const& /*gv*/, Vector const& form, int_t<0>, valid)
      {
        static_assert( dim == GridView::dimension, "" );
        return {form};
      }

      // sharp a 2-form --> values of the vertices
      template <class GridView, class Vector>
      static Vector impl(GridView const& gv, Vector const& form, int_t<dim>, requires_t<(dim>0)>)
      {
        static_assert( dim == GridView::dimension, "" );

        Vector field{gv, 0, form.name()};
        Vector volumes{gv, 0, "volumes"};
        field = 0;

        for (auto const& e : elements(gv))
        {
          auto vol = gv.volume(e);
          for (auto const& v : vertices(e)) {
            field[v.index()] += form[e.index()];
            volumes[v.index()] += vol;
          }
        }

        for (auto const& v : vertices(gv))
        {
          assert( volumes[v.index()] > 0.0 );
          field[v.index()] /= volumes[v.index()];
        }

        return field;
      }

      template <class GridView, class Vector, int k>
      static auto impl(GridView const& gv, Vector const& form, int_t<k>) { return Sharp::impl(gv, form, int_<k>, valid_); }
    };


    template <int dim>
    struct Sharp<dim, dim>
    {
      // sharp a 2-form
      template <class GridView, class Vector>
      static Vector impl(GridView const& gv, Vector const& form, int_t<dim>, bool_t<false>)
      {
        static_assert( dim == GridView::dimension, "" );

        Vector field{form};
        for (auto const& e : elements(gv))
        {
          field[e.index()] /= gv.volume(e);
        }

        return field;
      }


      // sharp a 1-form to a vector-form by averaging over all edges of a cell
      // => cell-vector
      template <class GridView, class V, class WorldVector = typename GridView::GlobalCoordinate>
      static DOFVector<WorldVector> impl(GridView const& gv, DOFVector<V> const& form, int_t<1>, bool_t<true>)
      {
        constexpr int nVertices = dim+1;
        constexpr int nEdges = kombinations(nVertices, 2);

        DOFVector<WorldVector> field{gv, dim, form.name()};

        for (auto const& e : elements(gv))
        {
          SmallMatrix<double, nEdges, dim> mat;
          SmallVector<double, nEdges> rhs;

          std::array<WorldVector, nEdges> edge_vec;
          int j = 0;
          for (auto const& edge : edges(e)) {
            edge_vec[j++] = edge.geometry().vector();
          }

          int i = 0;
          for (auto const& edge : edges(e)) {
            for (int j = 0; j < dim; ++j)
              mat(i,j) = edge_vec[i].dot(edge_vec[j]);
            rhs[i] = form[edge.index()];
            ++i;
          }

          SmallVector<double, dim> lambda = qr_solve(mat,rhs);

          WorldVector vec = lambda[0] * edge_vec[0];
          for (int i = 1; i < dim; ++i)
            vec += lambda[i] * edge_vec[i];

          field[e.index()] = vec;
        }
        return field;
      }

      template <class GridView, class Vector, int k>
      static auto impl(GridView const& gv, Vector const& form, int_t<k>) { return Sharp::impl(gv, form, int_<k>, bool_<(k==1 && dim > 1)>); }

    };


    // ---------------------------------------------------------------------------------------------


    template <int K, int dim>
    struct Interpol
    {
      template <class GridView, class Vector, class Functor, class Projection>
      static void impl(GridView const& gv, Vector& field, Functor f, Projection p)
      {
        static_assert( dim == GridView::dimension, "" );

        for (auto const& e : entities(gv, Dim_<K>))
        {
          field[e.index()] = f( p(gv.center(e)) );
        }
      }
    };


  } // end namespace aux
} // end namespace Dec
