#pragma once

#include "fixmatvec/FixMat.hpp"
#include "fixmatvec/Factorization.hpp"
#include "fixmatvec/FixMatOperations.hpp"
#include "fixmatvec/ReductionOperations.hpp"

#if defined(DEC_HAS_EIGEN)

#include "linear_algebra/eigen.hpp"

#elif defined(DEC_HAS_PETSC)

#include "linear_algebra/petsc.hpp"

#else // default implementation

#include "linear_algebra/default.hpp"

#endif

namespace Dec
{
  template <class T, std::size_t N, std::size_t M>
  using SmallMatrix = FixMat<T,N,M>;

  template <class T, std::size_t N>
  using SmallVector = FixVec<T,N>;

  template <class T, std::size_t N, std::size_t M>
  void set_to_zero(FixMat<T,N,M>& mat) { mat = zeros<T,N,M>(); }

} // end namespace Dec
