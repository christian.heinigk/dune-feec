#pragma once

#include <array>
#include <vector>

#include <dune/grid/common/gridenums.hh>

#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/common/StaticLoops.hpp>

#include <dune/dec/parallel/ParallelDofMapper.hpp>

namespace Dec
{
  // storage for various data per entity. TODO: maybe activate/deactive some data with flags
  template <int dim>
  class EntityData
  {
  public:

    template <class BaseGridView>
    EntityData(BaseGridView const& gv)
      : mapper_(constructTuple<ParallelDofMapper>(gv))
    {
      init(gv);
    }

    /// Returns the partitionType of the entity `e`
    template <class Entity,
      REQUIRES(Entity::codimension <= dim) >
    Dune::PartitionType partitionType(Entity const& e) const
    {
      return partitionType_[Entity::codimension][e.index()];
    }

    /// Return the partitionType of the entity of codim `cd` with index `i`
    template <int cd>
    Dune::PartitionType partitionType(std::size_t i) const
    {
      return partitionType_[cd][i];
    }


    /// Returns whether entity `e` is owned by the current rank or not
    template <class Entity,
      REQUIRES(Entity::codimension <= dim) >
    bool owns(Entity const& e) const
    {
      return std::get<Entity::codimension>(mapper_).owns(e);
    }

    /// Returns whether entity with codim `cd` and index `i` is owned by the current rank or not
    template <int cd>
    bool owns(std::size_t i) const
    {
      return std::get<cd>(mapper_).owns(i);
    }

    std::size_t memory() const
    {
      std::size_t mem = 0;

      for (auto const& p : partitionType_)
        mem += p.capacity()*sizeof(Dune::PartitionType) + sizeof(p);

      forEach(range_<0,dim+1>, [&](auto const cd) {
        mem += std::get<cd>(mapper_).memory();
      });

      return mem;
    }


  private:

    template <class BaseGridView>
    void init(BaseGridView const& gv);


  private:

    std::array<std::vector<Dune::PartitionType>, dim+1> partitionType_;
    ParallelDofMapper mapper_;
  };



  template <int dim>
    template <class BaseGridView>
  void EntityData<dim>::init(BaseGridView const& gv)
  {
    for (int cd = 0; cd <= dim; ++cd)
      partitionType_[cd].resize(gv.size(cd));

    auto const& indexSet = gv.indexSet();
    for (auto const& e : elements(gv)) {
      partitionType_[0][indexSet.index(e)] = e.partitionType();
      forEach(range_<1,dim+1>, [&](auto const cd) {
        for (unsigned int i = 0; i < e.subEntities(cd); ++i)
          partitionType_[cd][indexSet.subIndex(e,i,cd)] = e.template subEntity<cd>(i).partitionType();
      });
    }
  }

} // end namespace Dec
