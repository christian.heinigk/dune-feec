#pragma once

#include "FlatSharp.hpp"
#include "halfedgegrid/HalfEdge.hpp"

namespace Dec
{
  template <class GridView>
  class MusicalIsomorphism
  {
    using size_type = HalfEdge::IndexType;

  public:
    MusicalIsomorphism(GridView const& gv)
      : gv_(gv)
    {}

    /// Returns a flatted DiscreteForm, \see aux::Flat.
    /**
     * \tparam From dimension of the input discrete field
     * \tparam To dimension of the output discrete form [=From]
     **/
    template <int From, int To = From, class Vector>
    auto flat(Vector const& vec, int_t<From> = {}, int_t<To> = {}) const
    {
//       static_assert( std::is_same<Type, tag::field>::value, "Can only flat DiscreteFields!" );
      Expects( size_type(vec.size()) == gv_.size(GridView::dimension - From) );
      return aux::Flat<To, GridView::dimension>::impl(gv_, vec, int_<From>);
    }

    /// Returns a sharped DiscreteField, \see aux::Sharp.
    /**
     * \tparam From dimension of the input discrete form
     * \tparam To dimension of the output discrete field [=From]
     **/
    template <int From, int To = From, class Vector>
    auto sharp(Vector const& vec, int_t<From> = {}, int_t<To> = {}) const
    {
//       static_assert( std::is_same<Type, tag::form>::value, "Can only sharp DiscreteForms!" );
      Expects( size_type(vec.size()) == gv_.size(GridView::dimension - From) );
      return aux::Sharp<To, GridView::dimension>::impl(gv_, vec, int_<From>);
    }

    /// Evaluate a functor at the centers of the entities of dimension K
    template <class Vector, int To, class Functor>
    void interpol(Vector& vec, int_t<To>, Functor f)
    {
      interpol(vec, int_<To>, f, [](auto const& x) { return x; });
    }

    /// Evaluate a functor at the projected centers of the entities of dimension K
    template <class Vector, int To, class Functor, class Projection>
    void interpol(Vector& vec, int_t<To>, Functor f, Projection p)
    {
      using ValueType = Value_t<Vector>;
      using WorldVector = Dune::FieldVector<float_type, GridView::dimensionworld>;

//       static_assert( std::is_same<Type, tag::field>::value,
//                      "Interpolation only works for DiscreteFields!" );
      static_assert( std::is_convertible<std::result_of_t<Functor(WorldVector)>, ValueType>::value,
                     "Functor result-type can not be assigned to the value-type of this DiscreteForm." );
      static_assert( std::is_convertible<std::result_of_t<Projection(WorldVector)>, WorldVector>::value,
                     "Projection needs to map WorldVector to WorldVector." );

      Expects( size_type(vec.size()) == gv_.size(GridView::dimension - To) );
      aux::Interpol<To, GridView::dimension>::impl(gv_, vec, f, p);
    }

    /// Returns a reference to the underlying grid
    GridView const& gridView() const
    {
      return gv_;
    }

  private:
    GridView gv_;
  };

} // end namespace Dec
