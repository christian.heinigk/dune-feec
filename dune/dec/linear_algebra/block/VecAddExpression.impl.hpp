#pragma once

#include <dune/dec/Dec.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{

/// Represents the expression `x + alpha*y`
template <class T, std::size_t N>
VecAddExpression<T,N>::VecAddExpression(BlockVector<T,N> const& x, VecScaleExpression<T,N> const& yscale)
  : x_(x)
  , y_(yscale.vec_)
  , alpha_(yscale.alpha_)
{}

/// out = x + y
template <class T, std::size_t N>
void VecAddExpression<T,N>::assign_impl(BlockVector<T,N>& out) const
{
  for (std::size_t i = 0; i < N; ++i)
    out[i] = x_[i] + alpha_ * y_[i];
}

/// out += x + y
template <class T, std::size_t N>
void VecAddExpression<T,N>::add_assign_impl(BlockVector<T,N>& out) const
{
  for (std::size_t i = 0; i < N; ++i)
    out[i] += x_[i] + alpha_ * y_[i];
}

/// out += factor * x + factor * y
template <class T, std::size_t N>
void VecAddExpression<T,N>::add_assign_impl(BlockVector<T,N>& out, T factor) const
{
  for (std::size_t i = 0; i < N; ++i)
    out[i] += factor * (x_[i] + alpha_ * y_[i]);
}

} // end namespace Dec
