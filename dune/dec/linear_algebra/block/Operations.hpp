#pragma once

#include "BlockMatrix.hpp"
#include "BlockVector.hpp"

#include "VecAddExpression.hpp"
#include "VecAddExpression.impl.hpp"
#include "VecScaleExpression.hpp"
#include "VecScaleExpression.impl.hpp"

namespace Dec
{
  template <class T, std::size_t N>
  inline BlockVector<T,N>& random(BlockVector<T,N>& vec, T mean = 0, T amplitude = 2)
  {
    for (std::size_t i = 0; i < N; ++i)
      random(vec[i], mean, amplitude);
    return vec;
  }

  template <class T, std::size_t N, std::size_t M>
  BlockVector<T, N> operator*(BlockMatrix<T, N, M> const& mat, BlockVector<T, M> const& vec)
  {
    BlockVector<T, N> result{mat.blockSizes().first};
    for (std::size_t i = 0; i < N; ++i) {
      if (mat(i, 0).size() > 0)
        result[i] = mat(i, 0) * vec[0];
      else
        result[i].setZero();

      for (std::size_t j = 1; j < M; ++j) {
        if (mat(i, j).size() > 0)
          result[i] += mat(i, j) * vec[j];
      }
    }
    return result;
  }

  template <class T, std::size_t N, std::size_t M>
  T residuum(BlockMatrix<T, N, M> const& A, BlockVector<T, M> const& x, BlockVector<T, N> const& b)
  {
    BlockVector<T, N> r(b); r -= A*x;
    return two_norm(r);
  }

} // end namespace Dec
