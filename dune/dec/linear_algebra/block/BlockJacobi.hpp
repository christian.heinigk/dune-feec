#pragma once

#include <dune/common/tuplevector.hh>

#include <dune/dec/common/Compatible.hpp>
#include <dune/dec/linear_algebra/block.hpp>
#include <dune/dec/linear_algebra/interface/Solver.hpp>

namespace Dec
{
  /**
    * \addtogroup linear_solver
    * @{
    **/

  /// \brief Block-Jacobi method, can be used as solver and preconditioner
  /**
   * The (variadic) template parameters `Solver...` must model `LinearSolver`, i.e.
   * must provide the methods
   * - `compute()` (initialization of the solver)
   * - `solve()` (solve with initial solution u=0)
   * - `solveWithGuess()` (solve with provided initial solution)
   **/
  template <class... Solver>
  struct BlockJacobi
      : public SolverBase<BlockJacobi<Solver...>>
  {
    static constexpr int N = sizeof...(Solver);

    using Matrix = BlockMatrix<float_type, N, N>;
    using Vector = BlockVector<float_type, N>;

    /// Constructor, stores references to all solvers
    BlockJacobi(Solver&... solver)
      : solver_(&solver...)
    {}

    /// Calls `compute` on all solvers, with the diagonal matrix \f$ A_{ii}\f$ and stores
    /// pointer to the matrix.
    BlockJacobi& compute(Matrix const& mat)
    {
      forEach(range_<0,N>, [this,&mat](auto const _i)
      {
        this->solver_[_i]->compute(mat(_i,_i));
      });
      mat_ = &mat;

      return *this;
    }

    /// Solve \f$ A_{ii} u_i = b_i\f$
    void solve(Vector const& b, Vector& u) const
    {
      forEach(range_<0,N>, [this,&b,&u](auto const _i)
      {
        this->solver_[_i]->solve(b[_i], u[_i]);
      });
    }

    /// Solve \f$ A_{ii} d = b_i - (A\cdot u)_i,\; u += d\f$
    void solveWithGuess(Vector const& b, Vector& u) const
    {
      assert( mat_ != nullptr && "Call BlockJacobi::compute() first" );

      Vector r(b);
      r -= (*mat_)*u;

      Vector d(u, tag::ressource{});
      solve(r, d);

      u += d;
    }

  private:
    Dune::TupleVector<Solver*...> solver_;
    Matrix const* mat_ = nullptr;

  };

  /// Generator for Block-Jacobi solver, \relates BlockJacobi
  template <class... Solver>
  BlockJacobi<Solver...> makeBlockJacobi(Solver&... solver)
  {
    return {solver...};
  }

  /** @} */

} // end namespace Dec
