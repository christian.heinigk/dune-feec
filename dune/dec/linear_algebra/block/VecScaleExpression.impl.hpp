#pragma once

#include <dune/dec/Dec.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{

/// out = a*y
template <class T, std::size_t N>
void VecScaleExpression<T,N>::assign_impl(BlockVector<T,N>& out) const
{
  for (std::size_t i = 0; i < N; ++i)
    out[i] = alpha_ * vec_[i];
}

/// out += a*y
template <class T, std::size_t N>
void VecScaleExpression<T,N>::add_assign_impl(BlockVector<T,N>& out) const
{
  for (std::size_t i = 0; i < N; ++i)
    out[i] += alpha_ * vec_[i];
}

/// out += factor * a*y
template <class T, std::size_t N>
void VecScaleExpression<T,N>::add_assign_impl(BlockVector<T,N>& out, T factor) const
{
  for (std::size_t i = 0; i < N; ++i)
    out[i] += (factor * alpha_) * vec_[i];
}

} // end namespace Dec
