#pragma once

#include <numeric>
#include <tuple>
#include <vector>

#include <dune/dec/GridConcepts.hpp>
#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/Mapper.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{
  template <class Vector>
  class Splitter
  {
  public:

    Splitter(Vector const& vector, std::vector<std::size_t> const& sizes)
      : vector_(vector)
      , mapper_(sizes)
    {}

    Vector const& vector() { return vector_; }
    BlockMapper const& mapper() { return mapper_; }

  private:

    Vector const& vector_;
    BlockMapper mapper_;
  };

  template <class Vector>
  Splitter<Vector> split(Vector const& vector, std::vector<std::size_t> sizes)
  {
    return {vector, sizes};
  }


  template <class T, std::size_t N>
  class BlockVector
      : public FixVec<DOFVector<T>, N>
  {
    using Super = FixVec<DOFVector<T>, N>;

  public:
    using value_type = T;
    using size_type = typename DOFVector<T>::size_type;

    /// Constructor
    BlockVector(std::vector<std::size_t> sizes)
      : mapper_(sizes)
    {
      Expects( N == sizes.size() );
      for (std::size_t i = 0; i < N; ++i)
        (*this)[i].resize(sizes[i]);
    }

    template <class GV,
      REQUIRES( concepts::GridView<GV> )>
    BlockVector(GV const& gv, std::vector<int> Ks)
    {
      Expects( N == Ks.size() );
      std::vector<std::size_t> sizes(N);
      for (std::size_t i = 0; i < N; ++i) {
        sizes[i] = gv.size(GV::dimension - Ks[i]);
        (*this)[i].resize(sizes[i]);
      }

      mapper_ = BlockMapper(sizes);
    }


    template <class Vector_>
    BlockVector(Splitter<Vector_> const& splitter)
      : mapper_(splitter.mapper())
    {
      auto const& input = splitter.vector();
      for (std::size_t i = 0; i < N; ++i) {
        (*this)[i].resize(mapper_.size(i));
        (*this)[i] = input.segment(mapper_.shift(i),mapper_.size(i));
      }
    }


    BlockVector(BlockVector const& that, tag::ressource)
      : mapper_(that.mapper_)
      , vector_(mapper_.size())
    {
      for (std::size_t i = 0; i < N; ++i)
        (*this)[i].resize(mapper_.size(i));
    }


    BlockVector& operator=(DOFVector<T> const& that)
    {
      Expects( that.size() == size_type(mapper_.size()) );
      for (std::size_t i = 0; i < N; ++i)
        (*this)[i] = that.segment(mapper_.shift(i),mapper_.size(i));

      return *this;
    }

    template <class Vector_>
    BlockVector& operator=(Splitter<Vector_> const& splitter)
    {
      mapper_ = splitter.mapper();
      auto const& input = splitter.vector();
      for (std::size_t i = 0; i < N; ++i) {
        (*this)[i].resize(mapper_.size(i));
        (*this)[i] = input.segment(mapper_.shift(i),mapper_.size(i));
      }

      return *this;
    }

    using Super::operator=;

    /// Sets all vectors to zero
    void setZero()
    {
      for (std::size_t i = 0; i < N; ++i)
        (*this)[i].setZero();
    }

    /// Sets all vectors to zero
    void setConstant(T value)
    {
      for (std::size_t i = 0; i < N; ++i)
        (*this)[i].setConstant(value);
    }

    DOFVector<T>& vector() { return vector_; }
    DOFVector<T> const& vector() const { return vector_; }

    /// Create a contiguous vector from the blocks
    DOFVector<T> const& compress()
    {
      vector_.resize(mapper_.size());
      for (std::size_t i = 0; i < N; ++i)
        vector_.segment(mapper_.shift(i),mapper_.size(i)) = (*this)[i];

      return vector_;
    }

  private:

    BlockMapper mapper_;
    DOFVector<T> vector_; // global vector
  };

} // end namespace Dec
