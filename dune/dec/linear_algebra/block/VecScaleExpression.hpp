#pragma once

#include <dune/dec/Dec.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

#include "VecAddExpression.hpp"

namespace Dec
{
  template <class,std::size_t> class BlockVector;

  /// Represents the expression `alpha*vec`
  template <class T, std::size_t N>
  class VecScaleExpression
      : public LinearAlgebraExpression<VecScaleExpression<T,N>>
  {
  public:
    VecScaleExpression(BlockVector<T,N> const& vec, T alpha)
      : vec_(vec)
      , alpha_(alpha)
    {}

    /// out = alpha * vec
    void assign_impl(BlockVector<T,N>& out) const;

    /// out += alpha * vec
    void add_assign_impl(BlockVector<T,N>& out) const;

    /// out += alpha*factor * vec
    void add_assign_impl(BlockVector<T,N>& out, T factor) const;

  public:
    BlockVector<T,N> const& vec_;
    T alpha_;

    template <class,std::size_t> friend class VecAddExpression;
  };

} // end namespace Dec
