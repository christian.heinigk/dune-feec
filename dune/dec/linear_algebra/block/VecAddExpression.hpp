#pragma once

#include <dune/dec/Dec.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{
  template <class,std::size_t> class BlockVector;
  template <class,std::size_t> class VecScaleExpression;

  /// Represents the expression `x + alpha*y`
  template <class T, std::size_t N>
  class VecAddExpression
      : public LinearAlgebraExpression<VecAddExpression<T,N>>
  {
  public:
    VecAddExpression(BlockVector<T,N> const& x, BlockVector<T,N> const& y, T alpha = 1.0)
      : x_(x)
      , y_(y)
      , alpha_(alpha)
    {}

    VecAddExpression(BlockVector<T,N> const& x, VecScaleExpression<T,N> const& yscale);

    /// out = x + y
    void assign_impl(BlockVector<T,N>& out) const;

    /// out += x + y
    void add_assign_impl(BlockVector<T,N>& out) const;

    /// out += factor * x + factor * y
    void add_assign_impl(BlockVector<T,N>& out, T factor) const;

  public:
    BlockVector<T,N> const& x_;
    BlockVector<T,N> const& y_;
    T alpha_;
  };

} // end namespace Dec
