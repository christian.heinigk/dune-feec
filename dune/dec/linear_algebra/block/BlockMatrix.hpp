#pragma once

#include <numeric>
#include <tuple>
#include <vector>

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/Mapper.hpp>

#include "BlockVector.hpp"

namespace Dec
{
  template <class T, std::size_t N, std::size_t M>
  class BlockMatrix
      : public FixMat<DOFMatrix<T>, N, M>
  {
    using Super = FixMat<DOFMatrix<T>, N, M>;

  public:

    using value_type = T;
    using size_type = std::size_t;
    using Matrix = DOFMatrix<T>;

    /// Constructor
    BlockMatrix(size_type n = N, size_type m = M)
    {
      Expects( n == N && m == M );
    }

    /// Return number of non-zeros
    std::size_t nonZeros() const
    {
      std::size_t nnz = 0;
      for (std::size_t r = 0; r < N; ++r)
        for (std::size_t c = 0; c < M; ++c)
          nnz += (*this)(r, c).nonZeros();
      return nnz;
    }

    /// Sets all matrices to zero
    void setZero()
    {
      for (std::size_t r = 0; r < N; ++r)
        for (std::size_t c = 0; c < M; ++c)
          (*this)(r, c).setZero();
    }

    /// Return the global sparse matrix
    Matrix const& matrix() const { return matrix_; }

    /// Create a sparse-matrix from the blocks
    Matrix const& compress()
    {
      Expects( N == M );

      auto sizes = blockSizes();
      BlockMapper m(sizes.first);

      matrix_.resize(m.size(), m.size());
      matrix_.setZero();
      matrix_.reserve(nonZeros());
      {
        auto ins = matrix_.inserter();

        for (std::size_t r = 0; r < N; ++r) {
          for (std::size_t c = 0; c < M; ++c) {
            auto const& mat = (*this)(r, c);
            if (mat.size() > 0) {
              m.block(r, c);
              for (auto const& x : nonzeros(mat))
                ins(m.row(std::get<0>(x)), m.col(std::get<1>(x))) << std::get<2>(x);
            }
          }
        }
      }

      return matrix_;
    }

    /// Return the row/col block sizes, as pair of vectors
    auto blockSizes() const
    {
      std::vector<std::size_t> row_sizes(N);
      std::vector<std::size_t> col_sizes(M);

      std::size_t rows = 0, cols = 0;
      for (std::size_t r = 0; r < N; ++r) {
        std::size_t s = 0;
        for (std::size_t c = 0; c< M; ++c)
          s = max(s, (*this)(r,c).num_rows());
        rows += s;
        row_sizes[r] = s;
      }

      for (std::size_t c = 0; c < M; ++c) {
        std::size_t s = 0;
        for (std::size_t r = 0; r < N; ++r)
          s = max(s, (*this)(r,c).num_cols());
        cols += s;
        col_sizes[c] = s;
      }

      return std::make_pair(row_sizes, col_sizes);
    }

  private:

    Matrix matrix_; // global sparse matrix

  };

} // end namespace Dec
