#pragma once

#include <cstddef>
#include <string>

#include <dune/dec/common/Common.hpp>

namespace Dec
{
  template <class SizeType>
  class DOFVectorBase
  {
  public:

    using size_type = SizeType;

    DOFVectorBase(size_type size, std::string name)
      : size_(size)
      , name_(name)
    {}

    size_type size() const { return size_; }

    size_type num_rows() const
    {
      return size();
    }

    size_type num_cols() const
    {
      return 1;
    }

    std::string name() const
    {
      return name_;
    }

  protected:
    size_type size_;
    std::string name_;
  };

} // end namespace Dec
