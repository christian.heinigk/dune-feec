#pragma once

namespace Dec
{
  namespace precon
  {
    /**
      * \addtogroup preconditioner
      * @{
      **/

    /// \brief An identity preconditioner, i.e. \f$ A \simeq I \f$. \see \ref Preconditioner.
    class Identity
    {
    public:

      template <class Matrix>
      Identity& compute(Matrix const& /*A*/) { return *this; }

      template <class Vector>
      Vector solve(Vector const& b) const
      {
        return b;
      }

      template <class VectorB, class VectorX>
      void solve(VectorB const& b, VectorX& x) const
      {
        x = b;
      }
    };

    /** @} */

  } // end namespace precon
} // end namespace Dec

