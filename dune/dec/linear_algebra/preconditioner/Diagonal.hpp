#pragma once

#include <vector>

#include <dune/dec/common/Common.hpp>

namespace Dec
{
  namespace precon
  {
    /**
      * \addtogroup preconditioner
      * @{
      **/

    /// \brief An identity preconditioner, i.e. \f$ A \simeq diag(A) \f$. \see \ref Preconditioner.
    class Diagonal
    {
    public:

      template <class Matrix>
      Diagonal& compute(Matrix const& A)
      {
        Expects( A.num_rows() == A.num_cols() );

        diag.resize( A.num_rows() );
        for (std::size_t i = 0; i < diag.size(); ++i)
          diag[i] = A(i,i);

        return *this;
      }

      template <class Vector>
      Vector solve(Vector const& b) const
      {
        Vector x(b, tag::ressource{});
        solve(b, x);
        return x;
      }

      template <class VectorB, class VectorX>
      void solve(VectorB const& b, VectorX& x) const
      {
        Expects( std::size_t(b.size()) == diag.size() );
        x.resize(b.size());
        for (std::size_t i = 0; i < diag.size(); ++i)
          x[i] = b[i] / diag[i];
      }

    private:
      std::vector<float_type> diag;
    };

    /** @} */

  } // end namespace precon
} // end namespace Dec

