#pragma once


/**
  * \defgroup preconditioner Preconditioner
  * \brief Preconditioners are used to accelerate krylov subspace methods.
  *
  * Preconditioners, Solvers and Smoothers are very similar. \see \ref preconditioner, \ref smoother, \ref linear_solver.
  **/

#include "preconditioner/Diagonal.hpp"
#include "preconditioner/Identity.hpp"
#include "smoother/GaussSeidel.hpp"
#include "smoother/Jacobi.hpp"
