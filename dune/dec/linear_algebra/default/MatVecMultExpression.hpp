#pragma once

#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{
  template <class> class DOFMatrix;
  template <class> class DOFVector;

  template <class T>
  class MatVecMultExpression
      : public LinearAlgebraExpression<MatVecMultExpression<T>>
  {
  public:
    MatVecMultExpression(DOFMatrix<T> const& A, DOFVector<T> const& vec)
      : A_(A)
      , vec_(vec)
    {
      assert_msg( A.num_cols() == vec.size(),
        "Incompatible sizes [",A.num_rows()," x ",A.num_cols(),"] * [",vec.size(),"]");
    }

    void assign_impl(DOFVector<T>& out) const;

    void add_assign_impl(DOFVector<T>& out) const;

    void add_assign_impl(DOFVector<T>& out, float_type factor) const;

    std::size_t num_rows_impl() const;

    std::size_t num_cols_impl() const;

  private:
    DOFMatrix<T> const& A_;
    DOFVector<T> const& vec_;
  };

#ifndef DEC_NO_EXTERN_MATVECMULTEXPRESSION
  // explicit instantiation in MatVecMultExpression.cpp
  extern template class MatVecMultExpression<float_type>;
#endif

} // end namespace Dec
