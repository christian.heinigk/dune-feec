#pragma once

#include <string>
#include <valarray>

#include <dune/dec/Dec.hpp>
#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/common/Math.hpp>
#include <dune/dec/linear_algebra/DOFVectorBase.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>
#include <dune/dec/linear_algebra/default/SegmentView.hpp>


namespace Dec
{
  // A vector data-structure to store solution values
  template <class T>
  class DOFVector
      : public DOFVectorBase<std::size_t>
      , public std::valarray<T>
  {
    using Super = std::valarray<T>;

  public:
    using value_type = T;
    using size_type = typename DOFVectorBase::size_type;

    /// Constructor taking the size of the vector
    DOFVector(std::size_t size = 0u, std::string name = "")
      : DOFVectorBase{size, name}
      , Super(size)
    {}

    /// Constructor taking the size and the initial value of the vector
    DOFVector(std::size_t size, value_type value, std::string name = "")
      : DOFVectorBase{size, name}
      , Super(value, size)
    {}

    /// Constructor taking the grid and the dim 'K' of the entities, the value are located on.
    template <class Grid,
      REQUIRES(!std::is_arithmetic<Grid>::value)>
    DOFVector(Grid const& grid, int K, std::string name = "")
      : DOFVector{grid.size(Grid::dimension - K), name}
    {}

    /// Constructor taking the grid and the dim 'K' of the entities, the value are located on.
    template <class Grid,
      REQUIRES(!std::is_arithmetic<Grid>::value)>
    DOFVector(Grid const& grid, int K, value_type value, std::string name = "")
      : DOFVector{grid.size(Grid::dimension - K), value, name}
    {}

    DOFVector(DOFVector const&) = default;
    DOFVector(DOFVector&&) = default;

    /// Copy constructor (copies ressouces only)
    DOFVector(DOFVector const& that, tag::ressource)
      : DOFVector{that.size_, that.name_}
    {}

    template <class M>
    DOFVector(LinearAlgebraExpression<M> const& expr)
      : DOFVector{expr.size()}
    {
      expr.assign(*this);
    }

    DOFVector& operator=(DOFVector const&) = default;
    DOFVector& operator=(DOFVector&&) = default;

    template <class M>
    DOFVector& operator=(LinearAlgebraExpression<M> const& expr)
    {
      expr.assign(*this);
      return *this;
    }
    using Super::operator=;

    template <class M>
    DOFVector& operator+=(LinearAlgebraExpression<M> const& expr)
    {
      expr.add_assign(*this);
      return *this;
    }
    using Super::operator+=;

    template <class M>
    DOFVector& operator-=(LinearAlgebraExpression<M> const& expr)
    {
      expr.add_assign(*this, -1.0);
      return *this;
    }
    using Super::operator-=;

    using DOFVectorBase::size;
    using DOFVectorBase::num_rows;
    using DOFVectorBase::num_cols;

  private:
    Super&       super()       { return static_cast<Super&>(*this); }
    Super const& super() const { return static_cast<Super const&>(*this); }

  public:
    SegmentView<T> segment(std::size_t start, std::size_t len = std::size_t(-1))
    {
      return {*this, start, std::min(start+len, size())};
    }
  };

} // end namespace Dec
