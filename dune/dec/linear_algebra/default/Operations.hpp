#pragma once

#include "DOFVector.hpp"
#include "DOFMatrix.hpp"
#include "MatVecMultExpression.hpp"
#include "MatVecMultExpression.impl.hpp"

namespace Dec
{

  template <class T>
  inline DOFVector<T>& random(DOFVector<T>& vec, T mean = 0, T amplitude = 2)
  {
    for (auto& v : vec)
      random(v, mean, amplitude);
    return vec;
  }

  template <class T>
  inline T unary_dot(DOFVector<T> const& vec)
  {
    T result = 0;
    for (T const& v : vec)
      result += sqr(v);
    return result;
  }

  template <class T>
  inline T two_norm(DOFVector<T> const& vec)
  {
    using std::sqrt;
    return sqrt(unary_dot(vec));
  }

  template <class T>
  inline T dot(DOFVector<T> const& vec1, DOFVector<T> const& vec2)
  {
    T result = 0;
    for (auto it1 = begin(vec1), it2 = begin(vec2); it1 != end(vec1); ++it1, ++it2)
      result += (*it1) * (*it2);
    return result;
  }


  template <class T>
  MatVecMultExpression<T> operator*(DOFMatrix<T> const& A, DOFVector<T> const& x)
  {
    return {A, x};
  }

  template <class T>
  T residuum(DOFMatrix<T> const& A, DOFVector<T> const& x, DOFVector<T> const& b)
  {
    DOFVector<T> r(b); r -= A*x;
    return two_norm(r);
  }


} // end namespace Dec
