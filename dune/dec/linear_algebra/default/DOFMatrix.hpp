#pragma once

#include <map>
#include <vector>
#include <tuple>

#include <dune/dec/Dec.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/Inserter.hpp>

namespace Dec
{
  // forward declaration
  template <class T> class DOFVector;

  /// A sparse matrix implemented in coordinate format.
  template <class T>
  class DOFMatrix
      : public std::vector< std::tuple<std::size_t, std::size_t, T> > // {row, col, value}
  {
    using Self = DOFMatrix;
    using Super = std::vector< std::tuple<std::size_t, std::size_t, T> >;

    template <class> friend class MatVecMultExpression;
    template <class, class...> friend class DOFMatrixInserter;
    template <class> friend class NonZerosRange;

  public:
    using value_type = T;
    using size_type = std::size_t;

    template <class... Properties>
    using Inserter = DOFMatrixInserter<T, Properties...>;

    DOFMatrix(size_type rows = 0u, size_type cols = 0u)
      : rows_(rows)
      , cols_(cols)
      , diagonal_(std::min(rows,cols))
    {}

    DOFMatrix(DOFMatrix const&) = default;
    DOFMatrix(DOFMatrix&&) = default;

    DOFMatrix& operator=(DOFMatrix const&) = default;
    DOFMatrix& operator=(DOFMatrix&&) = default;

    DOFMatrix& operator=(value_type v)
    {
      assert_msg( v == 0, "Set to zero only!");
      this->clear();
      std::fill(diagonal_.begin(), diagonal_.end(), T(0));
      return *this;
    }

    std::size_t size() const
    {
      return rows_*cols_;
    }

    std::size_t num_rows() const
    {
      return rows_;
    }

    std::size_t num_cols() const
    {
      return cols_;
    }

    void resize(size_type rows, size_type cols)
    {
      rows_ = rows;
      cols_ = cols;

      diagonal_.resize(std::min(rows_,cols_));
    }

    value_type operator()(size_type row, size_type col) const
    {
      if (row == col)
        return diagonal_[row];

      value_type value = 0;
      size_type r,c;
      value_type v;
      for (auto& entry : *this) {
        std::tie(r,c,v) = std::move(entry);
        if (row == r && col == c)
          value += v;
      }
      return value;
    }

    template <class... Properties>
    Inserter<Properties...> inserter(size_type slotsize = 20, Types<Properties...> = {})
    {
      return {*this, slotsize};
    }

    void clear_dirichlet_row(std::size_t idx)
    {
      diagonal_[idx] = T(1);
      for (auto& entry : *this) {
        if (std::get<0>(entry) == idx)
          std::get<2>(entry) = T(0);
      }
    }

    friend void print(DOFMatrix const& m)
    {
      msg("diagonal = [");
      for (auto const& d : m.diagonal_)
        msg_(d," ");
      msg("]");
      msg("off-diagonal = [");
      for (auto const& t : static_cast<Super const&>(m))
        msg_("(",std::get<0>(t),",",std::get<1>(t),",",std::get<2>(t),") ");
      msg("]");
    }

  private:
    size_type rows_;
    size_type cols_;

    std::vector<value_type> diagonal_;

    bool initialized = false;
  };


  /// Implementation of inserter for DOFMatrix
  template <class T, class... Properties>
  class DOFMatrixInserter
  {
    using Traits = InserterProperties<Properties...>;
    using value_type = T;
    using size_type = typename DOFMatrix<T>::size_type;

    using Container = std::map< std::pair<size_type, size_type>, value_type >;

    struct ScalarInserter
    {
      struct Proxy
      {
        Proxy& operator<<(value_type const& value)
        {
          insert(data, {r,c}, value, insertion_mode<Traits::operation>{});
          return *this;
        }

        Proxy& operator+=(value_type const& value)
        {
          insert(data, {r,c}, value, insertion_mode<ADD_VALUES>{});
          return *this;
        }

        Proxy& operator=(value_type const& value)
        {
          insert(data, {r,c}, value, insertion_mode<INSERT_VALUES>{});
          return *this;
        }

        static void insert(Container& data_, std::pair<size_type, size_type> rc, value_type const& value, insertion_mode<ADD_VALUES>)
        {
          data_[rc] += value;
        }

        static void insert(Container& data_, std::pair<size_type, size_type> rc, value_type const& value, insertion_mode<INSERT_VALUES>)
        {
          data_[rc] = value;
        }

        Container& data;
        size_type r;
        size_type c;
      };

      Proxy operator[](size_type c)
      {
        return {data, r, c};
      }

      Container& data;
      size_type r;
    };

  public:
    DOFMatrixInserter(DOFMatrix<T>& mat, size_type slotsize)
      : mat_(mat)
    {
      assert_msg( mat.initialized == false || Traits::operation == ADD_VALUES,
        "Insertion mode INSERT_VALUES only possible for empty matrices! Use the default ADD_VALUES instead." );
      mat_.reserve(mat.num_rows() * slotsize);
    }

    ~DOFMatrixInserter()
    {
      for (auto const& e : data) {
        if (e.first.first == e.first.second)
          mat_.diagonal_[e.first.first] += e.second;
        else
          mat_.emplace_back(e.first.first, e.first.second, e.second);
      }
      mat_.initialized = true;
    }

    template <class ElementMatrix>
    DOFMatrixInserter& operator<<(ElementMatrix const& element_matrix)
    {
      for (std::size_t i = 0; i < element_matrix.num_rows(); ++i)
        for (std::size_t j = 0; j < element_matrix.num_cols(); ++j)
          ScalarInserter::Proxy::insert(data, {element_matrix.rows()[i], element_matrix.cols()[j]}, element_matrix.values()[i][j], insertion_mode<Traits::operation>{});
      return *this;
    }

    typename ScalarInserter::Proxy operator()(size_type r, size_type c)
    {
      return {data, r, c};
    }

    ScalarInserter operator[](size_type r)
    {
      return {data, r};
    }

  private:
    DOFMatrix<T>& mat_;

    Container data;
  };

} // end namespace Dec
