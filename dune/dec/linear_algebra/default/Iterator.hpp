#pragma once

#include "DOFMatrix.hpp"

namespace Dec
{
  /// A range wrapper that represents all non-zeros entries of a matrix
  template <class T>
  class NonZerosRange
  {
    using Base = typename DOFMatrix<T>::Super;

    // an iterator over all non-zeros
    struct const_iterator
        : public std::iterator<std::forward_iterator_tag, std::tuple<std::size_t, std::size_t, T> >
    {
      using value_type = std::tuple<std::size_t, std::size_t, T>;

      const_iterator(DOFMatrix<T> const& mat, std::size_t index)
        : mat_(mat)
        , index_(index)
        , it_(static_cast<Base const&>(mat).begin())
      {}

      const_iterator& operator++()    { increment(); return *this; }
      const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; }

      bool operator==(const_iterator const& other) const { return index_ == other.index_; }
      bool operator!=(const_iterator const& other) const { return !(*this == other); }

      value_type operator*() const { return {row(), col(), value()}; }

    private:

      bool diag() const { return index_ < mat_.diagonal_.size(); }

      void increment()
      {
        ++index_;
        if (!diag())
          ++it_;
      }

      std::size_t row()   const { return diag() ? index_ : std::get<0>(*it_); }
      std::size_t col()   const { return diag() ? index_ : std::get<1>(*it_); }
      std::size_t value() const { return diag() ? mat_.diagonal_[index_] : std::get<2>(*it_); }

    private:

      DOFMatrix<T> const& mat_;
      std::size_t index_;
      typename Base::const_iterator it_;

    };


  public:

    /// Constructor, stores a reference to `mat`.
    NonZerosRange(DOFMatrix<T> const& mat)
      : mat_(mat)
    {}

    const_iterator begin() const { return {mat_, 0u}; }
    const_iterator end()   const { return {mat_, mat_.diagonal_.size() + static_cast<Base const&>(mat_).size()}; }


  private:

    DOFMatrix<T> const& mat_;
  };


  /// Generator for the \ref NonZerosRange, \relates DOFMatrix
  template <class T>
  NonZerosRange<T> nonzeros(DOFMatrix<T> const& mat)
  {
    return {mat};
  }


} // end namespace Dec
