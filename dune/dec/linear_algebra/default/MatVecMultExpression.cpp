#define DEC_NO_EXTERN_MATVECMULTEXPRESSION
#include "MatVecMultExpression.hpp"
#include "MatVecMultExpression.impl.hpp"
#undef DEC_NO_EXTERN_MATVECMULTEXPRESSION


namespace Dec
{
  // explicit template instatiation
  template class MatVecMultExpression<float_type>;

} // end namespace Dec
