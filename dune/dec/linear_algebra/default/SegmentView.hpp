#pragma once

#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{
  template <class> class DOFVector;

  template <class T>
  class SegmentView
      : public LinearAlgebraExpression<SegmentView<T>>
  {
  public:

    using size_type = std::size_t;

    SegmentView(DOFVector<T>& vec, size_type start, size_type end)
      : vec_(vec)
      , start_(start)
      , end_(end)
    {}

    void assign_impl(DOFVector<T>& out) const
    {
      assign_to(out, [](auto& lhs, auto const& rhs) { lhs = rhs; });
    }

    void add_assign_impl(DOFVector<T>& out) const
    {
      assign_to(out, [](auto& lhs, auto const& rhs) { lhs += rhs; });
    }

    void add_assign_impl(DOFVector<T>& out, float_type factor) const
    {
      assign_to(out, [factor](auto& lhs, auto const& rhs) { lhs += factor * rhs; });
    }

    std::size_t num_rows_impl() const
    {
      return end_ - start_;
    }

    std::size_t num_cols_impl() const
    {
      return 1;
    }

    SegmentView& operator=(DOFVector<T> const& that)
    {
      assign_from(that, [](auto& lhs, auto const& rhs) { lhs = rhs; });
      return *this;
    }

    SegmentView& operator+=(DOFVector<T> const& that)
    {
      assign_from(that, [](auto& lhs, auto const& rhs) { lhs += rhs; });
      return *this;
    }

    SegmentView& operator-=(DOFVector<T> const& that)
    {
      assign_from(that, [](auto& lhs, auto const& rhs) { lhs -= rhs; });
      return *this;
    }

  private:
    template <class Output, class Op>
    void assign_to(Output& out, Op op) const
    {
      for (size_type i = start_; i < end_; ++i)
        op(out[i], vec_[i - start_]);
    }

    template <class Input, class Op>
    void assign_from(Input const& in, Op op)
    {
      for (size_type i = start_; i < end_; ++i)
        op(vec_[i - start_], in[i - start_]);
    }

  private:
    DOFVector<T>& vec_;
    size_type start_;
    size_type end_;
  };

} // end namespace Dec
