#pragma once

#include <tuple>

#include "DOFVector.hpp"
#include "DOFMatrix.hpp"

namespace Dec {

template <class T>
void MatVecMultExpression<T>::assign_impl(DOFVector<T>& out) const
{
  out = 0;
  add_assign_impl(out);
}

template <class T>
void MatVecMultExpression<T>::add_assign_impl(DOFVector<T>& out) const
{
  for (auto const& triple : A_)
    out[std::get<0>(triple)] += vec_[std::get<1>(triple)] * std::get<2>(triple);
  for (std::size_t i = 0; i < out.size(); ++i)
    out[i] += A_.diagonal_[i] * vec_[i];
}

template <class T>
void MatVecMultExpression<T>::add_assign_impl(DOFVector<T>& out, float_type factor) const
{
  for (auto const& triple : A_)
    out[std::get<0>(triple)] += factor*vec_[std::get<1>(triple)] * std::get<2>(triple);
  for (std::size_t i = 0; i < out.size(); ++i)
    out[i] += factor*A_.diagonal_[i] * vec_[i];
}

template <class T>
std::size_t MatVecMultExpression<T>::num_rows_impl() const
{
  return A_.num_rows();
}

template <class T>
std::size_t MatVecMultExpression<T>::num_cols_impl() const
{
  return 1;
}

} // end namespace Dec
