#define DEC_NO_EXTERN_DOFMATRIX
#include "DOFMatrix.hpp"
#undef DEC_NO_EXTERN_DOFMATRIX


namespace Dec
{
  // explicit template instatiation
  template class DOFMatrix<float_type>;

} // end namespace Dec
