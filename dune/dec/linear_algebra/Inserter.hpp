#pragma once

#ifdef DEC_HAS_PETSC
#include <petscsys.h> // -> InsertMode
#include <petscmat.h> // -> MatAssemblyType
#endif

namespace Dec
{
  /// \brief Inserter that can be used to fill DOFMatrix.
  /** Most of the inserter buffer the values before the matrix
    * is finally created. Matrices provide a getter method to
    * create the inserter.
    * Example:
    * ```
    * DOFMatrix<T> mat(...);
    * {
    *   auto ins = mat.inserter<...>();
    *   ins[i][j] << value;
    *   ins << element_matrix;
    * }
    * ```
    * An inserter must be destroyed to actually write the data.
    **/
  template <class T, class... Properties> class DOFMatrixInserter;

#ifndef DEC_HAS_PETSC
  enum InsertMode { ADD_VALUES, INSERT_VALUES };
  enum MatAssemblyType { MAT_FLUSH_ASSEMBLY, MAT_FINAL_ASSEMBLY };
#endif

  template <InsertMode mode>
  using insertion_mode = std::integral_constant<InsertMode, mode>;

  template <MatAssemblyType type>
  using assembly_type = std::integral_constant<MatAssemblyType, type>;


  /// \brief Propagate properties to the inserter.
  /** Allows to specifiy properties in any order as template parameters
    * when enclosed in integral-constants of the specific type.
    * Examples:
    * ```
    * InserterProperties< insertion_mode<ADD_VALUES>, assembly_type<MAT_FINAL_ASSEMBLY> >
    * InserterProperties< assembly_type<MAT_FLUSH_ASSEMBLY> >
    * ```
    **/
  template <class... Properties> struct InserterProperties;

  template <> struct InserterProperties<>
  {
    static constexpr InsertMode operation = ADD_VALUES;
    static constexpr MatAssemblyType assembly = MAT_FINAL_ASSEMBLY;
  };

  template <InsertMode m, class... Properties>
  struct InserterProperties<insertion_mode<m>, Properties...>
    : public InserterProperties<Properties...>
  {
    static constexpr InsertMode operation = m;
  };

  template <MatAssemblyType t, class... Properties>
  struct InserterProperties<assembly_type<t>, Properties...>
    : public InserterProperties<Properties...>
  {
    static constexpr MatAssemblyType assembly = t;
  };

} // end namespace Dec
