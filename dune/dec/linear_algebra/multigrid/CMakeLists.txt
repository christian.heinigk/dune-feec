install(FILES
  DefaultTransfer.hpp
  GalerkinTransfer.hpp
  Hierarchy.hpp
  MGSolver.hpp
  DESTINATION include/dune/dec/linear_algebra/multigrid/
)
