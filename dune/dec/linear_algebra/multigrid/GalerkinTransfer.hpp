#pragma once

#include "DefaultTransfer.hpp"

namespace Dec
{
  /// \brief Coarse-grid matrices are constructed by restriction of the fine-grid matrix
  /// using the interpolation and its transposed restriction operator.
  template <class Grid>
  class GalerkinTransfer
      : public DefaultTransfer<Grid, tag::adjoint>
  {
    using Super = DefaultTransfer<Grid, tag::adjoint>;

  public:

    GalerkinTransfer(Grid const& grid)
      : Super(grid)
    {}

    /// \brief Restriction of the fine-grid operator to the coarse grid, using the interpolation
    /// and its adjoint restriction operator,  i.e. \f$ A_{coarse} = R \cdot A_{fine} \cdot I,  with R = I^\top \f$.
    template <class LinearOperatorIn, class LinearOperatorOut, class... Args>
    void restrictMatrix(int l, LinearOperatorIn const& in, LinearOperatorOut& out, Args&&...) const
    {
      Super::restrict(l, in.matrix(), out.matrix(), false);
    }
  };

} // end namespace Dec
