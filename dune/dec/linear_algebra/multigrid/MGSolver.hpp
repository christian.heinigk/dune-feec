#pragma once

#include <functional>
#include <vector>

#ifndef DEC_HAS_EIGEN
#error "Eigen backend required"
#endif

#include <Eigen/UmfPackSupport> //  default coarse-space solver

#include "Hierarchy.hpp"

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/linear_algebra/interface/Solver.hpp>

namespace Dec
{
  /// Struct to store parameter for multi-grid methods, \relates MGSolver
  struct MGParameter
  {
    int nu1; // pre-smoothing
    int nu2; // post-smoothing
    int gamma; // gamma-cycles
  };


  /**
    * \addtogroup linear_solver
    * @{
    **/


  /// \brief Implementation of a geometric multi-grid method
  /**
   * \tparam LinearOperator Class with 3 methods: `init()`, `build()` and `matrix()`.
   * \tparam Transfer       Transfer matrices and vectors between levels. Must provide restrict and
   *                        interpol methods for vectors and matrices.
   * \tparam CoarseSpaceSolver The linear solver used on the coarse-space grid.
   *
   * \see Transfer: \ref DefaultTransfer, \ref GalerkinTransfer
   **/
  template <class LinearOperator,
            class Transfer,
            class CoarseSpaceSolver = Eigen::UmfPackLU<DOFMatrix<float_type>::Matrix>>
  class MGSolver
      : public SolverBase<MGSolver<LinearOperator, Transfer, CoarseSpaceSolver>>
  {
    using Super = SolverBase<MGSolver<LinearOperator, Transfer, CoarseSpaceSolver>>;
    
  public:

    /// \brief Constructor, initializes incredients of multi-grid method.
    /**
     * \param smoother Smoothing iteration, a functor `smooth(A, u, b)`
     *
     * \see Smoother: \ref GaussSeidel, \ref Jacobi
     **/
    template <class Smoother>
    MGSolver(Transfer transfer, Smoother&& smoother, MGParameter param)
      : transfer_(transfer)
      , smooth_(std::forward<Smoother>(smoother))
      , param_(param)
    {}

    /// Initialize the hierarchy of linear operators. Must be called before \ref compute and \ref solve.
    template <class... Args>
    void init(int minLevel, int maxLevel, Args&&... args)
    {
      minLevel_ = minLevel;
      maxLevel_ = maxLevel;

      solution_.resize(minLevel_, maxLevel_);
      rhs_.resize(minLevel_, maxLevel_);

      hierarchy_.clear();
      hierarchy_.resize(minLevel_, maxLevel_);
      hierarchy_.init(transfer_, std::forward<Args>(args)...);

      initialized_ = true;
    }

    /// Assemble the linear operator on all levels and provide coarse-grid solver
    MGSolver& compute(DOFMatrix<float_type> const&) { return *this; }

    /// Assemble the linear operator on all levels and provide coarse-grid solver
    template <class... Args>
    MGSolver& compute(LinearOperator& finest, Args&&... args)
    {
      assert_msg( initialized_, "Call MGSolver::init() before!" );
      hierarchy_.build(finest, transfer_, std::forward<Args>(args)...);

      for (int l = minLevel_; l < maxLevel_; ++l) {
        solution_[l].resize(matrix(l).num_cols());
        rhs_[l].resize(matrix(l).num_rows());
      }

      if (!analyzed_) {
        coarseSolver_.analyzePattern(matrix(minLevel()));
        analyzed_ = true;
      }
      coarseSolver_.factorize(matrix(minLevel()));

      return *this;
    }

    /// Solve the linear system with sol=0 as initial value
    void solve(DOFVector<double> const& rhs, DOFVector<double>& sol) const
    {
      sol.setZero();
      solveWithGuess(rhs, sol);
    }

    /// Solve the linear system with `sol` provided as initial value
    void solveWithGuess(DOFVector<double> const& rhs, DOFVector<double>& sol) const
    {
      int l = maxLevel();
      cycle(l, matrix(l), sol, rhs);
    }
    
    using Super::solve;
    using Super::solveWithGuess;

    /// Return the minimal multi-grid level
    int minLevel() const { return minLevel_; }

    /// Return the maximal multi-grid level
    int maxLevel() const { return maxLevel_; }

    /// Return a reference to the matrix on level `l`
    DOFMatrix<float_type> const& matrix(int l) const { return hierarchy_[l].matrix(); }

#ifndef DOXYGEN
    /// Return the linear operator in the level `l`
    LinearOperator const& levelOperator(int l) const { return hierarchy_[l]; }
    LinearOperator&       levelOperator(int l)       { return hierarchy_[l];}

    /// Return the linear operator in the leaf-level
    LinearOperator const& leafOperator() const { return levelOperator(maxLevel()); }
    LinearOperator&       leafOperator()       { return levelOperator(maxLevel()); }

    Hierarchy<LinearOperator>& hierarchy() { return hierarchy_; }
#endif

  private:

    /// A multi-grid cycle starting in level `l`.
    void cycle(int l, DOFMatrix<float_type> const& mat, DOFVector<float_type>& sol, DOFVector<float_type> const& rhs) const
    {
      assert_msg(l > minLevel(), "(coarseLevel < fineLevel-1) not fulfilled");

      // pre-smoothing
      for (int i = 0; i < param_.nu1; ++i)
        smooth_(mat, sol, rhs); // -> sol

      auto d = rhs - mat*sol; // compute defect
      transfer_.restrict(l-1, d, rhs_[l-1]); // restrict the defect

      if (l-1 == minLevel()) {
        // solve on the coarse grid
        solution_[l-1] = coarseSolver_.solve(rhs_[l-1]);
      } else {
        // apply gamma l-grid cycles
        solution_[l-1].setZero();
        for (int g = 0; g < param_.gamma; ++g)
          cycle(l-1, matrix(l-1), solution_[l-1], rhs_[l-1]);
      }

      transfer_.prolongate(l-1, solution_[l-1], sol, true);

      // post-smoothing
      for (int i = 0; i < param_.nu2; ++i)
        smooth_(mat, sol, rhs); // -> sol
    }


  private:
    int minLevel_ = 0;
    int maxLevel_ = 0;

    bool initialized_ = false;

    Hierarchy<LinearOperator> hierarchy_;
    mutable Hierarchy<DOFVector<float_type>> solution_;
    mutable Hierarchy<DOFVector<float_type>> rhs_;

    Transfer transfer_;

    using Smoother = std::function<void(DOFMatrix<float_type> const& /*A*/,
                                        DOFVector<float_type>&       /*u*/,
                                        DOFVector<float_type> const& /*b*/)>;
    Smoother smooth_;

    MGParameter param_;

    CoarseSpaceSolver coarseSolver_;
    bool analyzed_ = false;
  };

  /** @} */

} // end namespace Dec
