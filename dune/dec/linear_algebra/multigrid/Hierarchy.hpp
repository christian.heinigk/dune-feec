#pragma once

#include <cassert>
#include <vector>

namespace Dec
{
  /// \brief A hierarchy of linear operators from min- to max-level.
  /**
   * A hierarchy is a vector of type `T` ranging from index `from` to index `to` (i.e. shifted index)
   * and has two additional methods, i.e. init() and build().
   *
   * The type `T` is typically a \ref DOFVector or \ref DOFMatrix, i.e. a vector-hierarchy or a
   * matrix-hierarchy, respectively.
   *
   * \relates MGSolver
   **/
  template <class T>
  class Hierarchy
      : public std::vector<T>
  {
    using Super = std::vector<T>;


  public:

    Hierarchy() = default;

    /// Constructor, takes index range
    Hierarchy(int from, int to)
      : Super(std::max(0, to - from))
      , from_(from)
      , to_(to)
    {
      assert( to > from );
    }

    /// Constructor, takes index range and initial value
    Hierarchy(int from, int to, T const& value)
      : Super(std::max(0, to - from), value)
      , from_(from)
      , to_(to)
    {
      assert( to > from );
    }

    /// Initialize the entries in the hierarchy, \see \ref DefaultTransfer
    template <class Transfer, class... Args>
    void init(Transfer const& transfer, Args&&... args)
    {
      for (int l = to_-1; l >= from_; --l)
        transfer.init(l, at(l), std::forward<Args>(args)...);
    }

    /// Build the hierarchy by coarsen a finest object, using the functor `coarsen`, \see \ref DefaultTransfer
    template <class Transfer, class... Args>
    void build(T& finest, Transfer const& transfer, Args&&... args)
    {
      setFinest(finest);

      for (int l = to_-1; l >= from_; --l)
        transfer.restrictMatrix(l, at(l+1), at(l), std::forward<Args>(args)...);
    }

    /// \brief Set the linear operator (or element of type `T`) at the finest level. Stores a pointer
    /// to this element.
    void setFinest(T& finest)
    {
      finest_ = &finest;
    }


    /// Changes the number of elements stored, change the index range to [from, to]
    void resize(int from, int to)
    {
      Super::resize(to - from);
      from_ = from;
      to_ = to;
    }

    /// \brief Changes the number of elements stored, change the index range to [from, to],
    /// and initialize all added element with `value`
    void resize(int from, int to, T const& value)
    {
      Super::resize(to - from, value);
      from_ = from;
      to_ = to;
    }

    /// reserves storage (to-from) and update boundaries
    void reserve(int from, int to)
    {
      Super::reserve(to - from);
      from_ = from;
      to_ = to;
    }


    /// Access the i'th element in the hierarchy, where `i` corresponds the relative level w.r.t. `from_`.
    T const& at(int i) const
    {
      assert( from_ <= i && i <= to_ && "Index out of range" );
      assert( (i < to_ || finest_) && "Hierarchy must be `build()`" );
      return i == to_ ? *finest_ : Super::at(i - from_);
    }

    /// Access the i'th element in the hierarchy, where `i` corresponds the relative level w.r.t. `from_`.
    T& at(int i)
    {
      assert( from_ <= i && i <= to_ && "Index out of range" );
      assert( (i < to_ || finest_) && "Hierarchy must be `build()`" );
      return i == to_ ? *finest_ : Super::at(i - from_);
    }

    T const& operator[](int i) const { return at(i); }
    T&       operator[](int i)       { return at(i); }


  private:

    int from_ = 0;
    int to_ = 0;

    T* finest_ = nullptr;
  };

} // end namespace Dec
