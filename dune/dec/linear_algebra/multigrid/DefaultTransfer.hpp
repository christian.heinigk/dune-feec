#pragma once

#include <dune/dec/GridTransfer.hpp>

namespace Dec
{
  /// \brief Transfer vectors and matrices from fine to coarse grid and prolongate vectors
  /// from coarse to fine grid.
  /**
   * \tparam Grid The grid that gives access to the (level-)grid-view and the transfer-operators.
   * \tparam RestrictStrategy Either \ref tag::adjoint, \ref tag::full_weighting, or \ref tag::injection.
   *
   * \see \ref GridTransfer
   **/
  template <class Grid, class RestrictStrategy = tag::adjoint>
  class DefaultTransfer
  {
  public:

    /// Constructor,  stores a reference to `grid`
    DefaultTransfer(Grid const& grid)
      : grid_(grid)
    {}

    /// Initialize the coarse-grid operator
    template <class LinearOperator, class... Args>
    void init(int l, LinearOperator& out, Args&&... args) const
    {
      out.init(grid_.levelGridView(l), std::forward<Args>(args)...);
    }

    /// Prolongation of the coarse input vector to the fine grid
    template <class VectorIn, class VectorOut>
    void prolongate(int l, VectorIn const& in, VectorOut& out, bool add = false) const
    {
      grid_.transfer_[l].prolongate(in, out, add);
    }

    /// Restriction of the fine input vector to the coarse grid, using the restriction
    /// operator based on the strategy `RestrictionStrategy` give as template-parameter,
    /// i.e. tag::adjoint,  tag::full_weighting or tag::injection.
    template <class VectorIn, class VectorOut>
    void restrict(int l, VectorIn const& in, VectorOut& out, bool add = false) const
    {
      grid_.transfer_[l].restrictImpl(in, out, RestrictStrategy{}, add);
    }

    /// Restriction of the fine-grid input operator to the coarse grid, by assembling on
    /// the coarse grid.
    template <class LinearOperatorIn, class LinearOperatorOut, class... Args>
    void restrictMatrix(int l, LinearOperatorIn const& /*in*/, LinearOperatorOut& out, Args&&... args) const
    {
      out.build(grid_.levelGridView(l), std::forward<Args>(args)...);
    }


  protected:

    Grid const& grid_;
  };

} // end namespace Dec
