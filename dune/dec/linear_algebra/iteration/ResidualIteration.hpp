#pragma once

#include <dune/dec/LinearAlgebra.hpp>

#include "BasicIteration.hpp"

namespace Dec
{
  /**
    * \addtogroup iteration
    * @{
    **/
  
  /// \brief Class representing an iteration that watches the residuum of a linear system
  /// and breaks if residuum is below some tolerance.
  template <class Real>
  class ResidualIteration
      : public BasicIteration<Real>
  {
    using Self = ResidualIteration;
    using Super = BasicIteration<Real>;

  public:

    /// Constructor, stores maximal number of iterations and break tolerance
    ResidualIteration(int numIter, Real tol)
      : Super(numIter)
      , tol_(tol)
    {}

    /// Stores relative value for the break test, i.e. if `nrmRhs == 1` the absolute
    /// value of the residuum will be tested.
    void init(Real nrmRhs = 1.0)
    {
      Super::init(nrmRhs);
      nrmRhs_ = nrmRhs;
    }

    /// Calculates the norm of the vecotr `b`, used as relative value for the break
    /// test.
    template <class Vector>
    void init(Vector const& b)
    {
      Super::init(b);
      nrmRhs_ = two_norm(b);
    }

    /// Tests whether number of iterations exceeds maximum,  or residuum greater
    /// than scaled tolerance.
    bool finished(Real resid) const
    {
      resid_ = resid;
      return Super::finished(resid) || resid_ < tol_ * nrmRhs_;
    }

    /// Calculates the residuum of the linear system A*u=b
    template <class Matrix, class VectorU, class VectorB>
    bool finished(Matrix const& A, VectorU const& u, VectorB const& b) const
    {
      return finished( residuum(A, u, b) );
    }

    /// Calculates the residuum as norm of the reisual vector
    template <class Vector>
    bool finished(Vector const& r) const
    {
      return finished( two_norm(r) );
    }

    /// Return the stored tolerance
    Real tol() const { return tol_; }

    /// Return the last calculated absolute residuum
    Real resid() const { return resid_; }

    /// Return the last calculated relative residuum,  i.e. divided by the stored
    /// relative value `nrmRhs`
    Real rel_resid() const { return resid_ / nrmRhs_; }

  protected:

    Real tol_;

    Real nrmRhs_ = 1.0;
    mutable Real resid_ = 0.0;
  };

  /** @} */

} // end namespace Dec
