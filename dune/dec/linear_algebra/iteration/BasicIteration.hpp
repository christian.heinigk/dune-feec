#pragma once

#include <dune/dec/common/ConceptsBase.hpp>

namespace Dec
{
  /**
    * \addtogroup iteration
    * @{
    **/

  /// \brief A basic iteration scheme, returns `finished() == false` while `i < numIter`
  template <class Real>
  class BasicIteration
  {
    using Self = BasicIteration;

  public:

    using real_type = Real;

    /// Constructor, stores maximal number of iterations `numIter`.
    BasicIteration(int numIter)
      : numIter_(numIter)
    {}

    /// Check whether iteration is finished
    template <class... Args>
    bool finished(Args&&...) const
    {
      return i_ >= numIter_;
    }

    /// Reset iteration count
    template <class... Args>
    void init(Args&&...)
    {
      i_ = 0;
    }

    /// (pre-) increment iteration
    Self& operator++()
    {
      ++i_;
      return *this;
    }

    /// (post-) increment iteration
    Self operator++(int)
    {
      Self tmp(*this);
      ++(*this);
      return tmp;
    }

    /// Return iteration count
    int operator*() const { return i_; }

    Real tol() const { return 0; }

  protected:

    int numIter_;
    int i_ = 0;
  };

  /** @} */


  namespace concepts
  {
#ifndef DOXYGEN
    namespace definition
    {
      struct Iteration
      {
        template <class I, class Real = typename I::real_type>
        auto requires_(I&& iter) -> decltype(
          concepts::valid_expr(
            ++iter,
            iter++,
            int( *iter ),
            static_cast<BasicIteration<Real> const&>(iter)
          ));
      };

    } // end namespace definition
#endif

    /** \addtogroup concept
     *  @{
     **/

    /// \brief Argument `I` is an iteration object.
    /**
     * Requirement:
     * - pre-/post-increment operator (go to next iteration)
     * - dereferencing returns integer (number of iteration)
     * - method `init()` (resets iteration to initial state)
     * - method `finished() -> bool` (tests whether to stop the iteration)
     **/
    template <class I>
    constexpr bool Iteration = models<definition::Iteration(I)>;

    /** @} */

  } // end namespace concepts

} // end namespace Dec


