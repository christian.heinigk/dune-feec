#pragma once

#include <functional>

#include <dune/dec/LinearAlgebra.hpp>

#include "BasicIteration.hpp"

namespace Dec
{
  /**
    * \addtogroup iteration
    * @{
    **/

  /// \brief Class representing an iteration that watches the residuum of a 
  /// linear system and breaks if residuum is below some tolerance.
  /// The residuum is calculated by a functor passed to the class.
  /**
   * Requirement:
   * - `IterBase` models `concepts::Iteration`.
   * 
   * \see \ref BasicIteration.
   **/
  template <class IterBase,
    REQUIRES( concepts::Iteration<IterBase> )>
  class GenericIteration
      : public IterBase
  {
    using Super = IterBase;

  public:
    
    using real_type = typename Super::real_type;

    /// Constructor, stores function to calc the residuum and initializes the base-class iteration
    template <class... Args>
    GenericIteration(std::function<real_type(void)> residual, Args&&... args)
      : Super(std::forward<Args>(args)...)
      , residual_(residual)
    {}

    /// Calls the \ref Super::finished() with the evaluation of \ref residual_.
    template <class... Args>
    bool finished(Args&&...) const
    {
      return Super::finished(residual_());
    }
    
  protected:

    std::function<real_type(void)> residual_;
  };

  /** @} */

} // end namespace Dec
