#pragma once

#include <dune/dec/LinearAlgebra.hpp>

namespace Dec
{
  /**
    * \addtogroup linear_solver
    * @{
    **/

  /// \brief Apply a solver repeatedly. \see Iteration: \ref BasicIteration, \ref ResidualIteration
  template <class Solver, class Iteration>
  struct IteratedSolver
  {
    template <class Solver_>
    IteratedSolver(Solver_&& solver, Iteration& iter)
      : solver_{std::forward<Solver_>(solver)}
      , iter_(iter)
    {}

    template <class... Args>
    IteratedSolver& compute(Args&&... args)
    {
      solver_->compute(std::forward<Args>(args)...);
      return *this;
    }

    template <class VectorIn, class VectorOut>
    void solve(VectorIn const& b, VectorOut& u) const
    {
      u.setZero();
      solveWithGuess(b, u);
    }

    template <class VectorIn, class VectorOut>
    void solveWithGuess(VectorIn const& b, VectorOut& u) const
    {
      for (iter_.init(b); !iter_.finished(); ++iter_)
        solver_->solveWithGuess(b, u);
    }

  private:
    SmartRef<Solver> solver_;
    Iteration& iter_;
  };

  /// Generator for iterated solver, \relates IteratedSolver
  template <class Solver, class Iteration>
  IteratedSolver<Decay_t<Solver>, Decay_t<Iteration>>
  iterated(Solver&& solver, Iteration& iter)
  {
    return {std::forward<Solver>(solver), iter};
  }

  /** @} */

} // end namespace Dec
