#pragma once

#include "ResidualIteration.hpp"

namespace Dec
{
  /**
    * \addtogroup iteration
    * @{
    **/

  /// An iteration that prints the actual residuum every few iterations
  template <class Real>
  class CyclicIteration
      : public ResidualIteration<Real>
  {
    using Super = ResidualIteration<Real>;

  public:

    /// Constructor, stores maximal number of iteration `numIter`, the break tolerance `tol`
    /// and an output cycle
    CyclicIteration(int numIter, Real tol, int cycle)
      : Super(numIter, tol)
      , cycle_(cycle)
    {}

    /// Tests whether iteration should be stoped and prints residuum every `cycle`s
    /// iteration.
    template <class... Args>
    bool finished(Args&&... args) const
    {
      bool f = Super::finished(std::forward<Args>(args)...);
      if (Super::i_ % cycle_ == 0)
        msg("residuum(", Super::i_, ") = ", Super::resid());
      return f;
    }

  protected:

    int cycle_;
  };


  /// Specialization of \ref CyclicIteration for cycle=1,  i.e. print every iteration.
  template <class Real>
  class NoisyIteration
      : public CyclicIteration<Real>
  {
    using Super = CyclicIteration<Real>;

  public:

    NoisyIteration(int numIter, Real tol)
      : Super(numIter, tol, 1)
    {}
  };

  /** @} */
  
} // end namespace Dec
