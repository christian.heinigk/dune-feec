#pragma once

/**
  * \defgroup iteration Iteration
  * \brief Repeated and controlled iteration of (linear) solvers
  **/

#include "iteration/BasicIteration.hpp"
#include "iteration/ResidualIteration.hpp"
#include "iteration/GenericIteration.hpp"
#include "iteration/NoisyIteration.hpp"
#include "iteration/IteratedSolver.hpp"
