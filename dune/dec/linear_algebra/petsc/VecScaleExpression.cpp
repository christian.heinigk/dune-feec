#include "VecScaleExpression.hpp"

#include "DOFVector.hpp"

namespace Dec {

VecScaleExpression::VecScaleExpression(aux::DOFVectorImpl const& vec, VecScaleExpression::scalar_type alpha)
  : vec(vec)
  , alpha(alpha)
{}

void VecScaleExpression::assign_impl(aux::DOFVectorImpl& out) const
{
  if (&out == &vec)
    VecScale(out.vec(), alpha);
  else {
    VecZeroEntries(out.vec());
    VecAXPY(out.vec(), alpha, vec.vec());
  }
}

void VecScaleExpression::add_assign_impl(aux::DOFVectorImpl& out) const
{
  if (&out == &vec)
    VecScale(out.vec(), 1 + alpha);
  else
    VecAXPY(out.vec(), alpha, vec.vec());
}

void VecScaleExpression::add_assign_impl(aux::DOFVectorImpl& out, VecScaleExpression::scalar_type factor) const
{
  if (&out == &vec)
    VecScale(out.vec(), 1 + factor*alpha);
  else
    VecAXPY(out.vec(), alpha*factor, vec.vec());
}

} // end namespace Dec
