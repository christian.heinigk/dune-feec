#pragma once

#include <petscsys.h>

#include <dune/dec/Dec.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

#include "VecAddExpression.hpp"
#include "DOFVector.hpp"

namespace Dec
{
  /// Represents the expression `alpha*vec`
  class VecScaleExpression
      : public LinearAlgebraExpression<VecScaleExpression>
  {
  public:

    using scalar_type = typename aux::DOFVectorImpl::value_type;

    VecScaleExpression(aux::DOFVectorImpl const& vec, scalar_type alpha);

    /// out = alpha * vec
    void assign_impl(aux::DOFVectorImpl& out) const;

    /// out += alpha * vec
    void add_assign_impl(aux::DOFVectorImpl& out) const;

    /// out += alpha*factor * vec
    void add_assign_impl(aux::DOFVectorImpl& out, scalar_type factor) const;

    std::size_t num_rows_impl() const
    {
      return vec.size();
    }

    std::size_t num_cols_impl() const
    {
      return 1;
    }

  private:
    aux::DOFVectorImpl const& vec;
    scalar_type alpha;

    friend class VecAddExpression;
  };

} // end namespace Dec
