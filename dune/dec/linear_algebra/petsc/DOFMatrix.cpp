#include "DOFMatrix.hpp"

#include <algorithm>

namespace Dec::aux {

DOFMatrixImpl::DOFMatrixImpl(DOFMatrixImpl::size_type rows, DOFMatrixImpl::size_type cols)
  : rows_(rows)
  , cols_(cols)
{
  init();
}


DOFMatrixImpl::DOFMatrixImpl(DOFMatrixImpl const& that)
  : rows_(that.rows_)
  , cols_(that.cols_)
  , symmetric_(that.symmetric_)
{
  if (that.is_assemled())
    MatDuplicate(that.data_, MAT_COPY_VALUES, &data_);
  else
    init();
}


DOFMatrixImpl::~DOFMatrixImpl()
{
  MatDestroy(&data_);
}


DOFMatrixImpl& DOFMatrixImpl::operator=(DOFMatrixImpl that)
{
  using std::swap;
  swap( data_, that.data_ );
  swap( rows_, that.rows_ );
  swap( cols_, that.cols_ );
  swap( symmetric_, that.symmetric_ );
  return *this;
}


void DOFMatrixImpl::resize(DOFMatrixImpl::size_type rows, DOFMatrixImpl::size_type cols)
{
  if (rows == rows_ && cols == cols_)
    return;

  assert_msg(rows_ == 0u && cols_ == 0u, "Resize non-empty matrix!");

  // Destroy old data to prevent memory leak
  MatDestroy(&data_);

  rows_ = rows;
  cols_ = cols;
  init();
}

void DOFMatrixImpl::clear_dirichlet_row(DOFMatrixImpl::size_type idx)
{
  if (is_assemled())
    MatZeroRows(data_, 1, &idx, 1.0, PETSC_NULL, PETSC_NULL);
  else
  {
    MatSetValue(data_, idx, idx, 1.0, INSERT_VALUES);
    MatAssemblyBegin(data_, MAT_FLUSH_ASSEMBLY);
    MatAssemblyEnd(data_, MAT_FLUSH_ASSEMBLY);
  }
}

void DOFMatrixImpl::init()
{
  MatCreate(PETSC_COMM_WORLD, &data_);
  MatSetSizes(data_, PETSC_DECIDE, PETSC_DECIDE, rows_, cols_);
  MatSetFromOptions(data_);
  MatSetUp(data_);
}

void print(DOFMatrixImpl const& mat)
{
  MatView(mat.data_, PETSC_VIEWER_STDOUT_WORLD);
}

} // end namespace Dec::aux
