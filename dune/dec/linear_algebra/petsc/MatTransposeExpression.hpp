#pragma once

#include <dune/dec/linear_algebra/Expression.hpp>

#include "MatMultExpression.hpp"

namespace Dec
{
  // forward declaration
  namespace aux
  {
    class DOFMatrix;
  } // end namespace aux

  class MatTransposeExpression
      : public LinearAlgebraExpression<MatTransposeExpression>
  {
  public:
    MatTransposeExpression(aux::DOFMatrixImpl const& A);

    void assign_impl(aux::DOFMatrixImpl& out) const;

    void add_assign_impl(aux::DOFMatrixImpl& out, double factor = 1.0) const;

    std::size_t num_rows_impl() const;

    std::size_t num_cols_impl() const;

  private:
    aux::DOFMatrixImpl const& A;

    friend class MatMultExpression;
  };

} // end namespace Dec
