#pragma once

#include <petscsys.h>

#include <dune/dec/Dec.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>
#include "DOFVector.hpp"

namespace Dec
{
  // forward declaration
  class VecScaleExpression;

  /// Represents the expression `x + alpha*y`
  class VecAddExpression
      : public LinearAlgebraExpression<VecAddExpression>
  {
  public:

    using scalar_type = typename aux::DOFVectorImpl::value_type;

    VecAddExpression(aux::DOFVectorImpl const& x, aux::DOFVectorImpl const& y, scalar_type alpha = 1.0);
    VecAddExpression(aux::DOFVectorImpl const& x, VecScaleExpression const& yscale);

    /// out = x + y
    void assign_impl(aux::DOFVectorImpl& out) const;

    /// out += x + y
    void add_assign_impl(aux::DOFVectorImpl& out) const;

    /// out += factor * x + factor * y
    void add_assign_impl(aux::DOFVectorImpl& out, scalar_type factor) const;

    std::size_t num_rows_impl() const
    {
      return x.size();
    }

    std::size_t num_cols_impl() const
    {
      return 1;
    }

  private:
    aux::DOFVectorImpl const& x;
    aux::DOFVectorImpl const& y;
    scalar_type alpha;
  };

} // end namespace Dec
