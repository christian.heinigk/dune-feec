#pragma once

#include <petscmat.h>

#include <dune/dec/common/Common.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/Inserter.hpp>

namespace Dec
{
  // forward declaration
  namespace aux
  {
    class DOFMatrixImpl;
  } // end namespace aux

  template <class /*T*/>
  using DOFMatrix = aux::DOFMatrixImpl;

  namespace aux
  {
    class DOFMatrixImpl
    {
    public:

      using value_type = PetscScalar;
      using size_type = PetscInt;

    public:

      DOFMatrixImpl(size_type rows = 0u, size_type cols = 0u);
      // Copy constructor
      DOFMatrixImpl(DOFMatrixImpl const& that);
      // Move constructor. NOTE: Should this be default? See user-defined destructor!
      DOFMatrixImpl(DOFMatrixImpl&& that) = default;

      ~DOFMatrixImpl();

      // copy and move assignment operator
      DOFMatrixImpl& operator=(DOFMatrixImpl that);

      DOFMatrixImpl& operator=(value_type v)
      {
        assert_msg( v == 0, "Set to zero only!");

        if (is_assemled())
          MatZeroEntries(data_);
        return *this;
      }

      size_type size() const
      {
        return num_rows() * num_cols();
      }

      size_type num_rows() const
      {
        return rows_;
      }

      size_type num_cols() const
      {
        return cols_;
      }

      bool is_symmetric() const
      {
        return symmetric_;
      }

      bool is_assemled() const
      {
        if (data_ == nullptr)
          return false;
        PetscBool result;
        MatAssembled(data_, &result);
        return result;
      }

      // Change the size of the matrix to `rows` x `cols`
      void resize(size_type rows, size_type cols);

      value_type operator()(size_type row, size_type col) const
      {
        error_exit("Not implemented for PETSc matrix.");
        return 0;
      }

      template <class... Properties>
      using Inserter = DOFMatrixInserter<value_type, Properties...>;

      template <class... Properties>
      Inserter<Properties...> inserter(size_type slotsize = 20, Types<Properties...> = {})
      {
        return {data_, slotsize};
      }

      void clear_dirichlet_row(size_type idx);


      Mat& mat() { return data_; }
      Mat const& mat() const { return data_; }

      // output of the matrix
      friend void print(DOFMatrixImpl const& mat);

    private:
      void init();

    private:
      size_type rows_;
      size_type cols_;
      Mat data_ = nullptr;
      bool symmetric_ = false;
    };

  } // end namespace aux

} // end namespace Dec
