#include "MatMultExpression.hpp"

#include "DOFMatrix.hpp"
#include "DOFVector.hpp"
#include "MatTransposeExpression.hpp"

namespace Dec {

MatMultExpression::MatMultExpression(aux::DOFMatrixImpl const& A, aux::DOFVectorImpl const& vec)
  : A(A)
  , vec(vec)
{}

MatMultExpression::MatMultExpression(MatTransposeExpression const& At, aux::DOFVectorImpl const& vec)
  : A(At.A)
  , vec(vec)
  , transposed(true)
{}

void MatMultExpression::assign_impl(aux::DOFVectorImpl& out) const
{
  if (transposed)
    MatMultTranspose(A.mat(), vec.vec(), out.vec());
  else
    MatMult(A.mat(), vec.vec(), out.vec());
}

void MatMultExpression::add_assign_impl(aux::DOFVectorImpl& out) const
{
  assert_msg(&out != &vec, "The vectors vec and out cannot be the same!");
  if (transposed)
    MatMultTransposeAdd(A.mat(), vec.vec(), out.vec(), out.vec());
  else
    MatMultAdd(A.mat(), vec.vec(), out.vec(), out.vec());
}

void MatMultExpression::add_assign_impl(aux::DOFVectorImpl& out, double factor) const
{
  aux::DOFVectorImpl vec2(vec); vec2 *= factor;
  if (transposed)
    MatMultTransposeAdd(A.mat(), vec2.vec(), out.vec(), out.vec());
  else
    MatMultAdd(A.mat(), vec2.vec(), out.vec(), out.vec());
}

std::size_t MatMultExpression::num_rows_impl() const
{
  return A.num_rows();
}

} // end namespace Dec
