#include "VecAddExpression.hpp"
#include "VecScaleExpression.hpp"

#include "DOFVector.hpp"

namespace Dec {

VecAddExpression::VecAddExpression(aux::DOFVectorImpl const& x, aux::DOFVectorImpl const& y, VecAddExpression::scalar_type alpha)
  : x(x)
  , y(y)
  , alpha(alpha)
{}

VecAddExpression::VecAddExpression(aux::DOFVectorImpl const& x, VecScaleExpression const& yscale)
  : x(x)
  , y(yscale.vec)
  , alpha(yscale.alpha)
{}

// z = x + y
void VecAddExpression::assign_impl(aux::DOFVectorImpl& z) const
{
  if (&z == &x && &x == &y) { // z =(1+alpha)*z
    if (alpha != -1)
      VecScale(z.vec(), 1+alpha);
    else
      VecZeroEntries(z.vec());
  } else if (&z == &x)        // z += alpha*y
    VecAXPY(z.vec(), alpha, y.vec());
  else if (&z == &y)        // z += x
    VecAYPX(z.vec(), alpha, x.vec());
  else
    VecAXPBYPCZ(z.vec(), 1, alpha, 0, x.vec(), y.vec());
}

// z += x + y
void VecAddExpression::add_assign_impl(aux::DOFVectorImpl& z) const
{
  if (&z == &x && &x == &y) { // z += (1+alpha)*z => z *= (2+alpha)
    if (alpha != -1)
      VecScale(z.vec(), 2+alpha);
  } else if (&z == &x)        // z = 2*z + alpha*y
    VecAXPBY(z.vec(), alpha, 2, y.vec());
  else if (&z == &y)        // z = (1+alpha)*z + x
    VecAYPX(z.vec(), 1+alpha, x.vec());
  else
    VecAXPBYPCZ(z.vec(), 1, alpha, 1, x.vec(), y.vec());
}

// z += factor*(x + alpha*y)
void VecAddExpression::add_assign_impl(aux::DOFVectorImpl& z, VecAddExpression::scalar_type factor) const
{
  if (&z == &x && &x == &y) { // z = (1 + factor + factor*alpha)*z
    if (alpha != -1)
      VecScale(z.vec(), 1 + factor + factor*alpha);
  } else if (&z == &x)        // z = (factor + 1)*z + factor*alpha*y
    VecAXPBY(z.vec(), factor*alpha, factor + 1, y.vec());
  else if (&z == &y)        // z = (1 + factor*alpha)*z + x
    VecAXPBY(z.vec(), factor, factor*alpha + 1, x.vec());
  else
    VecAXPBYPCZ(z.vec(), factor, factor*alpha, 1, x.vec(), y.vec());
}

} // end namespace Dec
