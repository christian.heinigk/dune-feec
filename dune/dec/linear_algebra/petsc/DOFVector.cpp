#include "DOFVector.hpp"

namespace Dec { namespace aux {

DOFVectorImpl::DOFVectorImpl(DOFVectorImpl::size_type size, std::string name)
  : DOFVectorBase{size, name}
{
  init();
}


DOFVectorImpl::DOFVectorImpl(DOFVectorImpl::size_type size, DOFVectorImpl::value_type value, std::string name)
  : DOFVectorImpl{size, name}
{
  VecSet(data_, value);
}


DOFVectorImpl::DOFVectorImpl(DOFVectorImpl const& that)
  : DOFVectorBase(that)
{
  VecDuplicate(that.data_, &data_);
  VecCopy(that.data_, data_);
}


DOFVectorImpl::DOFVectorImpl(DOFVectorImpl const& that, tag::ressource)
  : DOFVectorBase(that)
{
  VecDuplicate(that.data_, &data_);
}


DOFVectorImpl::~DOFVectorImpl()
{
  VecDestroy(&data_);
}


void DOFVectorImpl::resize(DOFVectorImpl::size_type size)
{
  if (size_ == size)
    return;

  assert_msg(size_ == 0u, "Resize non-empty vector!");

  // Destroy old data to prevent memory leak
  VecDestroy(&data_);

  size_ = size;
  init();
}


DOFVectorImpl& DOFVectorImpl::operator=(DOFVectorImpl that)
{
  using std::swap;
  swap( data_, that.data_ );
  return *this;
}


DOFVectorImpl& DOFVectorImpl::operator=(DOFVectorImpl::value_type const& value)
{
  VecSet(data_, value);
  return *this;
}


DOFVectorImpl& DOFVectorImpl::operator+=(DOFVectorImpl const& that)
{
  VecAXPY(data_, 1.0, that.data_);
  return *this;
}


DOFVectorImpl& DOFVectorImpl::operator-=(DOFVectorImpl const& that)
{
  VecAXPY(data_, -1.0, that.data_);
  return *this;
}


DOFVectorImpl& DOFVectorImpl::operator*=(DOFVectorImpl::value_type const& factor)
{
  VecScale(data_, factor);
  return *this;
}


DOFVectorImpl& DOFVectorImpl::operator/=(DOFVectorImpl::value_type const& factor)
{
  VecScale(data_, 1.0/factor);
  return *this;
}

void DOFVectorImpl::init()
{
  VecCreate(PETSC_COMM_WORLD, &data_);
  VecSetSizes(data_, PETSC_DECIDE, size_);
  VecSetFromOptions(data_);
  VecSetUp(data_);
}

void print(DOFVectorImpl const& vec)
{
  VecView(vec.data_, PETSC_VIEWER_STDOUT_WORLD);
}

} } // end namespace Dec::aux
