#pragma once

#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{
  // forward declaration
  namespace aux
  {
    class DOFVectorImpl;
    class DOFMatrixImpl;

  } // end namespace aux

  class MatTransposeExpression;

  class MatMultExpression
      : public LinearAlgebraExpression<MatMultExpression>
  {
  public:
    MatMultExpression(aux::DOFMatrixImpl const& A, aux::DOFVectorImpl const& vec);
    MatMultExpression(MatTransposeExpression const& At, aux::DOFVectorImpl const& vec);

    void assign_impl(aux::DOFVectorImpl& out) const;

    void add_assign_impl(aux::DOFVectorImpl& out) const;

    void add_assign_impl(aux::DOFVectorImpl& out, double factor) const;

    std::size_t num_rows_impl() const;

    std::size_t num_cols_impl() const
    {
      return 1;
    }

  private:
    aux::DOFMatrixImpl const& A;
    aux::DOFVectorImpl const& vec;
    bool transposed = false;
  };

} // end namespace Dec
