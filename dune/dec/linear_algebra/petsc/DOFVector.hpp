#pragma once

#include <petscvec.h>

#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/DOFVectorBase.hpp>
#include <dune/dec/linear_algebra/Expression.hpp>

namespace Dec
{
  // forward declaration
  namespace aux
  {
    class DOFVectorImpl;
  } // end namespace aux

  template <class /*T*/>
  using DOFVector = aux::DOFVectorImpl;

  namespace aux
  {
    class DOFVectorImpl
        : public DOFVectorBase<PetscInt>
    {
    public:

      using value_type = PetscScalar;
      using size_type = typename DOFVectorBase::size_type;

    public:
      // An iterator extracts the underlying array from the PETSc Vec and
      // works on the pointer directly.
      template <class Vector>
      class Iterator
          : public std::iterator<std::random_access_iterator_tag, PetscScalar>
      {
      public:
        using difference_type = std::ptrdiff_t;

        Iterator(Vector& vec)
          : vec(vec)
        {
          VecGetArray(vec, &ptr);
        }

        Iterator(Vector& vec, difference_type shift)
          : vec(vec)
          , restore(false)
        {
          VecGetArray(vec, &ptr);
          ptr += shift;
        }

        ~Iterator()
        {
          if (restore) {
            VecRestoreArray(vec, &ptr);
          }
        }

        // NOTE: what should be set for 'restore' in case of copy/move?
        Iterator(Iterator const&) = default;
        Iterator(Iterator&& that)
          : vec(that.vec)
          , restore(that.restore)
          , ptr(that.ptr)
        {
          that.restore = false;
        }

        Iterator& operator++()    { ++ptr; return *this; }
        Iterator  operator++(int) { Iterator tmp(*this); operator++(); return tmp; }
        Iterator& operator+=(difference_type shift) { ptr += shift; return *this; }
        Iterator  operator+ (difference_type shift) { Iterator tmp(*this); operator+=(shift); return tmp; }
        Iterator& operator-=(difference_type shift) { ptr -= shift; return *this; }
        Iterator  operator- (difference_type shift) { Iterator tmp(*this); operator-=(shift); return tmp; }
        difference_type operator- (Iterator const& that) { return ptr - that.ptr; }

        bool operator==(const Iterator& rhs) { return ptr == rhs.ptr; }
        bool operator!=(const Iterator& rhs) { return ptr != rhs.ptr; }

        value_type const& operator*() const { return *ptr; }

      protected:
        Vector& vec;
        bool restore = true;
        value_type* ptr = NULL;
      };

      using ConstIterator = Iterator<Vec const>;

      class MutableIterator : public Iterator<Vec>
      {
      public:
        using difference_type = Iterator<Vec>::difference_type;

        MutableIterator(Vec& vec) : Iterator<Vec>(vec) {}
        MutableIterator(Vec& vec, difference_type shift) : Iterator<Vec>(vec, shift) {}

        MutableIterator(MutableIterator const&) = default;
        MutableIterator(MutableIterator&&) = default;

        value_type& operator*() { return *ptr; }
      };

      struct Proxy
      {
        Proxy& operator=(value_type const& value)
        {
          VecSetValue(vec, r, value, INSERT_VALUES);
          return *this;
        }

        Proxy& operator+=(value_type const& value)
        {
          VecSetValue(vec, r, value, ADD_VALUES);
          return *this;
        }

        Proxy& operator-=(value_type const& value)
        {
          VecSetValue(vec, r, -value, ADD_VALUES);
          return *this;
        }

        Proxy& operator*=(value_type const& value)
        {
          value_type old_value;
          VecGetValues(vec, 1, &r, &old_value);
          VecSetValue(vec, r, old_value * value, INSERT_VALUES);
          return *this;
        }

        Proxy& operator/=(value_type const& value)
        {
          value_type old_value;
          VecGetValues(vec, 1, &r, &old_value);
          VecSetValue(vec, r, old_value / value, INSERT_VALUES);
          return *this;
        }

        operator value_type() const
        {
          value_type old_value;
          VecGetValues(vec, 1, &r, &old_value);
          return old_value;
        }

        Vec vec;
        size_type r;
      };

      DOFVectorImpl(size_type size = 0u, std::string name = "");
      DOFVectorImpl(size_type size, value_type value, std::string name = "");

      template <class Grid,
        REQUIRES(!std::is_arithmetic<Grid>::value)>
      DOFVectorImpl(Grid const& grid, int K, std::string name = "")
        : DOFVectorImpl(grid.size(Grid::dimension - K), name)
      {}

      template <class Grid,
        REQUIRES(!std::is_arithmetic<Grid>::value)>
      DOFVectorImpl(Grid const& grid, int K, value_type value, std::string name = "")
        : DOFVectorImpl(grid.size(Grid::dimension - K), value, name)
      {}

      // Copy constructor
      DOFVectorImpl(DOFVectorImpl const& that);

      // Copy constructor (copies ressouces only)
      DOFVectorImpl(DOFVectorImpl const& that, tag::ressource);

      // Move constructor NOTE: is default correct here? See user-defined destructor!
      DOFVectorImpl(DOFVectorImpl&& that) = default;

      template <class M>
      DOFVectorImpl(LinearAlgebraExpression<M> const& expr)
        : DOFVectorImpl(expr.size())
      {
        expr.assign(*this);
      }

      // Destructor. Calls VecDestroy
      ~DOFVectorImpl();

      // Change the size of the vector to `size`
      void resize(size_type size);

      // copy and move assignment operator
      DOFVectorImpl& operator=(DOFVectorImpl that);

      using DOFVectorBase::size;
      using DOFVectorBase::num_rows;
      using DOFVectorBase::num_cols;

      MutableIterator begin()
      {
        return {data_};
      }

      ConstIterator begin() const
      {
        return {data_};
      }

      MutableIterator end()
      {
        size_type n;
        VecGetLocalSize(data_, &n);
        return {data_, MutableIterator::difference_type(n)};
      }

      ConstIterator end() const
      {
        size_type n;
        VecGetLocalSize(data_, &n);
        return {data_, ConstIterator::difference_type(n)};
      }

      Vec& vec() { return data_; }
      Vec const& vec() const { return data_; }

      Proxy operator[](size_type r)
      {
        return {data_, r};
      }

      Proxy operator[](size_type r) const
      {
        return {data_, r};
      }


      DOFVectorImpl& operator=(value_type const& value);
      DOFVectorImpl& operator+=(DOFVectorImpl const& that);
      DOFVectorImpl& operator-=(DOFVectorImpl const& that);
      DOFVectorImpl& operator*=(value_type const& factor);
      DOFVectorImpl& operator/=(value_type const& factor);

      // Returns a view to a subvector, starting at index `start` with length `len`
      int segment(size_type start, size_type len = size_type(-1))
      {
        error_exit("SegmentView not implemented for PETSc matrix.");
        return 0;
      }

      template <class M>
      DOFVectorImpl& operator=(LinearAlgebraExpression<M> const& expr)
      {
        expr.assign(*this);
        return *this;
      }

      template <class M>
      DOFVectorImpl& operator+=(LinearAlgebraExpression<M> const& expr)
      {
        expr.add_assign(*this);
        return *this;
      }

      template <class M>
      DOFVectorImpl& operator-=(LinearAlgebraExpression<M> const& expr)
      {
        expr.add_assign(*this, -1.0);
        return *this;
      }

      // output of the vector
      friend void print(DOFVectorImpl const& vec);

    private:
      void init();

    private:
      Vec data_;
    };

  } // end namespace aux

} // end namespace Dec
