#include "MatTransposeExpression.hpp"

#include "DOFMatrix.hpp"

namespace Dec {

MatTransposeExpression::MatTransposeExpression(aux::DOFMatrixImpl const& A)
  : A(A)
{}

void MatTransposeExpression::assign_impl(aux::DOFMatrixImpl& out) const
{
  //out = A^t
  if (&out == &A)
    MatTranspose(A.mat(), MAT_REUSE_MATRIX, &out.mat());
  else
    MatTranspose(A.mat(), MAT_INITIAL_MATRIX, &out.mat());
}

void MatTransposeExpression::add_assign_impl(aux::DOFMatrixImpl& out, double factor) const
{
  // out += factor * A^t
  Mat At;
  MatCreateTranspose(A.mat(), &At);
  MatAXPY(out.mat(), factor, At, A.is_symmetric() ? SAME_NONZERO_PATTERN : DIFFERENT_NONZERO_PATTERN);
  MatDestroy(&At);
}

std::size_t MatTransposeExpression::num_rows_impl() const
{
  return A.num_cols();
}

std::size_t MatTransposeExpression::num_cols_impl() const
{
  return A.num_rows();
}

} // end namespace Dec
