#pragma once

#include <petscmat.h>

#include "DOFMatrix.hpp"

namespace Dec
{
  template <class T, class... Properties>
  class DOFMatrixInserter
  {
    using Traits = InserterProperties<Properties...>;
    using value_type = T;
    using size_type = typename aux::DOFMatrixImpl::size_type;

    struct ScalarInserter
    {
      struct Proxy
      {
        Proxy& operator<<(value_type const& value)
        {
          // TODO: Test if this can be implemented more efficiently
          // by caching all entries and using `MatSetValues` once.
          MatSetValue(mat, r, c, value, Traits::operation);
          return *this;
        }

        Mat& mat;
        size_type r;
        size_type c;
      };

      Proxy operator[](size_type c)
      {
        return {mat, r, c};
      }

      Mat& mat;
      size_type r;
    };

  public:
    DOFMatrixInserter(Mat& mat, size_type slotsize)
      : mat(mat)
    {
      PetscBool assembled;
      MatAssembled(mat, &assembled);
      if (assembled) {
        MatType type;
        MatGetType(mat, &type);

        size_type rows, cols;
        MatGetSize(mat, &rows, &cols);
        slotsize = std::min(cols, slotsize);
        if (strcmp(type, MATSEQAIJ) == 0) {
          MatSeqAIJSetPreallocation(mat, slotsize, PETSC_NULL);
        } else if (strcmp(type, MATMPIAIJ) == 0) {
          MatMPIAIJSetPreallocation(mat, slotsize, PETSC_NULL, slotsize/2, PETSC_NULL);
        } else {
          error_exit("Matrix type ",type," not supported!");
        }
      }
    }

    DOFMatrixInserter(aux::DOFMatrixImpl& dofmat, size_type slotsize)
      : DOFMatrixInserter{dofmat.mat(), slotsize}
    {}

    // Copy constructor is deleted
    DOFMatrixInserter(DOFMatrixInserter const&) = delete;

    // Move constructor
    DOFMatrixInserter(DOFMatrixInserter&& that)
      : mat(that.mat)
      , assemble(that.assemble)
    {
      that.assemble = false;
    }

    ~DOFMatrixInserter()
    {
      if (assemble) {
        MatAssemblyBegin(mat, Traits::assembly);
        MatAssemblyEnd(mat, Traits::assembly);
      }
    }

    template <class ElementMatrix>
    DOFMatrixInserter& operator<<(ElementMatrix const& element_matrix)
    {
      MatSetValues(mat, element_matrix.num_rows(), element_matrix.rows(),
                        element_matrix.num_cols(), element_matrix.cols(),
                        element_matrix.values(), Traits::operation);
      return *this;
    }

    typename ScalarInserter::Proxy operator()(size_type r, size_type c)
    {
      return {mat, r, c};
    }

    ScalarInserter operator[](size_type r)
    {
      return {mat, r};
    }

  private:
    Mat& mat;
    bool assemble = true;
  };

} // end namespace Dec
