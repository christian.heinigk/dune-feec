#pragma once

#include "DOFMatrix.hpp"
#include "DOFVector.hpp"

#include <dune/dec/linear_algebra/Expression.hpp>

#include "MatMultExpression.hpp"
#include "MatTransposeExpression.hpp"
#include "VecAddExpression.hpp"
#include "VecScaleExpression.hpp"

namespace Dec
{
  inline aux::DOFVectorImpl& random(aux::DOFVectorImpl& vec, aux::DOFVectorImpl::value_type mean = 0, aux::DOFVectorImpl::value_type amplitude = 2)
  {
    PetscRandom rctx;
    PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
    PetscRandomSetInterval(rctx, mean - 0.5*amplitude, mean + 0.5*amplitude);
    PetscRandomSetFromOptions(rctx);
    VecSetRandom(vec.vec(), rctx);
    PetscRandomDestroy(&rctx);
    return vec;
  }

  inline PetscReal two_norm(aux::DOFVectorImpl const& vec)
  {
    PetscReal result;
    VecNorm(vec.vec(), NORM_2, &result);
    return result;
  }

  inline aux::DOFVectorImpl::value_type dot(aux::DOFVectorImpl const& a, aux::DOFVectorImpl const& b)
  {
    aux::DOFVectorImpl::value_type result;
    VecDot(a.vec(), b.vec(), &result);
    return result;
  }

  inline aux::DOFVectorImpl::value_type unary_dot(aux::DOFVectorImpl const& a)
  {
    return dot(a, a);
  }

  // ---------------------------------------------------------------------------

  //< lhs + rhs
  inline VecAddExpression operator+(aux::DOFVectorImpl const& lhs, aux::DOFVectorImpl const& rhs)
  {
    return {lhs, rhs};
  }

  //< lhs - rhs
  inline VecAddExpression operator-(aux::DOFVectorImpl lhs, aux::DOFVectorImpl const& rhs)
  {
    return {lhs, rhs, -1};
  }

  //< lhs * factor
  inline VecScaleExpression operator*(aux::DOFVectorImpl const& v, aux::DOFVectorImpl::value_type const& factor)
  {
    return {v, factor};
  }

  //< factor * lhs
  inline VecScaleExpression operator*(aux::DOFVectorImpl::value_type const& factor, aux::DOFVectorImpl const& v)
  {
    return {v, factor};
  }

  //< lhs / factor
  inline VecScaleExpression operator/(aux::DOFVectorImpl const& v, aux::DOFVectorImpl::value_type const& factor)
  {
    return {v, 1.0/factor};
  }

  // ---------------------------------------------------------------------------

  /// Return a proxy for the matrix-vector multiplication
  inline MatMultExpression operator*(aux::DOFMatrixImpl const& A, aux::DOFVectorImpl const& x)
  {
    return {A, x};
  }

  /// Returns proxy to A^t*x
  inline MatMultExpression operator*(MatTransposeExpression const& At, aux::DOFVectorImpl const& x)
  {
    return {At, x};
  }

  /// Returns proxy to A^t
  inline MatTransposeExpression transpose(aux::DOFMatrixImpl const& A)
  {
    return {A};
  }

  // ---------------------------------------------------------------------------

  inline PetscReal residuum(aux::DOFMatrixImpl const& A, aux::DOFVectorImpl const& x, aux::DOFVectorImpl const& b)
  {
    aux::DOFVectorImpl r(b);
    r -= A*x;
    return two_norm(r);
  }

} // end namespace Dec
