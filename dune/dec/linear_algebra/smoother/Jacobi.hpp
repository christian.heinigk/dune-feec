#pragma once

#ifndef DEC_HAS_EIGEN
#error "Eigen backend required"
#endif

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/interface/Solver.hpp>

namespace Dec
{
  /**
    * \addtogroup smoother
    * @{
    **/

  /// \brief Damped Jacobi smoother. Can be used as a preconditioner. \see \ref Smoother, \ref Preconditioner.
  class Jacobi
      : public SolverBase<Jacobi>
  {
    using Matrix = DOFMatrix<float_type>::Matrix;

  public:

    /// Constructor
    /**
     * \param w relaxation factor
     **/
    Jacobi(float_type w = 1.0)
      : w_(w)
    {}

    /// Apply one smoothing iteration to system \f$ A\cdot u = b \f$
    void operator()(DOFMatrix<float_type> const& A, DOFVector<float_type>& u, DOFVector<float_type> b) const
    {
      auto d = A.diagonal();
      auto D = d.asDiagonal();
      b -= A*u - D*u;

      for (int i = 0; i < u.size(); ++i)
        u[i] = w_ * b[i] / d[i] + (1 - w_)*u[i];
    }

    /// Store a pointer to the matrix \f$ A \f$
    Jacobi& compute(DOFMatrix<double> const& A)
    {
      A_ = &A;
      return *this;
    }

    /// \brief Solve the linear system \f$ A\cdot u = b \f$ with initial condition is zero,
    /// where \f$ A \f$ is set in the \ref compute() method.
    /**
     * Requirement:
     * - \ref compute() must be called in advance
     **/
    void solve(DOFVector<double> const& b, DOFVector<double>& u) const
    {
      assert_msg( A_ != nullptr, "Call Jacobi::compute() first" );

      // u.setZero()
      auto d = A_->diagonal();
      for (int i = 0; i < u.size(); ++i)
        u[i] = w_ * b[i] / d[i];
    }

    /// \brief Solve the linear system \f$ A\cdot u = b \f$ where \f$ A \f$ is set in the \ref compute() method.
    /**
     * Requirement:
     * - \ref compute() must be called in advance
     **/
    void solveWithGuess(DOFVector<double> const& b, DOFVector<double>& u) const
    {
      assert_msg( A_ != nullptr, "Call Jacobi::compute() first" );

      (*this)(*A_, u, b);
    }

  private:
    float_type w_; // relaxation parameter

    DOFMatrix<double> const* A_ = nullptr;
  };

  /** @} */

} // end namespace Dec
