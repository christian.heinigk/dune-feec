#pragma once

#ifndef DEC_HAS_EIGEN
#error "Eigen backend required"
#endif

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/interface/Solver.hpp>

namespace Dec
{
  /**
    * \addtogroup smoother
    * @{
    **/

  /// \brief Gauss-Seidel smoother. Can be used as a preconditioner. \see \ref Smoother, \ref Preconditioner.
  class GaussSeidel
      : public SolverBase<GaussSeidel>
  {
    using Super = SolverBase<GaussSeidel>;
    using Matrix = DOFMatrix<float_type>::Matrix;

  public:

    /// Apply one smoothing iteration to system \f$ A\cdot u = b \f$
    void operator()(DOFMatrix<float_type> const& A, DOFVector<float_type>& u, DOFVector<float_type> const& b) const
    {
      apply(A, u, b, bool_<Matrix::IsRowMajor>);
    }

    /// Store a pointer to the matrix \f$ A \f$
    GaussSeidel& compute(DOFMatrix<double> const& A)
    {
      A_ = &A;
      return *this;
    }

    /// \brief Solve the linear system \f$ A\cdot u = b \f$ where \f$ A \f$ is set in the \ref compute() method.
    /**
     * Requirement:
     * - \ref compute() must be called in advance
     **/
    void solveWithGuess(DOFVector<double> const& b, DOFVector<double>& u) const
    {
      assert_msg( A_ != nullptr, "Call GaussSeidel::compute() first" );

      (*this)(*A_, u, b);
    }
    
    using Super::solve;
    using Super::solveWithGuess;

  private:

    void apply(DOFMatrix<float_type> const& A, DOFVector<float_type>& u, DOFVector<float_type> const& b, bool_t<true>) const
    {
      for (int k = 0; k < A.outerSize(); ++k) {
        auto tmp = b[k];
        float_type diag = 0.0;
        for (typename Matrix::InnerIterator it(A, k); it; ++it) {
          if (it.col() != k)
            tmp -= it.value() * u[it.col()];
          else
            diag = it.value();
        }
        assert( diag != 0.0 );
        u[k] = tmp / diag;
      }
    }

    template <bool B>
    void apply(DOFMatrix<float_type> const& A, DOFVector<float_type>& u, DOFVector<float_type> const& b, bool_t<B>) const
    {
      DOFVector<double> v = b - A.triangularView<Eigen::StrictlyUpper>()*u;
      A.triangularView<Eigen::Lower>().solveInPlace(v);
      u = v;
    }

  private:

    DOFMatrix<double> const* A_ = nullptr;
  };

  /** @} */

} // end namespace Dec
