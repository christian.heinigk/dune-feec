#pragma once

#ifndef DEC_HAS_EIGEN
#error "Eigen backend required"
#endif

#include <dune/dec/LinearAlgebra.hpp>

namespace Dec
{
  /**
    * \addtogroup smoother
    * @{
    **/

  /// Symmetric Successive Overrelaxation (SSOR) smoother
  class SSOR
  {
    using Matrix = DOFMatrix<float_type>::Matrix;

  public:

    /// Constructor
    /**
     * \param w relaxation factor
     **/
    SSOR(float_type w = 1.0)
      : w_(w)
    {}

    /// \brief Apply one smoothing iteration to system \f$ A\cdot u = b \f$, based on a symmetric Successive
    /// Overrelaxation splitting method.
    /**
     * Requirement:
     * - \f$ A \f$ is row-major
     **/
    void operator()(DOFMatrix<float_type> const& A, DOFVector<float_type>& u, DOFVector<float_type> const& b) const
    {
      apply(A, u, b, bool_<Matrix::IsRowMajor>);
    }

  private:

    void apply(DOFMatrix<float_type> const& A, DOFVector<float_type>& u, DOFVector<float_type> const& b, bool_t<true>) const
    {
      // forward sweep
      for (int k = 0; k < A.outerSize(); ++k) {
        auto tmp = b[k];
        float_type diag = 0.0;
        for (typename Matrix::InnerIterator it(A, k); it; ++it) {
          if (it.col() != k)
            tmp -= it.value() * u[it.col()];
          else
            diag = it.value();
        }
        assert( diag != 0.0 );
        u[k] = w_*tmp / diag + (1-w_)*u[k];
      }

      // backward sweep
      for (int k = A.outerSize()-1; k >= 0; --k) {
        auto tmp = b[k];
        float_type diag = 0.0;
        for (typename Matrix::InnerIterator it(A, k); it; ++it) {
          if (it.col() != k)
            tmp -= it.value() * u[it.col()];
          else
            diag = it.value();
        }
        assert( diag != 0.0 );
        u[k] = w_*tmp / diag + (1-w_)*u[k];
      }
    }

    template <bool B>
    void apply(DOFMatrix<float_type> const& /*A*/, DOFVector<float_type>& /*u*/, DOFVector<float_type> const& /*b*/, bool_t<B>) const
    {
      static_assert(B, "Not implemented!");
    }

  private:
    float_type w_; // relaxation parameter
  };

  /** @} */

} // end namespace Dec
