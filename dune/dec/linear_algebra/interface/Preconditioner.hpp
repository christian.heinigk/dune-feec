#pragma once

namespace Dec
{
  /**
    * \addtogroup preconditioner
    * @{
    **/

  /// Interface of a preconditioner
  struct PreconBase
  {
    /// Prepare the preconditioner, based on the matrix \f$ A \f$
    virtual PreconBase& compute(DOFMatrix<float_type> const& A) = 0;

    /// \brief Apply the preconditioner \f$ P \simeq A^{-1} \f$ to the vector \f$ b \f$, i.e.
    /// calculate \f$ u = P\cdot b \f$.
    virtual void solve(DOFVector<float_type> const& b, DOFVector<float_type>& u) const = 0;
  };

  /** @} */

} // end namespace Dec
