#pragma once

namespace Dec
{
  /**
    * \addtogroup linear_solver
    * @{
    **/

  /// Interface of a linear solver
  template <class Impl>
  struct SolverBase
  {
    /// \brief Prepare the solver, based on the matrix \f$ A \f$
    /**
     * \note Should be implemented by derived class.
     **/
    template <class Matrix>
    Impl& compute(Matrix const& /*A*/)
    {
      error_exit("Should be implemented by derived class.");
    }

    /// \brief Solve the linear system \f$ A\cdot u = b \f$ with initial
    /// condition is zero.
    /**
     * Requirement:
     * - \ref compute() must be called in advance
     **/
    template <class VectorB, class VectorU>
    void solve(VectorB const& b, VectorU& u) const
    {
      u.setZero();
      solveWithGuessImpl(b, u);
    }

    /// \brief Solve the linear system \f$ A\cdot u = b \f$ with initial
    /// condition is zero using iteration.
    /**
     * Requirement:
     * - \ref compute() must be called in advance
     **/
    template <class VectorB, class VectorU, class Iteration>
    void solve(VectorB const& b, VectorU& u, Iteration& iter) const
    {
      u.setZero();
      solveWithGuess(b, u, iter);
    }

    /// \brief Solve the linear system \f$ A\cdot u = b \f$
    /**
     * Requirement:
     * - \ref compute() must be called in advance
     *
     * \note Should be implemented by derived class.
     **/
    template <class VectorB, class VectorU>
    void solveWithGuess(VectorB const& /*b*/, VectorU& /*u*/) const
    {
      error_exit("Should be implemented by derived class.");
    }

    /// \brief Solve the linear system \f$ A\cdot u = b \f$ using iteration.
    /**
     * Requirement:
     * - \ref compute() must be called in advance
     **/
    template <class VectorB, class VectorU, class Iteration>
    void solveWithGuess(VectorB const& b, VectorU& u, Iteration& iter) const
    {
      for (iter.init(b); !iter.finished(); ++iter)
        solveWithGuessImpl(b, u);
    }

  private:

    // redirect to implementation
    template <class VectorB, class VectorU>
    void solveWithGuessImpl(VectorB const& b, VectorU& u) const
    {
      static_cast<Impl const&>(*this).solveWithGuess(b, u);
    }
  };

  /** @} */

} // end namespace Dec
