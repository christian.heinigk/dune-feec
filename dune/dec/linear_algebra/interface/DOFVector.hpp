#pragma once

#include <string>

#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/linear_algebra/DOFVectorBase.hpp>

namespace Dec
{
  struct unspecified;

  /// A vector data-structure to store solution values
  template <class T>
  class DOFVector
      : public DOFVectorBase<unspecified>
  {
  public:

    /// type of the element in the matrix
    using value_type = T;

    /// type of the indices used to access the entries, typically \ref std::size_t
    using size_type = typename DOFVectorBase::size_type;


  public:

    /// Constructor taking the size of the vector and an optional name
    DOFVector(size_type size = 0u, std::string name = "");

    /// Constructor taking the size, the initial value of the vector and an optional name
    DOFVector(size_type size, value_type value, std::string name = "");

    /// \brief Constructor taking the grid and the dim 'K' of the entities, the values are located on
    /// and an optional name.
    /**
     * Calls another constructor, with size=grid.size( codim of entities ).
     * See also \ref DecGrid.
     **/
    template <class Grid,
      REQUIRES(!std::is_arithmetic<Grid>::value)>
    DOFVector(Grid const& grid, int K, std::string name = "");

    /// \brief Constructor taking the grid and the dim 'K' of the entities, the values are located on,
    /// an initial value and an optional name.
    /**
     * Calls another constructor, with size=grid.size( codim of entities ).
     * See also \ref DecGrid.
     **/
    template <class Grid,
      REQUIRES(!std::is_arithmetic<Grid>::value)>
    DOFVector(Grid const& grid, int K, value_type value, std::string name = "");

    /// Copy constructor (copies ressouces only, i.e. no values are copied)
    DOFVector(DOFVector const& that, tag::ressource);

    /// Change the size of the vector to `size`
    void resize(size_type size);

  public:

    /// Assigns the values of another DOFVector (or DOFVector expression)
    DOFVector& operator=(DOFVector const& that);

    /// Adds the values of another DOFVector (or DOFVector expression)
    DOFVector& operator+=(DOFVector const& that);

    /// Subtracts the values of another DOFVector (or DOFVector expression)
    DOFVector& operator-=(DOFVector const& that);

    /// Returns a view to a subvector, starting at index `start` with length `len`
    unspecified segment(size_type start, size_type len = size_type(-1));
  };

} // end namespace Dec
