#pragma once

namespace Dec
{
  /**
    * \addtogroup smoother
    * @{
    **/

  /// Interface of a smoother
  struct SmootherBase
  {
    /// Apply one smoothing iteration to the system \f$ A\cdot u = b \f$
    virtual void operator()(DOFMatrix<float_type> const& A, DOFVector<float_type>& u, DOFVector<float_type> const& b) const = 0;
  };

  /** @} */

} // end namespace Dec
