#pragma once

#include "DOFVector.hpp"
#include "DOFMatrix.hpp"

namespace Dec
{
  struct unspecified;

  /// Fills a \ref DOFVector `vec` with random values in the interval [mean-amplitude/2, mean+amplitude/2], \relates DOFVector
  template <class T>
  inline DOFVector<T>& random(DOFVector<T>& vec, T mean = 0, T amplitude = 2);


  /// Returns the two-norm of a \ref DOFVector `vec`, i.e. sqrt( sum_i vec_i^2 ) \relates DOFVector
  template <class T>
  inline T two_norm(DOFVector<T> const& vec);


  /// Returns the Euclidean inner product of two \ref DOFVectors, i.e. vec1^T * vec2, \relates DOFVector
  template <class T>
  inline T dot(DOFVector<T> const& vec1, DOFVector<T> const& vec2);


  /// Returns the square of a \ref DOFVector `vec`, i.e. vec^T*vec, \relates DOFVector
  template <class T>
  inline T unary_dot(DOFVector<T> const& vec)
  {
    return dot(vec, vec);
  }


  /// Returns the matrix-vector product of a \ref DOFMatrix with a \ref DOFVector, \relates DOFMatrix \relates DOFVector
  template <class T>
  unspecified operator*(DOFMatrix<T> const& A, DOFVector<T> const& x);


  /// Returns the residuum of a linear equation, i.e. |A*x - b|_2, in the two-norm, \relates DOFMatrix \relates DOFVector
  template <class T>
  T residuum(DOFMatrix<T> const& A, DOFVector<T> const& x, DOFVector<T> const& b)
  {
    DOFVector<T> r(b); r -= A*x;
    return two_norm(r);
  }

} // end namespace Dec
