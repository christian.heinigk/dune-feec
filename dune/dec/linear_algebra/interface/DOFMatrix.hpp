#pragma once

#include <cstddef>

#include <dune/dec/linear_algebra/Inserter.hpp>

namespace Dec
{
  struct unspecified;

  /// An interface to a sparse matrix implementation
  template <class T>
  class DOFMatrix
  {
  public:

    /// type of the element in the matrix
    using value_type = T;

    /// type of the indices used to access the entries, typically \ref std::size_t
    using size_type = unspecified;


  public:

    /// Constructor, that creates an emtpy matrix of size `rows` x `cols`.
    DOFMatrix(size_type rows = 0u, size_type cols = 0u);

    /// Assign a constant value to the matrix. This is implemented only for value 0
    DOFMatrix& operator=(value_type v);

    /// Returns the size of the matrix, i.e. `rows` x `cols`
    size_type size() const;

    /// Returns the number of rows of the matrix
    size_type num_rows() const;

    /// Returns the number of columns of the matrix.
    size_type num_cols() const;

    /// Change the size of the matrix to `rows` x `cols`
    void resize(size_type rows, size_type cols);

    /// \brief Access the (`row`,`cols`)'th element of the matrix. Should be efficient for
    /// diagonal entries. Returns 0 if no entry is found at the position.
    value_type operator()(size_type row, size_type col) const;

    // TODO: Use this alias template?
    /// type of the inserter
    template <class... Properties>
    using Inserter = DOFMatrixInserter<value_type, Properties...>;

    /// \brief Creates an inserter object, that allows to add new entries to the matrix. See also \ref DOFMatrixInserter.
    /**
     * \param slotsize    Corresponds to an upper bound of nz-entries per row.
     * \tparam Properties specify the \ref insertion_mode and \ref assembly_type
     **/
    template <class... Properties>
    Inserter<Properties...> inserter(size_type slotsize = 20, Types<Properties...> = {});

    /// Replaces the row `idx` with a unit-row, i.e. 1 on the diagonal, 0 otherwise
    void clear_dirichlet_row(size_type idx);
  };


  /// A matrix of values with row/column index indirection
  struct ElementMatrix
  {
    /// type of the values in the matrix
    using value_type = unspecified;

    /// type of the indices
    using size_type = unspecified;

    /// Returns the number of rows
    size_type num_rows() const;

    /// Returns the number of columns
    size_type num_cols() const;

    /// Returns a vector of row indices
    unspecified rows() const;

    /// Returns a vector of column indices
    unspecified cols() const;

    /// Returns a matrix of values, corresponding to the rows and columns
    unspecified values() const;
  };


  /// An inserter for the values at a given position
  template <class InsertionMode>
  struct ValueInserter
  {
    /// type of the values to be inserted
    using value_type = unspecified;

    /// \brief Insert a value at a position in the matrix
    /**
     * Insert a value depending on the InsertionMode into the matrix.
     * If InsertionMode == \ref insertion_mode<ADD_VALUES> then, the value should
     * be added to the matrix at the given position.
     * If InsertionMode == \ref insertion_mode<INSERT_VALUES> then the value should
     * replace all other values at the given position.
     **/
    ValueInserter& operator<<(value_type const& value);

    /// Inserts a value at a position in the matrix, using \ref insertion_mode<ADD_VALUES>
    ValueInserter& operator+=(value_type const& value);

    /// Inserts a value at a position in the matrix, using \ref insertion_mode<INSERT_VALUES>
    ValueInserter& operator=(value_type const& value);
  };


  template <class T, class... Properties>
  struct DOFMatrixInserter
  {
    /// type that gives access to properties, with default properties for all that are not set
    using Traits = InserterProperties<Properties...>;

    /// type of the values to be inserted
    using value_type = T;

    /// type if the indices
    using size_type = typename DOFMatrix<T>::size_type;

    /// \brief The destructor finishes the insertion, i.e. cleanup work.
    /**
     * If Traits::assembly == \ref assembly_type<MAT_FINAL_ASSEMBLY> then all values
     * are written to the matrix data-structure and the matrix is compressed.
     * If Traits::assembly == \ref assembly_type<MAT_FLUSH_ASSEMBLY> then all values
     * are written to the matrix, but more values can be inserted or added.
     **/
    ~DOFMatrixInserter();

    /// \brief Insert an element-metrix, that is, a matrix with minimal interface. See also \ref ElementMatrix.
    /**
     * The argument `element_matrix` must fulfill the concept of a \ref ElementMatrix:
     * matrix.num_rows() -> returns nr. of rows
     * matrix.num_cols() -> return nr. of columns
     * matrix.rows() -> returns a vector of row indices
     * matrix.cols() -> returns a vector of column indices
     * matrix.values() -> returns a matrix of values, corresponding to the rows and columns
     **/
    template <class ElementMatrix>
    DOFMatrixInserter& operator<<(ElementMatrix const& element_matrix);

    /// \brief Returns a proxy (or a reference) to a value inserter. See also \ref ValueInserter.
    /** A data-structor that allows to insert a value at position (`r`,`c`) into
      * the matrix.
      * The value inserter models the concept \ref ValueInserter< insertion_mode<Traits::operation> >.
      **/
    unspecified operator()(size_type r, size_type c);

    /// Returns a proxy (or a reference) for the bracket-operator [r][c].
    unspecified operator[](size_type r);
  };

} // end namespace Dec
