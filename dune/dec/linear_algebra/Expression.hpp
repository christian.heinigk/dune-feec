#pragma once

#include <dune/dec/Dec.hpp>

namespace Dec
{
  template <class Model>
  class LinearAlgebraExpression
  {
    // TODO: Use size_type?

  public:

    template <class Target>
    void assign(Target& that) const
    {
      self().assign_impl(that);
    }

    template <class Target>
    void add_assign(Target& that) const
    {
      self().add_assign_impl(that);
    }

    template <class Target>
    void add_assign(Target& that, float_type factor) const
    {
      self().add_assign_impl(that, factor);
    }


  public:

    std::size_t size() const
    {
      return num_rows() * num_cols();
    }

    std::size_t num_rows() const
    {
      return self().num_rows_impl();
    }

    std::size_t num_cols() const
    {
      return self().num_cols_impl();
    }


  protected:

    Model& self()
    {
      return static_cast<Model&>(*this);
    }

    Model const& self() const
    {
      return static_cast<Model const&>(*this);
    }

    std::size_t num_rows_impl() const
    {
      return 0;
    }

    std::size_t num_cols_impl() const
    {
      return 0;
    }
  };

} // end namespace Dec
