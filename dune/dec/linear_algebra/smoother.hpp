#pragma once


/**
  * \defgroup smoother Smoother
  * \brief Smoothers are used in multigrid methods to damp parts of the residuum.
  *
  * When applying a multigrid method to a linear system, the high-frequency parts of the residuum
  * can be damped and thus reduced by applying a few smoothing iterations.
  *
  * Some default smoothers are provided, e.g. \ref GaussSeidel or damped \ref Jacobi. All smoothers
  * must provide an interface `smoother(A, u, b)` that applys one smoothing iteration to the linear
  * system `A*u=b`.
  **/

#include "smoother/GaussSeidel.hpp"
#include "smoother/Jacobi.hpp"
#include "smoother/SOR.hpp"
#include "smoother/SSOR.hpp"
