#pragma once

#include <cmath>

#include <dune/dec/Dec.hpp>
#include <dune/dec/common/Output.hpp>

#include <dune/dec/linear_algebra/iteration/ResidualIteration.hpp>

namespace Dec
{
  namespace solver
  {
    /**
     * \addtogroup linear_solver
     * @{
     **/

    /// \brief Bi-Conjugate Gradient Stabilized.
    /**
    * Returns the number of solver iterations.
    *
    * \param[in]     A  A linear operator. Must provide matrix-vector product
    * \param[in,out] x  An array like container. Must be multiplicable with matrix. Is used as
    *                   initial value for the iteration.
    * \param[in]     b  An array-like container. Must be compatible with `x`
    * \param[in,out] iter An iteration object, that controls the number of iterations and break
    *                   tolerance.
    **/
    template <class Matrix, class VectorX, class VectorB, class Iteration>
    std::size_t bcgs(Matrix const& A, VectorX& x, VectorB const& b,
                     Iteration& iter)
    {
      using std::abs; using std::sqrt;

      using Vector = VectorX;
      using Value = Value_t<VectorX>;

      Value rho_1 = 0, rho_2 = 0, alpha = 0, beta = 0, gamma = 0, omega = 0;
      Vector p(x, tag::ressource{}), p_1(x, tag::ressource{}), s(x, tag::ressource{}),
            s_1(x, tag::ressource{}), t(x, tag::ressource{}), v(x, tag::ressource{});

      Vector r(b);
      r -= A * x;

      Vector rtilde(r);

      for (iter.init(); !iter.finished(r); ++iter) {
        rho_1 = dot(rtilde, r);
        assert_msg( rho_1 != 0.0, "unexpected orthogonality");

        if (*iter == 0)
          p = r;
        else {
          assert_msg( omega != 0.0, "unexpected orthogonality");
          beta = (rho_1 / rho_2) * (alpha / omega);
          p = r + beta * (p - omega * v);
        }
        p_1 = p; // solve(M, p);
        v = A * p_1;

        gamma = dot(rtilde, v);
        assert_msg(gamma != 0.0, "unexpected orthogonality");

        alpha = rho_1 / gamma;
        s = r - alpha * v;

        if (two_norm(s) < iter.tol()) {
          x += alpha * p_1;
          break;
        }
        s_1 = s; // solve(M, s);
        t = A * s_1;
        omega = dot(t, s) / dot(t, t);

        x += omega * s_1 + alpha * p_1;
        r = s - omega * t;

        rho_2 = rho_1;
      }
      return *iter;
    }

    /// \brief Bi-Conjugate Gradient Stabilized.
    template <class Matrix, class VectorX, class VectorB>
    std::size_t bcgs(Matrix const& A, VectorX& x, VectorB const& b, int numIter = 500, double tol = 1.e-10)
    {
      ResidualIteration<double> iter(numIter, tol);
      return bcgs(A, x, b, iter);
    }

    /// \brief Bi-Conjugate Gradient Stabilized with preconditioning.
    /**
    * Returns the number of solver iterations. \see \ref bcgs for details
    *
    * \param P A (left) preconditioner object. Must provide member function `solve(b,x)` that
    *          applies the proconditioner to the vector `b` and gives the result in vector `x`.
    **/
    template <class Matrix, class VectorX, class VectorB, class Precon, class Iteration>
    std::size_t pbcgs(Matrix const& A, VectorX& x, VectorB const& b, Precon const& P,
                Iteration& iter)
    {
      using std::abs; using std::sqrt;

      using Vector = VectorX;
      using Value = Value_t<VectorX>;

      Value rho_1 = 0, rho_2 = 0, alpha = 0, beta = 0, gamma = 0, omega = 0;
      Vector p(x, tag::ressource{}), p_1(x, tag::ressource{}), s(x, tag::ressource{}),
            s_1(x, tag::ressource{}), t(x, tag::ressource{}), v(x, tag::ressource{});

      Vector r(b);
      r -= A * x;

      Vector rtilde(r);

      for (iter.init(); !iter.finished(r); ++iter) {
        rho_1 = dot(rtilde, r);
        assert_msg( rho_1 != 0.0, "unexpected orthogonality");

        if (*iter == 0)
          p = r;
        else {
          assert_msg( omega != 0.0, "unexpected orthogonality");
          beta = (rho_1 / rho_2) * (alpha / omega);
          p = r + beta * (p - omega * v);
        }
        P.solve(p, p_1);
        v = A * p_1;

        gamma = dot(rtilde, v);
        assert_msg(gamma != 0.0, "unexpected orthogonality");

        alpha = rho_1 / gamma;
        s = r - alpha * v;

        if (two_norm(s) < iter.tol()) {
          x += alpha * p_1;
          break;
        }
        P.solve(s, s_1);
        t = A * s_1;
        omega = dot(t, s) / dot(t, t);

        x += omega * s_1 + alpha * p_1;
        r = s - omega * t;

        rho_2 = rho_1;
      }
      return *iter;
    }

    /// \brief Bi-Conjugate Gradient Stabilized with preconditioning.
    template <class Matrix, class VectorX, class VectorB,  class Precon>
    std::size_t pbcgs(Matrix const& A, VectorX& x, VectorB const& b, Precon const& P, int numIter = 500, double tol = 1.e-10)
    {
      ResidualIteration<double> iter(numIter, tol);
      return pbcgs(A, x, b, P, iter);
    }

    /** @} */

  } // end namespace solver
} // end namespace Dec
