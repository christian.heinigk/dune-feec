#pragma once

namespace Dec
{
  namespace solver
  {
    template <class Matrix, class VectorX, class VectorB, class Iter>
    int richardson(Matrix const& A, VectorX& x, VectorB const& b,
                   Iter& iter, float_type alpha = 1.0)
    {
      VectorB r(b); r -= A*x;
      for (iter.init(two_norm(b)); !iter.finished(two_norm(r)); ++iter) {
        x += alpha * r;
        r = b - A*x;
      }
      return *iter;
    }

    template <class Matrix, class VectorX, class VectorB>
    int richardson(Matrix const& A, VectorX& x, VectorB const& b, float_type alpha = 1.0, int numIter = 500, float_type tol = 1.e-10)
    {
      ResidualIteration<float_type> iter(numIter, tol);
      return richardson(A, x, b, iter, alpha);
    }


    template <class Matrix, class VectorX, class VectorB, class Precon, class Iter>
    int prichardson(Matrix const& A, VectorX& x, VectorB const& b,
                    Precon const& P, Iter& iter, float_type alpha = 1.0)
    {
      VectorB r(b); r -= A*x;
      VectorB r_(b, tag::ressource{});
      for (iter.init(two_norm(b)); !iter.finished(two_norm(r)); ++iter) {
        P.solve(r, r_);
        x += alpha * r_;

        r = b; r-= A*x;
      }
      return *iter;
    }

    template <class Matrix, class VectorX, class VectorB,  class Precon>
    int prichardson(Matrix const& A, VectorX& x, VectorB const& b, Precon const& P, float_type alpha = 1.0, int numIter = 500, float_type tol = 1.e-10)
    {
      ResidualIteration<float_type> iter(numIter, tol);
      return prichardson(A, x, b, P, iter, alpha);
    }

  } // end namespace solver
} // end namespace Dec
