#pragma once

#include <cmath>

#include <dune/dec/Dec.hpp>
#include <dune/dec/linear_algebra/iteration/ResidualIteration.hpp>

namespace Dec
{
  namespace solver
  {
    /**
     * \addtogroup linear_solver
     * @{
     **/

    /// \brief Conjugate Gradient method without preconditioning.
    /**
    * Solve the linear system A*x=b using a Conjugate Gradient method. Returns the number of solver
    * iterations.
    *
    * \param[in]     A  A linear operator. Must provide matrix-vector product
    * \param[in,out] x  An array like container. Must be multiplicable with matrix. Is used as
    *                   initial value for the iteration.
    * \param[in]     b  An array-like container. Must be compatible with `x`
    * \param[in,out] iter An iteration object, that controls the number of iterations and break
    *                   tolerance.
    **/
    template <class Matrix, class VectorX, class VectorB, class Iteration>
    std::size_t cg(Matrix const& A, VectorX& x, VectorB const& b,
                   Iteration& iter)
    {
      using std::abs; using std::sqrt;
      using Vector = VectorX;
      using Value = Value_t<VectorX>;

      Value rho = 0, rho_old = 0, alpha = 0;
      Vector p(x, tag::ressource{}), q(x, tag::ressource{});

      Vector r(b);
      r -= A * x;
      rho = dot(r, r);
      for (iter.init(); !iter.finished(sqrt(rho)); ++iter) {
        if (*iter == 0)
          p = r;
        else {
          p *= (rho / rho_old);
          p += r;
        }

        q = A * p;
        alpha = rho / dot(p, q);

        x += alpha * p;
        rho_old = rho;

        q *= alpha;
        r -= q;
        rho = dot(r, r);
      }

      return *iter;
    }

    /// \brief Conjugate Gradient method without preconditioning.
    /**
    * Returns the number of solver iterations. Creates a \ref ResidualIteration object based on the
    * parameters \ref numIter and \ref tol passed to the function.
    **/
    template <class Matrix, class VectorX, class VectorB>
    std::size_t cg(Matrix const& A, VectorX& x, VectorB const& b, int numIter = 500, float_type tol = 1.e-10)
    {
      ResidualIteration<float_type> iter(numIter, tol);
      return cg(A, x, b, iter);
    }


    /// \brief Conjugate Gradient method with left preconditioning.
    /**
    * Solve the linear system A*x=b using a Conjugate Gradient method. Returns the number of solver
    * iterations. \see \ref cg for details
    *
    * \param P A (left) preconditioner object. Must provide member function `solve(b,x)` that
    *          applies the proconditioner to the vector `b` and gives the result in vector `x`.
    **/
    template <class Matrix, class VectorX, class VectorB, class Precon, class Iteration>
    std::size_t pcg(Matrix const& A, VectorX& x, VectorB const& b, Precon const& P,
                    Iteration& iter)
    {
      using std::abs; using std::sqrt;
      using Vector = VectorX;
      using Value = Value_t<VectorX>;

      Value rho_old = 1;
      Vector p(x, tag::ressource{}), q(x, tag::ressource{}), z(x, tag::ressource{});

      Vector r(b);
      r -= A*x;
      Value rr = dot(r, r);

      for (iter.init(); !iter.finished(sqrt(rr)); ++iter) {
        P.solve(r, z); // => z
        Value rho = dot(r, z);
        if (*iter == 0)
          p = z;
        else {
          p *= (rho / rho_old);
          p += z;
        }

        q = A * p;
        Value alpha = rho / dot(p, q);

        x += alpha * p;
        rho_old = rho;

        q *= alpha;
        r -= q;
        rr = dot(r, r);
      }

      return *iter;
    }

    /// \brief Conjugate Gradient method with left preconditioning.
    /**
    * Returns the number of solver iterations. Creates a \ref ResidualIteration object based on the
    * parameters \ref numIter and \ref tol passed to the function.
    **/
    template <class Matrix, class VectorX, class VectorB,  class Precon>
    std::size_t pcg(Matrix const& A, VectorX& x, VectorB const& b, Precon const& P, int numIter = 500, float_type tol = 1.e-10)
    {
      ResidualIteration<float_type> iter(numIter, tol);
      return pcg(A, x, b, P, iter);
    }

    /** @} */

  } // end namespace solver
} // end namespace Dec
