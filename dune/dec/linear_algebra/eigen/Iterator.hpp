#pragma once

#include "DOFMatrix.hpp"

namespace Dec
{
  /// A range wrapper that represents all non-zeros entries of a matrix
  template <class T>
  class NonZerosRange
  {
    // an iterator over all non-zeros of the matrix
    struct const_iterator
        : public std::iterator<std::forward_iterator_tag, std::tuple<Eigen::Index, Eigen::Index, T> >
    {
      using value_type = std::tuple<Eigen::Index, Eigen::Index, T>;
      using Super = typename DOFMatrix<T>::Super;

      const_iterator(DOFMatrix<T> const& mat, std::size_t index)
        : mat_(mat)
        , index_(index)
        , outer_(0)
        , inner_(mat, 0)
      {}

      const_iterator& operator++()    { increment(); return *this; }
      const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; }

      bool operator==(const_iterator const& other) const { return index_ == other.index_; }
      bool operator!=(const_iterator const& other) const { return !(*this == other); }

      value_type operator*() const { return std::make_tuple(inner_.row(), inner_.col(), inner_.value()); }

    private:
      void increment()
      {
        ++index_;
        ++inner_;
        if (!inner_) {
          ++outer_;
          inner_ = typename Super::InnerIterator(mat_, outer_);
        }
      }

      DOFMatrix<T> const& mat_;
      std::size_t index_;
      Eigen::Index outer_;
      typename Super::InnerIterator inner_;
    };


  public:

    /// Constructor, stores a reference to `mat`.
    NonZerosRange(DOFMatrix<T> const& mat)
      : mat_(mat)
    {}

    const_iterator begin() const { return {mat_, 0}; }
    const_iterator end()   const { return {mat_, std::size_t(mat_.nonZeros())}; }


  private:

    DOFMatrix<T> const& mat_;
  };


  /// Generator for the \ref NonZerosRange, \relates DOFMatrix
  template <class T>
  NonZerosRange<T> nonzeros(DOFMatrix<T> const& mat)
  {
    return {mat};
  }


} // end namespace Dec
