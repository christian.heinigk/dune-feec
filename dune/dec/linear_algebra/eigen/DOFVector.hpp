#pragma once

#include <Eigen/Dense>

#include <dune/dec/Dec.hpp>
#include <dune/dec/GridConcepts.hpp>
#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/common/ScalarTypes.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/DOFVectorBase.hpp>

namespace Dec
{
  template <class T>
  class DOFVector
      : public DOFVectorBase<Eigen::Index>
      , public Eigen::Matrix<T, Eigen::Dynamic, 1>
  {
  public:

    using value_type = T;
    using size_type = typename DOFVectorBase::size_type;

  private:

    using Super = Eigen::Matrix<T, Eigen::Dynamic, 1>;

    template <class Vector>
    class Iterator
        : public std::iterator<std::random_access_iterator_tag, T>
    {
    public:
      Iterator(Vector& vec, size_type pos = 0)
        : vec(vec)
        , pos(pos)
      {}

      Iterator(Iterator const&) = default;
      Iterator(Iterator&&) = default;

      Iterator& operator++()    { ++pos; return *this; }
      Iterator  operator++(int) { Iterator tmp(*this); operator++(); return tmp; }
      Iterator& operator--()    { --pos; return *this; }
      Iterator  operator--(int) { Iterator tmp(*this); operator--(); return tmp; }
      Iterator& operator+=(size_type shift) { pos += shift; return *this; }
      Iterator  operator+ (size_type shift) { Iterator tmp(*this); operator+=(shift); return tmp; }
      Iterator& operator-=(size_type shift) { pos -= shift; return *this; }
      Iterator  operator- (size_type shift) { Iterator tmp(*this); operator-=(shift); return tmp; }
      size_type operator- (Iterator const& that) { return pos - that.pos; }

      bool operator==(const Iterator& rhs) { return pos == rhs.pos; }
      bool operator!=(const Iterator& rhs) { return pos != rhs.pos; }

      T const& operator*() const { return vec[pos]; }

    protected:
      Vector& vec;
      size_type pos;
    };

    using ConstIterator = Iterator<DOFVector const>;

    class MutableIterator : public Iterator<DOFVector>
    {
    public:
      MutableIterator(DOFVector& vec, size_type pos = 0)
        : Iterator<DOFVector>(vec, pos)
      {}

      MutableIterator(MutableIterator const&) = default;
      MutableIterator(MutableIterator&&) = default;

      T& operator*() { return this->vec[this->pos]; }
    };

  public:

    /// Constructor taking the size of the vector
    DOFVector(size_type size = 0u, std::string name = "")
      : DOFVectorBase{size, name}
      , Super(size)
    {
      this->setZero();
    }

    /// Constructor taking the size and the initial value of the vector
    DOFVector(size_type size, value_type value, std::string name = "")
      : DOFVectorBase{size, name}
      , Super(size)
    {
      this->setConstant(value);
    }

    /// Constructor taking the grid and the dim 'K' of the entities, the value are located on.
    template <class GV,
      REQUIRES( concepts::GridView<GV>)>
    DOFVector(GV const& gv, int K, std::string name = "")
      : DOFVector{size_type (gv.size(GV::dimension - K)), name}
    {}

    /// Constructor taking the grid and the dim 'K' of the entities, the value are located on.
    template <class GV,
      REQUIRES( concepts::GridView<GV>)>
    DOFVector(GV const& gv, int K, value_type value, std::string name = "")
      : DOFVector{size_type (gv.size(GV::dimension - K)), value, name}
    {}

    /// Copy/Move constructor
    DOFVector(DOFVector const& that) = default;
    DOFVector(DOFVector&& that) = default;

    /// Copy constructor (copies ressouces only) -> redirects to default copy constructor
    DOFVector(DOFVector const& that, tag::ressource)
      : DOFVectorBase{that.size_, that.name_}
      , Super(that.size_)
    {}

    /// Destructor.
    ~DOFVector() = default;

    // ---------------------------------------------------------------------------------------------

    // This constructor allows you to construct DOFVector from Eigen expressions
    template <class ThatDerived>
    DOFVector(Eigen::MatrixBase<ThatDerived> const& that)
      : DOFVectorBase{size_type(that.size()), "expressions"}
      , Super(that)
    {}

    // This method allows you to assign Eigen expressions to DOFVector
    template <class ThatDerived>
    DOFVector& operator=(Eigen::MatrixBase<ThatDerived> const& that)
    {
      Super::operator=(that);
      return *this;
    }

    // This constructor allows you to construct DOFVector from Eigen expressions
    template <class ThatDerived>
    DOFVector(Eigen::ArrayBase<ThatDerived> const& that)
      : DOFVectorBase{size_type(that.size()), "expressions"}
      , Super(that)
    {}

    // This method allows you to assign Eigen expressions to DOFVector
    template <class ThatDerived>
    DOFVector& operator=(Eigen::ArrayBase<ThatDerived> const& that)
    {
      Super::operator=(that);
      return *this;
    }

    template <class S,
      REQUIRES(concepts::Arithmetic<S>)>
    DOFVector& operator=(S const& scalar)
    {
      Super::setConstant(scalar);
      return *this;
    }

    // ---------------------------------------------------------------------------------------------

    /// Copy/Move assignment operator
    DOFVector& operator=(DOFVector const& that) = default;
    DOFVector& operator=(DOFVector&& that) = default;
    using Super::operator=;

    using DOFVectorBase::size;
    using DOFVectorBase::num_rows;
    using DOFVectorBase::num_cols;

    void resize(size_type s)
    {
      Super::resize(s);
      size_ = s;
    }

    MutableIterator begin()       { return {*this}; }
    ConstIterator   begin() const { return {*this}; }

    MutableIterator end()       { return {*this, size()}; }
    ConstIterator   end() const { return {*this, size()}; }

    /// output of the vector
    friend void print(DOFVector const& vec)
    {
      DEC_MSG(vec);
    }

    std::size_t memory() const
    {
      return size()*sizeof(T);
    }
  };

} // end namespace Dec
