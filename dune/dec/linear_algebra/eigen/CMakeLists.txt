if (NOT DEC_HAS_EIGEN)
  message(FATAL_ERROR "Need Eigen3 to be enabled in order to build this subdirectory!")
endif (NOT DEC_HAS_EIGEN)

dune_library_add_sources(dunefeec SOURCES
  DOFMatrix.cpp
  DOFVector.cpp
)

install(FILES
  DOFMatrix.hpp
  DOFVector.hpp
  Iterator.hpp
  Operations.hpp
  DESTINATION include/dune/dec/linear_algebra/eigen/
)
