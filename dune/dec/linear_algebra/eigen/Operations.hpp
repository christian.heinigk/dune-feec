#pragma once

namespace Dec
{

  template <class T>
  inline DOFVector<T>& random(DOFVector<T>& vec, T mean = 0, T amplitude = 2)
  {
    vec.setRandom();
    vec *= amplitude;
    vec += Eigen::VectorXd::Constant(vec.size(), mean);
    return vec;
  }

  template <class T>
  inline T two_norm(DOFVector<T> const& vec)
  {
    return vec.norm();
  }

  template <class T>
  T unary_dot(DOFVector<T> const& a)
  {
    return a.dot(a);
  }

  template <class T>
  T dot(DOFVector<T> const& a, DOFVector<T> const& b)
  {
    return a.dot(b);
  }

  template <class T>
  T residuum(DOFMatrix<T> const& A, DOFVector<T> const& x, DOFVector<T> const& b)
  {
    return (b - A*x).norm();
  }
  

  /// Symmetric dirichlet boundary conditions
  template <class T>
  void dirichletBC(int idx, DOFMatrix<T>& A, DOFVector<T>& b, T value = 0)
  {
    using Matrix = typename DOFMatrix<T>::Matrix;
    
    if (Matrix::IsRowMajor) {
      for (int k = 0; k < A.outerSize(); ++k) { // rows
        for (typename Matrix::InnerIterator it(A, k); it && it.col() <= idx; ++it) { // nz-cols
          if (it.col() == idx) {
            b[k] -= value * it.value();
            it.valueRef() = T(0);
            break;
          }
        }
      }
      
      for (typename Matrix::InnerIterator it(A, idx); it; ++it) // row(idx)
        it.valueRef() = (it.row() == it.col() ? T(1) : T(0));
        
    } else { // ColMajor
      for (typename Matrix::InnerIterator it(A, idx); it; ++it) { // col(idx)
        if (it.row() != idx) {
          b[it.row()] -= value * it.value();
          it.valueRef() = T(0);
        }
      }
        
      for (int k = 0; k < A.outerSize(); ++k) { // cols
        for (typename Matrix::InnerIterator it(A, k); it && it.row() <= idx; ++it) { // nz-rows
          if (it.row() == idx) {
            it.valueRef() = (it.row() == it.col() ? T(1) : T(0));
            break;
          }
        }
      }
    }
    
    b[idx] = value;
  }

} // end namespace Dec
