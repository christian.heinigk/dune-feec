#pragma once

#include <Eigen/SparseCore>

#include <dune/dec/Dec.hpp>
#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/common/Index.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/linear_algebra/Inserter.hpp>

namespace Dec
{
  /// A system matrix, based in Eigen3 SparseMatrix
  template <class T, int Orientation = Eigen::RowMajor>
  class DOFMatrix
      : public Eigen::SparseMatrix<T, Orientation>
  {
    using Super = Eigen::SparseMatrix<T, Orientation>;

    template <class> friend class NonZerosRange;

  public:
    using value_type = T;
    using size_type = Eigen::Index;
    using Matrix = Super;

    template <class... Properties>
    using Inserter = DOFMatrixInserter<T, Properties...>;

    DOFMatrix(size_type rows = 0u, size_type cols = 0u)
      : Super(rows, cols)
    {}

    /// Copy/Move constructor
    DOFMatrix(DOFMatrix const& that) = default;
    DOFMatrix(DOFMatrix&& that) = default;

    ~DOFMatrix() = default;

    // This constructor allows you to construct DOFVector from Eigen expressions
    template <class ThatDerived>
    DOFMatrix(Eigen::SparseMatrixBase<ThatDerived> const& that)
      : Super(that)
    {}

    // This method allows you to assign Eigen expressions to DOFVector
    template <class ThatDerived>
    DOFMatrix& operator=(Eigen::SparseMatrixBase<ThatDerived> const& that)
    {
      Super::operator=(that);
      return *this;
    }

    /// Copy/Move assignment operator
    DOFMatrix& operator=(DOFMatrix const& that) = default;
    DOFMatrix& operator=(DOFMatrix&& that) = default;

    DOFMatrix& operator=(value_type v)
    {
      assert_msg( v == 0, "Set to zero only!");
      this->setZero();
      return *this;
    }

    std::size_t size() const
    {
      return Super::size();
    }

    std::size_t num_rows() const
    {
      return Super::rows();
    }

    std::size_t num_cols() const
    {
      return Super::cols();
    }

    value_type operator()(size_type row, size_type col) const
    {
      return this->coeff(row, col);
    }

    template <class... Properties>
    Inserter<Properties...> inserter(std::size_t slotsize = 20, Types<Properties...> = {})
    {
      return {*this, slotsize};
    }

    void clear_dirichlet_row(size_type idx, bool set_diagonal = true)
    {
      clear_dirichlet_row(idx, set_diagonal, int_<Orientation>);
    }

    template <int O,
      REQUIRES(O == Eigen::ColMajor)>
    void clear_dirichlet_row(size_type idx, bool set_diagonal, int_t<O>)
    {
      for (int k = 0; k < this->outerSize(); ++k) {
        for (typename Matrix::InnerIterator it(*this, k); it; ++it) {
          if (it.row() == idx)
            it.valueRef() = (it.row() == it.col() && set_diagonal ? T(1) : T(0));
        }
      }
    }

    template <int O,
      REQUIRES(O == Eigen::RowMajor)>
    void clear_dirichlet_row(size_type idx, bool set_diagonal, int_t<O>)
    {
      Eigen::SparseVector<T> unit_vector(this->cols());
      if (set_diagonal)
        unit_vector.coeffRef(idx) = T(1);

      this->row(idx) = unit_vector;
    }

    friend void print(DOFMatrix const& m)
    {
      msg(m);
    }

    std::size_t memory() const
    {
      return num_rows()*sizeof(size_type) + this->nonZeros()*(sizeof(size_type) + sizeof(T));
    }
  };


  /// \brief An inserter for Eigen::SparseMatrix.
  /** Fills a triplet-list first and on destruction, creates the
    * sparse data from this list and compressed the resulting matrix.
    **/
  template <class T, class... Properties>
  class DOFMatrixInserter
  {
    using Traits = InserterProperties<Properties...>;
    using TripletList = std::vector<Eigen::Triplet<T, Eigen::Index>>;
    using value_type = T;
    using size_type = Eigen::Index;

    struct ScalarInserter
    {
      struct Proxy
      {
        Proxy& operator<<(value_type const& value)
        {
          tl.emplace_back(r, c, value);
          return *this;
        }

        TripletList& tl;
        size_type r;
        size_type c;
      };

      Proxy operator[](size_type c)
      {
        return {tl, r, c};
      }

      TripletList& tl;
      size_type r;
    };

  public:
    DOFMatrixInserter(DOFMatrix<T>& mat, std::size_t slotsize)
      : mat(mat)
    {
      tl.reserve(mat.num_rows() * slotsize);
    }

    ~DOFMatrixInserter()
    {
      mat.setFromTriplets(tl.begin(), tl.end());
      if (Traits::assembly == MAT_FINAL_ASSEMBLY) {
        mat.makeCompressed();
        mat.prune(1.e-10);
      }
    }

    template <class ElementMatrix>
    DOFMatrixInserter& operator<<(ElementMatrix const& element_matrix)
    {
      for (std::size_t i = 0; i < element_matrix.num_rows(); ++i)
        for (std::size_t j = 0; j < element_matrix.num_cols(); ++j)
          tl.emplace_back(element_matrix.rows()[i], element_matrix.cols()[j], element_matrix.values()[i][j]);
      return *this;
    }

    typename ScalarInserter::Proxy operator()(size_type r, size_type c)
    {
      return {tl, r, c};
    }

    ScalarInserter operator[](size_type r)
    {
      return {tl, r};
    }

  private:
    DOFMatrix<T>& mat;
    TripletList tl;
  };

} // end namespace Dec
