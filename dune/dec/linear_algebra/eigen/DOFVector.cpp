#define DEC_NO_EXTERN_DOFVECTOR
#include "DOFVector.hpp"
#undef DEC_NO_EXTERN_DOFVECTOR


namespace Dec
{
  // explicit template instatiation
  template class DOFVector<float_type>;

} // end namespace Dec
