#pragma once

#include <cmath>
#include <cstddef>
#include <type_traits>

namespace Dec
{
  /// A vector-like interface that evaluates to the maximum of two vectors.
  template <class F1, class F2>
  struct MaxVector
  {
    using value_type = std::common_type_t<typename F1::value_type, typename F2::value_type>;

    MaxVector(F1 const& f1, F2 const& f2)
      : f1_(f1)
      , f2_(f2)
    {}

    value_type operator[](std::size_t i) const
    {
      return std::max(value_type(f1_[i]), value_type(f2_[i]));
    }

    F1 f1_;
    F2 f2_;
  };

  template <class F1, class F2>
  MaxVector<F1,F2> max_vector(F1 const& f1, F2 const& f2)
  {
    return {f1,f2};
  }

} // end namespace Dec
