#pragma once

#include <tuple>
#include <type_traits>

#include <dune/dec/common/Common.hpp>
#include <dune/dec/common/Get.hpp>
#include <dune/dec/common/Size.hpp>
#include <dune/dec/utility/IntSeq.hpp>

namespace Dec
{
  namespace aux
  {
    template <class Tuple, std::size_t N>
    struct ConstructTuple
    {
      // add arg to repeated constructor argument list
      template <class Arg, class... Args>
      static Tuple make(Arg&& arg, Args&&... args)
      {
        return ConstructTuple<Tuple, N-1>::make(
            std::forward<Arg>(arg), std::forward<Arg>(arg), std::forward<Args>(args)...);
      }
    };

    template <class Tuple>
    struct ConstructTuple<Tuple, 1>
    {
      template <class... Args>
      static Tuple make(Args&&... args)
      {
        static_assert(Size<Tuple> == sizeof...(args),
            "Nr. of argument != tuple-size");
        return Tuple{std::forward<Args>(args)...};
      }
    };

    template <class Tuple>
    struct ConstructTuple<Tuple, 0>
    {
      template <class... Args>
      static Tuple make(Args&&... args)
      {
        static_assert(Size<Tuple> == 0,
            "Construction of empty tuples with empty argument list only!");
        return {};
      }
    };


    template <class Tuple>
    struct FoldTuples
    {
      // add arg to repeated constructor argument list
      template <std::size_t... Is, class... Tuples>
      static Tuple make(Indices<std::size_t,Is...>, Tuples&&... tuples)
      {
        return Tuple{make_element(index_<Is>, std::forward<Tuples>(tuples)...)...};
      }

      template <std::size_t I, class... Tuples>
      static std::tuple_element_t<I, Tuple> make_element(index_t<I>, Tuples&&... tuples)
      {
        using std::get;
        return std::tuple_element_t<I, Tuple>{get<I>(std::forward<Tuples>(tuples))...};
      }
    };

  } // end namespace aux


  /// Construct a tuple with each element constructed by the same argument arg.
  /// Returns tuple { tuple_element_t<0>(arg), tuple_element_t<1>(arg), ... }
  template <class Tuple, class Arg>
  Tuple constructTuple(Arg&& arg)
  {
    return aux::ConstructTuple<Tuple, Size<Tuple>>::make(
        std::forward<Arg>(arg));
  }

  /// Construct a tuple of tuples, by folding a list of tuples of the same size.
  /// Return tuple { {arg0[0], arg1[0], arg2[0], ...}, {arg0[1], arg1[1], arg2[1], ...}, ...}
  template <class Tuple, class... Args>
  Tuple foldTuples(Args&&... args)
  {
    return aux::FoldTuples<Tuple>::make(MakeIndexSeq_t<Size<Tuple>>(),
        std::forward<Args>(args)...);
  }


  namespace aux
  {
    template <template<class> class Base, class Tuple, class Indices> struct MakeTuple;

    template <template<class> class Base, class Tuple, std::size_t... I>
    struct MakeTuple<Base, Tuple, Indices<std::size_t,I...>>
    {
      using type = std::tuple<Base<std::tuple_element_t<I, Tuple>>...>;
    };

  } // end namespace aux


  /// Constructs tuple type, by wrapping Base around the tuple elements.
  /// Returns tuple type tuple<Base<tuple_element_t<0>>, Base<tuple_element_t<1>>, ...>
  template <template<class> class Base, class Tuple>
  using MakeTuple_t =
    typename aux::MakeTuple<Base, Tuple, MakeIndexSeq_t<Size<Tuple>>>::type;


  namespace aux
  {
    template <template<class,class> class Base, class Tuple1, class Tuple2, class Indices> struct MakeTuple2;

    template <template<class,class> class Base, class Tuple1, class Tuple2, std::size_t... I>
    struct MakeTuple2<Base, Tuple1, Tuple2, Indices<std::size_t,I...>>
    {
      using type = std::tuple<Base<std::tuple_element_t<I, Tuple1>, std::tuple_element_t<I, Tuple2>>...>;
    };

  } // end namespace aux


  /// Constructs tuple type, by wrapping Base around the tuple elements.
  /// Returns tuple type tuple<Base<tuple_element_t<0,T1>,tuple_element_t<0,T2>>, Base<tuple_element_t<1,T1>,tuple_element_t<1,T2>>, ...>
  template <template<class,class> class Base, class Tuple1, class Tuple2>
  using MakeTuple2_t =
    typename aux::MakeTuple2<Base, Tuple1, Tuple2, MakeIndexSeq_t<Size<Tuple1>>>::type;


  namespace aux
  {
    template <template<class...> class Base, class Tuple, class Indices> struct ExpandTuple;

    template <template<class...> class Base, class Tuple, std::size_t... I>
    struct ExpandTuple<Base, Tuple, Indices<std::size_t,I...>>
    {
      using type = Base<std::tuple_element_t<I, Tuple>...>;
    };

  } // end namespace aux


  /// Expands tuple element types into template list of Base class.
  /// Returns Base<tuple_element<0>, tuple_element<1>, ...>
  template <template<class...> class Base, class Tuple>
  using ExpandTuple_t =
    typename aux::ExpandTuple<Base, Tuple, MakeIndexSeq_t<Size<Tuple>>>::type;


  namespace aux
  {
    template <class T, class Indices> struct RepeatedTuple;

    template <class T, std::size_t... I>
    struct RepeatedTuple<T, Indices<std::size_t,I...>>
    {
      template <std::size_t, class U> using Id = U;
      using type = std::tuple<Id<I, T>...>;
    };

  } // end namespace aux

  /// Construct tuple of N times type T.
  /// Returns tuple<T, T, T, ...>
  template <std::size_t N, class T>
  using Repeat_t =
    typename aux::RepeatedTuple<T, MakeIndexSeq_t<N>>::type;

} // end namespace Dec
