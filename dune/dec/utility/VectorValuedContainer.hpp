#pragma once

#include <dune/dec/LinearAlgebra.hpp>

namespace Dec
{
  template <class T, int ncomps>
  struct VectorValuedContainer
  {
    T const& operator[](std::size_t i) const
    {
      std::size_t const r = i / ncomps;
      std::size_t const c = i % ncomps;
      return data[r][c];
    }

    std::size_t size() const { return data.size() * ncomps; }

    DOFVector<Dune::FieldVector<T,ncomps>> const& data;
  };

} // end namespace Dec
