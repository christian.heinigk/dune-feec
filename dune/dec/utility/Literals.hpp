#pragma once

#include <dune/dec/common/Index.hpp>

namespace Dec
{
  // inspired by Boost.hana
  // see also: http://blog.mattbierner.com/stupid-template-tricks-stdintegral_constant-user-defined-literal/

  namespace aux
  {
    constexpr unsigned char2digit(const char c)
    {
      assert(c >= '0' && c <= '9' && "Unknown digit in integral constant");
      return unsigned(c) - unsigned('0');
    }

    template <size_t N>
    constexpr size_t string2num(const char (&arr)[N])
    {
      assert(arr[0] != '-' && "Negative integral constant");

      size_t result = 0;
      size_t power  = 1;

      for (size_t i = 0; i < N; ++i) {
          char c = arr[N - 1 - i];
          result+= char2digit(c) * power;
          power *= 10u;
      }

      return result;
    }

  } // end namespace aux

  /// Literal to create integer compile-time constant, e.g. 0_c -> index_<0>
  template <char... digits>
  constexpr auto operator"" _c()
  {
    return index_<aux::string2num<sizeof...(digits)>({digits...})>;
  }

} // end namespace Dec
