#pragma once

#include <tuple>
#include <algorithm>

#include <dune/dec/common/Concepts.hpp>

namespace Dec
{
  /// Pair of values of type A and B with first < second
  template <class T,
    REQUIRES(concepts::LessThanComparable<T,T>) >
  class SortedPair
      : public std::pair<T,T>
  {
  public:
    SortedPair(T const& a, T const& b)
      : std::pair<T,T>( std::minmax(a, b) )
    {
      signum = (this->first != a ? -1 : 1);
    }

    SortedPair(SortedPair const&) = default;
    SortedPair(SortedPair&&) = default;

    SortedPair(std::pair<T,T> const& that)
      : SortedPair(that.first, that.second)
    {}

    SortedPair(std::array<T,2> const& that)
      : SortedPair(that[0], that[1])
    {}

    SortedPair& operator=(SortedPair const&) = default;
    SortedPair& operator=(SortedPair&&) = default;

    SortedPair& operator=(std::pair<T,T> const& that)
    {
      SortedPair copy(that);
      *this = copy;
    }

    SortedPair& operator=(std::array<T,2> const& that)
    {
      SortedPair copy(that);
      *this = copy;
    }

    T const& operator[](std::size_t i) const
    {
      return i == 0 ? this->first : this->second;
    }

    int signum = 1;
  };

} // end namespace Dec
