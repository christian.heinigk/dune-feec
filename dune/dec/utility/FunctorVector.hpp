#pragma once

#include <cstddef>
#include <type_traits>

namespace Dec
{
  /// A vector-like interface that evaluates a functor.
  template <class F>
  struct FunctorVector
  {
    using value_type = std::result_of_t<F(std::size_t)>;

    FunctorVector(F const& f)
      : f_(f)
    {}

    value_type operator[](std::size_t i) const
    {
      return f_(i);
    }

    F f_;
  };

  template <class F>
  FunctorVector<F> make_vector(F const& f)
  {
    return {f};
  }

} // end namespace Dec
