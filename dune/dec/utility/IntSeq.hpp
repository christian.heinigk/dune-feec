#pragma once

// from http://stackoverflow.com/questions/17424477/implementation-c14-make-integer-sequence/17426611#17426611
namespace Dec
{
  /// class that represents a sequence of indices
  template <class Int, Int... Is>
  struct Indices
  {
    using type = Indices;
    using data = std::tuple<std::integral_constant<Int, Is>...>;

    /// Returns the ith index in the range as integral constant
    template <std::size_t i>
    constexpr auto operator[](index_t<i>) const
    {
      return std::tuple_element<i,data>{};
    }

    /// Return whether the range is empty
    static constexpr bool empty() { return sizeof...(Is) == 0; }

    /// Returns the size of the range
    static constexpr auto size() { return std::integral_constant<Int, Int(sizeof...(Is))>{}; }

    /// Returns whether the range is not empty
    constexpr operator bool() const { return !empty(); }
  };

  namespace aux
  {
    template <class Int, Int s, class S>
    struct ConcatSeq;

    template <class Int, Int s, Int... i>
    struct ConcatSeq<Int, s, Indices<Int, i... >>
    {
      using type = Indices<Int, i..., (s + i)... >;
    };

    template <bool, class S>
    struct IncSeq_if
    {
      using type = S;
    };

    template <class Int, Int... Is>
    struct IncSeq_if<true, Indices<Int, Is...>>
    {
      using type = Indices<Int, Is..., Int(sizeof...(Is))>;
    };

  } // end namespace aux

  template <class Int, Int N>
  struct MakeSeq
  {
    using type = typename aux::IncSeq_if<(N % 2 != 0),
      typename aux::ConcatSeq<Int, N/2, typename MakeSeq<Int, N/2>::type>::type >::type;
  };

#ifndef DOXYGEN
  // break condition
  template <> struct MakeSeq<int, int(0)> { using type = Indices<int>; };
  template <> struct MakeSeq<std::size_t, std::size_t(0)> { using type = Indices<std::size_t>; };
#endif

  /// Alias template to create a sequence of indices
  template <class Int, Int N>
  using MakeSeq_t = typename MakeSeq<Int, N>::type;

  template <int N>
  using MakeIntSeq_t = typename MakeSeq<int, N>::type;

  template <std::size_t N>
  using MakeIndexSeq_t = typename MakeSeq<std::size_t, N>::type;

} // end namespace Dec
