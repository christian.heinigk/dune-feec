#pragma once

#include <type_traits>

#include <dune/dec/common/Common.hpp>

namespace Dec
{
  /// \brief A pointer class that deletes only when owning the pointer.
  template <class T>
  class ClonablePtr
  {
  private:
    struct alloc_tag {}; ///< hidden helper struct, used by \ref make

  public:
    using Self = ClonablePtr;
    using element_type = T;

    ClonablePtr() = default;

    /// Constructor from pointer. Can only be used via make method,
    /// Transfers ownership.
    ClonablePtr(owner<T>* p, alloc_tag) noexcept
      : p(p)
      , is_owner(true)
    {}

    /// Constructor from reference
    ClonablePtr(T& ref) noexcept
      : p(&ref)
      , is_owner(false)
    {}

    /// Destructor, deletes in case of owner only
    ~ClonablePtr() noexcept
    {
      if (is_owner)
        delete p;
    }

    /// Copy constructor, creates a clone of the pointed to object
    ClonablePtr(Self const& that) noexcept( std::is_nothrow_copy_constructible<T>::value )
      : p(new T(*that.p))
      , is_owner(true)
    {}

    /// Move constructor, copies the pointer only.
    ClonablePtr(Self&& that) noexcept
      : p(that.p)
      , is_owner(that.is_owner)
    {
      that.p = nullptr;
      that.is_owner = false;
    }

    /// Copy and move assignment operator, using the copy-and-swap idiom
    Self& operator=(Self that) noexcept
    {
      swap(that);
      return *this;
    }

    /// Factory method. creates a new Object of type T and stores the pointer.
    template <class... Args>
    static Self make(Args&&... args)
      noexcept( std::is_nothrow_constructible<T, std::decay_t<Args>...>::value )
    {
      return {new T(std::forward<Args>(args)...), Self::alloc_tag()};
    }

    /// Access-method by dereferencing
    T& operator*() const noexcept
    {
      return *p;
    }

    /// Access-method by pointer access
    T* operator->() const noexcept
    {
      return p;
    }

    /// retrieve the underlying pointer
    T* get() const noexcept
    {
      return p;
    }

    /// Test whether pointer is nullptr
    operator bool() const noexcept
    {
      return !(p == nullptr);
    }

    void swap(Self& that) noexcept
    {
      using std::swap;
      swap(p, that.p);
      swap(is_owner, that.is_owner);
    }

  private:
    T* p = nullptr;        ///< managed pointer
    bool is_owner = false; ///< true, if class is owner of pointer, false otherwise
  };

  template <class T>
  void swap(ClonablePtr<T>& a, ClonablePtr<T>& b) noexcept
  {
    a.swap(b);
  }

} // end namespace Dec
