#pragma once

#include <array>
#include <memory>

#include <dune/dec/common/Common.hpp>

namespace Dec
{
  /// Minimalistic vector implementation, with a push_back function and fixed_size capacity
  template <class T, std::size_t N>
  class CapacityVector
      : public std::array<T, N>
  {
  public:

    static constexpr std::size_t capacity = N;

  public: // Element access:

    T* begin() { return this->data(); }
    T* end() { return begin() + pos; }

    T const* begin() const { return this->data(); }
    T const* end() const { return begin() + pos; }

    T const* cbegin() const { return this->data(); }
    T const* cend() const { return cbegin() + pos; }


    void push_back(T const& value)
    {
      Expects( pos < capacity );
      (*this)[pos++] = value;
    }

    void push_back(T&& value)
    {
      Expects( pos < capacity );
      (*this)[pos++] = std::move(value);
    }


  public: // Capacity:

    bool empty() const { return pos == 0; }
    std::size_t size() const { return pos; }


  public: // Modifiers:

    void clear() { pos = 0; }
    void reserve(std::size_t n) { Expects( n <= capacity ); }


  private:

    std::size_t pos = 0;
  };

} // end namespace Dec
