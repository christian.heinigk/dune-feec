#include "Timer.hpp"

namespace Dec {

Timer::Timer()
  : t0(Clock::now())
{}

void Timer::reset()
{
  t0 = Clock::now();
}

typename Timer::value_type Timer::elapsed() const
{
  auto t1 = Clock::now();
  fsec fs = t1 - t0;
  return fs.count();
}

} // end namespace Dec
