#pragma once

#include <cstdlib>

#include "QuadMath.hpp"

namespace Dune
{
  template <class T, int N>
  class FieldVector;
  
  template <class T, int N, int M>
  class FieldMatrix;
}

namespace Dec
{
  // forward declarations
  template <class T, size_t N, size_t M>
  class FixMat;

  namespace aux
  {
    // General cast class that implements an `eval` method to cast a string to
    // a number of type `T`. Provide specializations for concrete floating-point
    // types below!
    template <class T>
    struct Cast {
      static T eval(char const* str) { return T(str); }
    };

    template <> struct Cast<float> {
      static float eval(char const* str) { return std::strtof(str, NULL); }
    };
    template <> struct Cast<double> {
      static double eval(char const* str) { return std::strtod(str, NULL); }
    };
    template <> struct Cast<long double> {
      static long double eval(char const* str) { return std::strtold(str, NULL); }
    };
    

    template <> struct Cast<int> {
      static int eval(char const* str) { return std::atoi(str); }
    };
    template <> struct Cast<long> {
      static long eval(char const* str) { return std::atol(str); }
    };
    template <> struct Cast<long long> {
      static long long eval(char const* str) { return std::atoll(str); }
    };
    
    
    template <> struct Cast<unsigned int> {
      static unsigned int eval(char const* str) { return std::strtoul(str, NULL, 10); }
    };
    template <> struct Cast<unsigned long> {
      static unsigned long eval(char const* str) { return std::strtoul(str, NULL, 10); }
    };
    template <> struct Cast<unsigned long long> {
      static unsigned long long eval(char const* str) { return std::strtoull(str, NULL, 10); }
    };

    
    template <class T> struct Cast<FixMat<T,1,1>> {
      static FixMat<T,1,1> eval(char const* str) { return Cast<T>::eval(str); }
    };

    template <class T> struct Cast<Dune::FieldVector<T,1>> {
      static Dune::FieldVector<T,1> eval(char const* str) { return Cast<T>::eval(str); }
    };
    
    template <class T> struct Cast<Dune::FieldMatrix<T,1,1>> {
      static Dune::FieldMatrix<T,1,1> eval(char const* str) { return Cast<T>::eval(str); }
    };

#if DEC_HAS_FLOAT128
    // specialization for quadprecision floating-point type.
    template <> struct Cast<__float128> {
      static __float128 eval(char const* str) { return strtoflt128(str, NULL); }
    };
#endif // DEC_HAS_FLOAT128
  }

  template <class T>
  T cast(char const* str) { return aux::Cast<T>::eval(str); }

  template <class T>
  T cast(std::string const& str) { return aux::Cast<T>::eval(str.c_str()); }
  
} // end namespace Dec
