#pragma once

#include <chrono>

namespace Dec
{
  /// time measurement methods
  class Timer
  {
    using value_type = double;
    using Clock     = std::chrono::high_resolution_clock;
    using TimePoint = std::chrono::time_point<Clock>;
    using fsec      = std::chrono::duration<value_type>;

  public:
    /// initializes the timer with current time
    Timer();

    /// resets the timer to current time
    void reset();

    /// returns the elapsed time (from construction or last reset) to now in seconds
    value_type elapsed() const;

  protected:
    /// start time
    TimePoint t0;
  };


  template <class GridView>
  class ParallelTimer
      : public Timer
  {
  public:
    ParallelTimer(GridView const& gv)
      : gv_(gv)
    {}

    auto elapsed() const
    {
      auto e = Timer::elapsed();
      return gv_.comm().max(e);
    }

  protected:
    GridView gv_;
  };

}  // end namespace Dec
