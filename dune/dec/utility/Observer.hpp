#pragma once

#include <cmath>
#include <limits>

namespace Dec
{
  class Observer
  {
  public:
    virtual bool operator()(double t) const = 0;

    virtual Observer& operator++() = 0;
  };

  template <bool WriteOut>
  class GenericObserver : public Observer
  {
  public:
    virtual bool operator()(double /*t*/) const override
    {
      return WriteOut;
    }

    virtual GenericObserver& operator++() override
    {
      idx++;
      return *this;
    }

    std::size_t index() const
    {
      return idx;
    }

  private:
    std::size_t idx = 1;
  };

  // Never write anything
  using BasicObserver = GenericObserver<false>;

  // Always write out
  using NoisyObserver = GenericObserver<true>;

  // Write after time-interval
  class CyclicObserver : public Observer
  {
  public:
    CyclicObserver(double dt_write = 0.1, double tend = 1.0, double t0 = 0.0)
      : dt_write(dt_write)
      , tend(tend)
      , t0(t0)
    {}

    virtual bool operator()(double t) const override
    {
      return t >= (t0 + write_idx * dt_write) - tol || std::abs(t - tend) < tol;
    }

    virtual CyclicObserver& operator++() override
    {
      write_idx++;
      return *this;
    }

    std::size_t index() const
    {
      return write_idx;
    }
    
    double timestep() const 
    {
      return dt_write;
    }

  private:
    double dt_write;
    double tend, t0;
    std::size_t write_idx = 1;
    double tol = std::numeric_limits<double>::epsilon();
  };

} // end namespace Dec
