#pragma once

#include <string>

#ifdef HAVE_VAMPIR
#include <vt_user.h>
#endif

namespace Dec
{
#ifdef HAVE_VAMPIR
  template <unsigned int Key>
  class VampirTrace
  {
  public:
    VampirTrace(std::string name_ = std::to_string(Key))
      : name(name_)
    {
      VT_USER_START(name.c_str());
    }

    ~VampirTrace()
    {
      VT_USER_END(name.c_str());
    }

  private:
    std::string name;
  };
#else
  template <unsigned int>
  struct VampirTrace
  {
    VampirTrace(std::string = "") {}
  };
#endif

} // end namespace Dec
