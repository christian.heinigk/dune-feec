#pragma once

#include <tuple>

#include "IntSeq.hpp"
#include <dune/dec/common/ScalarTypes.hpp>

namespace Dec
{
  /// \brief Construct matrix of types. \ingroup container
  /**
    Matrix of types using tuples of tuples:
    ```
    std::tuple<
      std::tuple<E<0,0>, E<0,1>, ... E<0,M-1>>,
      std::tuple<E<1,0>, E<1,1>, ... E<1,M-1>>,
      ...
      std::tuple<E<N-1,0>, E<N-1,1>, ... E<N-1,M-1>>
    >;
    ```
  **/
  template <class Int, Int N, Int M, template <Int, Int> class E,
    REQUIRES(concepts::Integral<Int>) >
  struct MatrixTuple
  {
#ifndef DOXYGEN
    template <Int I, class Idx> struct ColTuple;

    template <Int I, Int... J>
    struct ColTuple<I, Indices<Int, J...>>
    {
      using type = std::tuple<E<I,J>...>;
    };

    template <class Idx> struct RowTuple;

    template <Int... I>
    struct RowTuple<Indices<Int, I...>>
    {
      using type = std::tuple<typename ColTuple<I, MakeSeq_t<Int, M>>::type...>;
    };

    using type = typename RowTuple<MakeSeq_t<Int, N>>::type;
#endif
  };

#ifdef DEC_HAS_CXX17
  template <auto N, auto M, template <auto, auto> class E>
  using MatrixTuple_t = typename MatrixTuple<decltype(N), M, N, E>::type;
#else
  template <std::size_t N, std::size_t M, template <std::size_t, std::size_t> class E>
  using MatrixTuple_t = typename MatrixTuple<std::size_t, M, N, E>::type;
#endif

  template <class Int, Int N, template <Int> class E,
    REQUIRES(concepts::Integral<Int>) >
  struct VectorTuple
  {
#ifndef DOXYGEN
    template <class Idx> struct RowTuple;

    template <Int... I>
    struct RowTuple<Indices<Int, I...>>
    {
      using type = std::tuple<E<i>...>;
    };

    using type = typename RowTuple<MakeSeq_t<Int, N>>::type;
#endif
  };

#ifdef DEC_HAS_CXX17
  template <auto N, template <auto> class E>
  using VectorTuple_t = typename VectorTuple<decltype(N), N, E>::type;
#else
  template <std::size_t N, template <std::size_t> class E>
  using VectorTuple_t = typename VectorTuple<std::size_t, N, E>::type;
#endif

} // end namespace Dec
