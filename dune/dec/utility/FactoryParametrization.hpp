#pragma once

#include <type_traits>
#include <vector>

#include <dune/grid/common/gridfactory.hh>

#include <dune/dec/LocalView.hpp>

namespace Dec
{
  template <class GridType, class Projection>
  struct Parametrization
  {
    using Grid = GridType;
    using ctype = typename Grid::ctype;

    static constexpr int dimension = Grid::dimension;
    static constexpr int dimensionworld = Grid::dimensionworld;

    using LeafIntersection = typename Grid::LeafIntersection;

    template <int cd>
    using Codim = typename Grid::template Codim<cd>;
  };

}


namespace Dune
{

  /// \brief A factory class for \ref SimpleGrid.
  template <class Grid, class Projection>
  class GridFactory<Dec::Parametrization<Grid, Projection>>
      : public GridFactoryInterface<Dec::Parametrization<Grid, Projection>>
  {
    using GridWrapper = Dec::Parametrization<Grid, Projection>;

    /// Type used by the grid for coordinates
    using ctype = typename Grid::ctype;

    static constexpr int dim = Grid::dimension;
    static constexpr int dow = Grid::dimensionworld;

    using LocalCoordinate = FieldVector<ctype, dim>;
    using GlobalCoordinate = FieldVector<ctype, dow>;

    using ProjectionBase = VirtualFunction<LocalCoordinate, GlobalCoordinate>;

  public:

    /// Default constructor
    GridFactory()
    {}

    /// Insert a vertex into the coarse grid
    virtual void insertVertex(GlobalCoordinate const& pos) override
    {
      GlobalCoordinate new_pos(pos);
      Projection::project(new_pos);

      coordinates.push_back(new_pos);
      factory.insertVertex(new_pos);
    }

    /// Insert an element into the coarse grid.
    virtual void insertElement(GeometryType const& type,
                               std::vector<unsigned int> const& vertices) override
    {
      assert( type.isSimplex() );

      auto view = Dec::local_view(coordinates, vertices);
      factory.insertElement(type, vertices, std::shared_ptr<ProjectionBase>(new Projection(view)) );
    }

    virtual void insertBoundarySegment(std::vector<unsigned int> const& /*vertices*/) override
    {
      DEC_MSG("Not implemented!");
    }

    /// Finalize grid creation and hand over the grid.
    Grid* create()
    {
      return factory.createGrid();
    }

    virtual GridWrapper* createGrid() override
    {
      Dec::error_exit("Should not be created. Use non-virtual method `create()` instead, to create the underlying grid!");
      return nullptr;
    }

  private:

    // buffers for the mesh data
    std::vector<GlobalCoordinate> coordinates;

    GridFactory<Grid> factory;
  };

} // end namespace Dune
