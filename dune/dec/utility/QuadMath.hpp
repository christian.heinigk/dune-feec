#if DEC_HAS_FLOAT128
#include <ostream>
#include <quadmath.h>

#include <dune/dec/common/Output.hpp>

namespace Dec
{
  template <class CharT, class Traits>
  std::basic_ostream<CahrT, Traits>& operator<<(std::basic_ostream<CahrT, Traits>& out,
                                                __float128 const& value)
  {
    const std::size_t bufSize = 128;
    CharT buf[128];

    std::string format = "%." + std::to_string(out.precision()) + "Q" +
                         (out.flags() | std::ios_base::scientific) ? "e" : "f";
    const int numChars = quadmath_snprintf(buf, bufSize, format.c_str(), value);
    assert_msg(size_t(numChars) < bufSize, "Failed to print __float128 value: buffer overflow");
    out << buf;
    return out;
  }

} // end namespace Dec
#endif // DEC_HAS_FLOAT128
