#pragma once

#include <algorithm>
#include <array>
#include <tuple>
#include <vector>

#include <dune/dec/common/Concepts.hpp>

namespace Dec
{
  /** \ingroup container
   * \brief Tuple of values of type T always stored sorted.
   **/
  template <class T, size_t N,
    REQUIRES(concepts::LessThanComparable<T,T>) >
  class SortedArray
      : public std::array<T,N>
  {
  public:
    template <class... Ts,
      REQUIRES(sizeof...(Ts) == N) >
    SortedArray(Ts const&... ts)
      : std::array<T,N>{{ts...}}
    {
      if (N > 1)
        sort();
    }

    SortedArray(SortedArray const&) = default;
    SortedArray(SortedArray&&) = default;

    SortedArray(std::array<T,N> const& that)
      : std::array<T,N>(that)
    {
      if (N > 1)
        sort();
    }

    SortedArray& operator=(SortedArray const&) = default;
    SortedArray& operator=(SortedArray&&) = default;

    SortedArray& operator=(std::array<T,N> const& that)
    {
      SortedArray copy(that);
      *this = copy;
      return *this;
    }

  private:

    // implementation of a bubble-sort algorithm, to count the number of transpositions, i.e. the
    // sign of the permutation.
    void sort()
    {
      std::size_t i = N;
      do {
        std::size_t i1 = 0;
        for (std::size_t j = 0; j < i-1; ++j) {
          if ((*this)[j] > (*this)[j+1]) {
            std::swap((*this)[j], (*this)[j+1]);
            signum *= -1;
            i1 = j+1;
          }
        }
        i = i1;
      } while (i > 0);
    }

  public:

    int signum = 1;
  };

} // end namespace Dec
