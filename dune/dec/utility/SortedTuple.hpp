#pragma once

#include "SortedPair.hpp"
// #include "SortedTriple.hpp"
#include "SortedArray.hpp"

namespace Dec
{
  namespace aux
  {
    template <class T, int d>
    struct SortedTupleImpl
    {
      using type = SortedArray<T,d>;
    };

    template <class T>
    struct SortedTupleImpl<T,2>
    {
      using type = SortedPair<T>;
    };
  }

  template <class T, int d>
  using SortedTuple = typename aux::SortedTupleImpl<T,d>::type;

} // end namespace Dec
