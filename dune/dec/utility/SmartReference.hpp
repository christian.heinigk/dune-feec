#pragma once

#include <memory>

#include <dune/dec/common/Common.hpp>
#include <dune/dec/common/Compatible.hpp>

namespace Dec
{
  /// \brief Store a reference or a copy of the data
  template <class T>
  class SmartRef
  {
  public:
    /// Constructor from reference
    SmartRef(T& data) noexcept
      : data_(data)
    {}

    /// Constructor from temporary
    SmartRef(T&& data)
      : copy_(std::shared_ptr<T>(new T{std::move(data)}))
      , data_(*copy_)
    {}

    /// Constructor that stores a copy
    template <class T_,
      REQUIRES( concepts::Compatible<T, T_> )>
    SmartRef(T_&& data, tag::store)
      : copy_(std::shared_ptr<T>(new T{std::forward<T_>(data)}))
      , data_(*copy_)
    {}

    /// Access-method by dereferencing
    T const& operator*() const noexcept
    {
      return data_;
    }

    /// Access-method by dereferencing
    T& operator*() noexcept
    {
      return data_;
    }

    /// Access-method by pointer access
    T const* operator->() const noexcept
    {
      return &data_;
    }

    /// Access-method by pointer access
    T* operator->() noexcept
    {
      return &data_;
    }

    /// retrieve the underlying pointer
    T* get() const noexcept
    {
      return &data_;
    }


  private:

    std::shared_ptr<T> copy_ = nullptr;
    T& data_;
  };


  /// \brief Store a const reference or a copy of the data
  template <class T>
  class SmartRef<T const>
  {
  public:
    /// Constructor from reference
    SmartRef(T const& data) noexcept
      : data_(data)
    {}

    /// Constructor from temporary
    SmartRef(T&& data)
      : copy_(std::shared_ptr<T>(new T{std::move(data)}))
      , data_(*copy_)
    {}

    /// Constructor that stores a copy
    template <class T_,
      REQUIRES( concepts::Compatible<T, T_> )>
    SmartRef(T_&& data, tag::store)
      : copy_(std::shared_ptr<T>(new T{std::forward<T_>(data)}))
      , data_(*copy_)
    {}

    /// Access-method by dereferencing
    T const& operator*() const noexcept
    {
      return data_;
    }

    /// Access-method by pointer access
    T const* operator->() const noexcept
    {
      return &data_;
    }

    /// retrieve the underlying pointer
    T const* get() const noexcept
    {
      return &data_;
    }


  private:

    std::shared_ptr<T> copy_ = nullptr;
    T const& data_;
  };

} // end namespace Dec
