#pragma once

#include <type_traits>

#include "ranges/Map.hpp"

namespace Dec
{
  template <class Data, class Indices>
  auto local_view(Data const& data, Indices&& indices)
  {
    return ranges::map([&data](std::size_t i) { return data[i]; }, std::forward<Indices>(indices));
  }

  template <class Data, class Indices>
  auto local_view(Data const& data, Indices const& indices, tag::store)
  {
    return ranges::map([&data](std::size_t i) { return data[i]; }, indices, tag::store{});
  }

} // end namespace Dec
