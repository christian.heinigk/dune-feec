#pragma once

#include <vector>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/affinegeometry.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/LocalView.hpp>
#include <dune/dec/common/Size.hpp>
#include <dune/dec/common/Output.hpp>

namespace Dec
{
  template <int codim, int dim, class Iter>
  struct SimpleEntitySeed
  {
    static constexpr int dimension = dim - codim;
    static constexpr int codimension = codim;

    Iter it;
  };


  template <int cd>
  class SimpleDummyEntity
  {
  public:

    static constexpr int codimension = cd;

    Dune::PartitionType partitionType() const { return Dune::InteriorEntity; }
  };


  template <int cd, int dim, class Vec, class Coordinates>
  class SimpleEntity
  {
    template <int,class,class> friend class SimpleIndexSet;

  public:

    static constexpr int codimension = cd;
    static constexpr int dimension = dim; // grid dimension
    static constexpr int mydimension = dim-cd;

    using WorldVector = typename Coordinates::value_type;
    static constexpr int dimensionworld = Size<WorldVector>;


  private: // Member types:

    using Iter = typename Vec::const_iterator;
    using RefElements = Dune::ReferenceElements<float_type, mydimension>;

    using Geometry = Dune::AffineGeometry<float_type, mydimension, dimensionworld>;


  public: // Member functions for Entity and Geometry:

    SimpleEntity(Iter const& it, Coordinates const& coordinates)
      : it_(it)
      , coordinates_(coordinates)
    {}

    /// Return the level of this entity, NOTE: only leaf-level = macro-level implemented
    int level() const { return 0; }

    Dune::PartitionType partitionType() const { return Dune::InteriorEntity; }

    /// Return the entity type identifier
    Dune::GeometryType type() const { return {Dune::GeometryType::simplex, mydimension}; }

    /// Return the geometry of this entity
    Geometry geometry() const
    {
      auto coords_view = local_view(coordinates_, *it_, tag::store{});
      return { RefElements::simplex(), coords_view };
    }

    /// Return the number of subEntities of codimension cc.
    unsigned int subEntities(unsigned int cc) const
    {
      return RefElements::simplex().size(cc - codimension);
    }

    template <int codim>
    SimpleDummyEntity<codim> subEntity(int /*i*/) const { return {}; }

    /// Obtain coordinates of the i-th corner
    WorldVector const& corner(int i) const
    {
      auto idx = (*it_)[i];
      return coordinates_[idx];
    }

    SimpleEntitySeed<cd,dim,Iter> seed() const { return {it_}; }


  private: // Member variables:

    Iter it_;
    Coordinates const& coordinates_;
  };

} // end namespace Dec
