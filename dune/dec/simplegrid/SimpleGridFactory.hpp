#pragma once

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/grid/common/gridfactory.hh>

#include "SimpleGrid.hpp"
#include <dune/dec/common/Output.hpp>

namespace Dune
{
  /// \brief A factory class for \ref SimpleGrid.
  template <int dim, int dow, class FloatType, class SizeType>
  class GridFactory<Dec::SimpleGrid<dim, dow, FloatType, SizeType>>
      : public GridFactoryInterface<Dec::SimpleGrid<dim, dow, FloatType, SizeType>>
  {
    using Grid = Dec::SimpleGrid<dim, dow, FloatType, SizeType>;

    /// Type used by the grid for coordinates
    using ctype = FloatType;

    using WorldVector = FieldVector<FloatType,dow>;

  public:

    /// Default constructor
    GridFactory(std::size_t expected_num_coordintes = 3, std::size_t expected_num_elements = 1)
    {
      coordinates.reserve(expected_num_coordintes);
      elements.reserve(expected_num_elements);
    }

    /// Insert a vertex into the coarse grid
    virtual void insertVertex(WorldVector const& pos) override
    {
      coordinates.push_back(pos);
    }

    /// Insert an element into the coarse grid.
    virtual void insertElement(GeometryType const& type,
                               std::vector<unsigned int> const& vertices) override
    {
      assert( type.isSimplex() );
      elements.emplace_back(vertices.begin(), vertices.end());
    }

    virtual void insertBoundarySegment(std::vector<unsigned int> const& /*vertices*/) override
    {
      DEC_MSG("Not implemented!");
    }

    /// Finalize grid creation and hand over the grid.
    virtual /*owner*/ Grid* createGrid() override
    {
      return new Grid(std::move(elements), std::move(coordinates));
    }

  private:

    // buffers for the mesh data
    std::vector<WorldVector> coordinates; // TODO: replace with dequeue
    std::vector<std::vector<SizeType>> elements;
  };

} // end namespace Dune
