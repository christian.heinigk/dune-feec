#pragma once

#include <vector>
#include <algorithm>
#include <type_traits>

#include <dune/common/parallel/collectivecommunication.hh>

#include "SimpleEntity.hpp"
#include "SimpleIndexSet.hpp"

#include <dune/dec/common/Dim.hpp>
#include <dune/dec/common/Logical.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/common/Tags.hpp>
#include <dune/dec/ranges/IndexRange.hpp>
#include <dune/dec/utility/SmartReference.hpp>

namespace Dec
{
  /** \brief An implementation of a simple grid wrapper, where the grid is
   *  described by two vectors: a connectivity vector for the elements and a
   *  vector of vertex coordinates.
   *
   *  Only codim=0 and codim=dim entities can be accessed in this grid, i.e.
   *  elements and vertices.
   **/
  template <int dim, int dow, class FloatType = double, class SizeType = std::size_t>
  class SimpleGrid
  {
  public:

    static constexpr int dimension = dim;
    static constexpr int dimensionworld = dow;

    using ctype = FloatType;
    using size_type = SizeType;

    using WorldVector = Dune::FieldVector<FloatType, dow>;

    using Elements = std::vector<std::vector<size_type>>;
    using Vertices = ranges::IndexRange<std::array<size_type,1>, size_type>;
    using Coordinates = std::vector<WorldVector>;

    using LeafGridView = SimpleGrid;
    using LeafIndexSet = SimpleIndexSet<dim, Elements, Vertices>;
    using LeafIntersection = int; // dummy-type

    struct Traits
    {
      using CollectiveCommunication = Dune::CollectiveCommunication<Dune::No_Comm>;
    };

  private:

    // An iterator that returns itself on dereferencing
    template <class Vec, class Entity>
    struct Iterator
        : public Vec::const_iterator
    {
      using Iter = typename Vec::const_iterator;

      Iterator(Iter const& it, Coordinates const& coordinates)
        : Iter(it)
        , coordinates_(coordinates)
      {}

      Entity operator*() const { return {super(), coordinates_}; }
      Entity operator[](typename Iter::difference_type idx)
      {
        *this += idx;
        return {super(), coordinates_};
      }


    private:

      Iter const& super() const { return *this; }

      Coordinates const& coordinates_;
    };

  public:

    template <int cd>
    struct Codim
    {
      using Entity = if_<cd == 0,
        SimpleEntity<0, dim, Elements, Coordinates>,
        SimpleEntity<dim, dim, Vertices, Coordinates>>;
    };


  private:

    using ElementIterator = Iterator<Elements, typename Codim<0>::Entity>;
    using VertexIterator = Iterator<Vertices, typename Codim<dim>::Entity>;


  public: // Member functions:

    /// \brief Constructor, takes a vector of element connectivity and a vector of vertex coordinates.
    /**
     * An element-vector is a `vector<vector<unsigned int>>` of indices representing the connectivity of the grid
     * The coordinate vector represents the coordinates of the vertices of the grid
     * Optionally the tag `tag::store` can be provided, to create a copy of the passed vectors, otherwise
     * the data is **refereced**, or moved if passed as rvalue.
     */
    template <class Elements_, class Coordinates_, class... Tags>
    SimpleGrid(Elements_&& elements,
               Coordinates_&& coordinates,
               Tags... tags)
      : elements_(std::forward<Elements_>(elements), tags...)
      , coordinates_(std::forward<Coordinates_>(coordinates), tags...)
      , vertices_(coordinates_->size())
      , indexSet_(*elements_, vertices_)
    {
      indexSet_.init(*this, Dim_<dim>);
    }

    /// Return the maximal refinementlevel of the grid. Since it can not be refined, this is always 0.
    int maxLevel() const { return 0; }

    /// Return the grids leaf index set
    LeafIndexSet const& leafIndexSet() const
    {
      return indexSet_;
    }

    LeafIndexSet const& levelIndexSet(int /*level*/) const
    {
      return indexSet_;
    }

    LeafIndexSet const& indexSet() const
    {
      return indexSet_;
    }

    SimpleGrid& leafGridView() { return *this; }
    SimpleGrid const& leafGridView() const { return *this; }

    SimpleGrid& levelGridView(int /*level*/) { return *this; }
    SimpleGrid const& levelGridView(int /*level*/) const { return *this; }

    /// Return an iterator to subEntities of codim `cd`. Only implemented for cd in {0,dim}.
    template <int cd>
    auto begin() const { return begin_impl(Codim_<cd>, Call_); }

    /// Return an end-iterator to subEntities of codim `cd`. Only implemented for cd in {0,dim}.
    template <int cd>
    auto end() const { return end_impl(Codim_<cd>, Call_); }

    /// Return the number of entities of codim `codim`
    int size(int codim) const
    {
      return indexSet_.size(codim);
    }

    int size(int /*level*/, int codim) const
    {
      return indexSet_.size(codim);
    }

    /// Obtain Entity from EntitySeed.
    template <class EntitySeed>
    typename Codim<EntitySeed::codimension>::Entity
    entity(EntitySeed const& seed) const
    {
      return {seed.it, coordinates_};
    }

    /// Return a vector of vertex coordinates
    Coordinates const& vertices() const { return *coordinates_; }

    /// Returns a vector of element connectivity
    Elements const& elements() const { return *elements_; }

    /// Communicate data on this view
    template <class DataHandleImp, class T>
    void communicate(Dune::CommDataHandleIF<DataHandleImp, T>& /*data*/,
                     Dune::InterfaceType /*iftype*/,
                     Dune::CommunicationDirection /*dir*/) const
    {}

    auto const& comm() const { return comm_; }

  private: // Implementation details:

    // element iterators
    ElementIterator begin_impl(Codim_t<0>, PriorityTag_t<1>) const { return {elements_->cbegin(), *coordinates_}; }
    ElementIterator end_impl  (Codim_t<0>, PriorityTag_t<1>) const { return {elements_->cend(), *coordinates_}; }

    // vertex iterators
    VertexIterator begin_impl(Codim_t<dim>, PriorityTag_t<0>) const { return {vertices_.cbegin(), *coordinates_}; }
    VertexIterator end_impl  (Codim_t<dim>, PriorityTag_t<0>) const { return {vertices_.cend(), *coordinates_}; }

    template <int d>
    int begin_impl(Codim_t<d>, PriorityTag_t<0>) const
    {
      static_assert(d == 0 || d == dim, "Iterator for subEntities not implemented!"); return 0;
    }

    template <int d>
    int end_impl(Codim_t<d>, PriorityTag_t<0>) const
    {
      static_assert(d == 0 || d == dim, "Iterator for subEntities not implemented!"); return 0;
    }


  private: // Member variables:

    SmartRef<Elements const> elements_;
    SmartRef<Coordinates const> coordinates_;

    Vertices vertices_;

    LeafIndexSet indexSet_;

    Dune::CollectiveCommunication<Dune::No_Comm> comm_;
  };

} // end namespace Dec
