#pragma once

#include <map>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/dec/common/Dim.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/common/Tags.hpp>
#include <dune/dec/utility/SortedTuple.hpp>

namespace Dec
{
  /// A datastructure to access the indices of entities
  template <int dim, class Elements, class Vertices>
  class SimpleIndexSet
  {
    using RefElements = Dune::ReferenceElements<double, dim>;

  public:

    using IndexType = std::size_t;

  public: // Member functions:

    SimpleIndexSet(Elements const& elements,
                   Vertices const& vertices)
      : elements_(elements)
      , vertices_(vertices)
    {}

    template <class Grid>
    void init(Grid const& /*grid*/, Dim_t<0>) {}

    template <class Grid>
    void init(Grid const& /*grid*/, Dim_t<1>) {}

    template <class Grid, int d>
    void init(Grid const& grid, Dim_t<d>)
    {
      auto const& refElem = RefElements::simplex();

      facets_.resize(elements_.size());

      using Sorted = SortedTuple<std::size_t, dim>;
      std::map<Sorted, std::size_t> facets;

      std::size_t edge = 0;
      for (auto const& elem : Dune::elements(grid.leafGridView())) {
        std::size_t idx = index(elem);

        std::array<std::size_t, dim> indices{};
        for (int i = 0; i < refElem.size(1); ++i) {
          for (int j = 0; j < refElem.size(i,1,dim); ++j) {
            int pos = refElem.subEntity(i,1,j,dim);
            indices[j] = elements_[idx][pos];
          }
          Sorted sorted{indices};

          auto ins = facets.insert(std::make_pair(sorted, edge));
          if (ins.second)
            facets_[idx][i] = edge++;
          else
            facets_[idx][i] = ins.first->second;
        }
      }

      num_edges_ = edge;
    }

    /// Return the number of entities of codim `codim`
    int size(int codim) const
    {
      warn_msg(codim == 0 || codim == 1 || codim == dim,
        "Method size() not implemented for this codim. Returning 0.");

      return codim == 0   ? elements_.size() :
             codim == dim ? vertices_.size() :
             codim == 1   ? num_edges_ : 0;  /* unknown size of entities */
    }

    template <class Entity>
    std::size_t index(Entity const& e) const
    {
      static_assert(Entity::codimension == 0 || Entity::codimension == dim,
                    "Method index() not implemented for entity!");
      return index_impl(e, Codim_<Entity::codimension>, Call_);
    }

    template <class Entity>
    std::size_t subIndex(Entity const& e, int i, unsigned int ccodim) const
    {
      static_assert(Entity::codimension == 0 || Entity::codimension == dim,
                    "Method subIndex() not implemented for entity!");
      switch (ccodim) {
        case 1:
          // facet of element
          assert( i < dim+1 );
          return facets_[index(e)][i]; break;
        case dim:
          // vertex of element or vertex index
          return (*e.it_)[i]; break;
        default:
          return 0; /* unknown index of entity */
      }
    }


  private: // Implementation details:

    template <class Entity>
    std::size_t index_impl(Entity const& e, Codim_t<0>, PriorityTag_t<1>) const
    {
      return std::distance(elements_.begin(), e.it_);
    }

    template <class Entity>
    std::size_t index_impl(Entity const& e, Codim_t<dim>, PriorityTag_t<0>) const
    {
      return std::distance(vertices_.begin(), e.it_);
    }


  private: // Member variables:

    Elements const& elements_;
    Vertices const& vertices_;

    std::vector<std::array<std::size_t, dim+1>> facets_;
    std::size_t num_edges_;
  };

} // end namespace Dec
