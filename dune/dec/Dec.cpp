#include "Dec.hpp"

#include <cmath>
#include <ctime>

#ifdef DEC_HAS_PETSC
#include "petscsys.h"
#endif

#ifdef HAVE_MPI
#define OLD_HAVE_MPI 1
#undef HAVE_MPI
#else
#define OLD_HAVE_MPI 0
#endif

#include <dune/common/parallel/mpihelper.hh>

#if OLD_HAVE_MPI
#define HAVE_MPI
#endif

namespace Dec {

decpde::decpde(int& argc, char**& argv)
  : mpihelper_(Dune::MPIHelper::instance(argc, argv))
{
  std::srand(std::time(0));
}

decpde::~decpde()
{
}

} // end namespace Dec
