#pragma once

#include <algorithm>
#include <iostream>
#include <iterator>

#include <dune/dec/common/Operations.hpp>

namespace Dec
{
  namespace ranges
  {
    /// \brief A Range implementation based on `first` and `last` iterators. The increment and
    /// dereference operation can be modified using functors.
    template <class Iter, class Increment, class Dereference>
    struct IteratorRange
    {
      using value_type = Decay_t<std::result_of_t<Dereference(Iter)>>;

      struct const_iterator
          : public std::iterator<std::forward_iterator_tag, IteratorRange::value_type>
      {
        using value_type = IteratorRange::value_type;

        const_iterator(Iter it, Iter end, Increment inc, Dereference deref)
          : it_(it)
          , end_(end)
          , inc_(inc)
          , deref_(deref)
        {}

        const_iterator& operator++()
        {
          inc_(it_, end_);
          return *this;
        }
        const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; }

        bool operator==(const_iterator const& other) const { return it_ == other.it_; }
        bool operator!=(const_iterator const& other) const { return !(*this == other); }

        value_type operator*() const { return deref_(it_); }

        Iter it_;  // the current iterator
        Iter end_; // end end-iterator
        Increment inc_; // an increment functor
        Dereference deref_;
      };

      IteratorRange() = default;

      /// \brief Constructor, stores the begin and end iterator `first` and `last`, respectively.
      /// Additionally an increment functor and a dereferencing functor is stored.
      /**
       * \param first An iterator pointing at the begin of a sequence
       * \param last  An iterator pointing past the end of a sequence
       * \param inc   A functor that is called on the iterators when incrementing. The iterator is
       *              passed-by-reference to the functor.
       * \param deref A functor that is called when dereferencing an iterator. Must return the
       *              value the iterator points to.
       * \param s     An (optional) parameter the defines the number of elements in the range if known
       *              in advance.
       **/
      IteratorRange(Iter first, Iter last, Increment inc, Dereference deref, std::size_t s = std::size_t(-1))
        : first_(first)
        , last_(last)
        , inc_(inc)
        , deref_(deref)
        , size_(s)
      {}

      /// Provide an iterator to the begin of the range
      const_iterator cbegin() const { return {first_, last_, inc_, deref_}; }
      /// Provide an iterator to the end of the range
      const_iterator cend()   const { return {last_, last_, inc_, deref_}; }

      /// Provide an iterator to the begin of the range
      const_iterator begin() const { return cbegin(); }
      /// Provide an iterator to the end of the range
      const_iterator end()   const { return cend(); }

      /// Returns the size of the range, i.e. the distance of begin and end iterator
      std::size_t size() const { return size_ != std::size_t(-1) ? size_ : std::distance(begin(), end()); }


    private:

      Iter first_;
      Iter last_;
      Increment inc_;
      Dereference deref_;

      std::size_t size_ = std::size_t(-1);
    };


    /// \brief Generator for \ref IteratorRange, \relates IteratorRange
    /**
     * Requirement:
     * - `Iter` models `ForwardIterator`
     * - `Increment` models `Callable<Iter>` (iterator passed-by-reference)
     * - `Dereference` models `Collable<Iter>` and returns `Value_t<Iter>`
     *
     * By default `Increment` is the usual pre-increment operator and `Dereference` is the usual
     * dereferencing operator. \see \ref Dec::operation::increment, \ref Dec::operation::dereferencing
     **/
    template <class Iter, class Increment = Dec::operation::increment, class Dereference = Dec::operation::dereferencing>
    IteratorRange<Iter, Increment, Dereference>
    make_range(Iter first, Iter last, Increment inc = {}, Dereference deref = {}, std::size_t size = std::size_t(-1))
    {
      return {first, last, inc, deref, size};
    }


    /// A cyclic range is ra range where the iterators first and last are equal, but the range is not (necessarily) empty.
    template <class Iter, class Increment, class Dereference>
    struct CyclicIteratorRange
    {
      using value_type = Decay_t<std::result_of_t<Dereference(Iter)>>;

      struct const_iterator
          : public std::iterator<std::forward_iterator_tag, CyclicIteratorRange::value_type>
      {
        using value_type = CyclicIteratorRange::value_type;

        const_iterator(Iter it, Increment inc, Dereference deref, bool start)
          : it_(it)
          , inc_(inc)
          , deref_(deref)
          , start_(start)
        {}

        const_iterator& operator++()
        {
          inc_(it_);
          start_ = false;
          return *this;
        }
        const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; }

        bool operator==(const_iterator const& other) const { return it_ == other.it_ && start_ == other.start_; }
        bool operator!=(const_iterator const& other) const { return !(*this == other); }

        value_type operator*() const { return deref_(it_); }

        Iter it_;  // the current iterator
        Increment inc_; // an increment functor
        Dereference deref_;
        bool start_;
      };

      CyclicIteratorRange() = default;

      /// \brief Constructor, stored the begin and end iterator `first` and `last`, respectively.
      /// Additionally an increment functor and a dereferencing functor is stored.
      /**
       * \param first An iterator pointing at the begin of a sequence
       * \param last  An iterator pointing past the end of a sequence
       * \param inc   A functor that is called on the iterators when incrementing. The iterator is
       *              passed-by-reference to the functor.
       * \param deref A functor that is called when dereferencing an iterator. Must return the
       *              value the iterator points to.
       * \param s     An (optional) parameter the defines the number of elements in the range if known
       *              in advance.
       **/
      CyclicIteratorRange(Iter first, Iter last, Increment inc, Dereference deref, std::size_t s = std::size_t(-1))
        : first_(first)
        , last_(last)
        , inc_(inc)
        , deref_(deref)
        , size_(s)
      {}

      // provide an iterator to the begin of the range
      const_iterator cbegin() const { return {first_, inc_, deref_, true}; }
      // provide an iterator to the end of the range
      const_iterator cend()   const { return {last_, inc_, deref_, false}; }

      // provide an iterator to the begin of the range
      const_iterator begin() const { return cbegin(); }
      // provide an iterator to the end of the range
      const_iterator end()   const { return cend(); }

      /// Returns the size of the range, i.e. the distance of begin and end iterator
      std::size_t size() const { return size_ != std::size_t(-1) ? size_ : std::distance(begin(), end()); }


    private:

      Iter first_;
      Iter last_;
      Increment inc_;
      Dereference deref_;

      std::size_t size_ = std::size_t(-1);
    };


    /// \brief Generator for \ref CyclicIteratorRange, \relates CyclicIteratorRange
    /**
     * Requirement:
     * - `Iter` models `ForwardIterator`
     * - `Increment` models `Callable<Iter>` (iterator passed-by-reference)
     * - `Dereference` models `Collable<Iter>` and returns `Value_t<Iter>`
     *
     * By default `Increment` is the usual pre-increment operator and `Dereference` is the usual
     * dereferencing operator. \see \ref Dec::operation::increment, \ref Dec::operation::dereferencing
     **/
    template <class Iter, class Increment = Dec::operation::increment, class Dereference = Dec::operation::dereferencing>
    CyclicIteratorRange<Iter, Increment, Dereference>
    make_cyclic_range(Iter first, Iter last, Increment inc = {}, Dereference deref = {}, std::size_t size = std::size_t(-1))
    {
      return {first, last, inc, deref, size};
    }

  } // end namespace ranges
} // end namespace Dec
