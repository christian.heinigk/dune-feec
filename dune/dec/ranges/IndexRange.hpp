#pragma once

#include <iterator>
#include <type_traits>

namespace Dec
{
  namespace ranges
  {
    /// \brief A range that represents the index range [0,size_). Provides random-access iterators.
    template <class ValueType = std::size_t, class SizeType = std::size_t>
    class IndexRange
    {
    public: // Member types:

      using value_type = ValueType;
      using size_type = SizeType;

      struct const_iterator
          : public std::iterator<std::random_access_iterator_tag, ValueType>
      {
        using value_type = ValueType;
        using difference_type = std::ptrdiff_t;

        const_iterator(size_type index)
          : index_(index)
        {}

        const_iterator& operator++()    { ++index_; return *this; }
        const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; }
        const_iterator& operator--()    { --index_; return *this; }
        const_iterator  operator--(int) { const_iterator tmp(*this); --(*this); return tmp; }

        const_iterator& operator+=(difference_type rhs) { index_ += rhs; return *this; }
        const_iterator& operator-=(difference_type rhs) { index_ -= rhs; return *this; }

        difference_type operator-(const_iterator const& rhs) const { return index_ - rhs.index_; }

        const_iterator operator+(difference_type rhs) const { return {index_ + rhs}; }
        const_iterator operator-(difference_type rhs) const { return {index_ - rhs}; }

        friend const_iterator operator+(difference_type lhs, const const_iterator& rhs) { return {lhs + rhs.index_}; }
        friend const_iterator operator-(difference_type lhs, const const_iterator& rhs) { return {lhs - rhs.index_}; }

        bool operator==(const_iterator const& other) const { return index_ == other.index_; }
        bool operator!=(const_iterator const& other) const { return !(*this == other); }
        bool operator> (const_iterator const& other) const { return index_ > other.index_; }
        bool operator< (const_iterator const& other) const { return index_ < other.index_; }
        bool operator>=(const_iterator const& other) const { return index_ >= other.index_; }
        bool operator<=(const_iterator const& other) const { return index_ <= other.index_; }

        value_type operator*() const { return get(Type<value_type>{}); }
        value_type operator[](difference_type idx) { index_ += idx; return get(Type<value_type>{}); }

        template <class T>
        value_type get(Type<std::array<T,1>>) const { return {{index_}}; }

        template <class T>
        value_type get(Type<T>) const { return {index_}; }

        size_type index_;
      };

    public: // Constructors:

      IndexRange(size_type n = 0)
        : size_(n)
      {}

    public: // Element access:

      value_type at(size_type i) const
      {
        assert_msg( i < size_, "Index out of range [0,",size_,")");
        return {i};
      }

      value_type operator[](size_type i) const
      {
        return {i};
      }

      value_type front() const { return {0}; }
      value_type back() const { return {size_ - 1}; }


    public: // Iterators:

      const_iterator begin() const { return cbegin(); }
      const_iterator cbegin() const { return {0}; }

      const_iterator end() const { return cend(); }
      const_iterator cend() const { return {size_}; }


    public: // Capacity:

      bool empty() const { return size_ == 0; }
      size_type size() const { return size_; }


    public: // Modifiers:

      void clear() { size_ = 0; }
      void resize(size_type n) { size_ = n; }

      size_type size_ = 0;
    };

    template <class S>
    bool operator==(IndexRange<S> const& lhs, IndexRange<S> const& rhs) { return lhs.size() == rhs.size(); }

    template <class S>
    bool operator!=(IndexRange<S> const& lhs, IndexRange<S> const& rhs) { return lhs.size() != rhs.size(); }


    /// Generator for an \ref IndexRange, \relates IndexRange.
    /**
     * Example of usage:
     * ```
     * for (auto const& i : irange(5))
     *   std::cout << i << ' '; // prints: 0 1 2 3 4
     * ```
     **/
    template <class T,
      std::enable_if_t<std::is_arithmetic<T>::value, int*> = nullptr >
    IndexRange<T,T> irange(T last)
    {
      return {last};
    }

  } // end namespace ranges

} // end namespace Dec
