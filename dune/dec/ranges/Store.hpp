#pragma once

#include <vector>

namespace Dec
{
  template <class Range>
  std::vector<typename Range::value_type> store(Range const& range)
  {
    return {range.begin(), range.end()};
  }
} // end namespace Dec
