#pragma once

#include <algorithm>
#include <iterator>
#include <memory>
#include <type_traits>

#include "RangeConcepts.hpp"
#include <dune/dec/common/Common.hpp>
#include <dune/dec/common/Compatible.hpp>
#include <dune/dec/utility/SmartReference.hpp>

namespace Dec
{
  namespace ranges
  {
    // NOTE: maybe implement this as an IteratorRange

    /// \brief A range adapter that applys a opper function to each element of a range while
    /// iterating through it. This is similar to pythons `op()` functionality.
    // TODO: add specializations for bidirectional/random-access ranges
    template <class UnaryFunctor, class Range>
    class Map
    {
    public:
      using RangeIterator = typename Range::const_iterator;

      using T = Value_t<RangeIterator>;
      using value_type = Decay_t<std::result_of_t<UnaryFunctor(T)>>;


      class const_iterator
          : public std::iterator<std::forward_iterator_tag, value_type>
      {
      public:
        using value_type = typename Map::value_type;

        /// Constructor, gets an iterator to an element of the Range and the generator functor
        const_iterator(UnaryFunctor op, RangeIterator it)
          : op_(op)
          , it_(it)
        {}

        const_iterator& operator++()    { ++it_; return *this; }
        const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; }

        bool operator==(const_iterator const& other) const { return it_ == other.it_; }
        bool operator!=(const_iterator const& other) const { return !(*this == other); }

        value_type operator*()  const { return op_(*it_); }

      private:
        UnaryFunctor op_;
        RangeIterator it_;
      };

      /// Constructor, stores copy of `op` and moves the `range` to local variable
      template <class Range_,
        REQUIRES( concepts::Compatible<Range, Range_> )>
      Map(UnaryFunctor const& op, Range_&& range)
        : op_(op)
        , range_{std::forward<Range_>(range)}
      {}

      /// Constructor, stores copy of `op` and moves the `range` to local variable
      template <class Range_,
        REQUIRES( concepts::Compatible<Range, Range_> )>
      Map(UnaryFunctor const& op, Range_&& range, tag::store)
        : op_(op)
        , range_{std::forward<Range_>(range), tag::store{}}
      {}

      // provide an iterator to the begin/end of the mapped range
      const_iterator cbegin() const { return {op_, range_->begin()}; }
      const_iterator cend()   const { return {op_, range_->end()}; }

      // provide an iterator to the begin/end of the mapped range
      const_iterator begin() const { return cbegin(); }
      const_iterator end()   const { return cend(); }

      /// Returns the mapped i'th element of the range
      value_type operator[](std::size_t i) const { return accessImpl(i, Type<Range>{}); }

      /// \brief Returns the size of the range, i.e. if the underlying range provides a method `size()`
      /// then return its value, otherwise calculate the size, as the distance of begin and end iterator.
      std::size_t size() const { return sizeImpl(Type<Range>{}); }


    private: // implementation details:

      template <class R,
        REQUIRES(concepts::RandomAccessibleRange<R>) >
      value_type accessImpl(std::size_t i, Type<R>) const { return op_((*range_)[i]); }

      template <class R,
        REQUIRES(not concepts::RandomAccessibleRange<R>) >
      value_type accessImpl(std::size_t /*i*/, Type<R>) const
      {
        static_assert(concepts::RandomAccessibleRange<R>, "Element access not implemented for this range!");
        return 0;
      }

      template <class R,
        REQUIRES(concepts::RangeWithSize<R>) >
      std::size_t sizeImpl(Type<R>) const { return range_->size(); }

      template <class R,
        REQUIRES(not concepts::RangeWithSize<R>) >
      std::size_t sizeImpl(Type<R>) const { return std::distance(begin(), end()); }


    private:
      /// a unary functor mapping the dereferenced iterator to an output value
      UnaryFunctor op_;

      /// reference to a range that is traversed. Maybe copied.
      SmartRef<Range const> range_;
    };


    /// Generator for a mapped range. \relates Map
    /**
     * Example of usage:
     * ```
     * std::vector<int> range = {1,2,3,4,5};
     * for(auto const& x : map([](auto i) { return 2*i; }, range))
     *   std::cout << x << ' ';
     * ```
     **/
    template <class UnaryFunctor, class Range>
    Map<Decay_t<UnaryFunctor>, Decay_t<Range>>
    map(UnaryFunctor&& op, Range&& range)
    {
      return {std::forward<UnaryFunctor>(op), std::forward<Range>(range)};
    }

    template <class UnaryFunctor, class Range>
    Map<Decay_t<UnaryFunctor>, Decay_t<Range>>
    map(UnaryFunctor&& op, Range&& range, tag::store)
    {
      return {std::forward<UnaryFunctor>(op), std::forward<Range>(range), tag::store{}};
    }

  } // end namespace ranges

} // end namespace Dec
