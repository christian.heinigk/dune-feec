#pragma once

#include <dune/dec/common/ConceptsBase.hpp>

namespace Dec
{
  namespace concepts
  {
    namespace definition
    {
      // a.size()
      struct HasSize
      {
        template <class A>
        auto requires_(A&& a) -> decltype( a.size() );
      };

      struct RandomAccessible
      {
        template <class A>
        auto requires_(A&& a) -> decltype( a[std::size_t(0)] );
      };

    } // end namespace definition


    /// \brief Argument R has method size()
    template <class R>
    constexpr bool RangeWithSize = models<definition::HasSize(R)>;

    /// \brief Argument R has bracket operator
    template <class R>
    constexpr bool RandomAccessibleRange = models<definition::RandomAccessible(R)>;

  } // end namespace concepts
} // end namespace Dec
