#pragma once

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <memory>
#include <tuple>

#include "RangeConcepts.hpp"
#include <dune/dec/common/Common.hpp>
#include <dune/dec/common/Compatible.hpp>
#include <dune/dec/utility/SmartReference.hpp>

namespace Dec
{
  namespace ranges
  {
    /// \brief A range adapter to provide an index for each visited element
    template <class Range>
    class Enumerate
    {
      using RangeIterator = typename Range::const_iterator;


    public:

      /// The value-type of the iterator, i.e. a pair (index, value)
      using value_type = std::pair<std::size_t, Value_t<RangeIterator>>;

      struct const_iterator
          : public std::iterator<std::forward_iterator_tag, value_type>
      {
        using value_type = typename Enumerate::value_type;

        const_iterator(RangeIterator iter, std::size_t index)
          : iter_(iter)
          , index_(index)
        {}

        const_iterator& operator++()    { ++iter_; ++index_; return *this; }
        const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; }

        bool operator==(const_iterator const& other) const { return iter_ == other.iter_; }
        bool operator!=(const_iterator const& other) const { return !(*this == other); }

        value_type operator*() const { return {index_, *iter_}; }

        RangeIterator iter_;
        std::size_t index_;
      };


    public:

      /// Constructor, stores a reference or a copy to `range`.
      template <class Range_,
        REQUIRES( concepts::Compatible<Range, Range_> )>
      Enumerate(Range_&& range)
        : range_{std::forward<Range_>(range)}
      {}

      /// Constructor, stores a reference or a copy to `range`.
      template <class Range_,
        REQUIRES( concepts::Compatible<Range, Range_> )>
      Enumerate(Range_&& range, tag::store)
        : range_{std::forward<Range_>(range), tag::store{}}
      {}

      /// Provide an iterator to the begin of the range
      const_iterator cbegin() const { return {range_->begin(), 0u}; }
      /// Provide an iterator to the end of the range
      const_iterator cend() const { return {range_->end(), std::size_t(-1)}; }

      /// Provide an iterator to the begin of the range
      const_iterator begin() const { return cbegin(); }
      /// Provide an iterator to the end of the range
      const_iterator end() const { return cend(); }

      /// \brief Returns the size of the range, i.e. if the underlying range provides a method `size()`
      /// then return its value, otherwise calculate the size, as the distance of begin and end iterator.
      std::size_t size() const { return sizeImpl(Type<Range>{}); }


    private:

      template <class R,
        REQUIRES(concepts::RangeWithSize<R>) >
      std::size_t sizeImpl(Type<R>) const { return range_->size(); }

      template <class R,
        REQUIRES(not concepts::RangeWithSize<R>) >
      std::size_t sizeImpl(Type<R>) const { return std::distance(begin(), end()); }


    private:

      SmartRef<Range const> range_;
    };


    /// \brief Generator for a range that provides an index and the value of the entries of an adapted range.
    /**
     * Returns a pair of (index, value) while iterating through the range. \relates Enumerate
     *
     * Requirement:
     * - `Range` models `ForwardIteratorRange`,
     *
     * Example of usage:
     * ```
     * std::vector<int> vec = {1,2,3,4,5};
     * for (auto p : enumerate(vec))
     *   std::cout << p.first << ") " << p.second << '\n';
     * ```
     **/
    template <class Range>
    Enumerate<Decay_t<Range>> enumerate(Range&& range)
    {
      return {std::forward<Range>(range)};
    }

  } // end namespace ranges
} // end namespace Dec
