#pragma once

#include <limits>

#include "IndexRange.hpp"
#include "Map.hpp"

namespace Dec
{
  template <class F>
  auto make_range(F const& f, std::size_t n = std::numeric_limits<std::size_t>::max())
  {
    return ranges::map(f, ranges::irange(n));
  }

} // end namespace Dec
