#pragma once

namespace Dec
{
  namespace ranges
  {



    template <class Range, class Adapter>
    Pipe<Decay_t<Range>, Decay_t<Adapter>> operator|(Range&& range, Adapter&& adapter)
    {
      return {std::forward<Range>(range), std::forward<Adapter>(adapter)};
    }

  } // end namespace ranges
} // end namespac Dec
