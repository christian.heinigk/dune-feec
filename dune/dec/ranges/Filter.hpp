#pragma once

#include <dune/dec/common/Common.hpp>
#include "IteratorRange.hpp"

namespace Dec
{
  namespace ranges
  {
    template <class Predicate>
    class Filter
    {
    public:
      Filter(Predicate const& pred)
        : pred_(pred)
      {}

      template <class Iterator, class EndIterator>
      void next(Iterator& it, EndIterator const end) const
      {
        while (it != end && !pred_(*it))
          ++it;
      }

      template <class Iterator, class EndIterator>
      void operator()(Iterator& it, EndIterator const end) const
      {
        ++it;
        next(it, end);
      }

    private:
      Predicate pred_;
    };


    template <class Predicate>
    Filter<Decay_t<Predicate>> filter(Predicate&& pred)
    {
      return {std::forward<Predicate>(pred)};
    }

    template <class Range, class P>
    auto operator|(Range&& range, Filter<P> const& filter)
    {
      auto first = range.begin();
      auto last = range.end();
      filter.next(first,last); // find first allowed entry

      return make_range(first, last, filter);
    }

  } // end namespace ranges

} // end namespace Dec
