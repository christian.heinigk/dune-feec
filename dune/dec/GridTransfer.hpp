#pragma once

#include <map>
#include <vector>

#include <dune/geometry/referenceelements.hh>

#include "LinearAlgebra.hpp"

namespace Dec
{
  namespace tag
  {
    struct full_weighting {};
    struct injection {};
    struct adjoint {};
  }

  /// Interpolation matrices to transfer between two grid levels
  template <int dim, class Strategy = tag::full_weighting>
  class GridTransfer
  {
    template <class, class> friend class DefaultTransfer;

  public:

    /// Constructor, stores copy of `mat1` and `mat2`.
    /**
     * \param mat1 Transfer-matrix from coarse to fine
     * \param mat2 Transfer-matrix from fine to coarse
     * \param map  Index mapping: map[coarse_idx] == fine_idx
     **/
    template <class Matrix1, class Matrix2, class Map>
    GridTransfer(Matrix1&& mat1, Matrix2&& mat2, Map&& map)
      : mat1_(std::forward<Matrix1>(mat1))
      , mat2_(std::forward<Matrix2>(mat2))
      , indexmap_(std::forward<Map>(map))
    {}

    /// Interpolate a vector from coarse to fine grid
    template <class Vector1, class Vector2>
    void prolongate(Vector1 const& in, Vector2& out, bool add = false) const
    {
      assert( std::size_t(in.size()) == mat1_.num_cols() );
      out.resize(mat1_.num_rows());

      if (add)
        out.noalias() += mat1_ * in;
      else {
        out.noalias() = mat1_ * in;
      }
    }

    /// Interpolate a vector from fine to coarse grid
    template <class Vector1, class Vector2>
    void restrict(Vector1 const& in, Vector2& out, bool add = false) const
    {
      restrictImpl(in, out, Strategy{}, add);
    }

//     // simple injection
//     template <class Vector1, class Vector2>
//     void injection(Vector1 const& in, Vector2& out, bool add = false) const
//     {
//       assert( std::size_t(in.size()) == mat1_.num_rows() );
//       out.resize(mat1_.num_cols());
//
//       auto stored = in.eval();
//       if (add) {
//         for (std::size_t i = 0; i < indexmap_.size(); ++i)
//           out[i] += stored[indexmap_[i]];
//       } else {
//         for (std::size_t i = 0; i < indexmap_.size(); ++i)
//           out[i] = stored[indexmap_[i]];
//       }
//     }

//     template <class MatrixIn, class MatrixOut>
//     void coarse(MatrixIn const& in, MatrixOut& out) const
//     {
// //       out = (1.0/(1<<dim)) * ((mat1_.transpose() * in).pruned() * mat1_).pruned();
//       out = mat2_ * in * mat1_;
//     }


    std::size_t memory() const
    {
      std::size_t mem = 0;
      mem += indexmap_.capacity()*sizeof(std::size_t) + sizeof(indexmap_);
      mem += mat1_.memory();
      mem += mat2_.memory();

      return mem;
    }

  private: // implementation of restriction strategies

    template <class Vector1, class Vector2>
    void restrictImpl(Vector1 const& in, Vector2& out, tag::full_weighting, bool add = false) const
    {
      assert( std::size_t(in.size()) == mat2_.num_cols() );
      out.resize(mat2_.num_rows());

      if (add)
        out.noalias() += mat2_ * in;
      else {
        out.noalias() = mat2_ * in;
      }
    }

    template <class Vector1, class Vector2>
    void restrictImpl(Vector1 const& in, Vector2& out, tag::adjoint, bool add = false) const
    {
      assert( std::size_t(in.size()) == mat2_.num_cols() );
      out.resize(mat2_.num_rows());

      if (add)
        out.noalias() += (mat1_.transpose() * in) * (1.0/(1<<dim));
      else {
        out.noalias() = mat1_.transpose() * in;
        out *= 1.0/(1<<dim);
      }
    }

    template <class Vector1, class Vector2>
    void restrictImpl(Vector1 const& in, Vector2& out, tag::injection, bool add = false) const
    {
      assert( std::size_t(in.size()) == mat1_.num_rows() );
      out.resize(mat1_.num_cols());

      auto stored = in.eval();
      if (add) {
        for (std::size_t i = 0; i < indexmap_.size(); ++i)
          out[i] += stored[indexmap_[i]];
      } else {
        for (std::size_t i = 0; i < indexmap_.size(); ++i)
          out[i] = stored[indexmap_[i]];
      }
    }


  private:

    DOFMatrix<float_type> mat1_;
    DOFMatrix<float_type> mat2_;
    std::vector<std::size_t> indexmap_;
  };


#ifndef DOXYGEN
  // dummy implementation for grids that do not have an id-set
  template <class GridBase, class = void>
  class GradTransferFactory
  {
    static constexpr int dim = GridBase::dimension;

  public:
    GradTransferFactory(GridBase const&) {}

    GridTransfer<dim> create() const
    {
      std::vector<std::size_t> indexmap;
      DOFMatrix<float_type> mat1;
      DOFMatrix<float_type> mat2;

      return {mat1,  mat2,  indexmap};
    }
  };
#endif


  /// Factory for the interpolation matrices between two grid levels. \relates GridTransfer
  template <class GridBase>
  class GradTransferFactory<GridBase, Void_t<typename GridBase::LocalIdSet>>
  {
    static constexpr int dim = GridBase::dimension;

  public:
    GradTransferFactory(GridBase const& grid)
      : grid_(grid)
      , coarseLevel_(grid.maxLevel())
      , coarseVertices_(grid.size(dim))
    {
      auto const& idSet = grid_.localIdSet();
      auto const& indexSet = grid_.leafIndexSet();

      for (auto const& v : vertices(grid_.leafGridView()))
        id2index_[idSet.id(v)] = indexSet.index(v);
    }

    GridTransfer<dim> create() const
    {
      assert( coarseLevel_ < grid_.maxLevel() );
      auto const& idSet = grid_.localIdSet();
      auto const& indexSet = grid_.leafIndexSet();

      std::vector<std::size_t> indexmap(coarseVertices_);
      DOFMatrix<float_type> mat(grid_.size(dim), coarseVertices_);
      DOFMatrix<float_type> mat2(coarseVertices_, grid_.size(dim));
      std::vector<double> volume(coarseVertices_, 0.0);
      {
        auto ins = mat.inserter();
        auto ins2 = mat2.inserter();

        int numVertices = Dune::ReferenceElements<float_type,dim>::simplex().size(dim);

        std::vector<bool> visited(grid_.size(dim), false);
        for (auto const& elem : elements(grid_.leafGridView())) {
          for (int k = 0; k < numVertices; ++k) {
            auto v_index = indexSet.subIndex(elem,k,dim);

            auto v_id = idSet.subId(elem,k,dim);

            auto it = id2index_.find(v_id);
            if (it != id2index_.end()) {
              if (!visited[v_index]) {
                ins(v_index, it->second) << 1;
                indexmap[it->second] = v_index;
              }

              int k1 = (k+1)%numVertices, k2 = (k+numVertices-1)%numVertices;
              double vol = elem.geometry().volume();
              volume[it->second] += vol;

              auto v1_index = indexSet.subIndex(elem,k1,dim);
              auto v2_index = indexSet.subIndex(elem,k2,dim);

              ins2(it->second, v_index)  << vol;
              ins2(it->second, v1_index) << vol;
              ins2(it->second, v2_index) << vol;

            } else {
              if (visited[v_index])
                continue;
              auto father = elem;
              auto pos = Dune::ReferenceElements<float_type,dim>::simplex().position(k,dim); // local coord of k-th vertex
              do {
                pos = elem.geometryInFather().global(pos);
                father = father.father();
              } while (father.isNew() && father.hasFather());

              Dune::FieldVector<float_type, dim+1> lambda;
              float_type sum = 0;
              for (int i = 0; i < dim; ++i) {
                lambda[i+1] = pos[i];
                sum += pos[i];
              }
              lambda[0] = 1 - sum;
              assert( lambda.size() == numVertices );
              for (int i = 0; i < numVertices; ++i) {
                auto it2 = id2index_.find(idSet.subId(father,i,dim));
                assert( it2 != id2index_.end() );
                ins(v_index, it2->second) << lambda[i];
              }
            }

            visited[v_index] = true;
          }
        }
      }

      using Matrix = DOFMatrix<float_type>::Matrix;
      for (int k = 0; k < mat2.outerSize(); ++k) {
        for (typename Matrix::InnerIterator it(mat2, k); it; ++it)
          it.valueRef() /= 3.0*volume[k];
      }
      return {std::move(mat), std::move(mat2), std::move(indexmap)};
    }

  private:

    GridBase const& grid_;
    int coarseLevel_;
    std::size_t coarseVertices_;

    std::map<typename GridBase::LocalIdSet::IdType, std::size_t> id2index_;
  };



} // end namespace Dec
