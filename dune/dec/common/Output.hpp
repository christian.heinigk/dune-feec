#pragma once

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include <dune/dec/common/Common.hpp>

/**
 * \def DEC_NO_THROW
 * \brief The preprocessor constant sets whether to use c-asserts (if defined) or
 * to throw an exception in case of an error (if not defined).
 **/
#ifdef DEC_NO_THROW
  #include <cassert>
#else
  #include <stdexcept>
#endif

#ifdef DEC_HAS_MPI
  #include <mpi.h>
#endif

/**
 * \def DEC_ENABLE_MSG_DBG
 * \brief The preprocessor constant enables the functions \ref Dec::msg_dbg
 * and \ref Dec::assert_msg_dbg.
 *
 * If the value is set to 1 the functions \ref Dec::msg_dbg and \ref Dec::assert_msg_dbg
 * are implemented, otherwise empty. Default is value 0 if \ref NDEBUG is not
 * defined, otherwise value 1.
 **/
#ifndef DEC_ENABLE_MSG_DBG
  #ifndef NDEBUG
    #define DEC_ENABLE_MSG_DBG 1
  #else
    #define DEC_ENABLE_MSG_DBG 0
  #endif
#endif


namespace Dec
{
  namespace tag
  {
    struct own_rank {};
    struct all_ranks {};

    template <class OStream> OStream& operator<<(OStream& out, own_rank) { return out; }
    template <class OStream> OStream& operator<<(OStream& out, all_ranks) { return out; }
  }

  namespace aux
  {
    template <class OStream>
    OStream& concatImpl(OStream& out) { return out; }

    template <class OStream, class Arg0, class... Args>
    OStream& concatImpl(OStream& out, Arg0&& arg0, Args&&... args)
    {
      out << arg0; concatImpl(out, std::forward<Args>(args)...);
      return out;
    }

    template <class... Args>
    std::string to_string(Args&&... args)
    {
      std::stringstream ss; concatImpl(ss, std::forward<Args>(args)...);
      return ss.str();
    }

    template <class OStream, class... Args>
    OStream& msgImpl(OStream& out, Args&&... args)
    {
#ifdef DEC_HAS_MPI
      int rank = -1;
      int num_ranks = -1;
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);
      MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

      bool print = rank == 0 || Types<Decay_t<Args>...>::contains(Type<tag::all_ranks>{});
      if (num_ranks > 1) {
        if (print)
          concatImpl(out, "[",rank,"] ",std::forward<Args>(args)...);
      } else {
        concatImpl(out, std::forward<Args>(args)...);
      }
#else
      concatImpl(out, std::forward<Args>(args)...);
#endif
      return out;
    }

    template <class /*tag*/>
    struct OnlyOnce
    {
      bool called = false;

      void done() { called = true; }
      void reset() { called = false; }

      operator bool() const { return called; }

      static OnlyOnce& create()
      {
        static OnlyOnce self;
        return self;
      }
    };

    struct FuncnameOnce
    {
      std::size_t ID = 0;

      static FuncnameOnce& create()
      {
        static FuncnameOnce self;
        return self;
      }
    };
  }

  /// \brief print a message
  /**
   * Example:
   * ```
   * msg("Hello ", "World: ", 123); // prints "Hello World: 123\n"
   * ```
   **/
  template <class... Args>
  void msg(Args&&... args)
  {
    aux::msgImpl(std::cout, std::forward<Args>(args)..., "\n");
  }


  /// print a message and in the first call per function also the funcname
  template <class... Args>
  void msg_funcname(std::string funcname, Args&&... args)
  {
    std::size_t funcname_hash = std::hash<std::string>{}(funcname);

    auto& once = aux::FuncnameOnce::create();
    if (once.ID != funcname_hash) {
      msg("\n", funcname, "(): ");
      once.ID = funcname_hash;
    }
    msg(std::string(4,' '), std::forward<Args>(args)...);
  }

#define DEC_MSG(...) Dec::msg_funcname(__func__, __VA_ARGS__);

  /// print message only once per Tag
  //* Requirement: Tag has static method name() *//
  template <class Tag, class... Args>
  void msg_once(Args&&... args)
  {
    auto& called = aux::OnlyOnce<Tag>::create();
    if (!called)
      aux::msgImpl(std::cout, Tag::name(), "::", std::forward<Args>(args)..., "\n");
    called.done();
  }

  /// \brief print a message (without appended newline)
  /**
   * Example:
   * ```
   * msg_("Hello ", "World: ", 123); // prints "Hello World: 123"
   * ```
   **/
  template <class... Args>
  void msg_(Args&&... args)
  {
    aux::msgImpl(std::cout, std::forward<Args>(args)...);
  }


  /// \brief print a message and exit
  /**
   * If the preprocessor constant \ref DEC_NO_THROW is defined,
   * the c-assert macro is called, otherwise an exception of
   * type \ref std::runtime_Error is thrown.
   **/
  template <class... Args>
  void error_exit(Args&&... args)
  {
#ifdef DEC_NO_THROW
    aux::msgImpl(std::cerr, "ERROR: ", std::forward<Args>(args)..., "\n");
    std::exit(EXIT_FAILURE);
#else
    throw std::runtime_error( aux::to_string("ERROR: ", std::forward<Args>(args)...) );
#endif
  }


  /// \brief test for condition and in case of failure print message and exit
  /**
   * This function is equivalent to
   * ```
   * if (condition == false) error_exit(text);
   * ```
   * where `text` correspond to the arguments passed after the
   * \p condition argument.
   **/
  template <class... Args>
  void assert_msg(bool condition, Args&&... args)
  {
    if (!condition) { error_exit(std::forward<Args>(args)...); }
  }


  /// \brief test for condition and in case of failure print message
  /**
   * Same as \ref assert_msg but does not throw an exception, or call assert.
   * It just tests for the condition and prints a message with prepended
   * string "WARNING".
   **/
  template <class... Args>
  void warn_msg(bool condition, Args&&... args)
  {
    if (!condition) { msg("WARNING: ", std::forward<Args>(args)...); }
  }


#if DEC_ENABLE_MSG_DBG
  /// \brief print message, in debug mode only
  /**
   * Same as \ref msg, but is available only if preprocessor constant
   * \ref DEC_ENABLE_MSG_DBG is set to 1, otherwise the function is empty.
   **/
  template <class... Args>
  void msg_dbg(Args&&... args) { msg(std::forward<Args>(args)...); }


  /// \brief call assert_msg, in debug mode only
  /**
   * Same as \ref assert_msg, but is available only if preprocessor constant
   * \ref DEC_ENABLE_MSG_DBG is set to 1, otherwise the function is empty.
   **/
  template <class... Args>
  void assert_msg_dbg(bool condition, Args&&... args)
  {
    assert_msg(condition, std::forward<Args>(args)...);
  }
#else
  template <class... Args>
  void msg_dbg(Args&&...) {}

  template <class... Args>
  void assert_msg_dbg(bool, Args&&...) {}
#endif

} // end namespace Dec
