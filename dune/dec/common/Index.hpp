#pragma once

#include <cstddef>
#include <type_traits>

namespace Dec
{
  /// A wrapper for int type
  template <int I>
  using int_t = std::integral_constant<int, I>;

  /// Variable template to generate int-type
  template <int I>
  constexpr int_t<I> int_ = {};

  /// A wrapper for std::size_t type
  template <std::size_t I>
  using index_t = std::integral_constant<std::size_t, I>;

  /// Variable template to generate std::size_t-type
  template <std::size_t I>
  constexpr index_t<I> index_ = {};

  namespace aux
  {
    /// A range of indices [I,J)
    template <class Int, Int I, Int J>
    struct range_impl
    {
      using type = range_impl;

      /// Return the first element in the range
      static constexpr auto begin() { return std::integral_constant<Int, I>{}; }

      /// Returns the element after the last element in the range
      static constexpr auto end() { return std::integral_constant<Int, J>{}; }

      /// Returns the ith index in the range as integral constant
      template <std::size_t i>
      constexpr auto operator[](index_t<i>) const
      {
        return std::integral_constant<Int, I+Int(i)>{};
      }

      /// Return whether the range is empty
      static constexpr bool empty() { return I >= J; }

      /// Returns the size of the range
      static constexpr auto size() { return std::integral_constant<Int, J-I>{}; }
    };

    /// Extracts the Ith element element from the range
    template <std::size_t I, class Int, Int begin, Int end>
    constexpr auto get(range_impl<Int, begin, end> const& r) { return r[index_<I>]; }

  } // end namespace aux

  template <int I, int J>
  using intrange_t = aux::range_impl<int, I, J>;

  /// variable template to generate ranges
  template <int I, int J>
  constexpr intrange_t<I,J> intrange_ = {};

  template <std::size_t I, std::size_t J>
  using range_t = aux::range_impl<std::size_t, I, J>;

  template <std::size_t I, std::size_t J>
  constexpr range_t<I,J> range_ = {};


  namespace literal
  {
    static constexpr index_t<0> _0 = {};
    static constexpr index_t<1> _1 = {};
    static constexpr index_t<2> _2 = {};
    static constexpr index_t<3> _3 = {};
    static constexpr index_t<4> _4 = {};
    static constexpr index_t<5> _5 = {};
    static constexpr index_t<6> _6 = {};
    static constexpr index_t<7> _7 = {};
    static constexpr index_t<8> _8 = {};
    static constexpr index_t<9> _9 = {};
  }

} // end namespace Dec
