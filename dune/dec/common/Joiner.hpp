#pragma once

#include <string>
#include <sstream>

#include <dune/dec/common/Output.hpp>

namespace Dec
{
  struct Joiner
  {
    Joiner(const char* separator, std::size_t len)
      : separator_{separator, len}
    {}

    template <class Container>
    std::string operator()(Container const& container) const
    {
      if (container.empty())
        return "";

      auto it = container.begin();
      std::stringstream ss; ss << *it;
      for (++it; it != container.end(); ++it)
        ss << separator_ << *it;

      return ss.str();
    }

    std::string separator_;
  };

  Joiner operator ""_join(const char* separator, std::size_t len)
  {
    return {separator, len};
  }

} // end namespace Dec
