#pragma once

#include <type_traits>

#include "ConceptsBase.hpp"

namespace Dec
{
  /**
    * \defgroup concept Concepts
    * \brief Concept definitions
    * @{
    **/
  namespace concepts
  {
#ifdef HAVE_CONCEPTS

    template <class A, class B>
    concept bool Convertible = requires(A a, B b) { {a} -> B; {b} -> A };

    /// \brief Argument A is less-than comparable to type B, i.e. A < B is valid
    template <class A, class B = A>
    concept bool LessThanComparable = requires(A a, B b) { {a<b} -> bool; };

    /// \brief Argument A is addable to type B, i.e. A + B is valid
    template <class A, class B>
    concept bool Addable = requires(A a, B b) { a+b; b+a; };

    /// \brief Argument A is subtractable by type B, i.e. A - B is valid
    template <class A, class B>
    concept bool Subtractable = requires(A a, B b) { a-b; };

    /// \brief Argument A is multiplicable by type B, i.e. A * B is valid
    template <class A, class B>
    concept bool Multiplicable = requires(A a, B b) { a*b; b*a; };

    /// \brief Argument A is divisible by type B, i.e. A / B is valid
    template <class A, class B>
    concept bool Divisible = requires(A a, B b) { a/b; };

    /// \brief Argument F can be called with parameters Args...
    template <class F, class... Args>
    concept bool Callable = requires(F f, Args... args) { f(args...); };

#else // HAVE_CONCEPTS

#ifndef DOXYGEN
    namespace definition
    {
      // a < b
      struct LessThanComparable
      {
        template <class A, class B>
        auto requires_(A&& a, B&& b) -> decltype( a < b );
      };

      // a + b
      struct Addable
      {
        template <class A, class B>
        auto requires_(A&& a, B&& b) -> decltype(
          concepts::valid_expr(
            a + b,
            b + a
          ));
      };

      // a - b
      struct Subtractable
      {
        template <class A, class B>
        auto requires_(A&& a, B&& b) -> decltype( a - b );
      };

      // a * b
      struct Multiplicable
      {
        template <class A, class B>
        auto requires_(A&& a, B&& b) -> decltype(
          concepts::valid_expr(
            a * b,
            b * a
          ));
      };

      // a / b
      struct Divisible
      {
        template <class A, class B>
        auto requires_(A&& a, B&& b) -> decltype( a / b );
      };

      // f(args...)
      struct Callable
      {
        template <class F, class... Args>
        auto requires_(F&& f, Args&&... args) -> decltype( f(std::forward<Args>(args)...));
      };

    } // end namespace definition

#endif // DOXYGEN

    /// \brief Argument `A` id (implicitly) convertible to arguemnt `B` and vice versa.
    template <class A, class B >
    constexpr bool Convertible = std::is_convertible<A,B>::value && std::is_convertible<B,A>::value;

    /// \brief Argument A is less-than comparable to type B, i.e. A < B is valid
    template <class A, class B = A>
    constexpr bool LessThanComparable = models<definition::LessThanComparable(A, B)>;

    /// \brief Argument A is addable to type B, i.e. A + B is valid
    template <class A, class B>
    constexpr bool Addable = models<definition::Addable(A, B)>;

    /// \brief Argument A is subtractable by type B, i.e. A - B is valid
    template <class A, class B>
    constexpr bool Subtractable = models<definition::Subtractable(A, B)>;

    /// \brief Argument A is multiplicable by type B, i.e. A * B is valid
    template <class A, class B>
    constexpr bool Multiplicable = models<definition::Multiplicable(A, B)>;

    /// \brief Argument A is divisible by type B, i.e. A / B is valid
    template <class A, class B>
    constexpr bool Divisible = models<definition::Divisible(A, B)>;

    /// \brief Argument F can be called with parameters Args...
    template <class F, class... Args>
    constexpr bool Callable = models<definition::Callable(F, Args...)>;

#endif // HAVE_CONCEPTS

    /** @} **/

  } // end namespace concepts

} // end namespace Dec
