#pragma once

#include <array>
#include <cmath>

#include "Math.hpp"
#include "Operations.hpp"

namespace Dec
{
  /**
    * \defgroup array_operations Array operations
    * \brief Arithmetic operations on std::array
    * @{
    **/

  // basic arithmetic operations on std::arrays

  /// The assignment-addition of an array
  template <class T, std::size_t N>
  std::array<T, N>& operator+=(std::array<T, N>& lhs, std::array<T, N> const& rhs)
  {
    for (std::size_t i = 0; i < N; ++i)
      lhs[i] += rhs[i];
    return lhs;
  }

  /// Elementwise addition of arrays
  template <class T, std::size_t N>
  std::array<T, N> operator+(std::array<T, N> lhs, std::array<T, N> const& rhs)
  {
    lhs+= rhs;
    return lhs;
  }

  /// The assignment-subtraction of an array
  template <class T, std::size_t N>
  std::array<T, N>& operator-=(std::array<T, N>& lhs, std::array<T, N> const& rhs)
  {
    for (std::size_t i = 0; i < N; ++i)
      lhs[i] -= rhs[i];
    return lhs;
  }

  /// Elementwise subtraction of arrays
  template <class T, std::size_t N>
  std::array<T, N> operator-(std::array<T, N> lhs, std::array<T, N> const& rhs)
  {
    lhs-= rhs;
    return lhs;
  }

  /// Elementwise negation of entries of the array
  template <class T, std::size_t N>
  std::array<T, N> operator-(std::array<T, N> x)
  {
    for (std::size_t i = 0; i < N; ++i)
      x[i] *= -1;
    return x;
  }

  /// The assignment-multiplication of an array
  template <class T, std::size_t N>
  std::array<T, N>& operator*=(std::array<T, N>& lhs, T factor)
  {
    for (std::size_t i = 0; i < N; ++i)
      lhs[i] *= factor;
    return lhs;
  }

  /// Elementwise multiplication of arrays by a scalar (from right)
  template <class T, std::size_t N>
  std::array<T, N> operator*(std::array<T, N> lhs, T factor)
  {
    lhs*= factor;
    return lhs;
  }

  /// Elementwise multiplication of arrays by a scalar (from left)
  template <class T, std::size_t N>
  std::array<T, N> operator*(T factor, std::array<T, N> lhs)
  {
    lhs*= factor;
    return lhs;
  }

  /// The assignment-division of an array
  template <class T, std::size_t N>
  std::array<T, N>& operator/=(std::array<T, N>& lhs, T factor)
  {
    for (std::size_t i = 0; i < N; ++i)
      lhs[i] /= factor;
    return lhs;
  }

  /// Elementwise division of arrays by a scalar (from right)
  template <class T, std::size_t N>
  std::array<T, N> operator/(std::array<T, N> lhs, T factor)
  {
    lhs/= factor;
    return lhs;
  }


  // ----------------------------------------------------------------------------


  /// Cross-product a 2d-vector = orthogonal vector
  template <class T>
  std::array<T, 2> cross(std::array<T, 2> const& a)
  {
    return {{ a[1], -a[0] }};
  }

  /// Cross-product of two vectors (in 3d only)
  template <class T>
  std::array<T, 3> cross(std::array<T, 3> const& a, std::array<T, 3> const& b)
  {
    return {{ a[1]*b[2] - a[2]*b[1],
              a[2]*b[0] - a[0]*b[2],
              a[0]*b[1] - a[1]*b[0] }};
  }

  /// Dot-product of two vectors = lhs^H * rhs
  template <class T, std::size_t N>
  T dot(std::array<T, N> const& lhs, std::array<T, N> const& rhs)
  {
    operation::hermitian<T> conj_op{};
    T result = 0;
    for (std::size_t i = 0; i < N; ++i)
      result += conj_op(lhs[i]) * rhs[i];
    return result;
  }

  // ----------------------------------------------------------------------------


  namespace aux
  {
    template <class T, std::size_t N, class Operation>
    T accumulate(std::array<T, N> const& x, Operation op)
    {
      T result = 0;
      for (std::size_t i = 0; i < N; ++i)
        result = op(result, x[i]);
      return result;
    }

  } // end namespace aux

  /// Sum of vector entires.
  template <class T, std::size_t N>
  T sum(std::array<T, N> const& x)
  {
    return aux::accumulate(x, operation::plus{});
  }


  /// Dot-product with the vector itself
  template <class T, std::size_t N>
  auto unary_dot(std::array<T, N> const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + sqr(std::abs(b)); };
    return aux::accumulate(x, op);
  }

  /// Maximum over all vector entries
  template <class T, std::size_t N>
  auto max(std::array<T, N> const& x)
  {
    return aux::accumulate(x, operation::maximum{});
  }

  /// Minimum over all vector entries
  template <class T, std::size_t N>
  auto min(std::array<T, N> const& x)
  {
    return aux::accumulate(x, operation::minimum{});
  }

  /// Maximum of the absolute values of vector entries
  template <class T, std::size_t N>
  auto abs_max(std::array<T, N> const& x)
  {
    return aux::accumulate(x, operation::abs_max{});
  }

  /// Minimum of the absolute values of vector entries
  template <class T, std::size_t N>
  auto abs_min(std::array<T, N> const& x)
  {
    return aux::accumulate(x, operation::abs_min{});
  }


  // ----------------------------------------------------------------------------


  /** \ingroup vector_norms
   *  \brief The 1-norm of a vector = sum_i |x_i|
   **/
  template <class T, std::size_t N>
  auto one_norm(std::array<T, N> const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + std::abs(b); };
    return aux::accumulate(x, op);
  }

  /** \ingroup vector_norms
   *  \brief The euklidean 2-norm of a vector = sqrt( sum_i |x_i|^2 )
   **/
  template <class T, std::size_t N>
  auto two_norm(std::array<T, N> const& x)
  {
    return std::sqrt(unary_dot(x));
  }

  /** \ingroup vector_norms
   *  \brief The p-norm of a vector = ( sum_i |x_i|^p )^(1/p)
   **/
  template <int p, class T, std::size_t N>
  auto p_norm(std::array<T, N> const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + pow<p>(std::abs(b)); };
    return std::pow( aux::accumulate(x, op), 1.0/p );
  }

  /** \ingroup vector_norms
   *  \brief The infty-norm of a vector = max_i |x_i| = alias for \ref abs_max
   **/
  template <class T, std::size_t N>
  auto infty_norm(std::array<T, N> const& x)
  {
    return abs_max(x);
  }


  // ----------------------------------------------------------------------------


  /// The euklidean distance between two vectors = |lhs-rhs|_2
  template <class T, std::size_t N>
  T distance(std::array<T, N> const& lhs, std::array<T, N> const& rhs)
  {
    T result = 0;
    for (std::size_t i = 0; i < N; ++i)
      result += sqr(lhs[i] - rhs[i]);
    return std::sqrt(result);
  }


  template <class CharT, class Traits, class T, std::size_t N>
  std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& out,
                                                std::array<T, N> const& array)
  {
    out << '[';
    if (N > 0)
      out << array[0];
    for (std::size_t i = 1; i < N; ++i)
      out << ',' << array[i];
    out << ']';
    return out;
  }


  /** @} **/


} // end namespace Dec
