#pragma once

#include "Common.hpp"
#include "ConceptsBase.hpp"
#include "Logical.hpp"

namespace Dec
{
  namespace traits
  {
    template <class A, class B>
    struct is_compatible
        : std::is_same<Decay_t<A>, Decay_t<B>>
    {};

    template <class A, class B>
    struct is_compatible<Types<A>, Types<B>>
        : is_compatible<A,B>
    {};

    template <>
    struct is_compatible<Types<>, Types<>>
        : std::true_type
    {};

    template <class A0, class... As, class B0, class... Bs>
    struct is_compatible<Types<A0,As...>, Types<B0,Bs...>>
        : and_t<is_compatible<A0, B0>::value, is_compatible<Types<As...>, Types<Bs...>>::value>
    {};

  } // end namespace traits


  namespace concepts
  {
    /// Types are the same, up to decay of qualifiers
    template <class A, class B>
    constexpr bool Compatible = traits::is_compatible<A, B>::value;

  } // end namespace concepts

} // end namespace Dec
