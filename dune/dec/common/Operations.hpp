#pragma once

#include "Concepts.hpp"
#include "ScalarTypes.hpp"
#include "Math.hpp"

namespace Dec
{
  namespace operation
  {
    /** \defgroup operations Operations
     *  \brief Functors, representing unary/binary operations used in expressions.
     *  @{
     **/

    /// Functor returning the argument directly
    struct id
    {
      template <class Iter>
      decltype(auto) operator()(Iter&& it) const { return std::forward<Iter>(it); }
    };

    /// Functor representing a pre-increment operator
    struct increment
    {
      template <class Iter, class... EndIter>
      void operator()(Iter& it, EndIter&&...) const { ++it; }
    };

    /// Functor representing a dereferencing operator
    struct dereferencing
    {
      template <class Iter>
      auto operator()(Iter it) const { return *it; }
    };

    struct assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S& to) const
      {
        to = from;
      }
    };

    /// Operation that represents A+B
    struct plus
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return lhs + rhs;
      }
    };

    struct plus_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S& to) const
      {
        to += from;
      }
    };

    /// Operation that represents A-B
    struct minus
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return lhs - rhs;
      }
    };

    struct minus_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S& to) const
      {
        to -= from;
      }
    };

    /// Operation that represents A*B
    struct times
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return lhs * rhs;
      }
    };

    struct times_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S& to) const
      {
        to *= from;
      }
    };

    /// Operation that represents A/B
    struct divides
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return lhs / rhs;
      }
    };

    struct divides_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S& to) const
      {
        to /= from;
      }
    };

    /// Operation that represents A-B
    struct negate
    {
      template <class T>
      constexpr auto operator()(T const& x) const
      {
        return -x;
      }
    };

    /// Operation that represents max(A,B)
    struct maximum
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return max(lhs, rhs);
      }
    };

    /// Operation that represents min(A,B)
    struct minimum
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return min(lhs, rhs);
      }
    };

    /// Operation that represents max(|A|,|B|)
    struct abs_max
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return max(std::abs(lhs), std::abs(rhs));
      }
    };

    /// Operation that represents min(|A|,|B|)
    struct abs_min
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return min(std::abs(lhs), std::abs(rhs));
      }
    };

    /// Operation that represents A*factor
    template <class S, class = Void_t<>>
    struct times_scalar;

#ifdef HAVE_CONCEPTS
    template <concepts::Arithmetic S>
    struct times_scalar<S>
#else
    template <class S>
    struct times_scalar<S, Requires_t<concepts::Arithmetic<S>> >
#endif
    {
      constexpr explicit times_scalar(S const& scalar) : scalar(scalar) {}

      template <class T,
        REQUIRES(concepts::Multiplicable<T,S>) >
      constexpr auto operator()(T const& lhs) const
      {
        return lhs * scalar;
      }

    private:
      S scalar;
    };

    /// Operation that represents A/factor
    template <class S, class = Void_t<>>
    struct divides_scalar;

#ifdef HAVE_CONCEPTS
    template <concepts::Arithmetic S>
    struct divides_scalar<S>
#else
    template <class S>
    struct divides_scalar<S, Requires_t<concepts::Arithmetic<S>> >
#endif
    {
      constexpr explicit divides_scalar(S const& scalar) : scalar(scalar) {}

      template <class T,
        REQUIRES(concepts::Divisible<T,S>) >
      constexpr auto operator()(T const& lhs) const
      {
        return lhs / scalar;
      }

    private:
      S scalar;
    };


    /// Operation that represents conj(A) or A, if A is complex or not, respectively
    template <class T, class = Void_t<>>
    struct hermitian
    {
      constexpr T operator()(T const& x) const
      {
        return std::conj(x);
      }
    };

#ifdef HAVE_CONCEPTS
    template <concepts::Arithmetic T>
    struct hermitian<T>
#else
    template <class T>
    struct hermitian<T, Requires_t<concepts::Arithmetic<T>> >
#endif
    {
      constexpr T const& operator()(T const& x) const
      {
        return x;
      }
    };

    /** @} **/

  } // end namespace operation

} // end namespace Dec
