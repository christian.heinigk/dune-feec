#pragma once

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include "Concepts.hpp"
#include "Math.hpp"
#include "Operations.hpp"
#include "ScalarTypes.hpp"

namespace Dec
{
  // some arithmetic operations with Dune::FieldVector

  template <class T, int N, class S,
    REQUIRES(concepts::Arithmetic<S>) >
  Dune::FieldVector<T,N> operator*(Dune::FieldVector<T,N> v, S factor)
  {
    return v *= factor;
  }

  template <class S, class T, int N,
    REQUIRES(concepts::Arithmetic<S>) >
  Dune::FieldVector<T,N> operator*(S factor, Dune::FieldVector<T,N> v)
  {
    return v *= factor;
  }

  template <class T, int N, class S,
    REQUIRES(concepts::Arithmetic<S>) >
  Dune::FieldVector<T,N> operator/(Dune::FieldVector<T,N> v, S factor)
  {
    return v /= factor;
  }

  // some arithmetic operations with Dune::FieldMatrix

  template <class T, int N, int M>
  Dune::FieldMatrix<T,N,M> operator+(Dune::FieldMatrix<T,N,M> A, Dune::FieldMatrix<T,N,M> const& B)
  {
    return A += B;
  }

  template <class T, int N, int M>
  Dune::FieldMatrix<T,N,M> operator-(Dune::FieldMatrix<T,N,M> A, Dune::FieldMatrix<T,N,M> const& B)
  {
    return A -= B;
  }

  // ----------------------------------------------------------------------------

  /// Cross-product a 2d-vector = orthogonal vector
  template <class T>
  Dune::FieldVector<T, 2> cross(Dune::FieldVector<T, 2> const& a)
  {
    return {{ a[1], -a[0] }};
  }

  /// Cross-product of two vectors (in 3d only)
  template <class T>
  Dune::FieldVector<T, 3> cross(Dune::FieldVector<T, 3> const& a, Dune::FieldVector<T, 3> const& b)
  {
    return {{ a[1]*b[2] - a[2]*b[1],
              a[2]*b[0] - a[0]*b[2],
              a[0]*b[1] - a[1]*b[0] }};
  }

  /// Dot product (vec1^T * vec2)
  template <class T, class S, int N>
  auto dot(Dune::FieldVector<T,N> const& vec1, Dune::FieldVector<S,N> const& vec2)
  {
    return vec1.dot(vec2);
  }

  // ----------------------------------------------------------------------------

  namespace aux
  {
    template <class T, int N, class Operation>
    T accumulate(Dune::FieldVector<T, N> const& x, Operation op)
    {
      T result = 0;
      for (int i = 0; i < N; ++i)
        result = op(result, x[i]);
      return result;
    }

  } // end namespace aux

  /// Sum of vector entires.
  template <class T, int N>
  T sum(Dune::FieldVector<T, N> const& x)
  {
    return aux::accumulate(x, operation::plus{});
  }


  /// Dot-product with the vector itself
  template <class T, int N>
  auto unary_dot(Dune::FieldVector<T, N> const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + sqr(std::abs(b)); };
    return aux::accumulate(x, op);
  }

  /// Maximum over all vector entries
  template <class T, int N>
  auto max(Dune::FieldVector<T, N> const& x)
  {
    return aux::accumulate(x, operation::maximum{});
  }

  /// Minimum over all vector entries
  template <class T, int N>
  auto min(Dune::FieldVector<T, N> const& x)
  {
    return aux::accumulate(x, operation::minimum{});
  }

  /// Maximum of the absolute values of vector entries
  template <class T, int N>
  auto abs_max(Dune::FieldVector<T, N> const& x)
  {
    return aux::accumulate(x, operation::abs_max{});
  }

  /// Minimum of the absolute values of vector entries
  template <class T, int N>
  auto abs_min(Dune::FieldVector<T, N> const& x)
  {
    return aux::accumulate(x, operation::abs_min{});
  }

  // ----------------------------------------------------------------------------

  /** \ingroup vector_norms
   *  \brief The 1-norm of a vector = sum_i |x_i|
   **/
  template <class T, int N>
  auto one_norm(Dune::FieldVector<T, N> const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + std::abs(b); };
    return aux::accumulate(x, op);
  }

  /** \ingroup vector_norms
   *  \brief The euklidean 2-norm of a vector = sqrt( sum_i |x_i|^2 )
   **/
  template <class T, int N>
  auto two_norm(Dune::FieldVector<T, N> const& x)
  {
    return std::sqrt(unary_dot(x));
  }

  /** \ingroup vector_norms
   *  \brief The p-norm of a vector = ( sum_i |x_i|^p )^(1/p)
   **/
  template <int p, class T, int N>
  auto p_norm(Dune::FieldVector<T, N> const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + pow<p>(std::abs(b)); };
    return std::pow( aux::accumulate(x, op), 1.0/p );
  }

  /** \ingroup vector_norms
   *  \brief The infty-norm of a vector = max_i |x_i| = alias for \ref abs_max
   **/
  template <class T, int N>
  auto infty_norm(Dune::FieldVector<T, N> const& x)
  {
    return abs_max(x);
  }

  // ----------------------------------------------------------------------------

  /// The euklidean distance between two vectors = |lhs-rhs|_2
  template <class T, int N>
  T distance(Dune::FieldVector<T, N> const& lhs, Dune::FieldVector<T, N> const& rhs)
  {
    T result = 0;
    for (int i = 0; i < N; ++i)
      result += sqr(lhs[i] - rhs[i]);
    return std::sqrt(result);
  }

  // ----------------------------------------------------------------------------

  /// Outer product (vec1 * vec2^T)
  template <class T, class S, int N, int M, int K>
  auto outer(Dune::FieldMatrix<T,N,K> const& vec1, Dune::FieldMatrix<S,M,K> const& vec2)
  {
    using result_type = Dune::FieldMatrix<decltype( std::declval<T>() * std::declval<S>() ), N, M>;
    result_type mat;
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < M; ++j)
        mat[i][j] = vec1[i].dot(vec2[j]);
    return mat;
  }

  // ----------------------------------------------------------------------------

  template <class T>
  T det(Dune::FieldMatrix<T, 0, 0> const& /*mat*/)
  {
    return 0;
  }

  /// Determinant of a 1x1 matrix
  template <class T>
  T det(Dune::FieldMatrix<T, 1, 1> const& mat)
  {
    return mat[0][0];
  }

  /// Determinant of a 2x2 matrix
  template <class T>
  T det(Dune::FieldMatrix<T, 2, 2> const& mat)
  {
    return mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0];
  }

  /// Determinant of a 3x3 matrix
  template <class T>
  T det(Dune::FieldMatrix<T, 3, 3> const& mat)
  {
    return mat[0][0]*mat[1][1]*mat[2][2] + mat[0][1]*mat[1][2]*mat[2][0] + mat[0][2]*mat[1][0]*mat[2][1]
        - (mat[0][2]*mat[1][1]*mat[2][0] + mat[0][1]*mat[1][0]*mat[2][2] + mat[0][0]*mat[1][2]*mat[2][1]);
  }

  /// Determinant of a NxN matrix
  template <class T,  int N>
  T det(Dune::FieldMatrix<T, N, N> const& mat)
  {
    return mat.determinant();
  }

  /// Return the inverse of the matrix `mat`
  template <class T, int N>
  auto inv(Dune::FieldMatrix<T, N, N> mat)
  {
    mat.invert();
    return mat;
  }

  /// Solve the linear system A*x = b
  template <class T, int N>
  void solve(Dune::FieldMatrix<T, N, N> const& A,  Dune::FieldVector<T, N>& x,  Dune::FieldVector<T, N> const& b)
  {
    A.solve(x, b);
  }


  /// Gramian determinant = sqrt( det( DT^T * DF ) )
  template <class T, int N, int M>
  T gramian(Dune::FieldMatrix<T,N,M> const& DF)
  {
    using std::sqrt;
    return sqrt( det(outer(DF, DF)) );
  }

  /// Gramian determinant, specialization for 1 column matrices
  template <class T, int M>
  T gramian(Dune::FieldMatrix<T, 1, M> const& DF)
  {
    using std::sqrt;
    return sqrt(dot(DF[0], DF[0]));
  }

} // end namespace Dec
