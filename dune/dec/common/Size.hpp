#pragma once

#include <array>
#include <tuple>

#include "Common.hpp"
#include "Index.hpp"
#include "ScalarTypes.hpp"

namespace Dune
{
  template <class T, int N>
  class FieldVector;

  template <class T, int N, int M>
  class FieldMatrix;
}

namespace Dec
{
  template <class T, std::size_t N, std::size_t M>
  class FixMat;

  template <class Model, class T, std::size_t N, std::size_t M>
  struct MatExpr;

  // Get the number of elements in a tuple / pair / array / ...
  //@{
  namespace aux
  {
    template <class Tuple, class = Void_t<>>
    struct SizeImpl : index_t<0> {};

    template <class... Args>
    struct SizeImpl<std::tuple<Args...>>
        : index_t< sizeof...(Args) > {};

    template <class Arg0, class Arg1>
    struct SizeImpl<std::pair<Arg0, Arg1>>
        : index_t<2> {};

    template <class T, std::size_t N>
    struct SizeImpl<std::array<T,N>>
        : index_t<N> {};

    template <class T, int N>
    struct SizeImpl<Dune::FieldVector<T,N>>
        : index_t<N> {};

    template <class T, int N, int M>
    struct SizeImpl<Dune::FieldMatrix<T,N,M>>
        : index_t<N*M> {};


    // Specialization for arithmetic types
#ifdef HAVE_CONCEPTS
    template <concepts::Arithmetic T>
    struct SizeImpl<T>
#else
    template <class T>
    struct SizeImpl<T, Requires_t<concepts::Arithmetic<T>> >
#endif
        : index_t<1> {};

  } // end namespace aux
  //@}


  template <class T>
  constexpr std::size_t Size = aux::SizeImpl<Decay_t<T>>::value;


} // end namespace Dec
