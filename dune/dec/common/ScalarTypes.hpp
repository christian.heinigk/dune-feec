#pragma once

// std c++ headers
#include <complex>
#include <type_traits>

#include "ConceptsBase.hpp"

namespace Dec
{
  namespace traits
  {
    template <class T>
    struct is_integral
        : std::is_integral<std::decay_t<T>>
    {};

    template <class T>
    struct is_arithmetic
        : std::is_arithmetic<std::decay_t<T>>
    {};

    template <class T>
    struct is_complex
        : std::false_type
    {};

    template <class T>
    struct is_complex<std::complex<T>>
        : is_arithmetic<T>
    {};

  } // end namespace traits


  namespace concepts
  {
    /** \addtogroup concept
     *  @{
     **/

    /** \brief The types following the std type-trait \ref std::is_integral are
     *  categorized as *integral types*.
     **/
    template <class T>
    constexpr bool Integral = traits::is_integral<T>::value;

    /** \brief The types following the std type-trait \ref std::is_arithmetic are
     *  categorized as *arithmetic types*.
     **/
    template <class T>
    constexpr bool Arithmetic = traits::is_arithmetic<T>::value;

    /// \brief A concept for types std::complex<Arithmetic>
    template <class T>
    constexpr bool Complex = traits::is_complex<T>::value;

    /** @} **/

  } // end namespace concepts

} // end namespace Dec
