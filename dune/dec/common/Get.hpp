#pragma once

#include <array>
#include <tuple>
#include <type_traits>

namespace Dec
{
  template <class T, std::size_t N, std::size_t M>
  class FixMat;

  namespace aux
  {
    /// Get i'th element of tuple / pair / array / FixMat
    template <std::size_t idx, class T>
    struct Get
    {
      using type = T;
    };

    template <std::size_t idx, class... Args>
    struct Get<idx, std::tuple<Args...>>
    {
      using type = typename std::tuple_element<idx, std::tuple<Args...>>::type;
    };

    template <std::size_t idx, class Arg0, class Arg1>
    struct Get<idx, std::pair<Arg0, Arg1>>
    {
      using type = typename std::conditional<idx == 0, Arg0, Arg1>::type;
    };

    template <std::size_t idx, class T, std::size_t N>
    struct Get<idx, std::array<T, N>>
    {
      using type = T;
    };

    template <std::size_t idx, class T, std::size_t N, std::size_t M>
    struct Get<idx, FixMat<T,N,M>>
    {
      using type = T;
    };

  } //end namespace aux

  template <std::size_t idx, class Tuple>
  using Get = typename aux::Get<idx, Tuple>::type;

} // end namespace Dec
