#pragma once

#include <type_traits>

#include "Common.hpp"

#if defined(DOXYGEN)
  #define REQUIRES(...)
  #define CONCEPT constexpr
#else
  #define REQUIRES(...) std::enable_if_t<__VA_ARGS__ , int>* = nullptr
  #define CONCEPT constexpr
#endif

namespace Dec
{
  struct valid {};
  constexpr valid valid_ = {};

  struct invalid { invalid() = delete; };

  // Helper function for use in concept definitions.
  // This will always evaluate to true. If just allow
  // to turn a type into an expression. The failure happens
  // already during substitution for the type argument.
  template<typename T>
  constexpr std::true_type requires_type()
  {
    return {};
  }

  template <bool C, class T = void>
  using Requires_t = typename std::enable_if<C, T>::type;

  template <bool C>
  using requires_t = std::conditional_t<C, valid, invalid>;

  namespace concepts
  {
    namespace _aux
    {
      template <class Concept, class = Void_t<>>
      struct models
          : std::false_type
      {};

      template <class Concept, class... Ts>
      struct models<Concept(Ts...), Void_t< decltype(std::declval<Concept>().requires_(std::declval<Ts>()...)) >>
          : std::true_type
      {};

      struct valid_expr
      {
        template <class... Ts>
        void operator()(Ts&&...) const;

#if defined(__GNUC__) && !defined(__clang__)
        template <class... Ts>
        void operator()(Ts const&...) const;
#endif
      };

    } // end namespace _aux

    template <class Concept>
    constexpr bool models = _aux::models<Concept>::value;

    constexpr _aux::valid_expr valid_expr = {};

  } // end namespace concepts

} // end namespace Dec
