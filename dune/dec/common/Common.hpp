#pragma once

#include <type_traits>
#if DEC_HAS_GSL
#include <gsl/gsl>
#else
#include <cassert>
namespace gsl {
#define Expects(...) assert(__VA_ARGS__)
#define Ensures(...) assert(__VA_ARGS__)
}
#endif

namespace Dec
{
  namespace aux
  {
    // Workaround for MSVC (problems with alias templates in pack expansion)
    template <class, class T>
    struct InvokeType { using type = T; };

    // Workaround for MSVC (problems with alias templates in pack expansion)
    template <class... Args>
    struct MakeVoid { using type = void; };

#ifdef HAVE_CONCEPTS
    template <class T> struct ValueType;
    template <class T>
      requires requires() { typename T::value_type; }
    struct ValueType<T>
    {
      using type = typename T::value_type;
    };
#else
    template <class T>
    struct ValueType
    {
      using type = typename T::value_type;
    };
#endif

    template <class T>
    struct ValueType<T*>
    {
      using type = T;
    };

    template <class T, class = void>
    struct StoreType
    {
      using type = T;
    };


    template <class T, class = void>
    struct FieldType
    {
      using type = T;
    };

    template <class T>
    struct FieldType<T, typename MakeVoid<typename T::field_type>::type>
    {
      using type = typename T::field_type;
    };

    template <class T>
    struct FieldType<T, typename MakeVoid<typename T::value_type>::type>
    {
      using type = typename T::value_type;
    };

    template <class T>
    struct FieldType<T*>
    {
      using type = T;
    };
  }

  template <class T>
  using Decay_t = typename aux::InvokeType<T, typename std::decay<T>::type>::type;

  template <class F, class... Args>
  using ResultOf_t = typename std::result_of<F(Args...)>::type;

  template <class T>
  using Value_t = typename aux::ValueType<Decay_t<T>>::type;

  template <class T>
  using FieldType_t = typename aux::FieldType<Decay_t<T>>::type;

  template <class... Args>
  using Void_t = typename aux::MakeVoid<Args...>::type;

  template <class T>
  using Store_t = typename aux::StoreType<Decay_t<T>>::type;

  /// Identity type wrapper, represents the type itself
  template <class T>
  struct Type
  {
    using type = T;
  };

  /// Variadic type list
  template <class... Ts>
  struct Types
  {
    template <bool...> struct _bools {};

    template <class T>
    static constexpr bool contains(Type<T> = {})
    {
      return not std::is_same< _bools<false, std::is_same<T,Ts>::value...>,
                               _bools<std::is_same<T,Ts>::value..., false> >::value;
    }
  };

  template <class... Ts>
  using Types_t = Types<Decay_t<Ts>...>;

  template <class T>
  using owner = T;

  namespace tag
  {
    struct ressource {};
    struct store {};
  }

} // end namespace Dec
