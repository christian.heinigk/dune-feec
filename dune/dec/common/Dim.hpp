#pragma once

#include <dune/geometry/dimension.hh>

namespace Dec
{
  template <int d>
  using Dim_t = Dune::Dim<d>;

  template <int d>
  constexpr Dim_t<d> Dim_ = {};

  template <int cd>
  using Codim_t = Dune::Codim<cd>;

  template <int cd>
  constexpr Codim_t<cd> Codim_ = {};

} // end namespace Dec
