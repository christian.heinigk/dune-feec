#pragma once

#include <string>

#define DEC_GET_CHAR( x, n ) (( 0##n < sizeof( x ) ) ? x[ 0##n ] : char(0))

#define DEC_GET_CHARS_8( x, n ) \
  DEC_GET_CHAR(x, n##0), DEC_GET_CHAR(x, n##1), DEC_GET_CHAR(x, n##2), DEC_GET_CHAR(x, n##3), \
  DEC_GET_CHAR(x, n##4), DEC_GET_CHAR(x, n##5), DEC_GET_CHAR(x, n##6), DEC_GET_CHAR(x, n##7)

#define DEC_GET_CHARS_64(x) \
  DEC_GET_CHARS_8(x, 0), DEC_GET_CHARS_8(x, 1), DEC_GET_CHARS_8(x, 2), DEC_GET_CHARS_8(x, 3), \
  DEC_GET_CHARS_8(x, 4), DEC_GET_CHARS_8(x, 5), DEC_GET_CHARS_8(x, 6), DEC_GET_CHARS_8(x, 7)

#define TAG(x) TagWithName<char_string<DEC_GET_CHARS_64( x )>, __COUNTER__>

namespace Dec
{
  namespace tag {}

  template <char... cs>
  struct char_string {};

  template <class T>
  struct Tag_t { using type = T; };

  template <class T>
  constexpr Tag_t<T> Tag_ = {};

  template <int p>
  struct PriorityTag_t : public PriorityTag_t<p-1> { static_assert( p <= 42, "" ); };

  template <>
  struct PriorityTag_t<0> {};

  template <int p>
  constexpr PriorityTag_t<p> PriorityTag_ = {};

  constexpr PriorityTag_t<42> Call_ = {};

  template <class T, class tags>
  struct TagInList {
    // TODO: implement a test whether T is in tag list `tags`
  };


  template <class Tag, int ID>
  struct TagWithName;

  template <char... tag_chars, int ID>
  struct TagWithName<char_string<tag_chars...>, ID>
  {
    static std::string const name()
    {
      const char tag_str[] = {tag_chars...};
      return tag_str;
    }
  };


  template <char... cs>
  constexpr auto operator"" _tag()
  {
    return TagWithName<char_string<cs...>, sizeof...(cs)>{};
  }



} // end namespace Dec
