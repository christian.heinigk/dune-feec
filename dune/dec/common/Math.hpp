#pragma once

#include <cmath>
#include <type_traits>

#include <dune/dec/utility/Cast.hpp>

namespace Dec
{
  namespace constant
  {
    template <class T> struct pi { static const T value; };
    template <class T> struct e { static const T value; };
    template <class T> struct sqrt2 { static const T value; };
    template <class T> struct third { static const T value; };
    template <class T> struct two_third { static const T value; };

    template <class T>
    const T pi<T>::value = cast<T>("3.141592653589793238462643383279502884197169399375105820974944592307816406286");

    template <class T>
    const T e<T>::value  = cast<T>("2.718281828459045235360287471352662497757247093699959574966967627724076630353");

    template <class T>
    const T sqrt2<T>::value  = cast<T>("1.414213562373095048801688724209698078569671875376948073176679737990732478462");

    template <class T>
    const T third<T>::value  = cast<T>("0.333333333333333333333333333333333333333333333333333333333333333333333333333");

    template <class T>
    const T two_third<T>::value  = cast<T>("0.666666666666666666666666666666666666666666666666666666666666666666666666667");

  } // end namespace constant


  /// Return the circle number pi \p T
  template <class T = double>
  constexpr T pi() { return constant::pi<T>::value; }

  /// Return the euler number exp(1) \p T
  template <class T = double>
  constexpr T e() { return constant::e<T>::value; }

  /// Return the value sqrt(2) in type \p T
  template <class T = double>
  constexpr T sqrt2() { return constant::sqrt2<T>::value; }

  /// Return the value 1/3 in type \p T
  template <class T = double>
  constexpr T third() { return constant::third<T>::value; }

  /// Return the value 2/3 in type \p T
  template <class T = double>
  constexpr T two_third() { return constant::two_third<T>::value; }


  template <class T>
  T sign(T x) { return x > 0 ? T(1) : (x < 0 ? T(-1) : T(0)); }

  template <class T>
  T sqr(T x) { return x*x; }

  template <class T>
  T pow3(T x) { return x*x*x; }

  namespace aux
  {
    template <class T>
    T pow(T x, std::integral_constant<int, 1>) { return x; }

    template <class T>
    T pow(T /*x*/, std::integral_constant<int, 0>) { return T(1); }

    template <int p, class T>
    T pow(T x, std::integral_constant<int, p>) { return x * pow(x, std::integral_constant<int, p-1>{}); }
  }

  template <int p, class T>
  T pow(T x) { return aux::pow(x, std::integral_constant<int, p>{}); }

  /// Assign a random value in the interval [mean-amplitude/2, mean+amplitude/2] to the argument \p x
  template <class T>
  T& random(T& x, T mean = 0, T amplitude = 2)
  {
    return x = mean + amplitude * (std::rand()/T(RAND_MAX) - T(0.5));
  }


  template <class T1, class T2>
  auto min(T1 const& a, T2 const& b)
  {
    using T = std::common_type_t<T1,T2>;
    return T(a) < T(b) ? a : b;
  }

  template <class T1, class T2>
  auto max(T1 const& a, T2 const& b)
  {
    using T = std::common_type_t<T1,T2>;
    return T(a) < T(b) ? b : a;
  }

  constexpr long factorial(long n, long k = 1l)
  {
    return n <= k ? 1l : n*factorial(n-1l, k);
  }

  constexpr long kombinations(long n, long k)
  {
    return factorial(n,k) / factorial(n-k);
  }

} // end namespace Dec
