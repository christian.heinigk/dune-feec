#pragma once

#include <type_traits>

namespace Dec
{
  /// A wrapper for bool type
  template <bool B>
  using bool_t = std::integral_constant<bool, B>;

  /// Variable template to generate bool-type
  template <bool B>
  constexpr bool_t<B> bool_ = {};

  template <bool B, class A0, class A1>
  using if_ = std::conditional_t<B, A0, A1>;

  // some boolean operations
  // ---------------------------------------------------------------------------

  namespace aux
  {
    template <bool...> struct all_helper {};

  } // end namespace aux

  template <bool... Bs>
  using all_of_t = std::is_same<aux::all_helper<true, Bs...>, aux::all_helper<Bs..., true>>;

  template <bool... Bs>
  using and_t = all_of_t<Bs...>;

  template <bool... Bs>
  using none_of_t = std::is_same<aux::all_helper<false, Bs...>, aux::all_helper<Bs..., false>>;

  template <bool... Bs>
  using any_of_t = bool_t<not none_of_t<Bs...>::value>;

  template <bool... Bs>
  using or_t = any_of_t<Bs...>;


  template <bool... Bs>
  constexpr bool_t<and_t<Bs...>::value> and_ = {};

  template <bool B0, bool B1>
  constexpr bool_t<B0 && B1> operator&&(bool_t<B0>, bool_t<B1>) { return {}; }


  template <bool... Bs>
  constexpr bool_t<or_t<Bs...>::value> or_ = {};

  template <bool B0, bool B1>
  constexpr bool_t<B0 || B1> operator||(bool_t<B0>, bool_t<B1>) { return {}; }


  template <bool B>
  using not_t = bool_t<!B>;

  template <bool B>
  constexpr bool_t<not_t<B>::value> not_ = {};

  template <bool B>
  constexpr bool_t<!B> operator!(bool_t<B>) { return {}; }


} // end namespace Dec
