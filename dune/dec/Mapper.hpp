#pragma once

#include <vector>
#include <numeric>

namespace Dec
{
  struct IdentityMapper
  {
    template <class T>
    inline T row(T r) const { return r; }

    template <class T>
    inline T col(T c) const { return c; }
  };


  struct BlockMapper
  {
    BlockMapper(std::vector<std::size_t> sizes = {})
      : shift_(sizes.size())
    {
      for (std::size_t i = 0; i < sizes.size(); ++i)
        shift_[i] = std::accumulate(sizes.begin(), sizes.begin()+i, 0u);
      size_ = std::accumulate(sizes.begin(), sizes.end(), 0u);
    }

    template <class T>
    T row(T r) const { return row_shift + r; }

    template <class T>
    T col(T c) const { return col_shift + c; }

    void block(std::size_t i, std::size_t j = 0)
    {
      Expects( i < blocks() && j < blocks() );
      row_shift = shift_[i];
      col_shift = shift_[j];
    }

    std::size_t blocks() const
    {
      return shift_.size();
    }

    std::size_t shift(std::size_t i) const
    {
      Expects( i < blocks() );
      return shift_[i];
    }

    std::size_t size(std::size_t i) const
    {
      Expects( i < blocks() );
      return (i+1 < shift_.size() ? shift_[i+1] : size_) - shift_[i];
    }

    std::size_t size() const
    {
      return size_;
    }

  private:
    std::size_t row_shift = 0u;
    std::size_t col_shift = 0u;

    std::vector<std::size_t> shift_;
    std::size_t size_;
  };

} // end namespace Dec
