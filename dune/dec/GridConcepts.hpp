#pragma once

#include <dune/dec/common/ConceptsBase.hpp>

namespace Dec
{

  namespace concepts
  {
#ifndef DOXYGEN
    namespace definition
    {
      struct Grid
      {
        template <class G, int dim = G::dimension, int dow = G::dimensionworld>
        auto requires_(G&& grid) -> decltype(
          concepts::valid_expr(
            grid.leafGridView(),
            grid.leafIndexSet(),
            grid.levelGridView(int(0)),
            grid.levelIndexSet(int(0)),
            grid.globalRefine(int(0)),
            grid.maxLevel()
          ));
      };

      struct GridView
      {
        template <class GV, int dim = GV::dimension>
        auto requires_(GV&& gv) -> decltype(
          concepts::valid_expr(
            gv.grid(),
            gv.indexSet(),
            gv.size(int(0)),
            gv.template begin<0>(),
            gv.template end<0>(),
            gv.level()
          ));
      };

    } // end namespace definition
#endif

    /** \addtogroup concept
     *  @{
     **/

    /// \brief Argument `G` is a grid object.
    /**
     * Requirement:
     * - ...
     **/
    template <class G>
    constexpr bool Grid = models<definition::Grid(G)>;

    /// \brief Argument `GV` is a grid-view object.
    /**
     * Requirement:
     * - ...
     **/
    template <class GV>
    constexpr bool GridView = models<definition::GridView(GV)>;

    /** @} */

  } // end namespace concepts


} // end namespace Dec
