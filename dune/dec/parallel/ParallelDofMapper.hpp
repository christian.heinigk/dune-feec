#pragma once

#include <algorithm>
#include <tuple>
#include <vector>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/partitionset.hh>

#include <dune/dec/common/Common.hpp>

#include "EntityOwner.hpp"

namespace Dec
{
  template <int codim>
  class LocalCodimMapper
  {
  public:

    static constexpr Dune::PartitionIteratorType partitionIterator = Dune::All_Partition;
    using PartitionSet = decltype(Dune::partitionSet<partitionIterator>());

  public:

    /// Constructor
    template <class BaseGridView>
    LocalCodimMapper(BaseGridView const& gv)
    {
      update(gv);
    }


    /// Returns true if the entity is contained in the index set and at the same time the array index is returned.
    template <class Entity>
    bool contains(Entity const& e, int& result) const
    {
      if (Entity::codimension != codim)
        return false;
      if (!PartitionSet{}.contains(e.partitionType()))
        return false;

      result = int(index(e));
      return true;
    }


    /// Entity is owned by current rank
    template <class Entity,
      class = Void_t<decltype(std::declval<Entity>().index())> >
    bool owns(Entity const& e) const
    {
      static_assert( Entity::codimension == codim, "" );
      return owns(e.index());
    }

    bool owns(std::size_t i) const
    {
      assert( i < owned_.size() );
      return owned_[i];
    }


    /// Reinitialize mapper after grid has been modified.
    template <class BaseGridView>
    void update(BaseGridView const& gv)
    {
      std::vector<int> rank;
      EntityOwner<BaseGridView, codim> entityOwner(gv, rank);
      gv.communicate(entityOwner, Dune::All_All_Interface, Dune::ForwardCommunication);

      owned_.resize(rank.size());
      std::transform(rank.begin(), rank.end(), owned_.begin(), [r=gv.comm().rank(), n=entityOwner.notOwned](int min_r)
      {
        return (min_r == n || min_r == r);
      });
    }

    std::size_t memory() const
    {
      return sizeof(owned_);
    }

  private:

    // the processor-rank that owns a specific entity
    std::vector<bool> owned_;
  };


  using ParallelDofMapper = std::tuple<LocalCodimMapper<0>, LocalCodimMapper<1>, LocalCodimMapper<2>>;

} // end namespace Dec
