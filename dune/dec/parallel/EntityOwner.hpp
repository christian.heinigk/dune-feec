#pragma once

#include <limits>
#include <vector>

#include <dune/grid/common/datahandleif.hh>

namespace Dec
{
  template <class BaseGridView, int cd>
  class EntityOwner
      : public Dune::CommDataHandleIF<EntityOwner<BaseGridView, cd>, int>
  {
  public:
    const int notOwned = std::numeric_limits<int>::max();

    EntityOwner(BaseGridView const& gv, std::vector<int>& rank)
      : gv_(gv)
      , rank_(rank)
    {
      rank_.clear();
      rank_.resize(gv.size(cd), notOwned);
    }

    bool contains(int /*dim*/, int codim) const
    {
      return codim == cd;
    }

    bool fixedSize(int /*dim*/, int /*codim*/) const
    {
      return true;
    }

    template <class BaseEntity>
    std::size_t size(BaseEntity const& /*e_*/) const
    {
      return 1u;
    }

    template <class BaseEntity>
    int myrank(BaseEntity const& e_) const
    {
      return e_.partitionType() == Dune::GhostEntity ? notOwned : gv_.comm().rank();
    }

    template <class MessageBuffer, class BaseEntity>
    void gather(MessageBuffer& buff, BaseEntity const& e_) const
    {
      buff.write(myrank(e_));
    }

    template <class MessageBuffer, class BaseEntity>
    void scatter(MessageBuffer& buff, BaseEntity const& e_, std::size_t n)
    {
      assert( n == 1 );
      int other_rank = -1;
      buff.read(other_rank);

      auto const& indexSet = gv_.indexSet();
      auto idx = indexSet.index(e_);
      rank_[idx] = std::min({myrank(e_), rank_[idx], other_rank});
    }

  private:
    BaseGridView gv_;
    std::vector<int>& rank_;
  };

} // end namespace Dec
