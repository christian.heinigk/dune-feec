#pragma once

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/common/Common.hpp>

#include "DistributedVector.hpp"

namespace Dec
{
  template <class T, class GridView, int cd_row, int cd_col = cd_row>
  class DistributedMatrix
  {
  public:

    using value_type = typename DOFMatrix<T>::value_type;
    using size_type = typename DOFMatrix<T>::size_type;


  public:

    // construct a local matrix with default size
    DistributedMatrix(GridView const& gv)
      : gv_(gv)
      , localMatrix_(gv.size(cd_row), gv.size(cd_col))
    {}

    // take a copy of an other local matrix
    template <class LocalMatrix,
      REQUIRES(std::is_same<Value_t<LocalMatrix>, value_type>::value)>
    DistributedMatrix(GridView const& gv, LocalMatrix&& localMatrix)
      : gv_(gv)
      , localMatrix_(std::forward<LocalMatrix>(localMatrix))
    {}

    // return product A*x
    auto operator*(DistributedVector<T,GridView,cd_col> const& x) const
    {
      auto result = localMatrix_ * x.localVector_;
      return std::make_pair(result, tag::need_synchronization{});
    }

    // forward the operator of local-matrix inserter
    template <class... Properties>
    auto inserter(std::size_t slotsize = 20, Types<Properties...> = {})
    {
      return localMatrix_.inserter(slotsize, Types<Properties...>{});
    }


    void clear_dirichlet_row(size_type idx)
    {
      localMatrix_.clear_dirichlet_row(idx, gv_.indexSet().template owns<cd_row>(idx));
    }

  public:

    void resize(size_type r, size_type c)
    {
      localMatrix_.resize(r,c);
    }

    size_type size() const
    {
      return localMatrix_.size();
    }

    size_type num_rows() const
    {
      return localMatrix_.num_rows();
    }

    size_type num_cols() const
    {
      return localMatrix_.num_cols();
    }

    DOFMatrix<T> const& matrix() const
    {
      return localMatrix_;
    }

    DOFMatrix<T>& matrix()
    {
      return localMatrix_;
    }

  private:

    GridView gv_;
    DOFMatrix<T> localMatrix_;
  };


  template <class T, class GridView, int cd_row, int cd_col>
  T residuum(DistributedMatrix<T,GridView,cd_row,cd_col> const& A, DistributedVector<T,GridView,cd_col> const& x, DistributedVector<T,GridView,cd_row> const& b)
  {
    DistributedVector<T,GridView,cd_row> r{b};
    r -= A*x;
    return two_norm(r); // TODO: combine two synchronizations
  }

} // end namespace Dec
