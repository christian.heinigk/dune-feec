#pragma once

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/common/Common.hpp>
#include <dune/dec/common/Operations.hpp>
#include <dune/dec/common/Joiner.hpp>

#include <dune/dec/ranges/Filter.hpp>
#include <dune/dec/ranges/IndexRange.hpp>

namespace Dec
{
  namespace tag
  {
    struct need_synchronization {};
  }

  template <class BaseGridView, int cd>
  class SynchVectorHandle
      : public Dune::CommDataHandleIF<SynchVectorHandle<BaseGridView,cd>, int>
  {
    using id_type = typename BaseGridView::Grid::GlobalIdSet::IdType;
    using index_type = typename BaseGridView::IndexSet::IndexType;

  public:
    SynchVectorHandle(BaseGridView const& gv)
      : gv_(gv)
      , map_(gv.comm().size())
    {}

    bool contains(int /*dim*/, int codim) const
    {
      return codim == cd;
    }

    bool fixedSize(int /*dim*/, int /*codim*/) const
    {
      return true;
    }

    template <class BaseEntity>
    std::size_t size(BaseEntity const& /*e*/) const
    {
      return 1;
    }

    template <class MessageBuffer, class BaseEntity>
    void gather(MessageBuffer& buff, BaseEntity const& e) const
    {
      buff.write(int(gv_.comm().rank()));
    }

    template <class MessageBuffer, class BaseEntity>
    void scatter(MessageBuffer& buff, BaseEntity const& e, std::size_t n)
    {
      assert( n == 1 );

      int rank = -1;
      buff.read(rank);

      auto id = gv_.grid().globalIdSet().id(e);
      map_[rank][id] = gv_.indexSet().index(e);
    }

    void finish(std::vector< std::vector<std::size_t> >& out) const
    {
      out.resize(gv_.comm().size());

      for (int r = 0; r < gv_.comm().size(); ++r) {
        out[r].resize(map_[r].size());
        std::transform(map_[r].begin(), map_[r].end(), out[r].begin(), [](auto const& pair) { return pair.second; });
      }
    }

  private:
    BaseGridView gv_;

    std::vector< std::map<id_type, index_type> > map_;
  };


  template <int cd, class T>
  class SynchVector
  {
  public:
    template <class GridView>
    SynchVector(GridView const& gv)
      : rank_(gv.comm().rank())
      , size_(gv.comm().size())
      , comm_(gv.comm())
      , buff_in_(size_)
      , request_in_(size_)
      , request_out_(size_)
      , finished_(size_, true)
    {
      SynchVectorHandle<GridView,cd> synchVector(gv);
      gv.communicate(synchVector, Dune::InteriorBorder_All_Interface, Dune::ForwardCommunication);
      synchVector.finish(map_);

      std::size_t len = std::accumulate(map_.begin(), map_.end(), 0u, [](std::size_t l, auto const& v) { return std::max(l, v.size()); });
      buff_out_.resize(len);

      for (int r = 0; r < size_; ++r)
        buff_in_[r].resize(map_[r].size());
    }

    template <class Assigner>
    void synch(DOFVector<T>& vec, Assigner assign)
    {
      int received = 0;
      for (int r = 0; r < size_; ++r) {
        if (r == rank_ || map_[r].empty())
          continue;

        // send data to neighbours
        std::size_t i = 0;
        for (auto const& j : map_[r])
          buff_out_[i++] = vec[j];
        MPI_Isend(buff_out_.data(), map_[r].size(), Dune::MPITraits<T>::getType(), r, tag_, comm_, &request_out_[r]);

        // receive data from neighbours
        MPI_Irecv(buff_in_[r].data(), map_[r].size(), Dune::MPITraits<T>::getType(), r, tag_, comm_, &request_in_[r]);
        finished_[r] = false;
        ++received;
      }

      while (received > 0) {
        for (int r = 0; r < size_; ++r) {
          if (r == rank_ || map_[r].empty() || finished_[r])
            continue;

          int flag = -1;
          MPI_Status status;
          MPI_Test(&request_in_[r], &flag, &status);
          if (flag) {
            std::size_t i = 0;
            for (auto const& j : map_[r])
              assign(buff_in_[r][i++], vec[j]);
            finished_[r] = true;
            --received;
          }
        }
      }
    }

    // the owner assigns its value to all ranks
    void synch(DOFVector<T>& vec)
    {
      int received = 0;
      for (int r = 0; r < size_; ++r) {
        if (r == rank_ || map_[r].empty())
          continue;

        // send data to neighbours
        if (r > rank_) {
          std::size_t i = 0;
          for (auto const& j : map_[r])
            buff_out_[i++] = vec[j];
          MPI_Isend(buff_out_.data(), map_[r].size(), Dune::MPITraits<T>::getType(), r, tag_, comm_, &request_out_[r]);
        }

        if (r < rank_) {
          // receive data from neighbours
          MPI_Irecv(buff_in_[r].data(), map_[r].size(), Dune::MPITraits<T>::getType(), r, tag_, comm_, &request_in_[r]);
          finished_[r] = false;
          received++;
        } else {
          finished_[r] = true;
        }
      }

      // wait for all data to be received
      while (received > 0) {
        for (int r = 0; r < size_; ++r) {
          if (r >= rank_ || map_[r].empty() || finished_[r])
            continue;

          int flag = -1;
          MPI_Status status;
          MPI_Test(&request_in_[r], &flag, &status);
          if (flag) {
            finished_[r] = true;
            received--;
          }
        }
      }

      // assign values from larges rank to lowest
      for (int r = rank_ - 1; r >= 0; --r) {
        std::size_t i = 0;
        for (auto const& j : map_[r])
          vec[j] = buff_in_[r][i++];
      }
    }

  private:
    int rank_;      //< my rank
    int size_;      //< the size of the MPI communicator
    MPI_Comm comm_; //< the MPI communicator

    // data buffers for send and receive
    std::vector< std::vector<T> > buff_in_;
    std::vector<T> buff_out_;

    // status of the communications
    std::vector<MPI_Request> request_in_;
    std::vector<MPI_Request> request_out_;

    // sets for each rank whether the communication is finished
    std::vector<bool> finished_;

    // a mapping of indices, i.e. the order in which values are read from and written to a data-vector
    std::vector< std::vector<std::size_t> > map_;

    // a communication tag
    int tag_ = 3579;
  };



  template <class T, class GridView, int cd>
  class DistributedVector
      : public DOFVectorBase<typename DOFVector<T>::size_type>
  {
    template <class,class,int,int> friend class DistributedMatrix;
    using Super = DOFVectorBase<typename DOFVector<T>::size_type>;

  public:

    using value_type = typename DOFVector<T>::value_type;
    using size_type = typename DOFVector<T>::size_type;

    static constexpr int codimension = cd;


  public: // constructors

    DistributedVector(GridView const& gv)
      : Super(gv.size(cd),"distributed")
      , gv_(gv)
      , localVector_(gv.size(cd))
      , comm_(gv.base())
    {}

    DistributedVector(DistributedVector const& that, tag::ressource)
      : Super(that.size(), that.name())
      , gv_(that.gv_)
      , localVector_(gv_.size(that.codimension))
      , comm_(that.comm_)
    {}


  public: // (compound) assignment operators

    template <class ThatDerived>
    DistributedVector& operator=(Eigen::MatrixBase<ThatDerived> const& that)
    {
      localVector_ = that;
      return *this;
    }

    template <class Expr>
    DistributedVector& operator=(std::pair<Expr, tag::need_synchronization> const& that)
    {
      localVector_ = that.first;
      comm_.synch(localVector_, operation::plus_assign{});
      return *this;
    }

    DistributedVector& operator+=(DistributedVector const& rhs)
    {
      localVector_ += rhs.localVector_;
      return *this;
    }

    template <class ThatDerived>
    DistributedVector& operator+=(Eigen::MatrixBase<ThatDerived> const& rhs)
    {
      localVector_ += rhs;
      return *this;
    }

    template <class Expr>
    DistributedVector& operator+=(std::pair<Expr, tag::need_synchronization> const& that)
    {
      DOFVector<T> tmp{that.first};
      comm_.synch(tmp, operation::plus_assign{});

      localVector_ += tmp;
      return *this;
    }

    DistributedVector& operator-=(DistributedVector const& rhs)
    {
      localVector_ -= rhs.localVector_;
      return *this;
    }

    template <class ThatDerived>
    DistributedVector& operator-=(Eigen::MatrixBase<ThatDerived> const& rhs)
    {
      localVector_ += rhs;
      return *this;
    }

    template <class Expr>
    DistributedVector& operator-=(std::pair<Expr, tag::need_synchronization> const& that)
    {
      DOFVector<T> tmp{that.first};
      comm_.synch(tmp, operation::plus_assign{});

      localVector_ -= tmp;
      return *this;
    }

    DistributedVector& operator*=(T const& rhs)
    {
      localVector_ *= rhs;
      return *this;
    }

    DistributedVector& operator/=(T const& rhs)
    {
      localVector_ /= rhs;
      return *this;
    }


    void synch()
    {
      comm_.synch(localVector_);
    }

  public: // binary arithmetic operations

    friend auto operator+(DistributedVector<T,GridView,cd> const& lhs, DistributedVector<T,GridView,cd> const& rhs)
    {
      return lhs.localVector_ + rhs.localVector_;
    }

    friend auto operator-(DistributedVector<T,GridView,cd> const& lhs, DistributedVector<T,GridView,cd> const& rhs)
    {
      return lhs.localVector_ - rhs.localVector_;
    }

    template <class ThatDerived>
    friend auto operator+(DistributedVector<T,GridView,cd> const& lhs, Eigen::MatrixBase<ThatDerived> const& rhs)
    {
      return lhs.localVector_ + rhs;
    }

    template <class ThatDerived>
    friend auto operator+(Eigen::MatrixBase<ThatDerived> const& lhs, DistributedVector<T,GridView,cd> const& rhs)
    {
      return lhs + rhs.localVector_;
    }

    template <class ThatDerived>
    friend auto operator-(DistributedVector<T,GridView,cd> const& lhs, Eigen::MatrixBase<ThatDerived> const& rhs)
    {
      return lhs.localVector_ - rhs;
    }

    template <class ThatDerived>
    friend auto operator-(Eigen::MatrixBase<ThatDerived> const& lhs, DistributedVector<T,GridView,cd> const& rhs)
    {
      return lhs - rhs.localVector_;
    }

    friend auto operator*(DistributedVector<T,GridView,cd> const& lhs, T const& rhs)
    {
      return lhs.localVector_ * rhs;
    }

    friend auto operator*(T const& lhs, DistributedVector<T,GridView,cd> const& rhs)
    {
      return lhs * rhs.localVector_;
    }


    friend auto dot(DistributedVector<T,GridView,cd> const& lhs, DistributedVector<T,GridView,cd> const& rhs)
    {
      auto const& indexSet = lhs.gv_.indexSet();

      T x = 0;
      for (size_type i = 0; i < lhs.size(); ++i)
        x += indexSet.template owns<cd>(i) * lhs.vector()[i] * rhs.vector()[i];

      return lhs.gv_.comm().sum(x);
    }

    friend auto unary_dot(DistributedVector<T,GridView,cd> const& vec)
    {
      auto const& indexSet = vec.gv_.indexSet();

      T x = 0;
      for (size_type i = 0; i < vec.size(); ++i)
        x += indexSet.template owns<cd>(i) * sqr(vec.vector()[i]);

      return vec.gv_.comm().sum(x);
    }

    friend auto two_norm(DistributedVector<T,GridView,cd> const& vec)
    {
      return std::sqrt( unary_dot(vec) );
    }


  public:

    DOFVector<T> const& vector() const
    {
      return localVector_;
    }

    DOFVector<T>& vector()
    {
      return localVector_;
    }


  private:

    GridView gv_;
    DOFVector<T> localVector_;
    SynchVector<cd,T> comm_;
  };




} // end namespace Dec
