#pragma once

namespace Dec
{
  // NOTE: work in progress
  template <class GridView, int codim>
  class GlobalCodimMapper
      : public LocalCodimMapper<GridView, codim>
      , public Dune::Mapper<typename GridView::Grid, GlobalCodimMapper<GridView, codim>, typename HalfEdge::IndexType>
  {
  public:

    using Local = LocalCodimMapper<GridView, codim>;
    using IndexType = typename HalfEdge::IndexType;
    using PartitionSet = typename Local::PartitionSet;

  public:

    using Local::gridView_;

    /// Constructor, stores a copy of `gv`
    ParallelCodimMapper(GridView const& gv)
      : Local(gv)
    {
      update();
    }


    /// Map entity to global index
    template <class Entity>
    IndexType index(Entity const& e) const
    {
      assert( Entity::codimension == codim );
      assert_msg( PartitionSet{}.contains(e.partitionType()), "PartitionType ",e.partitionType()," is not in ",Local::partitionIterator );

      auto const& indexSet = gridView_.indexSet();
      IndexType idx = indexSet.index(e);

      return indexMap_[idx];
    }

    IndexType index(IndexType i) const
    {
      return indexMap_[i];
    }


    /// Return total number of entities in the entity set managed by the mapper.
    int size() const
    {
      return global_size_;
    }


    /// Returns true if the entity is contained in the index set and at the same time the array index is returned.
    template <class Entity>
    bool contains(Entity const& e, int& result) const
    {
      if (Entity::codimension != codim)
        return false;
      if (!PartitionSet{}.contains(e.partitionType()))
        return false;

      result = int(index(e));
      return true;
    }

    using Local::owns;
    using Local::rank;


    /// Reinitialize mapper after grid has been modified.
    void update()
    {
      auto const& indexSet = gridView_.indexSet();
      indexMap_.clear();
      indexMap_.resize(gridView_.size(codim));

      // get number of local indices owned by this processor
      local_size_ = 0;
      for (auto const& e : entities(gridView_, Codim_<codim>, PartitionSet{}))
        if (owns(e))
          ++local_size_;

      // sum up the size for ranks < my rank
      prev_size_ = -1;
      MPI_Exscan(&local_size_, &prev_size_, 1, MPI_INT, MPI_SUM, gridView_.comm().MPI_Comm());
      assert( prev_size >= 0 );

      // calculate global matrix size
      msg("prev_size = ",prev_size);
      global_size_ = gridView_.comm().sum(local_size_);

      // assign indices for owned entities
      IndexType idx = 0;
      for (auto const& e : entities(gridView_, Codim_<codim>, PartitionSet{}))
        indexMap_[indexSet.index(e)] = owns(e) ? prev_size + idx++ : 0u;

      // set global indices for non-owned entities on the border
      SynchVectorHandle<GridView,codim,IndexType,operation::plus_assign> synchVector(gridView_, indexMap_);
      gridView_.communicate(synchVector, Dune::InteriorBorder_InteriorBorder_Interface, Dune::ForwardCommunication);
    }


  private:

    // maps indices of PartitionIteratorType to global continuouse range
    DOFVector<IndexType> indexMap_;

    // number of indices in continuouse range
    int local_size_;
    int global_size_;
    int prev_size_;
  };

  template <class GridView>
  using GlobalDofMapper = std::tuple<GlobalCodimMapper<GridView,0>, GlobalCodimMapper<GridView,1>, GlobalCodimMapper<GridView,2>>;

} // end namespace Dec
