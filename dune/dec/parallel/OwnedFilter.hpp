#pragma once

namespace Dec
{
  template <class Mapper>
  class OwnedFilter
  {
  public:
    OwnedFilter(Mapper& mapper)
      : mapper_(&mapper)
    {}

    template <class Entity>
    bool operator()(Entity const& e) const
    {
      return mapper_->owns(e);
    }

  private:
    Mapper const* mapper_;
  };

  template <class Mapper>
  OwnedFilter<Mapper> owned(Mapper& mapper)
  {
    return {mapper};
  }

} // end namespace Dec
