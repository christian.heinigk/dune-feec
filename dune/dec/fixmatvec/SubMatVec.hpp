#pragma once

#include <cassert>

#include <dune/dec/common/Concepts.hpp>
#include <dune/dec/common/Logical.hpp>
#include <dune/dec/common/ScalarTypes.hpp>

namespace Dec
{
  // forward declaration
  template <class Model, class T, std::size_t N, std::size_t M> struct MatExpr;

  namespace view
  {
    namespace _aux
    {
      template <bool ConstView, class E, class T, std::size_t I0, std::size_t I1, std::size_t J0, std::size_t J1>
      struct SubMatImpl;

      // Const view on submatrix, must be used in matrix-expressions.
      template <class E, class T, std::size_t I0, std::size_t I1, std::size_t J0, std::size_t J1>
      struct SubMatImpl<true, E, T, I0, I1, J0, J1>
          : public MatExpr<SubMatImpl<true, E,T,I0,I1,J0,J1>, T, I1-I0, J1-J0>
      {
        using const_reference = typename E::const_reference;

        static constexpr std::size_t N = I1-I0;
        static constexpr std::size_t M = J1-J0;

        SubMatImpl(E const& expr)
          : mat_(expr)
        {}

        const_reference at(std::size_t i, std::size_t j) const
        {
          assert( i < N && j < M );
          return mat_(I0+i, J0+j);
        }

        const_reference at(std::size_t i) const
        {
          return M == 1 ? mat_(I0+i, J0) : mat_(I0, J0+i);
        }

      private:
        E const& mat_;
      };


      // Mutable view on submatrix, can be used in FixMat directly
      template <class E, class T, std::size_t I0, std::size_t I1, std::size_t J0, std::size_t J1>
      struct SubMatImpl<false, E, T, I0, I1, J0, J1>
          : public SubMatImpl<true, E,T,I0,I1,J0,J1>
      {
        using Super = SubMatImpl<true, E,T,I0,I1,J0,J1>;
        using Self = SubMatImpl;

        static constexpr std::size_t N = I1-I0;
        static constexpr std::size_t M = J1-J0;

        SubMatImpl(E& expr)
          : Super(expr)
          , mat_(expr)
        {}

        /// @{

        template <class Model, class S,
          REQUIRES(concepts::Convertible<S,T>) >
        Self& operator=(MatExpr<Model, S, N, M> const& expr)
        {
          return assign_impl(expr, [](auto& m, auto const& e) { m = e; });
        }

        template <class Model, class S,
          REQUIRES(concepts::Addable<T,S>) >
        Self& operator+=(MatExpr<Model, S, N, M> const& expr)
        {
          return assign_impl(expr, [](auto& m, auto const& e) { m += e; });
        }

        template <class Model, class S,
          REQUIRES(concepts::Subtractable<T,S>) >
        Self& operator-=(MatExpr<Model, S, N, M> const& expr)
        {
          return assign_impl(expr, [](auto& m, auto const& e) { m -= e; });
        }

        template <class Model, class S,
          REQUIRES(concepts::Multiplicable<T,S> && (N*M==1)) >
        Self& operator*=(MatExpr<Model, S, N, M> const& expr)
        {
          return assign_impl(expr, [](auto& m, auto const& e) { m *= e; });
        }

        template <class Model, class S,
          REQUIRES(concepts::Divisible<T,S> && (N*M==1)) >
        Self& operator/=(MatExpr<Model, S, N, M> const& expr)
        {
          return assign_impl(expr, [](auto& m, auto const& e) { m /= e; });
        }


        template <class S,
          REQUIRES(concepts::Arithmetic<S>) >
        Self& operator=(S const& scalar)
        {
          for (std::size_t i = I0; i < I1; ++i)
            for (std::size_t j = J0; j < J1; ++j)
              mat_(i, j) = scalar;
          return *this;
        }

        template <class S,
          REQUIRES(concepts::Arithmetic<S> && concepts::Addable<T,S> && (N*M==1)) >
        Self& operator+=(S const& scalar)
        {
          for (std::size_t i = I0; i < I1; ++i)
            for (std::size_t j = J0; j < J1; ++j)
              mat_(i, j) += scalar;
          return *this;
        }

        template <class S,
          REQUIRES(concepts::Arithmetic<S> && concepts::Subtractable<T,S> && (N*M==1)) >
        Self& operator-=(S const& scalar)
        {
          for (std::size_t i = I0; i < I1; ++i)
            for (std::size_t j = J0; j < J1; ++j)
              mat_(i, j) -= scalar;
          return *this;
        }

        template <class S,
          REQUIRES(concepts::Arithmetic<S> && concepts::Multiplicable<T,S>) >
        Self& operator*=(S const& scalar)
        {
          for (std::size_t i = I0; i < I1; ++i)
            for (std::size_t j = J0; j < J1; ++j)
              mat_(i, j) *= scalar;
          return *this;
        }

        template <class S,
          REQUIRES(concepts::Arithmetic<S> && concepts::Divisible<T,S>) >
        Self& operator/=(S const& scalar)
        {
          for (std::size_t i = I0; i < I1; ++i)
            for (std::size_t j = J0; j < J1; ++j)
              mat_(i, j) /= scalar;
          return *this;
        }

        /// @}

        // mutable-access operator
        T& operator()(std::size_t i, std::size_t j)
        {
          assert( i < N && j < M );
          return mat_(I0+i, J0+j);
        }

        T& operator[](std::size_t i)
        {
          return M == 1 ? mat_(I0+i, J0) : mat_(I0, J0+i);
        }

        using Super::operator();
        using Super::operator[];

      protected:

        template <class Model, class S, class Assigner>
        Self& assign_impl(MatExpr<Model, S, N, M> const& expr, Assigner assign)
        {
          for (std::size_t i = 0; i < N; ++i)
            for (std::size_t j = 0; j < M; ++j)
              assign(mat_(I0+i, J0+j), expr(i,j));
          return *this;
        }

      private:
        E& mat_;
      };

    } // end namespace _aux

    /// \brief Const/Mutable view on submatrix, must be used in matrix-expressions. \ingroup expression
    template <class E, class T, std::size_t I0, std::size_t I1, std::size_t J0, std::size_t J1>
    using SubMat = _aux::SubMatImpl<std::is_const<T>::value, E, T, I0, I1, J0, J1>;


    /// \brief Const/Mutable view on subvector, must be used in vector-expressions. \ingroup expression
    template <class E, class T, std::size_t N, std::size_t M, std::size_t I0, std::size_t I1>
    using SubVec = if_<M==1, SubMat<E, T, I0, I1, 0, 1>,
                             SubMat<E, T, 0, 1, I0, I1>>;


    /// \brief View on a matrix-row. \ingroup expression
    template <class E, class T, std::size_t M>
    struct RowVec
        : public MatExpr<RowVec<E,T,M>, T, 1, M>
    {
      using const_reference = typename E::const_reference;

      RowVec(E const& expr, std::size_t row)
        : mat_(expr)
        , row_(row)
      {}

      const_reference at(std::size_t /*i = 0*/, std::size_t j) const
      {
        assert( j < M );
        return mat_(row_, j);
      }

      const_reference at(std::size_t j) const
      {
        assert( j < M );
        return mat_(row_, j);
      }

    private:
      E const& mat_;
      std::size_t row_;
    };


    /// \brief View on a matrix-column. \ingroup expression
    template <class E, class T, std::size_t N>
    struct ColumnVec
        : public MatExpr<ColumnVec<E,T,N>, T, N, 1>
    {
      using const_reference = typename E::const_reference;

      ColumnVec(E const& expr, std::size_t col)
        : mat_(expr)
        , col_(col)
      {}

      const_reference at(std::size_t i, std::size_t = 0) const
      {
        assert( i < N );
        return mat_(i, col_);
      }

    private:
      E const& mat_;
      std::size_t col_;
    };


    /// \brief View on the upper triangular part of a matrix. \ingroup expression
    template <class E, class T, std::size_t N, std::size_t M>
    struct Upper
        : public MatExpr<Upper<E,T,N,M>, T, N, M>
    {
      using const_reference = typename E::const_reference;

      Upper(E const& expr)
        : mat_(expr)
      {}

      const_reference at(std::size_t i, std::size_t j) const
      {
        return i <= j ? mat_(i, j) : zero;
      }

    private:
      E const& mat_;
      T zero = T(0);
    };


    /// \brief View on the lower triangular part of a matrix. \ingroup expression
    template <class E, class T, std::size_t N, std::size_t M>
    struct Lower
        : public MatExpr<Lower<E,T,N,M>, T, N, M>
    {
      using const_reference = typename E::const_reference;

      Lower(E const& expr)
        : mat_(expr)
      {}

      const_reference at(std::size_t i, std::size_t j) const
      {
        return i >= j ? mat_(i, j) : zero;
      }

    private:
      E const& mat_;
      T zero = T(0);
    };

  } // end namespace view

} // end namespace Dec
