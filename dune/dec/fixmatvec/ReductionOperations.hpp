#pragma once

#include "FixMatExpression.hpp"
#include "FixMatOperations.hpp"

namespace Dec
{
  /** \defgroup matrix_vector_reduction Matrix/Vector reduction
   *  \brief Methods for matrix/vector reduction, like norms and accumulate.
   *  @{
   **/

  /** \relates FixMat
   *  \brief Inner product trace(mat1^H * mat2) = frobenius-product
   **/
  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M>
  auto inner(MatExpr<E0, T0, N, M> const& mat1, MatExpr<E1, T1, N, M> const& mat2)
  {
    using T = Decay_t<decltype( std::declval<T0>() * std::declval<T1>() )>;
    operation::hermitian<T0> conj_op{};
    T result = 0;
    for (std::size_t i = 0; i < N; ++i)
      for (std::size_t j = 0; j < M; ++j)
        result += conj_op(mat1(i,j)) * mat2(i,j);
    return result;
  }
  
  template <class T, class S,
    REQUIRES(concepts::Arithmetic<T> && concepts::Arithmetic<S>) >
  auto dot(T const& x, S const& y) 
  { 
    operation::hermitian<T> conj_op{};
    return conj_op(x) * y; 
  }

  /** \relates FixMat
   *  \brief Dot-product of two vectors = lhs^H * rhs
   **/
  template <class V0, class V1,
    REQUIRES(concepts::Vectors<V0,V1>) >
  auto dot(V0 const& lhs, V1 const& rhs)
  {
    assert (V0::size() > 0);

    auto result = dot(lhs[0], rhs[0]);
    for (std::size_t i = 1; i < V0::size(); ++i)
      result += dot(lhs[i], rhs[i]);
    return result;
  }


  namespace aux
  {
    template <class E, class T, std::size_t N, std::size_t M, class Operation>
    FieldType_t<T> accumulate(MatExpr<E, T, N, M> const& mat, Operation op)
    {
      FieldType_t<T> result = 0;
      for (std::size_t i = 0; i < N; ++i)
        for (std::size_t j = 0; j < M; ++j)
          result = op(result, mat(i,j));
      return result;
    }

  } // end namespace aux

  
  template <class T,
    REQUIRES(concepts::Arithmetic<T>) >
  T unary_dot(T const& x) { return sqr(std::abs(x)); }

  /** \relates FixMat
   *  \brief Dot-product with the vector itself
   **/
  template <class V,
    REQUIRES(concepts::Vector<V>) >
  auto unary_dot(V const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + unary_dot(b); };
    return aux::accumulate(x, op);
  }

  /** \relates FixMat
   *  \brief Maximum over all vector entries
   **/
  template <class E, class T, std::size_t N, std::size_t M,
    REQUIRES(concepts::LessThanComparable<T>) >
  auto max(MatExpr<E, T, N, M> const& mat)
  {
    return aux::accumulate(mat, operation::maximum{});
  }

  /** \relates FixMat
   *  \brief Minimum over all vector entries
   **/
  template <class E, class T, std::size_t N, std::size_t M,
    REQUIRES(concepts::LessThanComparable<T>) >
  auto min(MatExpr<E, T, N, M> const& mat)
  {
    return aux::accumulate(mat, operation::minimum{});
  }

  /** \relates FixMat
   *  \brief Maximum of the absolute values of vector entries
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  auto abs_max(MatExpr<E, T, N, M> const& mat)
  {
    return aux::accumulate(mat, operation::abs_max{});
  }

  /** \relates FixMat
   *  \brief Minimum of the absolute values of vector entries
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  auto abs_min(MatExpr<E, T, N, M> const& mat)
  {
    return aux::accumulate(mat, operation::abs_min{});
  }


  // ----------------------------------------------------------------------------


  /** \relates FixMat
   *  \brief The 1-norm of a vector = sum_i |x_i|
   **/
  template <class V,
    REQUIRES(concepts::Vector<V>) >
  auto one_norm(V const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + std::abs(b); };
    return aux::accumulate(x, op);
  }

  /** \relates FixMat
   *  \brief Returns the 1-norm of a matrix = max_j sum_i |a_ij|
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  T one_norm(MatExpr<E, T, N, M> const& mat)
  {
    T result = 0;
    for (std::size_t j = 0; j < M; ++j)
      result = std::max( result, one_norm(col(mat, j)) );
    return result;
  }

  /** \relates FixMat
   *  \brief The euklidean 2-norm of a vector = sqrt( sum_i |x_i|^2 )
   **/
  template <class V,
    REQUIRES(concepts::Vector<V>) >
  auto two_norm(V const& x)
  {
    return std::sqrt(unary_dot(x));
  }

  /** \relates FixMat
   *  \brief Returns the Frobenius-norm of a matrix = sqrt( trace( A^H * A ) )
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  T frobenius_norm(MatExpr<E, T, N, M> const& mat)
  {
    return std::sqrt(inner(mat, mat));
  }

  /** \relates FixMat
   *  \brief The p-norm of a vector = ( sum_i |x_i|^p )^(1/p)
   **/
  template <int p, class V,
    REQUIRES(concepts::Vector<V> && (p>=0)) >
  auto p_norm(V const& x)
  {
    auto op = [](auto const& a, auto const& b) { return a + pow<p>(std::abs(b)); };
    return std::pow( aux::accumulate(x, op), 1.0/p );
  }

  /** \relates FixMat
   *  \brief The infty-norm of a vector = max_i |x_i| = alias for \ref abs_max
   **/
  template <class V,
    REQUIRES(concepts::Vector<V>) >
  auto infty_norm(V const& x)
  {
    return abs_max(x);
  }

  /** \relates FixMat
   *  \brief Returns the infty-norm of a matrix = max_i sum_j |a_ij|
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  T infty_norm(MatExpr<E, T, N, M> const& mat)
  {
    T result = 0;
    for (std::size_t i = 0; i < N; ++i)
      result = std::max( result, one_norm(row(mat, i)) );
    return result;
  }


  // ----------------------------------------------------------------------------


  /** \relates FixMat
   *  \brief Sum of vector entires.
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  T sum(MatExpr<E, T, N, M> const& mat)
  {
    return aux::accumulate(mat, operation::plus{});
  }

  /** \relates FixMat
   *  \brief The trace of the matrix, i.e. sum of diagonal entries
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  T trace(MatExpr<E, T, N, M> const& mat)
  {
    return sum(diagonal(mat));
  }

  /** \relates FixMat
   *  \brief Determinant of a 1x1 matrix
   **/
  template <class E, class T>
  T det(MatExpr<E, T, 1, 1> const& mat)
  {
    return mat(0,0);
  }

  /** \relates FixMat
   *  \brief Determinant of a 2x2 matrix
   **/
  template <class E, class T>
  T det(MatExpr<E, T, 2, 2> const& mat)
  {
    return mat(0,0)*mat(1,1) - mat(0,1)*mat(1,0);
  }

  /** \relates FixMat
   *  \brief Determinant of a 3x3 matrix
   **/
  template <class E, class T>
  T det(MatExpr<E, T, 3, 3> const& mat)
  {
    return mat(0,0)*mat(1,1)*mat(2,2) + mat(0,1)*mat(1,2)*mat(2,0) + mat(0,2)*mat(1,0)*mat(2,1)
        - (mat(0,2)*mat(1,1)*mat(2,0) + mat(0,1)*mat(1,0)*mat(2,2) + mat(0,0)*mat(1,2)*mat(2,1));
  }


  /** \relates FixMat
   *  \brief The euklidean distance between two vectors = |lhs-rhs|_2
   **/
  template <class V0, class V1,
    REQUIRES(concepts::Vectors<V0,V1>) >
  auto distance(V0 const& lhs, V1 const& rhs)
  {
    return two_norm(lhs - rhs);
  }

  /** @} **/

} // end namespace Dec
