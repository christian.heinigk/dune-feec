#pragma once

#include <iostream>

#include "ExprConcepts.hpp"
#include "SubMatVec.hpp"

#include <dune/dec/common/Common.hpp>
#include <dune/dec/common/Index.hpp>
#include <dune/dec/common/Logical.hpp>
#include <dune/dec/common/Operations.hpp>
#include <dune/dec/common/Output.hpp>

namespace Dec
{
  /** \defgroup expression Expressions
   *  \brief Classes that represent Matrix-/Vectorexpressions.
   *  @{
   **/

  /// Base class for all expressions
  template <class Model>
  struct BaseExpression
  {
    Model& get()
    {
      return static_cast<Model&>(*this);
    }

    Model const& get() const
    {
      return static_cast<Model const&>(*this);
    }
  };

  // forward declaration
  template <class T, std::size_t N, std::size_t M> class FixMat;


  /// Expression representing a matrix, see \ref FixMat
  template <class Model, class T, std::size_t N, std::size_t M>
  struct MatExpr
      : public BaseExpression<Model>
  {
    using model = Model;
    using value_type = T;
    using const_reference = T const;

    /// @{

    /// Returns the (i,j)th entry of the matrix expression
    auto operator()(std::size_t i, std::size_t j) const
    {
      return this->get().at(i, j);
    }

    /// Returns the i'th entry of the vector expression
    auto operator[](std::size_t i) const
    {
      return this->get().at(i);
    }

    /// Create a const-view to a sub-matrix
    template <std::size_t I0, std::size_t I1, std::size_t J0, std::size_t J1>
    view::SubMat<const Model, T, I0, I1, J0, J1> const
    operator()(range_t<I0,I1>, range_t<J0,J1>) const
    {
      return { this->get() };
    }

    /// Create a const-view to a sub-matrix
    template <std::size_t I0, std::size_t I1, std::size_t J>
    view::SubMat<const Model, T, I0, I1, J, J+1> const
    operator()(range_t<I0,I1>, index_t<J>) const
    {
      return { this->get() };
    }

    /// Create a const-view to a sub-matrix
    template <std::size_t I, std::size_t J0, std::size_t J1>
    view::SubMat<const Model, T, I, I+1, J0, J1> const
    operator()(index_t<I>, range_t<J0,J1>) const
    {
      return { this->get() };
    }

    /// Create a const-view to a sub-vector
    template <std::size_t I0, std::size_t I1>
    view::SubVec<const Model, T, N, M, I0, I1> const
    operator[](range_t<I0,I1>) const
    {
      return { this->get() };
    }

    /// @}

    /// @{

    /// Returns the number of rows of the matrix
    static constexpr std::size_t num_rows()
    {
      return N;
    }

    /// Returns the number of columns of the matrix
    static constexpr std::size_t num_cols()
    {
      return M;
    }

    /// Returns the number of entries of the matrix
    static constexpr std::size_t size()
    {
      return N*M;
    }

    /// @}

    /// Create the underlying matrix from the expression values
    FixMat<T, N, M> store() const
    {
      return { *this };
    }
  };

  /** @} **/



  template <class T, std::size_t N, std::size_t M>
  struct IdentityExpr
      : public MatExpr<IdentityExpr<T,N,M>, T, N, M>
  {
    IdentityExpr(T const& value)
      : value_(value)
    {}

    template <class... Int>
    auto const& at(Int...) const
    {
      return value_;
   }

  private:
    T value_;
  };



  // A placeholder for a matrix expression
//   template <class E, class T, std::size_t N, std::size_t M>
//   struct matrix_expression_c {};
//
//
//   namespace aux
//   {
//     template <class, class E, class T, std::size_t N, std::size_t M>
//     matrix_expression_c<E,T,N,M>
//     base_type(MatExpr<E,T,N,M> const&) { return {}; }
//
//     template <class E>
//     using Base_t = decltype(base_type<E>(std::declval<E>()));
//
//   } // end namespace aux

//   template <class E>
//   struct Collection;
//
//   template <class Model, class T, std::size_t N, std::size_t M>
//   struct Collection<matrix_expression_c<Model, T, N, M>>
//   {
//     using Expr = Model;
//     using value_type = T;
//
//     static constexpr std::size_t rows = N;
//     static constexpr std::size_t cols = M;
//     static constexpr std::size_t size = N*M;
//   };
//
//   template <class Expr>
//   struct Collection : Collection<aux::Base_t<Expr>> {};

} // end namespace Dec
