#pragma once

#include <array>
#include <cmath>

#include "FixMatExpression.hpp"
#include "SubMatVec.hpp"

#include <dune/dec/common/Logical.hpp>
#include <dune/dec/common/Math.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/common/ScalarTypes.hpp>

namespace Dec
{
  /// Matrix data structure with fixed dimension
  template <class T, std::size_t N, std::size_t M>
  class FixMat
      : protected std::array<T, N*M>
      , public MatExpr< FixMat<T,N,M>, T, N, M>
  {
    using Super = std::array<T, N*M>;
    using Self = FixMat;
    using Expr = MatExpr<Self, T, N, M>;

  public:

    using model = Self;
    using value_type = T;
    using const_reference = T const&;

    /** Constructors
     *  @{
     **/

    /// Default constructor
    FixMat() : Super() {}

    /// Copy constructor
    template <class S,
      REQUIRES(concepts::Convertible<S,T>) >
    FixMat(FixMat<S, N, M> const& that)
    {
      std::copy(that.begin(), that.end(), this->begin());
    }

    /// \brief Constructor that accepps nested brace-init-lists, e.g. { {...}, {...} }.
    /**
     * Example:
     * ```
     * FixMat<double, 2, 2> A{ {1.0, 1.0}, {2.0, 1.0} };
     * ```
     **/
    template <class S,
      REQUIRES(concepts::Convertible<S,T>) >
    FixMat(std::initializer_list<std::initializer_list<S>> list)
    {
      assert_msg(list.size() <= N, "Initializer-List with too many elements!");
      auto elem = this->begin();

      for (auto const& l : list) {
        assert_msg(l.size() == M, "Initializer-List with too many elements! l.size=",l.size(),", M=",M);
        for (auto const& l_i : l) {
          if (elem == this->end())
            break;
          *elem = l_i;
          ++elem;
        }
      }
    }

    /// \brief Constructor that accepts brace-init-lists, e.g. {...}.
    /**
     * Example:
     * ```
     * FixVec<double, 2> A{1.0, 1.0};
     * ```
     **/
    template <class S,
      REQUIRES(concepts::Convertible<S,T>) >
    FixMat(std::initializer_list<S> list)
    {
      assert_msg(list.size() <= N*M, "Initializer-List with too many elements!");
      std::copy(list.begin(), list.end(), this->begin());
    }

    /// Copy constructor
    FixMat(FixMat const&) = default;

    /// Move constructor
    FixMat(FixMat&&) = default;

    /// Copy constructor, copy ressources only, i.e. do nothing
    FixMat(FixMat const&, tag::ressource) {}

    /// Construction of the matrix from an expression
    template <class Model, class S>
    FixMat(MatExpr<Model, S, N, M> const& expr)
      : Super()
    {
      assign_impl(expr, [](auto& m, auto const& e) { m = e; });
    }

    /** @} **/


    /** Assignment operations
     *  @{
     **/

    /// Copy assignment operator
    FixMat& operator=(FixMat const&) = default;

    /// Move assignment operatior
    FixMat& operator=(FixMat&&) = default;

    /// Assignment of an expression to the matrix
    template <class Model, class S,
      REQUIRES(concepts::Convertible<S,T>) >
    FixMat& operator=(MatExpr<Model, S, N, M> const& expr)
    {
      return assign_impl(expr, [](auto& m, auto const& e) { m = e; });
    }

    /// Assignment-addition operator
    template <class Model, class S,
      REQUIRES(concepts::Addable<T,S>) >
    FixMat& operator+=(MatExpr<Model, S, N, M> const& expr)
    {
      return assign_impl(expr, [](auto& m, auto const& e) { m += e; });
    }

    /// Assignment-subtraction operator
    template <class Model, class S,
      REQUIRES(concepts::Subtractable<T,S>) >
    FixMat& operator-=(MatExpr<Model, S, N, M> const& expr)
    {
      return assign_impl(expr, [](auto& m, auto const& e) { m -= e; });
    }

    /// Assignment-multiplication operator
    template <class S,
      REQUIRES(concepts::Arithmetic<S> && concepts::Multiplicable<T,S>) >
    FixMat& operator*=(S const& scalar)
    {
      using Id = IdentityExpr<S,N,M>;
      return assign_impl(Id(scalar), [](auto& m, auto const& e) { m *= e; });
    }

    /// Assignment-division operator
    template <class S,
      REQUIRES(concepts::Arithmetic<S> && concepts::Divisible<T,S>) >
    FixMat& operator/=(S const& scalar)
    {
      using Id = IdentityExpr<S,N,M>;
      return assign_impl(Id(scalar), [](auto& m, auto const& e) { m /= e; });
    }

    /** @} **/


    /** Element access operations
     *  @{
     **/

    /// Access the (i,j)th element of the matrix
    T& operator()(std::size_t i, std::size_t j)
    {
      std::size_t const idx = i*M + j;
      return static_cast<Super&>(*this)[idx];
    }

    /// Access the i'th entry of the vector
    T& operator[](std::size_t idx)
    {
      return static_cast<Super&>(*this)[idx];
    }

    /// Access the (i,j)th element of the matrix
    T const& operator()(std::size_t i, std::size_t j) const
    {
      std::size_t const idx = i*M + j;
      return static_cast<Super const&>(*this)[idx];
    }

    /// Access the i'th entry of the vector
    T const& operator[](std::size_t idx) const
    {
      return static_cast<Super const&>(*this)[idx];
    }

    /// \brief Generates a sub-matrix of indices [I0,I1)x[J0,J1)
    /**
     * Example:
     * ```
     * FixMat<double, 3, 3> A{ {1.0, 1.0, 1.0}, {2.0, 1.0, 0.0}, {3.0, 1.0, -1.0} };
     * FixMat<double, 2, 2> B{ {1.0, 1.0},      {2.0, 1.0} };
     * FixMat<double, 1, 2> C{ {1.0, 1.0} };
     *
     * assert( A(range_<0,2>, range_<0,2>) == B );
     * assert( A(index_<0>,   range_<0,2>) == C );
     * ```
     **/
    template <std::size_t I0, std::size_t I1, std::size_t J0, std::size_t J1>
    view::SubMat<Self, T, I0, I1, J0, J1> operator()(range_t<I0,I1>, range_t<J0,J1>)
    {
      return {*this};
    }

    template <std::size_t I0, std::size_t I1, std::size_t J>
    view::SubMat<Self, T, I0, I1, J, J+1> operator()(range_t<I0,I1>, index_t<J>)
    {
      return {*this};
    }

    template <std::size_t I, std::size_t J0, std::size_t J1>
    view::SubMat<Self, T, I, I+1, J0, J1> operator()(index_t<I>, range_t<J0,J1>)
    {
      return {*this};
    }

    /// \brief Generates a sub-vector of indices [I0,I1)
    /**
     * Example:
     * ```
     * FixVec<double, 2> a{2.0, 1.0};
     * FixMat<double, 1> b{1.0};
     *
     * assert( a[index_<1>] == b );
     * assert( a[range_<1,2>] == b );
     * ```
     **/
    template <std::size_t I0, std::size_t I1>
    view::SubVec<Self, T, N, M, I0, I1> operator[](range_t<I0,I1>)
    {
      return {*this};
    }

    using Expr::operator[];
//     using Super::operator[];
    using Expr::operator();

    /// @cond HIDDEN_SYMBOLS

    T const& at(std::size_t i, std::size_t j) const
    {
      std::size_t const idx = i*M + j;
      return static_cast<Super const&>(*this)[idx];
    }

    T const& at(std::size_t idx) const
    {
      return static_cast<Super const&>(*this)[idx];
    }

    /// @endcond

    /** @} **/

    using Super::begin;
    using Super::end;
    using Super::cbegin;
    using Super::cend;

    using Expr::num_rows;
    using Expr::num_cols;
    using Expr::size;

//     view::SubMat<Self, T, 0, N, 0, M> store_impl()
//     {
//       return {*this};
//     }

  protected:

    template <class Model, class S, class Assigner>
    Self& assign_impl(MatExpr<Model, S, N, M> const& expr, Assigner assign)
    {
      for (std::size_t idx = 0; idx < N*M; ++idx)
        assign((*this)[idx], expr[idx]);
      return *this;
    }
  };

  namespace aux
  {
    template <class T, std::size_t N, bool col_vec = true>
    struct FixVec
    {
      using type = FixMat<T, col_vec ? N : 1, col_vec ? 1 : N>;
    };

  } // end namespace aux


  /// Fixed size vector, \relates FixMat
  template <class T, std::size_t N, bool col_vec = true>
  using FixVec = typename aux::FixVec<T,N,col_vec>::type;


  namespace aux
  {
    template <class, class E, class T, std::size_t N, std::size_t M>
    FixMat<T,N,M> store_type_impl(MatExpr<E,T,N,M> const&);

    template <class E>
    struct StoreType<E, Void_t<decltype(store_type_impl<E>(std::declval<E>()))>>
    {
      using type = decltype(store_type_impl<E>(std::declval<E>()));
    };

  } // end namespace aux


  /// Output shift operator for fixed sizes matrices/vectors (expressions), \relates FixMat
  template <class CharT, class Traits, class E, class T, std::size_t N, std::size_t M>
  std::basic_ostream<CharT, Traits>&
  operator<<(std::basic_ostream<CharT, Traits>& out, MatExpr<E, T, N, M> const& mat)
  {
    out << '[';
    if (N > 0) {
      if (M > 0)
        out << mat(0,0);
      for (std::size_t j = 1; j < M; ++j)
        out << ',' << mat(0,j);
    }
    for (std::size_t i = 1; i < N; ++i) {
      out << "; ";
      if (M > 0)
        out << mat(i,0);
      for (std::size_t j = 1; j < M; ++j)
        out << ',' << mat(i,j);
    }
    out << ']';
    return out;
  }

} // end namespace Dec
