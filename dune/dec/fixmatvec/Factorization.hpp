#pragma once

#include <vector>

#include "FixMatOperations.hpp"
#include "ReductionOperations.hpp"

#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/common/StaticLoops.hpp>

namespace Dec
{
  namespace aux
  {
    template <class V,
      REQUIRES(concepts::Vector<V>) >
    auto householder(V const& x)
    {
      auto v = x.store();
      auto normx = two_norm(v);
      auto s = -sign(v[0]);

      auto v0 = v[0] - s*normx;

      v /= v0;
      v[0] = 1;

      auto beta = -s*v0/normx;

      return std::make_pair(v, beta);
    }

  } // end namespace aux


  /** \defgroup factorization Matrix-factorization
   *  \brief Methods for matrix factorization
   *  @{
   **/

#ifdef DOXYGEN

  /** \relates FixMat
   *  \brief Computes the QR-factorization of a matrix.
   *
   *  Requirement:
   *  - `T` models `concepts::Arithmetic`,
   *  - `N` >= `M`
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  FixMat<T,N,M> qr(MatExpr<E, T, N, M> const& mat);

#else // DOXYGEN

  template <class E, class T, std::size_t N, std::size_t M,
    REQUIRES(concepts::Arithmetic<T> && (N >= M)) >
  FixMat<T,N,M> qr(MatExpr<E, T, N, M> const& mat)
  {
    FixMat<T,N,M> A = mat.store();

    forEach(range_<0,M>, [&A](const auto J) {
      FixVec<T, N-J> v;
      T beta;
      std::tie(v,beta) = aux::householder( A(range_<J,N>, J) );

      A(range_<J,N>, range_<J,M>) -= outer(beta * v, v) * A(range_<J,N>, range_<J,M>);
      if (J < N-1)
        A(range_<J+1,N>, J) = v[range_<1, N-J>];
    });

    return A;
  }

#endif // DOXYGEN

  /** @} **/


  /** \defgroup linear_solver Linear solvers
   *  \brief Methods to solve a linear system of equations
   *  @{
   **/


#ifdef DOXYGEN

  /// \brief Solve a system with an upper triangular matrix
  /**
   * Requirement:
   * - `V` models concept `concepts::Vector` with size `N`
   * - `N >= M`
   * - `S` := `std::common_type_t<T0, Value_t<V>>`
   **/
  template <class E0, class T0, class V, std::size_t N, std::size_t M>
  FixVec<S, M> upper_tri_solve(MatExpr<E0, T0, N, M> const& mat, V const& b);


  /// \brief Solve a system with an lower triangular matrix
  /**
   * Requirement:
   * - `V` models concept `concepts::Vector` with size `N`
   * - `N >= M`
   * - `S` := `std::common_type_t<T0, Value_t<V>>`
   **/
  template <class E0, class T0, class V, std::size_t N, std::size_t M>
  FixVec<S, M> lower_tri_solve(MatExpr<E0, T0, N, M> const& mat, V const& b);


  /// \brief Solve an overdetermined system, using a qr factorization approach.
  /**
   * Requirement:
   * - `T0` models concept `concepts::Arithmetic`
   * - `V` models concept `concepts::Vector` with size `N`
   * - `N >= M`
   *
   * Result:
   * - `S` := `std::common_type_t<T0, Value_t<V>>`
   **/
  template <class E0, class T0, class V, std::size_t N, std::size_t M>
  FixVec<S, M> qr_solve(MatExpr<E0, T0, N, M> const& mat, V const& b);

#else // DOXYGEN

  template <class E0, class T0, class V, std::size_t N, std::size_t M,
    REQUIRES(concepts::Vector_size<V,N> && (N >= M)) >
  auto upper_tri_solve(MatExpr<E0, T0, N, M> const& mat, V const& b)
  {
    using T = Decay_t<decltype( std::declval<T0>() * std::declval<Value_t<V>>() )>;
    FixVec<T, M> x = b[range_<0,M>];
    x[M-1] /= mat(M-1,M-1);
    forEach(range_<2,M+1>, [&mat,&x](const auto _J) {
      constexpr auto J = index_<M - _J>;
      x[J] -= dot(mat(J,range_<J+1,M>), x[range_<J+1,M>]);
      x[J] /= mat(J, J);
    });
    return x;
  }


  template <class E0, class T0, class V, std::size_t N, std::size_t M,
    REQUIRES(concepts::Vector_size<V,N> && (N >= M)) >
  auto lower_tri_solve(MatExpr<E0, T0, N, M> const& mat, V const& b)
  {
    using T = Decay_t<decltype( std::declval<T0>() * std::declval<Value_t<V>>() )>;
    FixVec<T, M> x = b[range_<0,M>];
    x[0] /= mat(0,0);
    forEach(range_<1,M>, [&mat,&x](const auto J) {
      x[J] -= dot(mat(J,range_<0,J-1>), x[range_<0,J-1>]);
      x[J] /= mat(J, J);
    });
    return x;
  }


  template <class E0, class T0, std::size_t N, std::size_t M,
    REQUIRES(concepts::Arithmetic<T0>) >
  std::array<std::size_t, N> get_row_perm(MatExpr<E0, T0, N, M> const& A)
  {
    std::array<std::size_t,N> perm;
    for (std::size_t i = 0; i < N; ++i)
      perm[i] = i;

    for (std::size_t j = 0; j < std::min(N,M); ++j) {
      std::size_t max_i = 0;
      T0 max_value = -1;
      for (std::size_t i = j; i < N; ++i)
        if (std::abs(A(perm[i],j)) > max_value)
          max_i = i, max_value = std::abs(A(perm[i],j));

      std::swap(perm[j], perm[max_i]);
    }

    return perm;
  }



  template <class E0, class T0, class V, std::size_t N, std::size_t M,
    REQUIRES(concepts::Arithmetic<T0> && concepts::Vector_size<V,N> && (N >= M)) >
  auto qr_solve(MatExpr<E0, T0, N, M> const& mat, V const& b_)
  {
    auto A_ = mat.store();

    // row permutation, stores for each column the "best" row index
    auto perm = get_row_perm(A_);

    auto A = row_permutation(A_, perm).store();
    auto b = row_permutation(b_, perm);

    using T = Decay_t<decltype( std::declval<T0>() * std::declval<Value_t<V>>() )>;

    std::array<T,M> beta;
    forEach(range_<0,M>, [&A,&beta](const auto J) {
      FixVec<T0, N-J> v;
      std::tie(v,beta[J]) = aux::householder( A(range_<J,N>, J) );

      A(range_<J,N>, range_<J,M>) -= outer(beta[J] * v, v) * A(range_<J,N>, range_<J,M>);
      if (J < N-1)
        A(range_<J+1,N>, J) = v[range_<1, N-J>];
    });

    // c := Q^t * b
    FixVec<T,N> c = b;
    forEach(range_<0,M>, [&A,&c,&beta](const auto J) {
      auto v = A(range_<J,N>,J).store();
      v[0] = 1;
      c[range_<J,N>] -= v * (beta[J]*dot(v, c[range_<J,N>]));
    });

    // x = R^(-1)*c
    return upper_tri_solve(A, c);
  }

#endif // DOXYGEN

  /** @} **/

} // end namespace Dec
