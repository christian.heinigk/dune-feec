#pragma once

#include "FixMatExpression.hpp"

namespace Dec
{
  namespace expression
  {
    /** \ingroup expression
    * \brief Elementwise unary expression, that applies an operation to each element
    * of the assigned expression.
    **/
    template <class E, class Operation, class T, std::size_t N, std::size_t M>
    struct UnaryExpr
        : public MatExpr<UnaryExpr<E,Operation,T,N,M>, T, N, M>
    {
      UnaryExpr(E const& expr, Operation const& op)
        : expr_(expr)
        , op_(op)
      {}

      template <class... Int>
      auto at(Int... i) const
      {
        static_assert( and_< concepts::Integral<Int>... >, "Requires integer indices!" );
        return op_(expr_.at(i...));
      }

    private:
      E const& expr_;

      Operation op_;
    };


    /** \ingroup expression
    * \brief Elementwise binary expression, that applies an operation to each element
    *  of a pair the assigned expressions.
    **/
    template <class E0, class E1, class Operation, class T, std::size_t N, std::size_t M>
    struct BinaryExpr
        : public MatExpr<BinaryExpr<E0,E1,Operation,T,N,M>, T, N, M>
    {
      BinaryExpr(E0 const& lhs, E1 const& rhs, Operation const& op)
        : lhs_(lhs)
        , rhs_(rhs)
        , op_(op)
      {}

      template <class... Int>
      auto at(Int... i) const
      {
        static_assert( and_< concepts::Integral<Int>... >, "Requires integer indices!" );
        return op_(lhs_.at(i...), rhs_.at(i...));
      }

    private:
      E0 const& lhs_;
      E1 const& rhs_;

      Operation op_;
    };

  } // end namespace expression
} // end namespace Dec
