#pragma once

#include <dune/dec/common/ConceptsBase.hpp>
#include <dune/dec/common/Size.hpp>

namespace Dec
{
  // forward declaration
  template <class Model, class T, std::size_t N, std::size_t M> struct MatExpr;

  namespace concepts
  {
#ifndef DOXYGEN
    namespace definition
    {
      template <class E, class T, std::size_t N>
      std::true_type is_vector(MatExpr<E,T,N,1> const&);

      template <class E, class T, std::size_t N>
      std::true_type is_vector(MatExpr<E,T,1,N> const&);

      template <class E, class T>
      std::true_type is_vector(MatExpr<E,T,1,1> const&);

      std::false_type is_vector(...);

      template <class E, class T, std::size_t N, std::size_t M>
      std::true_type is_matrix(MatExpr<E,T,N,M> const&);

      std::false_type is_matrix(...);

    } // end namespace definition
#endif

    /** \addtogroup concept
     *  @{
     **/

    /// \brief Argument is a vector
    template <class V>
    constexpr bool Vector = decltype(definition::is_vector(std::declval<V>()))::value;

    /// \brief Argument is a matrix
    template <class E>
    constexpr bool Matrix = decltype(definition::is_matrix(std::declval<E>()))::value;

    /** @} **/
  }

  namespace aux
  {
    // specialization of the Size<...> implementation for expressions
    template <class E>
    struct SizeImpl<E, Requires_t<concepts::Matrix<E>> >
        : index_t<E::size()> {};
  }

  namespace concepts
  {
    /** \addtogroup concept
     *  @{
     **/

    /// \brief Argument is a vector of size N
    template <class V, std::size_t N>
    constexpr bool Vector_size = Vector<V> && Size<V> == N;

    /// \brief Argument is a matrix of size NxM
    template <class E, std::size_t N, std::size_t M>
    constexpr bool Matrix_size = Matrix<E> && E::num_rows() == N && E::num_cols() = M;

    /// \brief Arguments are vectors of the same size
    template <class V0, class V1>
    constexpr bool Vectors = Vector<V0> && Vector_size<V1, Size<V0>>;

    /// \brief Arguments are matrices of the same size
    template <class E0, class E1>
    constexpr bool Matrices = Matrix<E0> && Matrix_size<E1, E0::num_rows(), E0::num_cols()>;

    /** @} **/

  } // end namespace concepts

} // end namespace Dec
