install(FILES
  ElementwiseExpr.hpp
  ExprConcepts.hpp
  Factorization.hpp
  FixMat.hpp
  FixMatExpression.hpp
  FixMatOperations.hpp
  ReductionOperations.hpp
  SubMatVec.hpp
  DESTINATION include/dune/dec/fixmatvec/
)
