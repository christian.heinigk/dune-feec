#pragma once

#include "ElementwiseExpr.hpp"
#include "FixMat.hpp"

namespace Dec
{
  /// @{

#ifdef DOXYGEN

  /** \relates FixMat
   * \brief Elementwise addition of matrices.
   *
   * Requirements:
   * - concepts::Addable<T0,T1>
   **/
  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M>
  BinaryExpr<...>
  operator+(MatExpr<E0,T0,N,M> const& lhs, MatExpr<E1,T1,N,M> const& rhs);

  /** \relates FixMat
   *  \brief Elementwise subtraction of matrices
   *
   * Requirements:
   * - `concepts::Subtractable<T0,T1>`
   **/
  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M>
  BinaryExpr<...>
  operator-(MatExpr<E0,T0,N,M> const& lhs, MatExpr<E1,T1,N,M> const& rhs);

  /** \relates FixMat
   *  \brief Elementwise subtraction of matrices
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  UnaryExpr<...>
  operator-(MatExpr<E,T,N,M> const& mat);

  /** \relates FixMat
   *  \brief Elementwise multiplication of a matrix by a scalar (from right)
   *
   * Requirements:
   * - `concepts::Arithmetic<T1>`
   * - `concepts::Multiplicable<T0,T1>`
   **/
  template <class E0, class T0, std::size_t N, std::size_t M, class T1>
  UnaryExpr<...>
  operator*(MatExpr<E0,T0,N,M> const& e, T1 const& s);

  /** \relates FixMat
   *  \brief Elementwise multiplication of a matrix by a scalar (from left)
   *
   * Requirements:
   * - `concepts::Arithmetic<T1>`
   * - `concepts::Multiplicable<T0,T1>`
   **/
  template <class E0, class T0, std::size_t N, std::size_t M, class T1>
  UnaryExpr<...>
  operator*(T1 const& s, MatExpr<E0,T0,N,M> const& e);

  /** \relates FixMat
   *  \brief Elementwise division of a matrix by a scalar
   *
   * Requirements:
   * - `concepts::Arithmetic<T1>`
   * - `concepts::Divisible<T0,T1>`
   **/
  template <class E0, class T0, std::size_t N, std::size_t M, class T1>
  UnaryExpr<...>
  operator/(MatExpr<E0,T0,N,M> const& e, T1 const& s);

  /** \relates FixMat
   *  \brief Hadamard product / elementwise product of matrices
   *
   * Requirements:
   * - `concepts::Multiplicable<T0,T1>`
   **/
  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M>
  BinaryExpr<...>
  multiply(MatExpr<E0,T0,N,M> const& lhs, MatExpr<E1,T1,N,M> const& rhs);

#else // DOXYGEN

  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M,
    REQUIRES(concepts::Addable<T0,T1>) >
  auto operator+(MatExpr<E0,T0,N,M> const& lhs, MatExpr<E1,T1,N,M> const& rhs)
  {
    using T = Store_t<decltype( std::declval<T0>() + std::declval<T1>() )>;
    using Expr = expression::BinaryExpr<E0, E1, operation::plus, T, N, M>;
    return Expr{lhs.get(), rhs.get(), operation::plus{}};
  }

  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M,
    REQUIRES(concepts::Subtractable<T0,T1>) >
  auto operator-(MatExpr<E0,T0,N,M> const& lhs, MatExpr<E1,T1,N,M> const& rhs)
  {
    using T = Store_t<decltype( std::declval<T0>() - std::declval<T1>() )>;
    using Expr = expression::BinaryExpr<E0, E1, operation::minus, T, N, M>;
    return Expr{lhs.get(), rhs.get(), operation::minus{}};
  }

  template <class E, class T, std::size_t N, std::size_t M>
  auto operator-(MatExpr<E,T,N,M> const& mat)
  {
    using T_ = Store_t<decltype( -std::declval<T>() )>;
    using Expr = expression::UnaryExpr<E, operation::negate, T_, N, M>;
    return Expr{mat.get(), operation::negate{}};
  }

  template <class E0, class T0, std::size_t N, std::size_t M, class T1,
    REQUIRES(concepts::Arithmetic<T1> && concepts::Multiplicable<T0,T1>) >
  auto operator*(MatExpr<E0,T0,N,M> const& e, T1 const& s)
  {
    using T = Store_t<decltype( std::declval<T0>() * std::declval<T1>() )>;
    using Expr = expression::UnaryExpr<E0, operation::times_scalar<T1>, T, N, M>;
    return Expr{e.get(), operation::times_scalar<T1>(s)};
  }

  template <class E0, class T0, std::size_t N, std::size_t M, class T1,
    REQUIRES(concepts::Arithmetic<T1> && concepts::Multiplicable<T0,T1>) >
  auto operator*(T1 const& s, MatExpr<E0,T0,N,M> const& e)
  {
    using T = Store_t<decltype( std::declval<T1>() * std::declval<T0>() )>;
    using Expr = expression::UnaryExpr<E0, operation::times_scalar<T1>, T, N, M>;
    return Expr{e.get(), operation::times_scalar<T1>(s)};
  }

  template <class E0, class T0, std::size_t N, std::size_t M, class T1,
    REQUIRES(concepts::Arithmetic<T1> && concepts::Divisible<T0,T1>) >
  auto operator/(MatExpr<E0,T0,N,M> const& e, T1 const& s)
  {
    using T = Store_t<decltype( std::declval<T0>() / std::declval<T1>() )>;
    using Expr = expression::UnaryExpr<E0, operation::divides_scalar<T1>, T, N, M>;
    return Expr{e.get(), operation::divides_scalar<T1>(s)};
  }

  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M,
    REQUIRES(concepts::Multiplicable<T0,T1> /*&& concepts::Multiplicable<T0,T1>*/) >
  auto multiply(MatExpr<E0,T0,N,M> const& lhs, MatExpr<E1,T1,N,M> const& rhs)
  {
    using T = Store_t<decltype( std::declval<T0>() * std::declval<T1>() )>;
    using Expr = expression::BinaryExpr<E0, E1, operation::times, T, N, M>;
    return Expr{lhs.get(), rhs.get(), operation::times{}};
  }

#endif // DOXYGEN

  /// @}

  namespace expression
  {
    /** \ingroup expression
     *  \brief Expression representing a diagonal matrix, created from a vector.
     **/
    template <class E, class T, std::size_t N>
    struct DiagonalMat
        : public MatExpr<DiagonalMat<E,T,N>, T, N, N>
    {
      using const_reference = typename E::const_reference;

      DiagonalMat(E const& expr)
        : vec_(expr)
      {}

      const_reference at(std::size_t i, std::size_t j) const
      {
        return i==j ? vec_.at(i) : zero;
      }

    private:
      E const& vec_;
      T zero = T(0);
    };


    /** \ingroup expression
     *  \brief Expression representing the vector of the diagonal of a matrix.
     **/
    template <class E, class T, std::size_t N>
    struct DiagonalVec
        : public MatExpr<DiagonalVec<E,T,N>, T, N, 1>
    {
      using const_reference = typename E::const_reference;

      DiagonalVec(E const& expr)
        : mat_(expr)
      {}

      const_reference at(std::size_t i, std::size_t = 0) const
      {
        return mat_.at(i, i);
      }

    private:
      E const& mat_;
    };

  } // end namespace expression

  /** \relates FixMat
   *  \brief Convert a vector to diagonal matrix
   **/
  template <class E, class T, std::size_t N>
  expression::DiagonalMat<E, T, N> diagonal(MatExpr<E, T, N, 1> const& vec)
  {
    return {vec.get()};
  }

  template <class E, class T, std::size_t N>
  expression::DiagonalMat<E, T, N> diagonal(MatExpr<E, T, 1, N> const& vec)
  {
    return {vec.get()};
  }

  /** \relates FixMat
   *  \brief Extract the diagonal from a matrix
   **/
  template <class E, class T, std::size_t N>
  expression::DiagonalVec<E, T, N> diagonal(MatExpr<E, T, N, N> const& mat)
  {
    return {mat.get()};
  }

  template <class E, class T, std::size_t N>
  MatExpr<E, T, 1, 1> const& diagonal(MatExpr<E, T, 1, 1> const& mat)
  {
    return mat;
  }


  namespace expression
  {
    /** \ingroup expression
     *  \brief Expression representing the 2d cross product of a vector.
     **/
    template <class V>
    struct Cross2
        : public MatExpr<Cross2<V>, Value_t<V>, 2, 1>
    {
      using E = typename V::model;
      Cross2(E const& expr)
        : a_(expr)
      {}

      auto at(std::size_t i, std::size_t = 0) const
      {
        const std::size_t i0 = (i+1)%2;
        return (1-2*int(i)) * a_[i0];
      }

    private:
      E const& a_;
    };


    /** \ingroup expression
     *  \brief Expression representing the 3d cross product of two vectors.
     **/
    template <class V0, class V1>
    struct Cross3
        : public MatExpr<Cross3<V0,V1>, std::common_type_t<Value_t<V0>, Value_t<V1>>, 3, 1>
    {
      using E0 = typename V0::model;
      using E1 = typename V1::model;
      Cross3(E0 const& expr0, E1 const& expr1)
        : a_(expr0)
        , b_(expr1)
      {}

      auto at(std::size_t i, std::size_t = 0) const
      {
        const std::size_t i0 = (i+1)%3, i1 = (i+2)%3;
        return a_[i0]*b_[i1] - a_[i1]*b_[i0];
      }

    private:
      E0 const& a_;
      E1 const& b_;
    };

  } // end namespace expression


  /** \relates FixVec
   *  \brief Cross-product of two vectors (in 3d only)
   **/
  template <class V0, class V1,
    REQUIRES(concepts::Vector_size<V0,3> && concepts::Vector_size<V1,3>) >
  auto cross(V0 const& a, V1 const& b)
  {
    using Expr = expression::Cross3<V0,V1>;
    return Expr{a.get(), b.get()};
  }

  /** \relates FixVec
   *  \brief Cross-product a 2d-vector = orthogonal vector
   **/
  template <class V,
    REQUIRES(concepts::Vector_size<V, 2>) >
  auto cross(V const& a)
  {
    using Expr = expression::Cross2<V>;
    return Expr{a.get()};
  }


  namespace expression
  {
    /** \ingroup expression
     *  \brief Expression representing an identity matrix.
     **/
    template <class T, std::size_t N>
    struct Eye
        : public MatExpr<Eye<T,N>, T, N, N>
    {
      T at(std::size_t i, std::size_t j) const
      {
        return i==j ? T(1) : T(0);
      }
    };

    /** \ingroup expression
     *  \brief Expression representing a constant matrix.
     **/
    template <class T, std::size_t N, std::size_t M>
    struct Constant
        : public MatExpr<Constant<T,N,M>, T, N, M>
    {
      Constant(T value)
        : value_(value)
      {}

      T at(std::size_t, std::size_t = 0) const
      {
        return value_;
      }

    private:
      T value_;
    };

  } // end namespace expression


  /** \relates FixMat
   *  \brief Returns an expression representing a unit matrix
   **/
  template <class T, std::size_t N>
  expression::Eye<T, N> eye(index_t<N> = {})
  {
    return {};
  }

  /** \relates FixMat
   *  \brief Returns an expression representing a constant matrix = 1
   **/
  template <class T, std::size_t N, std::size_t M=N>
  expression::Constant<T, N, M> ones(index_t<N> = {}, index_t<M> = {})
  {
    return {T(1)};
  }

  /** \relates FixMat
   *  \brief Returns an expression representing a constant matrix = 0
   **/
  template <class T, std::size_t N, std::size_t M=N>
  expression::Constant<T, N, M> zeros(index_t<N> = {}, index_t<M> = {})
  {
    return {T(0)};
  }


  /** \relates FixMat
   *  \brief Extract the i-th row from a matrix
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  view::RowVec<const E, T, M> row(MatExpr<E, T, N, M> const& mat, std::size_t i)
  {
    return {mat.get(), i};
  }

  /** \relates FixMat
   *  \brief Extract the j-th column from a matrix
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  view::ColumnVec<const E, T, N> col(MatExpr<E, T, N, M> const& mat, std::size_t j)
  {
    return {mat.get(), j};
  }

  /** \relates FixMat
   *  \brief View on the upper triangular part of the matrix
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  view::Upper<E, T, N, M> upper(MatExpr<E, T, N, M> const& mat)
  {
    return {mat.get()};
  }

  /** \relates FixMat
   *  \brief View on the lower triangular part of the matrix
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  view::Lower<E, T, N, M> lower(MatExpr<E, T, N, M> const& mat)
  {
    return {mat.get()};
  }


  namespace expression
  {
    /** \ingroup expression
     *  \brief Expression representing the hermitian of a matrix
     **/
    template <class E, class T, std::size_t N, std::size_t M>
    struct Hermitian
        : public MatExpr<Hermitian<E,T,N,M>, T, M, N>
    {
      Hermitian(E const& expr)
        : expr_(expr)
      {}

      T at(std::size_t i, std::size_t j) const
      {
        return conj_op(expr_.at(j,i));
      }

    private:
      E const& expr_;
      operation::hermitian<T> conj_op;
    };

  } // end namespace expression

  /** \relates FixMat
   *  \brief Returns the transposed of the matrix (for real numbers only)
   **/
  template <class E, class T, std::size_t N, std::size_t M,
    REQUIRES(concepts::Arithmetic<T>) >
  auto trans(MatExpr<E, T, N, M> const& m)
  {
    using Expr = expression::Hermitian<E,T,N,M>;
    return Expr{ m.get() };
  }

  /** \relates FixMat
   *  \brief Returns the hermitian of the matrix
   **/
  template <class E, class T, std::size_t N, std::size_t M>
  expression::Hermitian<E,T,N,M>
  hermitian(MatExpr<E, T, N, M> const& m)
  {
    return { m.get() };
  }


  namespace expression
  {
    /** \ingroup expression
     *  \brief Expression representing the outer product of two vectors.
     **/
    template <class V0, class V1, class T>
    struct OuterProduct
        : public MatExpr<OuterProduct<V0, V1, T>, T, V0::size(), V1::size()>
    {
      using E0 = typename V0::model;
      using E1 = typename V1::model;
      OuterProduct(E0 const& expr0, E1 const& expr1)
        : expr0_(expr0)
        , expr1_(expr1)
      {}

      T at(std::size_t i, std::size_t j) const // NOTE: use `auto` here?
      {
        return expr0_.at(i) * expr1_.at(j);
      }

    private:
      E0 const& expr0_;
      E1 const& expr1_;
    };

  } // end namespace expression


  /** \relates FixMat
   *  \brief Create a matrix by the outer product of two vectors.
   **/
  template <class V0, class V1,
    REQUIRES(concepts::Vector<V0> && concepts::Vector<V1>) >
  auto outer(V0 const& vec0, V1 const& vec1)
  {
    using T = Decay_t<decltype( std::declval<Value_t<V0>>() * std::declval<Value_t<V1>>() )>;
    using Expr = expression::OuterProduct<V0, V1, T>;
    return Expr{vec0.get(), vec1.get()};
  }

#ifdef DOXYGEN

  /** \relates FixMat
   *  \brief Matrix-matrix product mat1*mat2
   *
   * Requirement:
   * - `concepts::Multiplicable<T0,T1>`
   **/
  template <class E0, class E1, class T0, class T1, std::size_t K, std::size_t N, std::size_t M>
  FixMat<S, N, M> operator*(MatExpr<E0, T0, N, K> const& mat1, MatExpr<E1, T1, K, M> const& mat2);

#else // DOXYGEN

  template <class E0, class E1, class T0, class T1, std::size_t K, std::size_t N, std::size_t M,
    REQUIRES(concepts::Multiplicable<T0,T1>) >
  auto operator*(MatExpr<E0, T0, N, K> const& mat1, MatExpr<E1, T1, K, M> const& mat2)
  {
    using T = Decay_t<decltype( std::declval<T0>() * std::declval<T1>() )>;
    FixMat<T, N, M> result{}; // value initialization by 0
    for (std::size_t i = 0; i < N; ++i)
      for (std::size_t k = 0; k < K; ++k)
        for (std::size_t j = 0; j < M; ++j)
          result(i,j) += mat1(i,k) * mat2(k,j);

    return result;
  }

#endif // DOXYGEN

  template <class E, class T, std::size_t N, std::size_t M>
  T det(MatExpr<E, T, N, M> const&)
  {
    error_exit("Determinant not yet implemented for this matrix!");
  }

#ifndef DOXYGEN

  // Inverse of a 1x1 matrix
  template <class E, class T>
  FixMat<T,1,1> inv(MatExpr<E, T, 1, 1> const& mat)
  {
    FixMat<T,1,1> result{ T(1)/mat(0,0) };
    return result;
  }

  // Inverse of a 2x2 matrix
  template <class E, class T>
  FixMat<T,2,2> inv(MatExpr<E, T, 2, 2> const& mat)
  {
    FixMat<T,2,2> result{ {mat(1,1), -mat(0,1)}, {-mat(1,0), mat(0,0)} };
    return result /= det(mat);
  }

  // Inverse of a 3x3 matrix
  template <class E, class T>
  FixMat<T,3,3> inv(MatExpr<E, T, 3, 3> const& mat)
  {
    auto A = mat.store();
    FixMat<T,3,3> result{
      {A(1,1)*A(2,2) - A(1,2)*A(2,1), A(0,2)*A(2,1) - A(0,1)*A(2,2), A(0,1)*A(1,2) - A(0,2)*A(1,1)},
      {A(1,2)*A(2,0) - A(1,0)*A(2,2), A(0,0)*A(2,2) - A(0,2)*A(2,0), A(0,2)*A(1,0) - A(0,0)*A(1,2)},
      {A(1,0)*A(2,1) - A(1,1)*A(2,0), A(0,1)*A(2,0) - A(0,0)*A(2,1), A(0,0)*A(1,1) - A(0,1)*A(1,0)} };
    return result /= det(A);
  }

#endif


  namespace expression
  {
    /** \ingroup expression
     *  \brief Expression representing  the minore-matrix, i.e. the matrix with deleted row and column.
     **/
    template <class E, class T, std::size_t N, std::size_t M>
    struct Minore
        : public MatExpr<Minore<E, T, N, M>, T, N-1, M-1>
    {
      using const_reference = typename E::const_reference;

      Minore(E const& expr, std::size_t row, std::size_t col)
        : mat_(expr)
        , row_(row)
        , col_(col)
      {}

      const_reference at(std::size_t i, std::size_t j) const
      {
        std::size_t i0 = i < row_ ? i : i+1,
                    j0 = j < col_ ? j : j+1;
        return mat_(i0, j0);
      }

    private:
      E const& mat_;
      std::size_t row_;
      std::size_t col_;
    };

  } // end namespace expression


  /** \relates FixMat
   *  \brief Minore of a square matrix
   **/
  template <class E, class T, std::size_t N>
  auto minore(MatExpr<E, T, N, N> const& mat, std::size_t i, std::size_t j)
  {
    return det( expression::Minore<E,T,N,N>(mat, i, j) );
  }

  /** \relates FixMat
   *  \brief Adjunkte of a square matrix
   **/
  template <class E, class T, std::size_t N>
  FixMat<T,N,N> adj(MatExpr<E, T, N, N> const& mat)
  {
    auto A = mat.store();

    FixMat<T,N,N> Adj;
    for (std::size_t i = 0; i < N; ++i)
      for (std::size_t j = 0; j < N; ++j)
        Adj(j,i) = std::pow(-1,int(i+j)) * minore(A, i, j);

    return Adj;
  }

  /** \relates FixMat
   *  \brief Inverse of a square NxN matrix
   **/
  template <class E, class T, std::size_t N>
  FixMat<T,N,N> inv(MatExpr<E, T, N, N> const& mat)
  {
    auto A = mat.store();

    FixMat<T,N,N> Adj;
    for (std::size_t i = 0; i < N; ++i)
      for (std::size_t j = 0; j < N; ++j)
        Adj(j,i) = std::pow(-1,int(i+j)) * minore(A, i, j);

    return Adj/=det(A);
  }

  // -------------------------------------------------------------------------------

  namespace expression
  {
    /** \ingroup expression
     *  \brief Expression representing a row-permutation of the matrix, i.e. row-indices are mapped.
     **/
    template <class E, class T, std::size_t N, std::size_t M, class Vec>
    struct RowPermutation
        : public MatExpr<RowPermutation<E, T, N, M, Vec>, T, N, M>
    {
      using const_reference = typename E::const_reference;

      RowPermutation(E const& expr, Vec const& p)
        : mat_(expr)
        , perm_(p)
      {}

      const_reference at(std::size_t i, std::size_t j) const
      {
        return mat_.at(perm_[i], j);
      }

      const_reference at(std::size_t idx) const
      {
        std::size_t i = idx/M;
        std::size_t j = idx%M;
        return mat_.at(perm_[i]*M + j);
      }

    private:
      E const& mat_;
      Vec const& perm_;
    };

  } // end namespace expression


  /** \relates FixMat
   *  \brief Permutation of a matrix
   **/
  template <class E, class T, std::size_t N, std::size_t M, class Vec>
  auto row_permutation(MatExpr<E, T, N, M> const& mat, Vec const& perm)
  {
    assert( perm.size() == N );
    return expression::RowPermutation<E,T,N,M,Vec>(mat.get(), perm);
  }

  // -------------------------------------------------------------------------------


  /// Compare the values of two expression, returns true if all values are equal
  template <class E0, class E1, class T0, class T1, std::size_t N, std::size_t M>
  bool compare(MatExpr<E0,T0,N,M> const& mat0, MatExpr<E1,T1,N,M> const& mat1)
  {
    for (std::size_t i = 0; i < N; ++i)
      for (std::size_t j = 0; j < M; ++j)
        if (mat0(i,j) != mat1(i,j))
          return false;
    return true;
  }


  template <class V0, class V1,
    REQUIRES(concepts::Vectors<V0,V1>) >
  bool compare(V0 const& vec0, V1 const& vec1)
  {
    for (std::size_t i = 0; i < V0::size(); ++i)
      if (vec0[i] != vec1[i])
        return false;
    return true;
  }


  template <class E0, class E1>
  bool operator==(BaseExpression<E0> const& expr0, BaseExpression<E1> const& expr1)
  {
    return compare(expr0.get(), expr1.get());
  }

  template <class E0, class E1>
  bool operator!=(BaseExpression<E0> const& expr0, BaseExpression<E1> const& expr1)
  {
    return !(expr0.get() == expr1.get());
  }

  template <class T0, class T1, std::size_t N, std::size_t M>
  bool operator==(FixMat<T0,N,M> const& mat0, FixMat<T1,N,M> const& mat1)
  {
    return compare(mat0, mat1);
  }

  template <class T0, class T1, std::size_t N, std::size_t M>
  bool operator!=(FixMat<T0,N,M> const& mat0, FixMat<T1,N,M> const& mat1)
  {
    return !(mat0 == mat1);
  }

} // end namespace Dec
