#pragma once

#include <limits>

#include <dune/dec/utility/FunctorVector.hpp>

namespace Dec
{
  /**
    * \defgroup operators Operators
    * \brief Predefined terms representing (differential) operators, to assemble a linear system.
    *
    * @{
    **/

    /*
    * Operators are used to simplify the assembling of linear systems. Therefore, standard
    * differential operators are provided that can be chained to describe a partial differential
    * equation in simple terms.
    *
    * Basic usage:
    * ```
    * DOFMatrix<double> mat;
    * OperatorXYZ<GridView> op(gridview);
    *
    * op.build(mat); // assemble operator into matrix.
    * ```
    *
    * The method `build()` creates a new inserter to insert coefficients of one specific
    * operator. When combining several operators, use the `assemble()` method. This expects
    * an inserter instead of a matrix.
    * ```
    * op.init(mat);
    * { // local scope
    *   auto ins = mat.inserter();
    *   op.assemble(ins);
    * }
    * op.finish(mat);
    **/


  class OperatorAccessor
  {
    template <class Model, class GridView, int N, int M> friend class Operator;

    template <class Derived>
    static auto nzrows(Derived const& derived)
    {
      return derived.nzrows_impl();
    }

    template <class Derived, class Inserter>
    static void assemble(Derived const& derived, Inserter& A, float_type factor)
    {
      derived.assemble_impl(A, factor);
    }

    template <class Derived, class Inserter, class Entity>
    static void assembleRow(Derived const& derived, Inserter& A, Entity const& e, float_type factor)
    {
      derived.assembleRow_impl(A, e, factor);
    }
  };


  /// \brief Base class for all operators, using CRT-Pattern.
  /**
   * Usage: Specify class Derived<GridView> : Operator<Derived, GridView, N, M>.
   * The parameter N and M specify the grid dimension of the
   * rows/ column entities.
   **/
  template <class Model, class GridView, int N, int M>
  class Operator
  {
    friend class OperatorAccessor;

    template <class,class,class,int,int> friend class ChainedOperator;
    template <class,class,int,int> friend class ScaleOperator;

    static_assert( 0 <= N && N <= GridView::dimension, "N out of range [0,dim]" );
    static_assert( 0 <= M && M <= GridView::dimension, "M out of range [0,dim]" );

  public:
    static constexpr int dimension = GridView::dimension;

    /// Constructor, stores a reference to the `grid`.
    Operator(GridView const& gv)
      : gv_(gv)
    {}


  public: // Methods:

    /// Returns reference to the grid
    GridView const& gridView() const
    {
      return gv_;
    }

    /// Returns the number of rows of the matrix
    std::size_t rows() const
    {
      return gv_.size(dimension - N);
    }

    /// Returns the number of columns of the matrix
    std::size_t cols() const
    {
      return gv_.size(dimension - M);
    }

    /// Returns the number of nonzeros in the major dimension
    auto nzrows() const
    {
      return OperatorAccessor::nzrows(derived());
    }

    template <class Matrix>
    void init(Matrix& A) const
    {
      A.resize(rows(), cols());
    }

    template <class Matrix>
    void finish(Matrix& /*A*/) const
    {}

    template <class Inserter>
    void assemble(Inserter& ins, float_type factor = 1) const
    {
      OperatorAccessor::assemble(derived(), ins, factor);
    }

    template <class Matrix>
    void build(Matrix& A, float_type factor = 1) const
    {
      init(A);
      {
        auto ins = A.inserter();
        assemble(ins, factor);
      }
      finish(A);
    }


  protected:

    template <class Inserter, class Entity>
    void assembleRow(Inserter& A, Entity const& e, float_type factor) const
    {
      OperatorAccessor::assembleRow(derived(), A, e, factor);
    }


    template <class T>
    T regularize(T const& x) const
    {
      return max(10*std::numeric_limits<T>::epsilon(), x);
    }


  protected: // Implementation details. Should be overridden in subclasses

    // default implementation: 1 columns per row
    auto nzrows_impl() const
    {
      return make_vector([](std::size_t) { return 20; });
    }

    // default implementation: traverse entities of dim N and assemble row
    template <class Inserter>
    void assemble_impl(Inserter& A, float_type factor) const
    {
      for (auto const& e : entities(gv_, Dim_<N>, Dune::Partitions::InteriorBorder{}))
        assembleRow(A, e, factor);
    }

    // default implementation: do nothing
    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& /*A*/, Entity const& /*e*/, float_type /*factor*/) const
    {
      /* do nothing */
    }


  public:

    Model& derived()
    {
      return static_cast<Model&>(*this);
    }

    Model const& derived() const
    {
      return static_cast<Model const&>(*this);
    }


  protected: // Member variables:

    GridView gv_;
  };

  /** @} */

} // end namespace Dec
