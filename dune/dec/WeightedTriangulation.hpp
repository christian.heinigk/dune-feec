#pragma once

#include <array>
#include <vector>

#include <dune/localfunctions/lagrange/p1.hh>

#include "LinearAlgebra.hpp"
#include "common/Dim.hpp"
#include "common/Index.hpp"
#include "common/StaticLoops.hpp"
#include "operators/Laplace.hpp"
#include "operators/FEMLaplace.hpp"
#include "operators/FEMCenterWeight.hpp"
#include "linear_algebra/krylov/cg.hpp"
#include "linear_algebra/preconditioner/Diagonal.hpp"
#include "ranges/Enumerate.hpp"
#include "utility/Timer.hpp"

namespace Dec
{
  class WeightedTriangulation
  {
    using Self = WeightedTriangulation;

    template <int dim>
    using P1 = Dune::P1LocalFiniteElement<float_type, float_type, dim>;

    template <int dow>
    using WorldVector = Dune::FieldVector<float_type, dow>;

    template <int dim>
    using JacobianType = Dune::FieldMatrix<double, 1, dim>;

  public:

    /// Constructor that initializes the geometric data
    template <class Grid>
    static void apply(Grid& grid)
    {
      using GridView = typename Grid::GridView;

      Timer t;
      DOFMatrix<double> A;
      FEMLaplace<GridView> laplace(grid.leafGridView());
      laplace.build(A);
      A.clear_dirichlet_row(0);

      DOFVector<double> b(grid.leafGridView(), 0);
      FEMCenterWeight<GridView> op_rhs(grid.leafGridView());
      op_rhs.assemble_vec(b);
      b[0] = 1.0;

      DOFVector<double> weights(grid.leafGridView(), 0, 1.0);
      auto precon = precon::Diagonal{};
      auto iter = solver::pcg(A, weights, b, precon.compute(A));
      msg("nr of iterations: ",iter);

      calc_center(grid, weights);

      // update dual volume
      grid.geometries_.back().init_dual_volume(grid.indexSet());

      msg("time for weighted triangulation: ",t.elapsed());
    }

  private: // Implementation details:

    template <class Grid, class Vector>
    static void calc_center(Grid& grid, Vector const& weights)
    {
      std::array<std::vector<bool>, Grid::dimension> visited;
      forEach(range_<0, Grid::dimension>, [&grid,&visited](auto const cd) {
        std::get<cd>(visited).resize(grid.size(cd), false);
      });

      for (auto const& element : elements(grid.leafGridView()))
      {
        forEach(range_<0, Grid::dimension>, [&grid, &weights, &element, &visited](auto const cd)
        {
          static constexpr int dim = Grid::dimension - cd;
          auto feSpace = P1<dim>{};

          auto const& localBasis = feSpace.localBasis();
          auto const& refElem = Dune::ReferenceElements<float_type, dim>::simplex();
          auto pos0 = refElem.position(0, dim);

          // The gradients of the shape functions on the reference element
          std::vector<JacobianType<dim>> referenceGradients;
          localBasis.evaluateJacobian(pos0, referenceGradients);

          for (auto const& entity : entities(element, Codim_<cd>)) {
            if (std::get<cd>(visited)[entity.index()])
              continue;

            auto geometry = entity.geometry();
            assert( geometry.affine() );

            auto const& jacobian = geometry.jacobianInverseTransposed(pos0);

            // c* = c^old - 0.5 * sum_j{ w_j*grad(psi_j) }
            auto c = grid.center(entity);

            for (auto v : enumerate(vertices(entity))) {
              // Compute the shape function gradients on the real element
              WorldVector<Grid::dimensionworld> grad;
              jacobian.mv(referenceGradients[v.first][0], grad);

              c -= (0.5*weights[v.second.index()]) * grad;
            }

            // set center of element
            std::get<dim>(grid.geometries_.back().centers_)[entity.index()] = c;
            std::get<cd>(visited)[entity.index()] = true;
          }
        }); // end for (cd)
      } // end for (element)
    }
  };

} // end namespace Dec
