#pragma once

#include <type_traits>

#include "common/Common.hpp"

#ifndef DEC_DIM
#define DEC_DIM 3
#endif
#ifndef DEC_DOW
#define DEC_DOW 3
#endif

#include <dune/common/parallel/mpihelper.hh>

namespace Dec
{
  /// type of floating point values
  using float_type = double;

  /// the dimension of the world the grid is embedded in
  static constexpr std::size_t dim_of_world = DEC_DOW;

  /// \brief In order to use \ref DOFVector and \ref DOFMatrix with PETSc backend,
  /// this class must be instantiated at the very first beginning of the main()
  class decpde
  {
  private:
    /// private constructor. Create an instance by \ref init
    decpde(int& argc, char**& argv);

    /// Destructor. Calls \ref PetscFinalize in case that PETSc backend is enabled.
    ~decpde();

    Dune::MPIHelper& mpihelper_;

  public:
    /// Create an singleton instance of the class \ref decpde
    static decpde& init(int& argc, char**& argv)
    {
      static decpde instance(argc, argv);
      return instance;
    }

    Dune::MPIHelper& mpihelper()
    {
      return mpihelper_;
    }
  };
} // end namespace Dec
