#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>
#include <Eigen/IterativeLinearSolvers>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/Parameter.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/linear_algebra/block.hpp>
#include <dune/dec/utility/FactoryParametrization.hpp>
#include <dune/dec/utility/SphereMapping.hpp>
#include <dune/dec/utility/Timer.hpp>

#include "PfcOperator.hpp"

using namespace Dec;

struct Radius
{
  template <class T>
  static float_type eval(T const&) { return 15.0; }
};

int main(int argc, char** argv)
{
  using namespace Dec::literal;
  decpde::init(argc, argv);

  assert( argc > 1 );

  Parameter param;
  read(argv[1], param);

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;
  using Param = Parametrization<GridBase, SphereMapping<DEC_DIM, DEC_DOW, Radius>>;

  Dune::GridFactory<Param> gridFactory;
  Dune::AlbertaReader<Param>().readGrid(param["grid"]["filename"].string_value(), gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.create() );

  gridBase->globalRefine(param["grid"]["refinement"].int_value());

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase, true);
  MusicalIsomorphism<GridView> transform(grid.leafGridView());

  using Matrix = DOFMatrix<double>;

  // matrix containing the linear term
  DOFVector<double> b(grid.leafGridView(), 0); // rhs vector
  DOFVector<double> solution(grid.leafGridView(), 0); // rhs vector


  double tau = param["time"]["timestep"].number_value();
  double eps = param["pfc"]["eps"].number_value(); // 0.4
  double mean_psi = param["pfc"]["psi"].number_value(); // -0.3;

  Timer timer;

  Identity<GridView,0> id(grid.leafGridView());
  LaplaceDeRham<GridView,0> laplace(grid.leafGridView());
  Nonlin<GridView> nonlin(grid.leafGridView(), solution);

  Matrix A,B;

  Matrix L;
  laplace.build(L);

  Matrix I;
  id.build(I, 1.0);

  Matrix B0 = -2.0*I + L;
  Matrix B1 = (L*B0).pruned();
  Matrix B2 = (1.0-eps)*I + B1;
  Matrix B3 = (L*B2).pruned();

  Matrix Blin = (1.0/tau)*I + B3;
//   Matrix Blin = (1.0/tau)*I + L*((1.0-eps)*I + L*(-2.0*I + L));

  DEC_MSG("time(assemble) = ",timer.elapsed());

  // set initial values
  transform.interpol(solution, int_<0>, [mean_psi](auto const&) {
    return mean_psi + 0.5*(std::rand()/double(RAND_MAX) - 0.5);
  });

  // create file writer
  using VtkWriter = Dune::VTKWriter<GridView>;
  std::shared_ptr<VtkWriter> writer = std::make_shared<VtkWriter>(grid.leafGridView());
  Dune::VTKSequenceWriter<GridView> pvdwriter(writer,
                                              param["output"]["filename"].string_value(),
                                              param["output"]["directory"].string_value(),
                                              "data");

  pvdwriter.addVertexData(solution, "psi");
  pvdwriter.write(0.0);

//   Eigen::UmfPackLU<Matrix> solver;
  Eigen::BiCGSTAB<Eigen::SparseMatrix<double>, Eigen::DiagonalPreconditioner<double> > solver;

  bool analyzed = false;
  for (double t = 0.0; t < param["time"]["end_time"].number_value(); t += tau)
  {
    DEC_MSG("");
    DEC_MSG("Timestep t=",t);

    timer.reset();
    B.setZero();
    nonlin.build(B);
//     A = Blin + L*B;

    B0 = (L*B).pruned();
    A = Blin + B0;

    auto const& psi = solution.array();
    DOFVector<double> psi3 = 2.0*cube(psi);
    b = (1.0/tau) * psi;
    b+= L*psi3;
    DEC_MSG("time(assemble) = ",timer.elapsed());

    timer.reset();
//     if (!analyzed) {
//       solver.analyzePattern(A);
//       analyzed = true;
//     }
//     solver.factorize(A);
    DEC_MSG("time(factorize) = ",timer.elapsed());

    timer.reset();
//     solution = solver.solve(b);
    solution = solver.compute(A).solveWithGuess(b, solution);
    DEC_MSG("time(solve) = ",timer.elapsed());
    DEC_MSG("estimated error: ", residuum(A, solution, b));

    timer.reset();
    pvdwriter.write(t+tau);
    DEC_MSG("time(write) = ",timer.elapsed());
  }

  pvdwriter.write(param["time"]["end_time"].number_value());
}
