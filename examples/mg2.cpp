#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/utility/Timer.hpp>
#include <dune/dec/linear_algebra/multigrid.hpp>
#include <dune/dec/linear_algebra/iteration.hpp>
#include <dune/dec/linear_algebra/smoother/Jacobi.hpp>

using namespace Dec;

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

template <class GridView>
double get_grid_width(GridView const& gv)
{
  double h = 0.0;
  for (auto const& e : edges(gv)) {
    if (gv.volume(e) > h)
      h = gv.volume(e);
  }

  return h;
}


template <class Grid>
class DirichletTransfer
    : public DefaultTransfer<Grid>
{
  using Super = DefaultTransfer<Grid>;
  using List = std::list<std::size_t>;

public:
  DirichletTransfer(Grid const& grid, int minLevel, int maxLevel)
    : Super(grid)
    , boundary_(minLevel, maxLevel)
  {}

  template <class LinearOperator, class F,  class... Args>
  void init(int l, LinearOperator& out, F&& f, Args&&... args) const
  {
    Super::init(l, out, std::forward<Args>(args)...);
    boundary_[l] = get_boundary<Grid::dimension>(Super::grid_.levelGridView(l), std::forward<F>(f));
  }

  template <class VectorIn, class VectorOut>
  void restrict(int l, VectorIn const& in, VectorOut& out, bool add = false) const
  {
    assert( !add );
    Super::restrict(l, in, out, add);
    for (std::size_t r : boundary_[l])
      out[r] = 0;
  }

  template <class LinOpIn, class LinOpOut, class... Args>
  void restrictMatrix(int l, LinOpIn const& in, LinOpOut& out, Args&&... args) const
  {
    // assemble on coarse grid level
    Super::restrictMatrix(l, in, out, std::forward<Args>(args)...);
    for (std::size_t r : boundary_[l])
      out.matrix().clear_dirichlet_row(r);
  }

private:

  mutable Hierarchy<List> boundary_;
};


struct PoissonOperator
{
  template <class GridView>
  void init(GridView const& gv)
  {
    LaplaceBeltrami<GridView> laplace(gv);
    laplace.build(matrix_, -1.0);
  }

  template <class GridView>
  void build(GridView const& gv) {}

  DOFMatrix<double> const& matrix() const { return matrix_; }
  DOFMatrix<double>&       matrix()       { return matrix_; }

private:
  DOFMatrix<double> matrix_;
};


int main(int argc, char** argv)
{
  decpde::init(argc, argv);
  Timer timer;

  assert_msg( argc > 1, "usage: ./ellipt grid-filename nSteps");

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  int nSteps = 2;
  if (argc > 2)
    nSteps = std::atoi(argv[2]);

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  grid.globalRefine(nSteps, true);

  double h = grid.width();
  msg("h=",h,", grid.size(2)=",grid.size(2));

  MusicalIsomorphism<GridView> transform(grid.leafGridView());

  timer.reset();
  PoissonOperator A;
  A.init(grid.leafGridView());

  DOFVector<double> b(grid.leafGridView(), 0),
                    u(grid.leafGridView(), 0);

  // rhs
  transform.interpol(b, int_<0>, [](auto const& x) {
    double r2 = unary_dot(x);
    double ux = exp(-10.0 * r2);
    return -(400.0 * r2 - 20.0 * DEC_DOW) * ux;
  });

  // boundary condition
  transform.interpol(u, int_<0>, [](auto const& x) {
    return exp(-10.0 * unary_dot(x));
  });

  auto boundary_fct = [](auto const& x) {
    return x[0] < 1.e-10 || x[0] > 1.0 - 1.e-10 || x[1] < 1.e-10 || x[1] > 1.0 - 1.e-10;
  };
  auto boundary = get_boundary<Grid::dimension>(grid.leafGridView(), boundary_fct);

  for (std::size_t r : boundary) {
    A.matrix().clear_dirichlet_row(r);
    b[r] = u[r];
  }
  msg("time(assemble on leafLevel) = ",timer.elapsed()," sec");

  int minLevel = 0;
  int maxLevel = grid.maxLevel();

  timer.reset();
  DirichletTransfer<Grid> transfer{grid, minLevel, maxLevel};
  MGParameter mg_param{1, 1, 2};

  using Solver = MGSolver<PoissonOperator, decltype(transfer)>;

  Solver solver(transfer, Jacobi{0.8}, mg_param);
  solver.init(minLevel, maxLevel, boundary_fct);
  msg("time(init solver) = ",timer.elapsed()," sec");

  timer.reset();
  GenericIteration<NoisyIteration<float_type>> mg_iter{[&A,&u,&b]() { return residuum(A.matrix(),u,b); }, 100, 1.e-7};
  solver.compute(A).solve(b, u, mg_iter);
  msg("time(solve) = ",timer.elapsed()," sec");
}
