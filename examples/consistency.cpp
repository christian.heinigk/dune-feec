#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <Eigen/UmfPackSupport>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/operators/FEMLaplace.hpp>
#include <dune/dec/operators/FEMCenterWeight.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/ChainedOperator.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/WeightedTriangulation.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>

using namespace Dec;

// laplace^RR(p) + p = (-cos(x)*sin(y) + x^2(y^2+x-2), 2*sin(x)*cos(y) + 4*x*y), p = 0 for x = 0
//
// => p = (x^2*(y^2 + x), sin(x)*cos(x))

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}



template <class T, int ncomps>
struct VectorValuedContainer
{
  T const& operator[](std::size_t i) const
  {
    std::size_t const r = i / ncomps;
    std::size_t const c = i % ncomps;
    return data[r][c];
  }

  std::size_t size() const { return data.size() * ncomps; }

  DOFVector<Dune::FieldVector<T,ncomps>> const& data;
};



int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert( argc > 1 );

  using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  using WorldVector = typename Grid::GlobalCoordinate;
  Grid grid(*gridBase);
  auto gv = grid.leafGridView();

  // move cell-centers in direction of barycenter
  WeightedTriangulation::apply(grid);
  MusicalIsomorphism<GridView> transform(gv);

  using Matrix = DOFMatrix<double>;
  Matrix A;

  LaplaceRR<GridView, 1> laplace(gv);
  Identity<GridView, 1> id(gv);
  laplace.init(A);
  {
    auto ins = A.inserter();
    laplace.assemble(ins, -1.0);
    id.assemble(ins, 1.0);
  }
  laplace.finish(A);

  DOFVector<WorldVector> b_(gv, 0, "b");
  transform.interpol(b_, int_<0>, [](auto const& x) {
    return WorldVector{ sqr(x[0])*(sqr(x[1]) + x[0] - 2) - std::cos(x[0])*std::sin(x[1]), 2*std::sin(x[0])*std::cos(x[1]) + 4*x[0]*x[1] };
  });
  auto b = transform.flat<0, 1>(b_);

  DOFVector<WorldVector> solution_(gv, 0);
  transform.interpol(solution_, int_<0>, [](auto const& x) {
    return WorldVector{ sqr(x[0])*(sqr(x[1]) + x[0]), std::sin(x[0])*std::cos(x[1]) };
  });
  auto solution = transform.flat<0, 1>(solution_);

  auto boundary = get_boundary<1>(gv, [](auto const& x) { return x[0] < 1.e-10 || x[0] > 1.0 - 1.e-10 || x[1] < 1.e-10 || x[1] > 1.0-1.e-10; });
  msg("#boundary-indices = ",boundary.size());
  for (auto i : boundary) {
    A.clear_dirichlet_row(i);
    b[i] = solution[i];
  }

  Eigen::UmfPackLU<typename Matrix::Matrix> solver;

  solver.analyzePattern(A);
  solver.factorize(A);

  DOFVector<double> x(gv, 1, "x");
  x = solver.solve(b);

  auto x_ = transform.sharp<1, 2>(x);

  msg("residuum:     ", residuum(A, x, b));

  Dune::VTKWriter<GridView> writer(gv);

  VectorValuedContainer<float_type, Grid::dimensionworld> solution_view{solution_};
  VectorValuedContainer<float_type, Grid::dimensionworld> x_view{x_};

  writer.addVertexData(solution_view, "solution", Grid::dimensionworld);
  writer.addCellData(x_view, "x", Grid::dimensionworld);
  writer.write("consistency");

  solution -= x;

  auto diff = transform.sharp<1, 2>(solution);

  double two_error = 0.0, inf_error = 0.0;
  for (auto const& e : elements(gv)) {
    two_error += unary_dot(diff[e.index()]) * gv.volume(e);
    inf_error = std::max(inf_error, abs_max(diff[e.index()]));
  }

  msg("|x - solution|_2 = ",std::sqrt(two_error));
  msg("|x - solution|_inf = ",inf_error);
}
