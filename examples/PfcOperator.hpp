#pragma once

#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/linear_algebra/multigrid/DefaultTransfer.hpp>
#include <dune/dec/linear_algebra/multigrid/Hierarchy.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/Laplace.hpp>

namespace Dec
{
  // Operator 3*psi'^2
  template <class GridView>
  class Nonlin
      : public Operator<Nonlin<GridView>, GridView, 0,0>
  {
    friend class Dec::OperatorAccessor;
    using Super = Operator<Nonlin<GridView>, GridView, 0,0>;
    using Vector = DOFVector<double>;

  public:

    /// Constructor
    Nonlin(GridView const& gv, Vector const& psi, float_type value = 1)
      : Super(gv)
      , psi_(psi)
      , value_(value)
    {}

  protected:

    template <class Inserter, class Entity>
    void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
    {
      std::size_t i = e.index();
      A(i,i) << factor*value_*3*sqr(psi_[i]);
    }

  private:

    Vector const& psi_; // old_solution
    float_type value_;
  };


  class PfcOperator
  {
    using Matrix = DOFMatrix<double>;

  public:

    PfcOperator()
    {}

  public:

    template <class GridView>
    void init(GridView const& gv, double tau, double eps)
    {
      Identity<GridView,0> id(gv);
      LaplaceBeltrami<GridView> laplace(gv);

      laplace.build(L_);

      Matrix I;
      id.build(I, 1.0);

      Matrix B0 = -2.0*I + L_;
      Matrix B1 = (L_*B0).pruned();
      Matrix B2 = (1.0-eps)*I + B1;
      Matrix B3 = (L_*B2).pruned();

      Blin_ = (1.0/tau)*I + B3;
    }

    template <class GridView>
    void build(GridView const& gv, DOFVector<double> const& psi)
    {
      Nonlin<GridView> nonlin(gv, psi);
      
      Matrix matNonlin;
      nonlin.build(matNonlin);
      
      Matrix Bnonlin_ = L_*matNonlin;

      matrix_ = Blin_;
      matrix_ += Bnonlin_;
    }

    Matrix const& matrix() const { return matrix_; }
    Matrix&       matrix()       { return matrix_; }

    Matrix const& laplace() const { return L_; }

  private:

    Matrix L_;
    Matrix Blin_;

    Matrix matrix_;
  };


  template <class Grid>
  struct PfcTransfer
      : public DefaultTransfer<Grid>
  {
    using Super = DefaultTransfer<Grid>;

    PfcTransfer(Grid const& grid, int minLevel, int maxLevel, DOFVector<double>& psi)
      : Super(grid)
      , solutions(minLevel,  maxLevel)
    {
      solutions.setFinest(psi);
    }

    template <class LinOpIn, class LinOpOut>
    void restrictMatrix(int l, LinOpIn const& in, LinOpOut& out) const
    {
      Super::restrict(l, solutions[l+1], solutions[l]);
      Super::restrictMatrix(l, in, out, solutions[l]);
    }

  private:

    // solution vector on different levels
    mutable Hierarchy<DOFVector<double>> solutions;
  };

} // end namespace Dec
