#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <array>
#include <iostream>
#include <memory>
#include <vector>

#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/utility/parmetisgridpartitioner.hh>
#include <dune/grid/uggrid.hh>

template <class GridView, int cd>
class Handle
    : public Dune::CommDataHandleIF<Handle<GridView, cd>, int>
{
public:
  Handle(GridView const& gv)
    : gv_(gv)
  {}

  bool contains(int /*dim*/, int codim) const { return codim == cd; }
  bool fixedSize(int /*dim*/, int /*codim*/) const { return true; }

  template <class Entity>
  std::size_t size(Entity const& /*e*/) const { return 1u; }

  template <class MessageBuffer, class Entity>
  void gather(MessageBuffer& buff, Entity const& e) const
  {
    auto const& indexSet = gv_.indexSet();
    std::cout << "[" << gv_.comm().rank() << "] send entity " << indexSet.index(e) << "\n";
    buff.write(1);
  }

  template <class MessageBuffer, class Entity>
  void scatter(MessageBuffer& buff, Entity const& e, std::size_t /*n*/)
  {
    int result = -1;
    buff.read(result);

    auto const& indexSet = gv_.indexSet();
    std::cout << "[" << gv_.comm().rank() << "] receive entity " << indexSet.index(e) << "\n";
  }

private:
  GridView gv_;
};


int main(int argc, char** argv)
{
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  // Create ug grid from structured grid
  std::array<unsigned int, 2> n = {{2, 2}};
  Dune::FieldVector<double, 2> lower = {{0, 0}};
  Dune::FieldVector<double, 2> upper = {{1, 1}};

  using GridBase = Dune::UGGrid<2>;
  std::shared_ptr<GridBase> gridBase = Dune::StructuredGridFactory<GridBase>::createSimplexGrid(lower, upper, n);

  using GridView = typename GridBase::LeafGridView;
  auto gv = gridBase->leafGridView();

  // Create initial partitioning using ParMETIS
  std::vector<unsigned> part(Dune::ParMetisGridPartitioner<GridView>::partition(gv, mpihelper));

  // Transfer partitioning from ParMETIS to our grid
  gridBase->loadBalance(part, 0);

  Handle<GridView, 1> handle(gv);
  gv.communicate(handle, Dune::InteriorBorder_All_Interface, Dune::ForwardCommunication);
}
