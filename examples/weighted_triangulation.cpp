#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/operators/FEMLaplace.hpp>
#include <dune/dec/operators/FEMCenterWeight.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>

using namespace Dec;

template <class Matrix>
void clearDirichletRows(Matrix& A, int idx)
{
#if DEC_HAS_EIGEN
  using value_type = typename Matrix::value_type;

  for (int k = 0; k < A.outerSize(); ++k) {
    for (typename Matrix::InnerIterator it(A, k); it; ++it) {
      if (it.index() != idx)
        break;

      it.valueRef() = it.row() == it.col() ? value_type(1) : value_type(0);
    }
  }
#endif
}

int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert( argc > 1 );

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  using Grid = DecGrid<GridBase>;
  Grid grid(*gridBase);

  using Matrix = DOFMatrix<double>;

  Timer t;
  Matrix A;

  FEMLaplace<Grid> laplace(grid);
  laplace.build(A);
  A.clear_dirichlet_row(0);
  msg("time(assemble) = ",t.elapsed());

  MusicalIsomorphism<Grid> transform(grid);

  DOFVector<double> b(grid, 0, "b");
  FEMCenterWeight<Grid> op_rhs(grid);
  op_rhs.assemble_vec(b);
  b[0] = 1.0;

  t.reset();
  DOFVector<double> weights(grid, 0, "weights");
  int iter = solver::bcgs(A, weights, b);
  msg("time(solve) = ",t.elapsed());
  msg("#iterations:     ", iter);

  Dune::VTKWriter<typename GridBase::LeafGridView> writer(gridBase->leafGridView());
  writer.addVertexData(weights, "weights");

  writer.write("weights");


  using GridView = typename Grid::LeafGridView;
  using FeSpace = Dune::Functions::PQ1NodalBasis<GridView>;

  FeSpace feSpace_(grid.leafGridView());
  auto localView = feSpace_.localView();
  auto localIndexSet = feSpace_.localIndexSet();

  for (auto const& element : elements(grid.leafGridView())) {
    localView.bind(element);
    localIndexSet.bind(localView);

    auto geometry = element.geometry();
    auto const& refElem = Dune::ReferenceElements<float_type, Grid::dimension>::simplex();
    auto const& localBasis = localView.tree().finiteElement().localBasis();

    // The transposed inverse Jacobian of the map from the reference element to the element
    auto const jacobian = geometry.jacobianInverseTransposed(refElem.position(0, Grid::dimension));

    auto c = grid.center(element);
    for (int v_i = 0; v_i < geometry.corners(); ++v_i) {
      auto const& pos = refElem.position(v_i, Grid::dimension);

      // The gradients of the shape functions on the reference element
      std::vector<Dune::FieldMatrix<double,1,Grid::dimension> > referenceGradients;
      localBasis.evaluateJacobian(pos, referenceGradients);

      // Compute the shape function gradients on the real element
      Dune::FieldVector<double,Grid::dimensionworld> grad;
      jacobian.mv(referenceGradients[v_i][0], grad);

      c -= 0.5*weights[localIndexSet.index(v_i)]*grad;
    }

    msg("new center of element(",grid.index(element),") = ",c);
  }
}
