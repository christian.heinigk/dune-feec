#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/dec/DecGrid.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/utility/FactoryParametrization.hpp>
#include <dune/dec/utility/SphereMapping.hpp>

#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>

using namespace Dec;

// -laplace(u) = 1
//       u(x0) = 0

struct UnitRadius
{
  template <class T>
  static float_type eval(T&&) { return 1.0; }
};


int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  assert_msg( argc > 1, "usage: ", argv[0], " grid-filename [nSteps]");

  using GridBase = Dune::FoamGrid<2, 3>;
  using Param = Parametrization<GridBase, SphereMapping<2, 3, UnitRadius>>;

  Dune::GridFactory<Param> gridFactory;
  Dune::AlbertaReader<Param>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.create() );

  int nSteps = 3;
  if (argc > 2)
    nSteps = std::atoi(argv[2]);

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  auto gv = grid.leafGridView();

  grid.globalRefine(nSteps);

  DOFMatrix<double> A;

  LaplaceBeltrami<GridView> laplace(gv);
  laplace.build(A);

  DOFVector<double> f(gv, 0, 1.0);

  auto boundary = {0};
  for (auto i : boundary) {
    A.clear_dirichlet_row(i);
    f[i] = 0.0;
  }

  Eigen::UmfPackLU<typename DOFMatrix<double>::Matrix> solver;

  DOFVector<double> u(gv, 0);
  solver.compute(A);
  u = solver.solve(f);
  msg("|A*u-f| = ", residuum(A, u, f));

  Dune::VTKWriter<GridView> writer(gv);
  writer.addVertexData(u, "u");
  writer.write("poisson");
}
