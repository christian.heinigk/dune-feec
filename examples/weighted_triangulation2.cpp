#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/WeightedTriangulation.hpp>

using namespace Dec;

int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert( argc > 1 );

  using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  using Grid = DecGrid<GridBase>;
  Grid grid(*gridBase);

  // move cell-centers in direction of barycenter
  WeightedTriangulation::apply(grid);

  for (auto const& e : vertices(grid.gridView()))
    msg("vertex[",e.index(),"]: ",grid.center(e), ", *vol=",grid.dual_volume(e));

  for (auto const& e : edges(grid.gridView()))
    msg("edge[",e.index(),"]: ",grid.center(e), ", *vol=",grid.dual_volume(e));

  for (auto const& e : elements(grid.gridView()))
    msg("element[",e.index(),"]: ",grid.center(e), ", *vol=",grid.dual_volume(e));
}
