#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>
#include <Eigen/Eigenvalues>

#include <unsupported/Eigen/SparseExtra>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/operators/FEMLaplace.hpp>
#include <dune/dec/operators/FEMCenterWeight.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/ChainedOperator.hpp>
#include <dune/dec/WeightedTriangulation.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/utility/FactoryParametrization.hpp>
#include <dune/dec/utility/SphereMapping.hpp>

using namespace Dec;

// laplace^RR(p) + p = (-cos(x)*sin(y) + x^2(y^2+x-2), 2*sin(x)*cos(y) + 4*x*y), p = 0 for x = 0
//
// => p = (x^2*(y^2 + x), sin(x)*cos(x))

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

template <class GridView>
std::tuple<double, double> get_grid_width(GridView const& gv)
{
  double h = 0.0, hstar = 0.0;
  for (auto const& e : edges(gv)) {
    if (!e.hasBoundaryIntersections() && gv.volume(e) > h) {
      h = gv.volume(e);
      hstar = gv.dual_volume(e);
    }
  }

  return std::make_tuple(h, hstar);
}



// RotRot-Laplace: (*d*d)
template <class GridView>
class Laplace
    : public Operator<Laplace<GridView>, GridView, 0, 0>
{
  friend class OperatorAccessor;

  using Super = Operator<Laplace<GridView>, GridView, 0, 0>;

public:

  static constexpr int dim = GridView::dimension;

  /// Constructor
  Laplace(GridView const& gv, double alpha)
    : Super(gv)
    , alpha(alpha)
  {}

  using Super::gridView;

//   auto nzrows_impl() const
//   {
//     return make_vector([this](std::size_t i)
//     {
//       auto const& elem = Dune::ReferenceElements<float_type, 1>::simplex();
//       auto const& T = this->grid().template entities<dim,dim-1>(i);
//       return T.size()*(elem.size(1) - 1) + 1;
//     });
//   }

  template <class Inserter, class Entity>
  void assembleRow_impl(Inserter& A, Entity const& v, float_type factor) const
  {
    std::size_t v_i = v.index();
    double dual_volume_v = gridView().dual_volume(v);

    if (alpha > 1.e-10) {
      if (edges(v).size() == 4)
        factor *= (1.0 - alpha);
      else
        factor *= (1.0 + 2*alpha - sqr(alpha)) / (1.0 + alpha);
    }

//     msg("v(",grid().index(v),"): dual_vol=",dual_volume_v);

    for (auto const& e : edges(v)) {
//       msg("  |*e|=",grid().dual_volume(e),", |e|=", grid().volume(e),", |*e|/|e|=",grid().dual_volume(e)/grid().volume(e));
      double coeff = factor * gridView().dual_volume(e) / (gridView().volume(e) * dual_volume_v);

      for (auto const& v_ : vertices(e)) {
        std::size_t v_j = v_.index();
        A(v_i,v_j) << (v_j == v_i ? -1 : 1) * coeff;
      }
    } // end for(e)
  }

  double alpha;
};



template <class GridView>
class FEMMass
{
  static constexpr int K = 0;

  using FeSpace = Dune::Functions::PQ1NodalBasis<GridView>;

public:

  /// Constructor
  FEMMass(GridView const& gv)
    : gv_(gv)
    , feSpace_(gv)
  {}

  template <class Vector>
  void assemble_vec(Vector& vec, float_type factor = 1) const
  {
    vec.resize(gv_.size(GridView::dimension));

    auto localView = feSpace_.localView();
    auto localIndexSet = feSpace_.localIndexSet();

    static constexpr int Size = GridView::dimension+1;
    SmallVector<float_type, Size> elementVector;

    for (auto const& element : elements(gv_)) {
      localView.bind(element);
      localIndexSet.bind(localView);

      set_to_zero(elementVector);

      auto geometry = element.geometry();
      auto const& localBasis = localView.tree().finiteElement().localBasis();

      auto const& quad = Dune::QuadratureRules<double, GridView::dimension>::rule(element.type(), 1);
      for (size_t iq = 0; iq < quad.size(); ++iq) {
        // Position of the current quadrature point in the reference element
        auto const& quadPos = quad[iq].position();

        // The multiplicative factor in the integral transformation formula
        double const scaling = factor * geometry.integrationElement(quadPos) * quad[iq].weight();

        std::vector<Dune::FieldVector<double,1> > shapeValues;
        localBasis.evaluateFunction(quadPos, shapeValues);

        for (int i = 0; i < Size; ++i) {
          int const local_i = localView.tree().localIndex(i);
          elementVector[local_i] += shapeValues[i] * scaling;
        }
      }

      for (int i = 0; i < Size; ++i) {
        // The global index of the i−th vertex of the element
        auto const row = localIndexSet.index(i);
        vec[row] += elementVector[i];
      }
    }
  }

private:

  GridView gv_;
  FeSpace feSpace_;
};


struct UnitRadius
{
  template <class T>
  static float_type eval(T&&) { return 1.0; }
};


int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert_msg( argc > 1, "usage: ./ellipt_refinement grid-filename [nSteps]");

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;
  using Param = Parametrization<GridBase, SphereMapping<DEC_DIM, DEC_DOW, UnitRadius>>;

  Dune::GridFactory<Param> gridFactory;
  Dune::AlbertaReader<Param>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.create() );

  int nSteps = 3;
  if (argc > 2)
    nSteps = std::atoi(argv[2]);

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  auto gv = grid.leafGridView();

  for (int i = 0; i < nSteps; ++i) {
    msg("refinement: ",(1+i)*DEC_DIM);
    if (i > 0)
      grid.globalRefine();


    // move cell-centers in direction of barycenter
//     WeightedTriangulation::apply(grid);

    double h, hstar;
    std::tie(h, hstar) = get_grid_width(gv);

    double alpha = hstar/h;
    msg("h=",h,", h*=",hstar,", alpha=",alpha);

    using Matrix = DOFMatrix<double>;
    Matrix A;

    LaplaceBeltrami<GridView> laplace(gv);
    laplace.build(A);

    DOFVector<double> b(gv, 0, 1.0, "b");

    auto boundary = {0};
    for (auto i : boundary) {
      A.clear_dirichlet_row(i);
      b[i] = 0.0;
    }

    Eigen::UmfPackLU<typename Matrix::Matrix> solver;

    solver.analyzePattern(A);
    solver.factorize(A);

    DOFVector<double> x(gv, 0, "x");
    x = solver.solve(b);

    msg("|A*x-b| = ", residuum(A, x, b));


    Matrix B;
    FEMLaplace<GridView> laplace2(gv);
    laplace2.build(B, 1.0);

    DOFVector<double> b_(gv, 0);
    FEMMass<GridView> mass(gv);
    mass.assemble_vec(b_, 1.0);

    for (auto i : boundary) {
      B.clear_dirichlet_row(i);
      b_[i] = 0.0;
    }

    solver.analyzePattern(B);
    solver.factorize(B);

    DOFVector<double> y(gv, 0, "y");
    y = solver.solve(b_);
    msg("|B*y-b| = ", residuum(B, y, b_));


    DOFVector<double> err = y; err -= x;

    msg("|x - y|_2 =", two_norm(err));

    Dune::VTKWriter<GridView> writer(gv);

    writer.addVertexData(x, "solution");
    writer.addVertexData(y, "FEM-solution");
    writer.write("ellipt_refinement_"+std::to_string(i));
  }
}
