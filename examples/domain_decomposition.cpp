#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <list>

#include <dune/dec/DecGrid.hpp>
// #include <dune/dec/PartitionTypeMapper.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/parallel/DistributedVector.hpp>
#include <dune/dec/parallel/DistributedMatrix.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/ranges/Filter.hpp>

#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/utility/parmetisgridpartitioner.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk.hh>

using namespace Dec;

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>, Dune::Partitions::InteriorBorder{}))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

int main(int argc, char** argv)
{
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  // Create ug grid from structured grid
  std::array<unsigned int, 2> n = {{4, 4}};
  Dune::FieldVector<double, 2> lower = {{0, 0}};
  Dune::FieldVector<double, 2> upper = {{1, 1}};

  using GridBase = Dune::UGGrid<2>;
  std::shared_ptr<GridBase> gridBase = Dune::StructuredGridFactory<GridBase>::createSimplexGrid(lower, upper, n);

  // Create initial partitioning using ParMETIS
  std::vector<unsigned> part(Dune::ParMetisGridPartitioner<GridBase::LeafGridView>::partition(gridBase->leafGridView(), mpihelper));

  // Transfer partitioning from ParMETIS to our grid
  gridBase->loadBalance(part, 0);

  using Grid = DecGrid<GridBase>;
  Grid grid(*gridBase);

  if (argc > 1)
    grid.globalRefine(std::atoi(argv[1]));

  using GridView = Grid::LeafGridView;
  const GridView gv = grid.leafGridView();

#if 0
//   msg("All:");
//   auto const& indexSet = gv.indexSet();
//   for (auto const& e : elements(gv)) {
//     msg("element ", indexSet.index(e));
//   }
//
//   msg("----------------------------------");
//
//   msg("Interior:");
//   for (auto const& e : elements(gv, Dune::Partitions::Interior{})) {
//     msg("element ", indexSet.index(e));
//   }
//
//   msg("Ghost:");
//   for (auto const& e : elements(gv, Dune::Partitions::Ghost{})) {
//     msg("element ", indexSet.index(e));
//   }
//
//   msg("----------------------------------");
//
//   msg("All edges:");
//   for (auto const& e : edges(gv)) {
//     msg("element ", indexSet.index(e));
//   }
//
//   msg("----------------------------------");

  msg("Interior elements:");
//   PartitionTypeMapper<GridView,0,Dune::Interior_Partition> mapper0(gv);
  for (auto const& e : elements(gv, Dune::Partitions::Interior{})) {
    msg("element ", e.index());
    for (auto const& v : vertices(e))
      msg("  v(",v.index(),")=", gv.center(v));
  }

  msg("InteriorBorder edges:");
  PartitionTypeMapper<GridView,1,Dune::Ghost_Partition> mapper1(gv);
  for (auto const& e : edges(gv, Dune::Partitions::InteriorBorder{})) {
    msg("edge ", e.index());
    for (auto const& v : vertices(e))
      msg("  v(",v.index(),")");
    msg("  owner=",mapper1.owns(e));
  }

  msg("Ghost edges:");
  for (auto const& e : edges(gv, Dune::Partitions::Ghost{})) {
    msg("edge ", e.index());
    for (auto const& v : vertices(e))
      msg("  v(",v.index(),")");
  }

  msg("Ghost elements:");
//   PartitionTypeMapper<GridView,0,Dune::Ghost_Partition> mapper2(gv);
  for (auto const& e : elements(gv, Dune::Partitions::Ghost{})) {
    msg("element ", e.index());
    for (auto const& v : vertices(e))
      msg("  v(",v.index(),")=", gv.center(v));
  }

  ParallelDefaultMapper<GridView,2> mapper2(gv);
  for (auto const& v : vertices(gv, Dune::Partitions::InteriorBorder{})) {
    msg("vertex ", v.index(), " center=",gv.center(v));
    msg("  |*v|=", gv.dual_volume(v));
    msg("  owner=",mapper2.owns(v));
  }

  msg("----------------------------------");

  msg("All edges:");
  ParallelDefaultMapper<GridView,1> m1(gv);
  for (auto const& e : edges(gv, Dune::Partitions::All{})) {
    msg("element ", e.index());
    msg("  owner=",m1.owns(e));
  }

  msg("----------------------------------");
#endif

  LaplaceBeltrami<GridView> laplace(gv);


  DistributedMatrix<double, GridView, 2, 2> A(gv);
  laplace.build(A);

  DistributedVector<double, GridView, 2> x(gv), b(gv);

  for (auto const& v : vertices(gv, Dune::Partitions::All{})) {
    x.vector()[v.index()] = gv.dual_volume(v);
  }

  b = A*x;
//   msg("b = A*x => ",b.vector());

  b += A*x;
//   msg("b += A*x => ",b.vector());

  b -= A*x;
//   msg("b -= A*x => ",b.vector());

//   msg("dot(b,x) = ",dot(b,x));

  b = x + 3.0 * (b - 2.0 * x);
//   msg("b = x + 3*(b - 2*x) => ",b.vector());

  b.vector().setConstant(1.0);

  auto boundary = get_boundary<2>(gv, [](auto const& x) {
    return x[0] < 1.e-10 || x[1] < 1.e-10;
  });
  for (auto i : boundary) {
    A.matrix().clear_dirichlet_row(i);
    b.vector()[i] = 0.0;
  }

  solver::bcgs(A,x,b);
//   msg("x = ",x.vector());
  msg("|A*x-b| = ",residuum(A,x,b));
  msg("|x| = ",two_norm(x));

  Dune::VTKWriter<GridView> vtkwriter(gv);
  vtkwriter.addVertexData(x.vector(), "x");
  vtkwriter.write("domain_decomposition_dec");

//   Dune::VTKWriter<typename GridBase::LeafGridView> vtkwriter2(gridBase->leafGridView());
//   vtkwriter2.addVertexData(x.vector(), "x");
//   vtkwriter2.write("domain_decomposition_ug");
//
//   auto const& indexSet = gv.indexSet();
//   for (auto const& v : vertices(gv, Dune::Partitions::All{}) ) {
//     msg("v",(indexSet.owns(v)?"*":""),"(",v.index(),") ",v.partitionType(),": ",gv.center(v));
//   }
//
//   for (auto const& e : elements(gv, Dune::Partitions::All{}) ) {
//     msg_("elem",(indexSet.owns(e)?"*":""),"(",e.index(),") ",e.partitionType(),": {");
//     for (auto const& v : vertices(e))
//       msg_(v.index(),' ');
//     msg("}");
//   }
//
//   msg("-------------------------------------------------------");
//
//   auto gv_ = gridBase->leafGridView();
//   auto const& is_ = gv_.indexSet();
//   for (auto const& e : elements(gv_, Dune::Partitions::All{}) ) {
//     msg_("elem(",is_.index(e),") ",e.partitionType(),": {");
//     for (int i = 0; i < e.subEntities(2); ++i)
//       msg_(is_.subIndex(e,i,2),' ');
//     msg("}");
//   }

  msg("grid.memory = ",grid.memory()/1024,"kB");
}
