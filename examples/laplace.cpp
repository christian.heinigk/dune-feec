#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/utility/Timer.hpp>


using namespace Dec;

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  assert( argc > 1 );

  using Grid = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<Grid> gridFactory;
  Dune::AlbertaReader<Grid>().readGrid(argv[1], gridFactory);
  std::unique_ptr<Grid> sub_grid( gridFactory.createGrid() );

  DecGrid<Grid> grid(*sub_grid);

  std::size_t N = grid.size(DEC_DIM);

  Timer t;

  t.reset();
  DOFMatrix<double> A(N,N);
  {
    auto ins = A.inserter();

    // assemble Laplace operator
    for (auto const& v : vertices(grid.gridView()))
    {
      std::size_t v_i = v.index();
      double dual_volume_v = grid.dual_volume(v);

      for (auto const& e : edges(v)) {
        double volume_e = grid.volume(e);
        double dual_volume_e = grid.dual_volume(e);

        double factor = dual_volume_e / std::max(1.e-8, volume_e * dual_volume_v);

        for (auto const& v_ : vertices(e)) {
          std::size_t v_j = v_.index();
          ins(v_i,v_j) << (v_j == v_i ? -1 : 1) * factor;
        }
      }
    }
  } // finish insertion
  msg("time(assemble) = ",t.elapsed());
  A.clear_dirichlet_row(0);

  using Vector = DOFVector<double>;
  Vector b(N, 1.0);
  b[0] = 0.0;

  t.reset();
  Vector x(N);
  std::size_t iter = solver::bcgs(A,b,x);
  msg("time(solve) = ",t.elapsed());
  msg("#iterations:     ", iter);
  msg("estimated error: ", residuum(A,b,x));
}
