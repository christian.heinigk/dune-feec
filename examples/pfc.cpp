#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/Parameter.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/linear_algebra/block.hpp>
#include <dune/dec/utility/FactoryParametrization.hpp>
#include <dune/dec/utility/SphereMapping.hpp>
#include <dune/dec/utility/Timer.hpp>

#include "PfcOperator.hpp"

using namespace Dec;

struct Radius
{
  template <class T>
  static float_type eval(T const&) { return 15.0; }
};

int main(int argc, char** argv)
{
  using namespace Dec::literal;
  decpde::init(argc, argv);

  assert( argc > 1 );

  Parameter param;
  read(argv[1], param);

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;
  using Param = Parametrization<GridBase, SphereMapping<DEC_DIM, DEC_DOW, Radius>>;

  Dune::GridFactory<Param> gridFactory;
  Dune::AlbertaReader<Param>().readGrid(param["grid"]["filename"].string_value(), gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.create() );

  gridBase->globalRefine(param["grid"]["refinement"].int_value());

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase, true);
  auto gv = grid.leafGridView();
  MusicalIsomorphism<GridView> transform(gv);

  using BMatrix = BlockMatrix<double, 3, 3>;
  using BVector = BlockVector<double, 3>;

  using Matrix = typename BMatrix::Matrix;

  // matrix containing the linear term
  BlockMatrix<double, 3, 3> A;

  BVector c(gv, {0,0,0}); // rhs vector
  BVector solution(gv, {0,0,0}); // rhs vector

  Matrix Blin;
  Matrix Bnonlin;

  double tau = param["time"]["timestep"].number_value();
  double eps = param["pfc"]["eps"].number_value(); // 0.4
  double mean_psi = param["pfc"]["psi"].number_value(); // -0.3;

  Timer timer;

  Identity<GridView,0> id(gv);
  LaplaceDeRham<GridView,0> laplace(gv);
  Nonlin<GridView> nonlin(gv, solution[0]);

  id.build(A(0,0), 1.0/tau);
  laplace.build(A(0,1));

  laplace.init(Blin);
  {
    auto ins = Blin.inserter();
    id.assemble(ins, -(1.0-eps));
    laplace.assemble(ins, 2.0);
  }
  laplace.finish(Blin);

  id.build(A(1,1));
  laplace.build(A(1,2));

  laplace.build(A(2,0));
  id.build(A(2,2));

  DEC_MSG("time(assemble) = ",timer.elapsed());

  // set initial values
  transform.interpol(solution[0], int_<0>, [mean_psi](auto const&) {
    return mean_psi + 0.5*(std::rand()/double(RAND_MAX) - 0.5);
  });

  for (std::size_t i = 1; i < 3; ++i) solution[i].setZero();

  // create file writer
  using VtkWriter = Dune::VTKWriter<GridView>;
  std::shared_ptr<VtkWriter> writer = std::make_shared<VtkWriter>(gv);
  Dune::VTKSequenceWriter<GridView> pvdwriter(writer,
                                              param["output"]["filename"].string_value(),
                                              param["output"]["directory"].string_value(),
                                              "data");

  pvdwriter.addVertexData(solution[0], "psi");
  pvdwriter.addVertexData(solution[1], "mu");
  pvdwriter.addVertexData(solution[2], "nu");

  pvdwriter.write(0.0);

  Eigen::UmfPackLU<Matrix> solver;
  bool analyzed = false;
  for (double t = 0.0; t < param["time"]["end_time"].number_value(); t += tau)
  {
    DEC_MSG("");
    DEC_MSG("Timestep t=",t);

    timer.reset();
    Bnonlin.setZero();
    nonlin.build(Bnonlin, -1.0);
    A(1,0) = Blin + Bnonlin;

    auto C = A.compress();
    auto const& psi = solution[0].array();
    c[0] = psi * (1.0/tau);
    c[1] = -2.0*cube(psi);
    c[2].setZero();
    DEC_MSG("time(assemble) = ",timer.elapsed());

    timer.reset();
    if (!analyzed) {
      solver.analyzePattern(C);
      analyzed = true;
    }
    solver.factorize(C);
    DEC_MSG("time(factorize) = ",timer.elapsed());

    timer.reset();
    solution = solver.solve(c.compress());
    DEC_MSG("time(solve) = ",timer.elapsed());
    DEC_MSG("estimated error: ", (c.vector() - C*solution.compress()).norm());

    timer.reset();
    pvdwriter.write(t+tau);
    DEC_MSG("time(write) = ",timer.elapsed());
  }

  pvdwriter.write(param["time"]["end_time"].number_value());
}
