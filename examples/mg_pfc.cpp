#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/Parameter.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/utility/Timer.hpp>
#include <dune/dec/utility/SphereMapping.hpp>
#include <dune/dec/utility/FactoryParametrization.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>

using namespace Dec;

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

template <class GridView>
double get_grid_width(GridView const& gv)
{
  double h = 0.0;
  for (auto const& e : edges(gv)) {
    if (gv.volume(e) > h)
      h = gv.volume(e);
  }

  return h;
}

// Operator 3*psi'^2
template <class GridView>
class Nonlin
    : public Operator<Nonlin<GridView>, GridView, 0,0>
{
  friend class Dec::OperatorAccessor;
  using Super = Operator<Nonlin<GridView>, GridView, 0,0>;
  using Vector = DOFVector<double>;

public:

  /// Constructor
  Nonlin(GridView const& gv, Vector const& psi, float_type value = 1)
    : Super(gv)
    , psi_(psi)
    , value_(value)
  {}

protected:

  template <class Inserter, class Entity>
  void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
  {
    std::size_t i = e.index();
    A(i,i) << factor*value_*3*sqr(psi_[i]);
  }

private:

  Vector const& psi_; // old_solution
  float_type value_;
};


template <class Grid>
class MGSolver
{
  template <class Matrix, class Vector>
  class Precon
  {
  public:
    Precon(MGSolver const& solver, std::vector<Matrix> const& A, std::vector<Vector>& u, std::vector<Vector>& b, int iter = 10)
      : solver_(solver)
      , A_(A)
      , u_(u)
      , b_(b)
      , l_(A.size()-1)
      , iter_(iter)
    {}

    template <class Vector1, class Vector2>
    void solve(Vector1 const& b, Vector2& out) const
    {
      msg("u_.size=",u_.size(),", l=",l_);
      u_.back().setZero();
      b_.back() = b;

      for (int i = 0; i < iter_; ++i)
        solver_.cycle(l_, A_, u_, b_);

      out = u_.back();
    }

    template <class Matrix1>
    void compute(Matrix1 const&)
    {
      solver_.compute(A_);
    }

  private:
    MGSolver const& solver_;

    std::vector<Matrix> const& A_;
    std::vector<Vector>& u_;
    std::vector<Vector>& b_;

    const int l_;
    const int iter_;
  };


public:
  MGSolver(Grid const& grid, int L = 0, int nu1 = 1, int nu2 = 1, int gamma = 2)
    : grid_(grid)
    , L_(L)
    , nu1_(nu1)
    , nu2_(nu2)
    , gamma_(gamma)
  {}

  template <class Matrix, class Vector>
  float_type solve(std::vector<Matrix> const& A, std::vector<Vector>& u, std::vector<Vector>& b, float_type tol = 1.e-8, int iter = 500) const
  {
    int l = A.size() - 1;
    float_type resid = 1.0, nrm_b = two_norm(b[l]);
    for (int i = 0; i < iter && resid > tol; ++i) {
      cycle(l, A, u, b);
      resid = residuum(A[l], u[l], b[l]) / nrm_b;
      if (i%10 == 0)
        msg("resid(",i,") = ",resid);
    }

    return resid;
  }


  template <class Matrix, class Vector>
  Precon<Matrix,Vector> precon(std::vector<Matrix> const& A, std::vector<Vector>& u, std::vector<Vector>& b, int iter = 10) const
  {
    return {*this, A, u, b, iter};
  }


  template <class Matrix>
  MGSolver& compute(std::vector<Matrix> const& A)
  {
    if (!analyzed) {
      coarseSolver.analyzePattern(A[L_]);
      analyzed = true;
    }
    coarseSolver.factorize(A[L_]);

    return *this;
  }

  int coarseLevel() const { return L_; }


private:

  template <class Matrix, class Vector1, class Vector2>
  void smooth(int nu, Matrix const& A, Vector1& u, Vector2 const& b) const
  {
    for (int iter = 0; iter < nu; ++iter) {
      for (int k = 0; k < A.outerSize(); ++k) {
        auto tmp = b[k];
        double diag = 0.0;
        for (typename Matrix::InnerIterator it(A, k); it; ++it) {
          if (it.col() != k)
            tmp -= it.value() * u[it.col()];
          else
            diag = it.value();
        }
        assert( diag != 0.0 );
        u[k] = tmp / diag;
      }
    }
  }

  template <class Matrix, class Vector>
  void cycle(int l, std::vector<Matrix> const& A, std::vector<Vector>& u, std::vector<Vector>& b) const
  {
    assert_msg(l > L_, "(coarseLevel < fineLevel-1) not fulfilled");

    smooth(nu1_, A[l], u[l], b[l]); // -> u[l]
    auto d = b[l] - A[l]*u[l]; // compute defect
    grid_.transfer_[l-1].restrict(d, b[l-1]); // restrict the defect

    if (l-1 == L_) {
      // solve on the coarse grid
      u[l-1] = coarseSolver.solve(b[l-1]);
    } else {
      // apply gamma l-grid cycles
      u[l-1].setZero();
      for (int g = 0; g < gamma_; ++g) {
        cycle(l-1, A, u, b);
      }
    }

    grid_.transfer_[l-1].prolongate(u[l-1], u[l], true);
    smooth(nu2_, A[l], u[l], b[l]); // -> u[l]
  }


private:
  Grid const& grid_;

  int L_;
  int nu1_;
  int nu2_;
  int gamma_;

  using Mat = DOFMatrix<float_type>::Matrix;
  Eigen::UmfPackLU<Mat> coarseSolver;
  bool analyzed = false;
};


// assemble stationary part
template <class GridView, class Matrix, class Vector>
void assemble(GridView const& gv, Matrix& A, Matrix& L, Vector const& psi, double tau, double eps)
{
  Identity<GridView,0> id(gv);
  LaplaceBeltrami<GridView> laplace(gv);

  laplace.build(L);

  Matrix I;
  id.build(I, 1.0);

  Matrix B0 = -2.0*I + L;
  Matrix B1 = (L*B0).pruned();
  Matrix B2 = (1.0-eps)*I + B1;
  Matrix B3 = (L*B2).pruned();

  A = (1.0/tau)*I + B3;

//   A = (1.0/tau)*I + L*((1.0-eps)*I + L*(-2.0*I + L));
}

// assemble instationary part
template <class GridView, class Matrix, class Vector>
void assemble2(GridView const& gv, Matrix const& L, Matrix const& Blin, Matrix& A, Vector const& psi, double tau, double eps)
{
  Nonlin<GridView> nonlin(gv, psi);

  Matrix Bnonlin;
  nonlin.build(Bnonlin);

  Matrix B0 = (L*Bnonlin).pruned();

  A = Blin + B0;
}

template <class GridView, class Matrix, class Vector>
void assemble_rhs(GridView const& gv, Matrix const& L, Vector const& psi, Vector& b, double tau)
{
  DOFVector<double> psi3 = 2.0*cube(psi.array());
  b = (1.0/tau) * psi.array();
  b+= L*psi3;
}

struct Radius
{
  template <class T>
  static float_type eval(T const&) { return 15.0; }
};

int main(int argc, char** argv)
{
  decpde::init(argc, argv);
  Timer timer;

  assert( argc > 1 );

  Parameter param;
  read(argv[1], param);

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;
  using Param = Parametrization<GridBase, SphereMapping<DEC_DIM, DEC_DOW, Radius>>;

  Dune::GridFactory<Param> gridFactory;
  Dune::AlbertaReader<Param>().readGrid(param["grid"]["filename"].string_value(), gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.create() );

  int nSteps = param["grid"]["refinement"].int_value()+1;

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  MusicalIsomorphism<GridView> transform(grid.leafGridView());

  double tau = param["time"]["timestep"].number_value();
  double eps = param["pfc"]["eps"].number_value(); // 0.4
  double mean_psi = param["pfc"]["psi"].number_value(); // -0.3;


  std::vector<DOFMatrix<double>> Blin(nSteps), Laplace(nSteps), A(nSteps);
  std::vector<DOFVector<double>> b,psi;

  for (int l = 0; l < nSteps; ++l) {
    if (l > 0)
      grid.globalRefine(1);

    b.emplace_back(grid.leafGridView(), 0, "b" + std::to_string(l)); // rhs vector
    psi.emplace_back(grid.leafGridView(), 0, "psi" + std::to_string(l)); // solution vector

    // random initial density
    transform.interpol(psi.back(), int_<0>, [mean_psi](auto const&) {
      return 0.0;
    });

    assemble(grid.leafGridView(), Blin[l], Laplace[l], psi[l], tau, eps);

//     transform.interpol(b.back(), int_<0>, [](auto const& x) {
//       double r2 = unary_dot(x);
//       double ux = exp(-10.0 * r2);
//       return -(400.0 * r2 - 20.0 * DEC_DOW) * ux;
//     });

    double h = get_grid_width(grid.leafGridView());
    msg("h=",h,", grid.size(2)=",grid.size(2));
  }

  transform.interpol(psi.back(), int_<0>, [mean_psi](auto const&) {
    return mean_psi + 0.5*(std::rand()/double(RAND_MAX) - 0.5);
  });

  msg("psi.size=",psi.back().size());
  msg("matrix.size=",Blin.back().num_rows()," x ",Blin.back().num_cols());

  // create file writer
  using VtkWriter = Dune::VTKWriter<GridView>;
  std::shared_ptr<VtkWriter> writer = std::make_shared<VtkWriter>(grid.leafGridView());
  Dune::VTKSequenceWriter<GridView> pvdwriter(writer,
                                              param["output"]["filename"].string_value(),
                                              param["output"]["directory"].string_value(),
                                              "data");

  pvdwriter.addVertexData(psi.back(), "psi");
  pvdwriter.write(0.0);

  auto solver_param = param["solver"];
  MGSolver<Grid> solver(grid, solver_param["coarse_level"].int_value(),
                              solver_param["nu1"].int_value(),
                              solver_param["nu2"].int_value(),
                              solver_param["gamma"].int_value());

  for (double t = 0.0; t < param["time"]["end_time"].number_value(); t += tau)
  {
    DEC_MSG("");
    DEC_MSG("Timestep t=",t);

    timer.reset();
    for (int l = solver.coarseLevel(); l < nSteps; ++l)
      assemble2(grid.levelGridView(l), Laplace[l], Blin[l], A[l], psi[l], tau, eps);
    assemble_rhs(grid.leafGridView(), Laplace.back(), psi.back(), b.back(), tau);
    DEC_MSG("time(assemble) = ",timer.elapsed());

    timer.reset();

    solver.compute(A);
    solver::pbcgs(A.back(), psi.back(), b.back(), solver.precon(A,psi,b,solver_param["iter"].int_value()));

//     auto resid = solver.compute(A).solve(A, psi, b, 1.e-7);
    DEC_MSG("time(solve) = ",timer.elapsed());
    DEC_MSG("estimated error: ", residuum(A.back(), psi.back(), b.back()));

    timer.reset();
    pvdwriter.write(t+tau);
    DEC_MSG("time(write) = ",timer.elapsed());
  }

  pvdwriter.write(param["time"]["end_time"].number_value());
}
