#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <list>

#include <dune/dec/DecGrid.hpp>
// #include <dune/dec/PartitionTypeMapper.hpp>
#include <dune/dec/common/Output.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/parallel/DistributedVector.hpp>
#include <dune/dec/parallel/DistributedMatrix.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/linear_algebra/iteration.hpp>
#include <dune/dec/utility/Timer.hpp>

#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/utility/parmetisgridpartitioner.hh>
#include <dune/alugrid/grid.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/grid/io/file/vtk.hh>

using namespace Dec;

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>, Dune::Partitions::InteriorBorder{}))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

template <class GridBase>
class LoadBalance
{
public:
  LoadBalance(GridBase const& grid, Dune::MPIHelper& mpihelper)
    : grid_(grid)
    , part_(Dune::ParMetisGridPartitioner<typename GridBase::LeafGridView>::partition(grid.leafGridView(), mpihelper))
  {}

  template <class Entity>
  int operator()(Entity const& e) const
  {
    auto gv = grid_.leafGridView();
    return part_[gv.indexSet().index(e)];
  }

  bool importRanks(std::set<int>& ranks) const { return false; }

private:
  GridBase const& grid_;

  std::vector<unsigned> part_;
};

int main(int argc, char** argv)
{
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);
  assert_msg( argc > 1, "usage: ./alugrid grid-filename [nSteps]");

  Timer t0;
  using GridBase = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>;
#if 1
  // Create ug grid from structured grid
  std::array<unsigned int, 2> n = {{10, 10}};
  Dune::FieldVector<double, 2> lower = {{0, 0}};
  Dune::FieldVector<double, 2> upper = {{1, 1}};

  std::shared_ptr<GridBase> gridBase = Dune::StructuredGridFactory<GridBase>::createSimplexGrid(lower, upper, n);
#endif

#if 0
  Dune::GridFactory<GridBase> gridFactory;
  if (mpihelper.rank() == 0)
    Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );
#endif
  msg("time(create gridBase) = ",t0.elapsed(),"sec");

  t0.reset();
  LoadBalance<GridBase> lb(*gridBase, mpihelper);
  gridBase->repartition(lb);
  msg("#elements = ",gridBase->leafGridView().size(0));
  msg("time(loadBalance) = ",t0.elapsed(),"sec");

  t0.reset();
  using Grid = DecGrid<GridBase>;
  Grid grid(*gridBase);
  msg("time(create DECGrid) = ",t0.elapsed(),"sec");

  if (argc > 2) {
    t0.reset();
    grid.globalRefine(std::atoi(argv[2]));
    msg("#elements after global refinement = ",gridBase->leafGridView().size(0));
    msg("time(global refine) = ",t0.elapsed(),"sec");
  }

  using GridView = Grid::LeafGridView;
  const GridView gv = grid.leafGridView();
  auto const& indexSet = gv.indexSet();

  ParallelTimer<GridView> t(gv);

  // -----------------------------------------------------------------------------------------------
#if 0
  for (auto const& v : vertices(gv, Dune::Partitions::All{}) ) {
    msg("v",(indexSet.owns(v)?"*":""),"(",v.index(),") ",v.partitionType(),": ",gv.center(v));
  }

  for (auto const& e : elements(gv, Dune::Partitions::All{}) ) {
    msg_("elem",(indexSet.owns(e)?"*":""),"(",e.index(),") ",e.partitionType(),": {");
    for (auto const& v : vertices(e))
      msg_(v.index(),' ');
    msg("}");
  }
#endif
  // -----------------------------------------------------------------------------------------------

  t.reset();
  LaplaceBeltrami<GridView> laplace(gv);

  DistributedMatrix<double, GridView, 2, 2> A(gv);
  laplace.build(A);
  msg("time(assemble) = ",t.elapsed(),"sec");

  DistributedVector<double, GridView, 2> b(gv);
  b.vector() = 1.0;

  DistributedVector<double, GridView, 2> x(b, tag::ressource{});
  x.vector() = 0.0;
//   random(x.vector());
  t.reset();
  x.synch();
  msg("time(synch) = ",t.elapsed(),"sec");


  typename Grid::GlobalCoordinate x0 = {0.0, 0.0};
  auto boundary = [&x0](auto const& x) {
    return two_norm(x - x0) < 1.e-10;
  };
  for (auto i : get_boundary<2>(gv,boundary)) {
    A.clear_dirichlet_row(i);
    b.vector()[i] = 0.0;
  }

  t.reset();
  BasicIteration<double> iter(100);
  solver::bcgs(A,x,b, iter);
  msg("time(solve) = ",t.elapsed(),"sec");

  msg("|A*x-b| = ",residuum(A,x,b));
  msg("|x| = ",two_norm(x));

  t.reset();
  Dune::VTKWriter<GridView> vtkwriter(gv);
  vtkwriter.addVertexData(x.vector(), "x");
  vtkwriter.write("alugrid");
  msg("time(write file) = ",t.elapsed(),"sec");

  msg("grid.memory = ",grid.memory()/1024,"kB");
}
