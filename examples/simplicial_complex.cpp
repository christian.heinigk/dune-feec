#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/SimplicialComplex.hpp>

using namespace Dec;

int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert( argc > 1 );

  using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  auto const& indexSet = gridBase->leafIndexSet();
  auto const& elem = Dune::ReferenceElements<float_type, DEC_DIM>::simplex();
  for (auto const& e : elements(gridBase->leafGridView()))
  {
    std::cout << "element[" << indexSet.index(e) << "]: {";

    for (int i = 0; i < elem.size(DEC_DIM); ++i) {
      std::cout << indexSet.subIndex(e, i, DEC_DIM) << " ";
    }
    std::cout << "}\n";
    for (int i = 0; i < elem.size(1); ++i) {
      std::cout << "  edge[" << i << "](" << indexSet.subIndex(e, i, 1) << "): (";
      for (int j = 0; j < elem.size(i,1,DEC_DIM); ++j) {
        std::size_t v_i = elem.subEntity(i, 1, j, DEC_DIM); // vertex index
        std::cout << indexSet.subIndex(e, v_i, DEC_DIM) << " ";
      }
      std::cout << ")\n";
    }
    std::cout << "\n";
  }


  SimplicialComplex<DEC_DIM> cmplx(*gridBase);

  std::cout << "#edges = " << cmplx.vertices_e.size() << "\n";
  std::cout << "#faces = " << cmplx.edges_f.size() << "\n";
  std::cout << "#cells = " << cmplx.faces_c.size() << "\n";

  size_t e_i = 0;
  for (auto const& vs : cmplx.vertices_e) {
    std::cout << "edge[" << e_i << "]: vertices = {";
    for (auto const& v : vs) {
      std::cout << (v.valid() ? '+' : '-') << v << " ";
    }
    std::cout << "}\n";
    e_i++;
  }
  std::cout << "\n";






  size_t f_i = 0;
  for (auto const& es : cmplx.edges_f) {
    std::cout << "face[" << f_i << "]: edges = {";
    for (auto const& e : es) {
      std::cout << (e.valid() ? '+' : '-') << e << " ";
    }
    std::cout << "}\n";
    f_i++;
  }

  std::cout << "\n\n----------------------------------------------\n\n";

  size_t v_i = 0;
  for (auto const& es : cmplx.edges_v) {
    std::cout << "vertex[" << v_i << "]: edges = {";
    for (auto const& e : es) {
      std::cout << (e.valid() ? '+' : '-') << e << " ";
    }
    std::cout << "}\n";
    v_i++;
  }
  std::cout << "\n";

  e_i = 0;
  for (auto const& fs : cmplx.faces_e) {
    std::cout << "edge[" << e_i << "]: faces = {";
    for (auto const& f : fs) {
      std::cout << (f.valid() ? '+' : '-') << f << " ";
    }
    std::cout << "}\n";
    e_i++;
  }

  std::cout << "\n\n----------------------------------------------\n\n";

  // traverse all elements
  for (size_t i = 0; i < cmplx.size(0); ++i) {
    std::cout << "element[" << i << "]: vertices = {";
    for (auto const& v : cmplx.subIndices<0,DEC_DIM>(i)) {
      std::cout << v << " ";
    }
    std::cout << "}\n";
  }

  std::cout << "\n\n----------------------------------------------\n\n";

  // traverse all elements
  for (size_t i = 0; i < cmplx.size(DEC_DIM); ++i) {
    std::cout << "vertex[" << i << "]: elements = {";
    for (auto const& c : cmplx.subIndices<DEC_DIM,0>(i)) {
      std::cout << c << " ";
    }
    std::cout << "}\n";
  }
}
