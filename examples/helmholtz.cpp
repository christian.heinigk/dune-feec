#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/utility/Timer.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/operators/ChainedOperator.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/utility/VectorValuedContainer.hpp>
// #include "WeightedTriangulation.hpp"

using namespace Dec;

struct RHS
{
  template <class WorldVector>
  WorldVector operator()(WorldVector const& coords) const
  {
    double x = coords[0];
    double y = coords[1];
    double z = coords[2];
    WorldVector rval;

    rval[0] = (0.25*y*(59049.*(55937. - 1701.*pow(x,6) + 45.*pow(x,4)*(377. + 756.*pow(y,2)) + 12.*pow(y,2)*(8473. + 4524.*pow(y,2) + 1296.*pow(y,4)) - 9.*pow(x,2)*(8473. + 15080.*pow(y,2) + 9072.*pow(y,4))) - 2916.*(933403. + 100845.*pow(x,4) + 963864.*pow(y,2) + 322704.*pow(y,4) - 54.*pow(x,2)*(13387. + 14940.*pow(y,2)))*pow(z,2) + 50544.*(15149. - 5679.*pow(x,2) + 7572.*pow(y,2))*pow(z,4) - 7.881632e7*pow(z,6)))/pow(81.*(-5. + 3.*pow(x,2) - 12.*pow(y,2)) + 148.*pow(z,2),3);

    rval[1] = (0.0625*x*(59049.*(-11399. + 243.*pow(x,6) - 9.*pow(x,4)*(323. + 2268.*pow(y,2)) - 36.*pow(y,2)*(6373. + 6460.*pow(y,2) + 3024.*pow(y,4)) + 3.*pow(x,2)*(6373. + 38760.*pow(y,2) + 45360.*pow(y,4))) + 2916.*(209341. + 15795.*pow(x,4) + 1080.*pow(y,2)*(1699. + 1170.*pow(y,2)) - 90.*pow(x,2)*(1699. + 7020.*pow(y,2)))*pow(z,2) + 81648.*(-1987. + 601.*pow(x,2) - 7212.*pow(y,2))*pow(z,4) + 1.448288e7*pow(z,6)))/pow(81.*(-5. + 3.*pow(x,2) - 12.*pow(y,2)) + 148.*pow(z,2),3);

    rval[2] = (559872.*x*y*z*(-207. + 27.*pow(x,2) - 108.*pow(y,2) + 52.*pow(z,2)))/pow(81.*(-5. + 3.*pow(x,2) - 12.*pow(y,2)) + 148.*pow(z,2),3);

    return rval;
  }
};

struct SOL
{
  template <class WorldVector>
  WorldVector operator()(const WorldVector& coords) const
  {
    double x = coords[0];
    double y = coords[1];
    WorldVector rval;

    rval[0] = -2.*y;

    rval[1] = x/2.;

    rval[2] = 0.;

    return rval;
  }
};


int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert( argc > 1 );

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;
//   using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  using WorldVector = typename Grid::GlobalCoordinate;

  Grid grid(*gridBase);
  MusicalIsomorphism<GridView> transform(grid.leafGridView());
//   WeightedTriangulation::apply(grid);

  using Matrix = DOFMatrix<double>;
  using Vector = DOFVector<double>;
  using VectorVec = DOFVector<WorldVector>;

  using VtkWriter = Dune::VTKWriter<GridView>;

  std::shared_ptr<VtkWriter> writer = std::make_shared<VtkWriter>(grid.leafGridView());

  VectorVec rhs_vec(grid.size(DEC_DIM));
  VectorVec sol_vec(grid.size(DEC_DIM));

  transform.interpol(sol_vec, int_<0>, SOL{});
  transform.interpol(rhs_vec, int_<0>, RHS{});

  auto rhs = transform.flat(rhs_vec, int_<0>, int_<1>);
  auto rhs_out = transform.sharp(rhs, int_<1>, int_<DEC_DIM>);
//   msg("rhs = ",rhs);

  Timer timer;
  Matrix B;

  Identity<GridView,1> id(grid.leafGridView(), 1.0);
  LaplaceDeRham<GridView,1> laplace(grid.leafGridView(), 1.0);

  auto op = laplace + id;

  op.build(B);
//   msg("B = ",B);
  msg("time(assemble) = ",timer.elapsed());

  Eigen::UmfPackLU<Matrix> solver;

  timer.reset();
  solver.analyzePattern(B);
  msg("time(analyzePattern) = ",timer.elapsed());

  timer.reset();
  solver.factorize(B);
  msg("time(factorize) = ",timer.elapsed());

  timer.reset();
  Vector sol = solver.solve(rhs);
  msg("time(solve) = ",timer.elapsed());
  msg("estimated error: ", residuum(B, sol, rhs));

  auto sol_out = transform.sharp(sol, int_<1>, int_<DEC_DIM>);

  using VecVector = VectorValuedContainer<double, DEC_DOW>;
  VecVector rhs_vecs{rhs_vec}, sol_vecs{sol_vec}, rhs_outs{rhs_out};
  VecVector sol_outs{sol_out};

  writer->addVertexData(rhs_vecs, "rhs_in", DEC_DOW);
  writer->addVertexData(sol_vecs, "exact", DEC_DOW);
  writer->addCellData(rhs_outs, "rhs_out", DEC_DOW);
  writer->addCellData(sol_outs, "solution", DEC_DOW);

  timer.reset();
  writer->write("helmholtz");
  msg("time(write) = ",timer.elapsed());
}
