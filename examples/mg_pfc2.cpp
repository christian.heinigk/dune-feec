#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/Parameter.hpp>
#include <dune/dec/utility/Timer.hpp>
#include <dune/dec/utility/SphereMapping.hpp>
#include <dune/dec/utility/FactoryParametrization.hpp>
#include <dune/dec/linear_algebra/multigrid.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/linear_algebra/iteration/NoisyIteration.hpp>
#include <dune/dec/linear_algebra/iteration/IteratedSolver.hpp>
#include <dune/dec/linear_algebra/smoother/GaussSeidel.hpp>

#include "PfcOperator.hpp"

using namespace Dec;

template <class GridView, class Matrix, class Vector>
void assemble_rhs(GridView const& gv, Matrix const& L, Vector const& psi, Vector& b, double tau)
{
  DOFVector<double> psi3 = 2.0*cube(psi.array());
  b = (1.0/tau) * psi.array();
  b+= L*psi3;
}

struct Radius
{
  template <class T>
  static float_type eval(T const&) { return 15.0; }
};

int main(int argc, char** argv)
{
  decpde::init(argc, argv);
  Timer timer;

  assert( argc > 1 );

  Parameter param;
  read(argv[1], param);

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;
  using Param = Parametrization<GridBase, SphereMapping<DEC_DIM, DEC_DOW, Radius>>;

  Dune::GridFactory<Param> gridFactory;
  Dune::AlbertaReader<Param>().readGrid(param["grid"]["filename"].string_value(), gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.create() );

  int nSteps = param["grid"]["refinement"].int_value();

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  MusicalIsomorphism<GridView> transform(grid.leafGridView());

  double tau = param["time"]["timestep"].number_value();
  double eps = param["pfc"]["eps"].number_value(); // 0.4
  double mean_psi = param["pfc"]["psi"].number_value(); // -0.3;

  grid.globalRefine(nSteps, true);
  double h = grid.width();
  msg("h=",h,", grid.size(2)=",grid.size(2));

  DOFVector<double> psi(grid.leafGridView(), 0, "psi");
  DOFVector<double> rhs(grid.leafGridView(), 0, "psi");

  // random initial density
  transform.interpol(psi, int_<0>, [mean_psi](auto const&) {
    return mean_psi + 0.5*(std::rand()/double(RAND_MAX) - 0.5);
  });

  msg("psi.size=",psi.size());

  // create file writer
  using VtkWriter = Dune::VTKWriter<GridView>;
  using PvdWriter = Dune::VTKSequenceWriter<GridView>;
  std::shared_ptr<VtkWriter> writer = std::make_shared<VtkWriter>(grid.leafGridView());
  PvdWriter pvdwriter(writer, param["output"]["filename"].string_value(),
                              param["output"]["directory"].string_value(), "data");

  pvdwriter.addVertexData(psi, "psi");
  pvdwriter.write(0.0);

  auto solver_param = param["solver"];
  MGParameter mg_param = { solver_param["nu1"].int_value(),
                           solver_param["nu2"].int_value(),
                           solver_param["gamma"].int_value(),
                            };

  int num_iter = solver_param["iter"].int_value();
  int minLevel = solver_param["coarse_level"].int_value();
  int maxLevel = grid.maxLevel();

  PfcOperator op;
  op.init(grid.leafGridView(), tau, eps);

  PfcTransfer<Grid> transfer(grid, minLevel, maxLevel, psi);
  using Precon = MGSolver<PfcOperator, decltype(transfer)>;

  Precon mg_precon(transfer, GaussSeidel{}, mg_param);
  mg_precon.init(minLevel, maxLevel, tau, eps);

  BasicIteration<float_type> mg_iter{num_iter};
  auto precon = iterated(mg_precon, mg_iter);

  auto const& mat = op.matrix();
  auto const& laplace = op.laplace();

  Timer timer2;
  for (double t = 0.0; t < param["time"]["end_time"].number_value(); t += tau)
  {
    DEC_MSG("");
    DEC_MSG("Timestep t=",t);

    timer.reset();
    assemble_rhs(grid.leafGridView(), laplace, psi, rhs, tau);
    op.build(grid.leafGridView(), psi);

    precon.compute(op);

    DEC_MSG("time(assemble) = ",timer.elapsed());

    timer.reset();
//     precon.solveWithGuess(rhs, psi);
    NoisyIteration<float_type> iter(500, 1.e-10);
    solver::pbcgs(mat, psi, rhs, precon, iter);

    DEC_MSG("time(solve) = ",timer.elapsed());
    DEC_MSG("estimated error: ", residuum(mat, psi, rhs));

    timer.reset();
    pvdwriter.write(t+tau);
    DEC_MSG("time(write) = ",timer.elapsed());
  }

  pvdwriter.write(param["time"]["end_time"].number_value());
  msg("elapsed time = ",timer2.elapsed());
}
