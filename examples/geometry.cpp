#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/utility/Timer.hpp>

using namespace Dec;

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  assert( argc > 1 );

  using Grid = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;
  Timer t;
  std::unique_ptr<Grid> gridBase( Dune::GmshReader<Grid>::read(argv[1]) );

  // Dune::GridFactory<Grid> gridBaseFactory;
  // Dune::AlbertaReader<Grid>().readGrid(argv[1], gridBaseFactory);
  // std::unique_ptr<Grid> gridBase( gridBaseFactory.createGrid() );
  msg("time(GmshReader)=",t.elapsed());


  t.reset();
  std::vector<Dune::FieldVector<double, DEC_DOW>> coordinates;
  coordinates.resize(gridBase->size(DEC_DIM));

  auto const& indexSet = gridBase->leafIndexSet();
  for (auto const& v : vertices(gridBase->leafGridView())) {
    coordinates[indexSet.index(v)] = v.geometry().center();
  }
  msg("time(coordinates)=",t.elapsed());

  t.reset();
  DecGrid<Grid> grid(*gridBase);
  msg("time(grid.initialize)=",t.elapsed());


  std::cout << "\n\n----------------------------------------------\n\n";

  // traverse all elements
  for (auto const& e : elements(grid.gridView())) {
    std::cout << "element[" << e.index() << "]: vertices = {";
    for (auto const& v : vertices(e)) {
      std::cout << v.index() << " ";
    }
    std::cout << "}, center = " << grid.center(e)
              << ", volume = " << grid.volume(e)
              << ", dual-volume = " << grid.dual_volume(e)<< "\n";
  }

  std::cout << "\n\n----------------------------------------------\n\n";

  // traverse all elements
  for (auto const& v : vertices(grid.gridView())) {
    std::cout << "vertex[" << v.index() << "]: elements = {";
    for (auto const& c : entities(v, Codim_<0>)) {
      std::cout << c.index() << " ";
    }
    std::cout << "}, center = " << grid.center(v)
              << ", volume = " << grid.volume(v)
              << ", dual-volume = " << grid.dual_volume(v)<< "\n";
  }

  std::cout << "\n\n----------------------------------------------\n\n";

  // traverse all elements
  for (auto const& e : edges(grid.gridView())) {
    std::cout << "edge[" << e.index() << "]: elements = {";
    for (auto const& c : entities(e, Codim_<0>)) {
      std::cout << c.index() << " ";
    }
    std::cout << "}, center = " << grid.center(e)
              << ", volume = " << grid.volume(e)
              << ", dual-volume = " << grid.dual_volume(e)<< "\n";
  }

}
