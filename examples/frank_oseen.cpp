#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>
#include <Eigen/IterativeLinearSolvers>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/linear_algebra/block.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/utility/FactoryParametrization.hpp>
#include <dune/dec/utility/SphereMapping.hpp>
#include <dune/dec/utility/VectorValuedContainer.hpp>
// #include "WeightedTriangulation.hpp"
#include <dune/dec/utility/Observer.hpp>
#include <dune/dec/utility/Timer.hpp>

#include <dune/dec/Parameter.hpp>

using namespace Dec;

// Operator a*b
template <class GridView>
class Nonlin
    : public Operator<Nonlin<GridView>, GridView, 1, 1>
{
  friend class Dec::OperatorAccessor;
  using Super = Operator<Nonlin<GridView>, GridView, 1, 1>;
  using Vector = DOFVector<double>;

public:

  /// Constructor
  Nonlin(GridView const& gv, Vector const& a, Vector const& b, float_type value = 1)
    : Super(gv)
    , a_(a)
    , b_(b)
    , value_(value)
  {}

  using Super::gridView;

  void assemble_rhs(DOFVector<double> const& in, DOFVector<double>& out)
  {
    for (auto const& e : edges(gridView())) {
      std::size_t i = e.index();

      double factor = 2.0*value_ / sqr(gridView().volume(e));
      out[i] += factor*(sqr(a_[i]) + sqr(b_[i])) * in[i];
    }
  }

protected:

  template <class Inserter, class Entity>
  void assembleRow_impl(Inserter& A, Entity const& e, float_type factor) const
  {
    assert( Entity::mydimension == 1 );
    std::size_t i = e.index();
    factor *= value_ / sqr(gridView().volume(e));

    A(i,i) << factor*a_[i]*b_[i];
  }

private:

  Vector const& a_; // alpha^k
  Vector const& b_; // (*alpha)^k
  float_type value_;
};

struct UnitRadius
{
  template <class Vec>
  static float_type eval(Vec const&) { return 1.0; }
};


struct PertRadius
{
  template <class Vec>
  static float_type eval(Vec const& y)
  {
    float_type r = y.two_norm();
    float_type theta = std::acos(y[2] / r);
    float_type phi = std::atan2(y[1], y[0]);

    return 1.0 + 0.1*std::cos(4*std::sin(theta)*phi); //*std::cos(8*std::sin(theta*2.0)*phi - 1.0);
  }
};


float_type rnd(float_type mean = 0, float_type amplitude = 2)
{
  return mean + amplitude * (std::rand()/float_type(RAND_MAX) - float_type(0.5));
}


int main(int argc, char** argv)
{
  using namespace Dec::literal;
  decpde::init(argc, argv);

  assert( argc > 1 );

  Parameter param;
  read(argv[1], param);

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;

  bool project = param["grid"]["project"].bool_value();

  std::unique_ptr<GridBase> gridBase;
  if (project) {
    using Param = Parametrization<GridBase, SphereMapping<DEC_DIM, DEC_DOW, PertRadius>>;

    Dune::GridFactory<Param> gridFactory;
    Dune::AlbertaReader<Param>().readGrid(param["grid"]["filename"].string_value(), gridFactory);
    gridBase.reset( gridFactory.create() );
  } else {
    Dune::GridFactory<GridBase> gridFactory;
    Dune::AlbertaReader<GridBase>().readGrid(param["grid"]["filename"].string_value(), gridFactory);
    gridBase.reset( gridFactory.createGrid() );
  }

  // refine and project the grid
  gridBase->globalRefine(param["grid"]["refinement"].int_value());


  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  using WorldVector = typename Grid::GlobalCoordinate;

  Grid grid(*gridBase, false);
  auto leafGridView = grid.leafGridView();
  MusicalIsomorphism<GridView> transform(leafGridView);

  // move cell-centers in direction of barycenter
//   WeightedTriangulation::apply(grid);

  using BMatrix = BlockMatrix<double, 2, 2>;
  using BVector = BlockVector<double, 2>;

  // matrix containing the linear term
  BMatrix A,B;
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      A(i,j).resize(grid.size(DEC_DIM - 1), grid.size(DEC_DIM - 1));
      B(i,j).resize(grid.size(DEC_DIM - 1), grid.size(DEC_DIM - 1));
    }
  }

  BVector b(leafGridView, {1,1}); // rhs vector
  BVector solution(leafGridView, {1,1});

  DEC_MSG("size(0) = ",grid.size(0),", size(1) = ",grid.size(1),", size(2) = ",grid.size(2));

  double tau = param["time"]["timestep"].number_value();
  double K = 1.0;
  double Kn = 1000.0;

  Timer timer;

  // define some operators
  Identity<GridView,1> id(leafGridView);
  LaplaceDeRham<GridView,1> laplace(leafGridView);

  Nonlin<GridView> aa(leafGridView, solution[0], solution[0], Kn);
  Nonlin<GridView> ab(leafGridView, solution[0], solution[1], Kn);
  Nonlin<GridView> bb(leafGridView, solution[1], solution[1], Kn);

  for (int i = 0; i < 2; ++i) {
    auto ins = A(i,i).inserter();
    id.assemble(ins, 1.0/tau - Kn);
    laplace.assemble(ins, K);
  }
  DEC_MSG("time(assemble) = ",timer.elapsed());


  DOFVector<WorldVector> p0(leafGridView, 0);

  // set initial values on vertices
  int initial = param["problem"]["initial"].int_value();

  if (initial == 1) {
    double lambda = 0.01;
    transform.interpol(p0, int_<0>, [lambda](auto const& x)
    {
      double cos4 = std::cos(M_PI/4.0);
      if (std::abs(x[1]) >= cos4)
        return WorldVector{ -x[0], 0.0, -x[2] };
      else if (x[0] >= cos4)
        return WorldVector{ 0.0, x[1], x[2] };
      else if (x[0] <= -cos4)
        return WorldVector{ 0.0, std::sin(M_PI*(x[1] - lambda)), -std::sin(M_PI*x[2]) };
      else
        return WorldVector{ std::abs(x[1]/cos4) - 1.0, x[1]/cos4, 0.0 };
    });
  } else if (initial == 2) {
    transform.interpol(p0, int_<0>, [](auto const&)
    {
      return WorldVector{ rnd(), rnd(), rnd() };
    });
  }

  // project and normalize initial vector
  for (auto const& v : vertices(leafGridView)) {
    std::size_t i = v.index();
    auto x = leafGridView.center(v);
    x /= two_norm(x);

    p0[i] -= x * dot(x, p0[i]);
    p0[i] /= two_norm(p0[i]);
  }

  // interpol p0 to alpha/*alpha
  for (auto const& e : edges(leafGridView)) {
    std::size_t i = e.index();

    auto vertices = leafGridView.indexSet().vertexIndices(e);
    auto v0 = vertices.begin();
    auto v1 = std::next(v0);

    auto p = 0.5*(p0[*v0] + p0[*v1]);
    solution[0][i] = dot(p, e.geometry().vector());

//     auto f = store(faces(e));
//     WorldVector dual_edge = f[0].sign(e)*grid.center(f[0]) + f[1].sign(e)*grid.center(f[1]);
    solution[1][i] = -leafGridView.volume(e)/leafGridView.dual_volume(e) * dot(p, e.geometry().dual_vector());
  }



  // create file writer
  using VtkWriter = Dune::VTKWriter<GridView>;
  std::shared_ptr<VtkWriter> writer = std::make_shared<VtkWriter>(leafGridView);
  Dune::VTKSequenceWriter<GridView> pvdwriter(writer,
                                              param["output"]["filename"].string_value(),
                                              param["output"]["directory"].string_value(),
                                              "data");

  using VecVector = VectorValuedContainer<double, DEC_DOW>;

  auto p_ = transform.sharp(solution[0], int_<1>, int_<2>);
  VecVector p{p_};
  pvdwriter.addCellData(p, "p", DEC_DOW);
//   pvdwriter.addVertexData(solution[0], "alpha");
//   pvdwriter.addVertexData(solution[1], "dual-alpha");

  pvdwriter.write(0.0);

//   Eigen::UmfPackLU<typename BMatrix::Matrix> solver;
//   bool analyzed = false;
  Eigen::BiCGSTAB<Eigen::SparseMatrix<double>, Eigen::DiagonalPreconditioner<double> > solver;

  DOFVector<double> x(solution[0].size() + solution[1].size());
  CyclicObserver observer(param["time"]["dt_write"].number_value(), param["time"]["end_time"].number_value());
  for (double t = 0.0; t < param["time"]["end_time"].number_value(); t += tau)
  {
    DEC_MSG("");
    DEC_MSG("Timestep t=",t);

    timer.reset();
    B.setZero();
    DEC_MSG("time(B.setZero()) = ",timer.elapsed());
    timer.reset();
    for (int i = 0; i < 2; ++i) {
      auto ins = B(i,i).inserter();
      aa.assemble(ins, i == 0 ? 3.0 : 1.0);
      bb.assemble(ins, i == 0 ? 1.0 : 3.0);

      auto ins2 = B(i, 1-i).inserter();
      ab.assemble(ins2, 2.0);
    }
    DEC_MSG("time(assemble(B)) = ",timer.elapsed());
    timer.reset();
    for (int i = 0; i < 2; ++i) {
      B(i,i) += A(i,i);
    }
    DEC_MSG("time(B+=A) = ",timer.elapsed());
    timer.reset();
    auto const& C = B.compress();
    DEC_MSG("time(B.compress) = ",timer.elapsed());
    timer.reset();

    for (int i = 0; i < 2; ++i) {
      b[i] = solution[i].array() * (1.0/tau) + 1.0;
      ab.assemble_rhs(solution[i], b[i]);
    }
    DEC_MSG("time(assemble(b)) = ",timer.elapsed());
    timer.reset();

    auto const& c = b.compress();
    DEC_MSG("time(b.compress) = ",timer.elapsed());
//     timer.reset();
//     DEC_MSG("time(assemble) = ",timer.elapsed());

    timer.reset();
//     if (!analyzed) {
//       solver.analyzePattern(C);
//       analyzed = true;
//     }
//     solver.factorize(C);
    x = solver.compute(C).solveWithGuess(c, x);

//     solver::bcgs(C, x, c);
//     solver::bcgs(B, solution, b);
    DEC_MSG("time(solve) = ",timer.elapsed());

    solution = x;
    DEC_MSG("estimated error: ", residuum(B, solution, b));

    timer.reset();

    if (observer(t + tau)) {
      p_ = transform.sharp(solution[0], int_<1>, int_<2>);
      pvdwriter.write(t+tau);
      DEC_MSG("time(write) = ",timer.elapsed());
      ++observer;
    }
  }

  pvdwriter.write(param["time"]["end_time"].number_value());
}
