#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <Eigen/UmfPackSupport>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/utility/Timer.hpp>

using namespace Dec;

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

template <class GridView>
double get_grid_width(GridView const& gv)
{
  double h = 0.0;
  for (auto const& e : edges(gv)) {
    if (gv.volume(e) > h)
      h = gv.volume(e);
  }

  return h;
}

template <class Matrix, class Vector1, class Vector2>
void smooth(int nu, Matrix const& A, Vector1& u, Vector2 const& b)
{
  for (int iter = 0; iter < nu; ++iter) {
    for (int k = 0; k < A.outerSize(); ++k) {
      auto tmp = b[k];
      double diag = 0.0;
      for (typename Matrix::InnerIterator it(A, k); it; ++it) {
        if (it.col() != k)
          tmp -= it.value() * u[it.col()];
        else
          diag = it.value();
      }
      assert( diag != 0.0 );
      u[k] = tmp / diag;
    }
  }
}

template <class Grid, class Matrix, class Vector, class Solver>
void mg(Grid const& grid, int l, int L, int gamma, std::vector<Matrix> const& A, std::vector<Vector>& u, std::vector<Vector>& b, Solver const& solver, int nu1, int nu2)
{
  smooth(nu1, A[l], u[l], b[l]); // -> u[l]
  auto d = b[l] - A[l]*u[l]; // compute defect
  grid.transfer_[l-1].restrict(d, b[l-1]); // restrict the defect


  auto boundary = get_boundary<Grid::dimension>(grid.levelGridView(l-1), [](auto const& x) {
    return x[0] < 1.e-10 || x[0] > 1.0 - 1.e-10 || x[1] < 1.e-10 || x[1] > 1.0 - 1.e-10;
  });

  for (auto j : boundary)
    b[l-1][j] = 0;

  if (l-1 == L) {
    // solve on the coarse grid
    u[l-1] = solver.solve(b[l-1]);
  } else {
    // apply gamma l-grid cycles
    u[l-1].setZero();
    for (int g = 0; g < gamma; ++g) {
      mg(grid, l-1, L, gamma, A, u, b, solver, nu1, nu2);
    }
  }

  grid.transfer_[l-1].prolongate(u[l-1], u[l], true);
  smooth(nu2, A[l], u[l], b[l]); // -> u[l]
}


int main(int argc, char** argv)
{
  decpde::init(argc, argv);
  Timer timer;

  assert_msg( argc > 1, "usage: ./ellipt grid-filename nSteps");

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  int nSteps = 2;
  if (argc > 2)
    nSteps = std::atoi(argv[2]);

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  MusicalIsomorphism<GridView> transform(grid.leafGridView());

  std::vector<DOFMatrix<double>> A(nSteps);
  std::vector<DOFVector<double>> b,u;

  LaplaceBeltrami<GridView> laplace(grid.leafGridView());
  for (int l = 0; l < nSteps; ++l) {
    if (l > 0) {
      grid.globalRefine(1);
    }

    b.emplace_back(grid.leafGridView(), 0, "b" + std::to_string(l)); // rhs vector
    u.emplace_back(grid.leafGridView(), 0, "u" + std::to_string(l)); // solution vector

    laplace.build(A[l], -1.0);

    transform.interpol(b.back(), int_<0>, [](auto const& x) {
      double r2 = unary_dot(x);
      double ux = exp(-10.0 * r2);
      return -(400.0 * r2 - 20.0 * DEC_DOW) * ux;
    });

    transform.interpol(u.back(), int_<0>, [](auto const& x) {
      return exp(-10.0 * unary_dot(x));
    });

    auto boundary = get_boundary<Grid::dimension>(grid.leafGridView(), [](auto const& x) {
      return x[0] < 1.e-10 || x[0] > 1.0 - 1.e-10 || x[1] < 1.e-10 || x[1] > 1.0 - 1.e-10;
    });

    for (auto j : boundary) {
      A[l].clear_dirichlet_row(j);
      if (l < nSteps-1)
        b[l][j] = 0;
      else
        b[l][j] = u[l][j];
    }

    double h = get_grid_width(grid.leafGridView());
    msg("h=",h,", grid.size(2)=",grid.size(2));
  }

  // two-grid cycle
  int nu1 = 1, nu2 = 1; // pre/post smoothing iterations
  int gamma = 2; // W-cycle
  int iter = 2;

  int l = nSteps-1; // fine level
  int L = 0; // coarse level

  // coarse-grid solver
  using Matrix = DOFMatrix<double>;
  Eigen::UmfPackLU<typename Matrix::Matrix> solver;
  solver.analyzePattern(A[L]);
  solver.factorize(A[L]);

  timer.reset();
  u[l].setZero();
  double resid = 1.0, nrm_b = two_norm(b[l]);
  for (int i = 0; i < iter && resid > 1.e-7; ++i) {
    mg(grid, l, L, gamma, A, u, b, solver, nu1, nu2);
    resid = residuum(A[l], u[l], b[l]) / nrm_b;
    msg("  residuum(",i,") = ",residuum(A[l], u[l], b[l]));
  }

  msg("time(solve) = ",timer.elapsed()," sec");
}
