#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/utility/Timer.hpp>
#include <dune/dec/operators/ChainedOperator.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>

using namespace Dec;

template <class Matrix>
void clearDirichletRows(Matrix& A, int idx)
{
#ifdef DEC_HAS_EIGEN
  using value_type = typename Matrix::value_type;

  for (int k = 0; k < A.outerSize(); ++k) {
    for (typename Matrix::InnerIterator it(A, k); it; ++it) {
      if (it.index() != idx)
        break;

      it.valueRef() = it.row() == it.col() ? value_type(1) : value_type(0);
    }
  }
#endif
}

template <class Grid, class Vector>
void flat2(Grid const& grid, Vector& vec)
{
  for (auto const& e : elements(grid.leafGridView()))
  {
    vec[grid.index(e)] *= grid.volume(e);
  }
}

template <class Grid, class Vector>
void sharp2(Grid const& grid, Vector& vec)
{
  for (auto const& e : elements(grid.leafGridView()))
  {
    vec[grid.index(e)] /= grid.volume(e);
  }
}

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  assert( argc > 1 );

  using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  using Matrix = DOFMatrix<double>;
  using Vector = DOFVector<double>;

  double tau = 0.1;

  Timer timer;
  Matrix B;

  Identity<GridView,1> id(grid.leafGridView(), 1.0/tau);
  LaplaceDeRham<GridView,1> laplace(grid.leafGridView());

  auto op = id + laplace;

  op.build(B);
//   if (B.rows() < 50)
//     msg("B=",B);

  msg("time(assemble) = ",timer.elapsed());

  using VtkWriter = Dune::VTKWriter<GridView>;
  using PvdWriter = Dune::VTKSequenceWriter<GridView>;

  auto writer = std::make_shared<VtkWriter>(grid.leafGridView());
  PvdWriter pvdwriter(writer, "heat");

//   Eigen::BiCGSTAB<Matrix> solver;
//   solver.compute(B);

  Vector c(grid.leafGridView(), 1); random(c);
//   flat2(grid, c);
  Vector y = c;

  pvdwriter.addCellData(y, "two_form");


  c *= 1.0/tau;
  // pvdwriter.addVertexData(c, "zero_form");
  for (double t = 0.0; t <= 10.0; t += tau)
  {
    msg("Timestep t=",t);
    pvdwriter.write(t);
//     sharp2(grid, y);

    timer.reset();
    solver::bcgs(B,y,c);
    msg("time(solve) = ",timer.elapsed());
    msg("estimated error: ", residuum(B,y,c));

    c = y * (1.0/tau);
  }

  pvdwriter.write(10.0);
}
