#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>

#include <dune/dec/simplegrid/SimpleGrid.hpp>

using namespace Dec;

template <class Grid>
void print(Grid const& grid)
{
  msg("------------------------------------------\n");
  auto const& is = grid.indexSet();
  msg("vertices_ = [");
  for (std::size_t i = 0; i < is.vertices_.size(); ++i)
    msg(is.vertices_[i]," ");
  msg("]");
  msg("edges_ = [");
  for (std::size_t i = 0; i < is.edges_.size(); ++i)
    msg(is.edges_[i]," ");
  msg("]");
  msg("faces_ = [");
  for (std::size_t i = 0; i < is.faces_.size(); ++i)
    msg(is.faces_[i]," ");
  msg("]");
  msg("half_edges_ = [");
  for (std::size_t i = 0; i < is.half_edges_.size(); ++i)
    msg("{[",i,"]: ",is.halfEdge(i).next_,", ",is.halfEdge(i).previous_,", ",is.halfEdge(i).opposite_,", ",is.halfEdge(i).vertex_,", ",is.halfEdge(i).edge_,", ",is.halfEdge(i).face_,"} ");
  msg("]");
}

int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  using WorldVector = Dune::FieldVector<float_type, 2>;

  WorldVector x0 = {0.0, 0.0};
  WorldVector x1 = {1.0, 0.0};
  WorldVector x2 = {1.0, 1.0};
  WorldVector x3 = {0.0, 1.0};

  std::vector<std::vector<int>> cells = { {2,0,1}, {2,0,3} };
  std::vector<WorldVector> coords = {x0, x1, x2, x3};

  using GridBase = Dec::SimpleGrid<2,2,float_type,int>;
  GridBase simplegrid(cells, coords);

  DecGrid<GridBase> grid(simplegrid);

  print(grid);


}
