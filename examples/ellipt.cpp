#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <Eigen/UmfPackSupport>
#include <Eigen/Eigenvalues>

#include <unsupported/Eigen/SparseExtra>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
// #include "operators/FEMLaplace.hpp"
// #include "operators/FEMCenterWeight.hpp"
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/operators/Identity.hpp>
#include <dune/dec/operators/ChainedOperator.hpp>

#include <dune/dec/MusicalIsomorphism.hpp>
// #include "WeightedTriangulation.hpp"
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>

using namespace Dec;

// laplace^RR(p) + p = (-cos(x)*sin(y) + x^2(y^2+x-2), 2*sin(x)*cos(y) + 4*x*y), p = 0 for x = 0
//
// => p = (x^2*(y^2 + x), sin(x)*cos(x))

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

template <class GridView>
std::tuple<double, double> get_grid_width(GridView const& gv)
{
  double h = 0.0, hstar = 0.0;
  for (auto const& e : edges(gv)) {
    if (!e.hasBoundaryIntersections() && gv.volume(e) > h) {
      h = gv.volume(e);
      hstar = gv.dual_volume(e);
    }
  }

  return std::make_tuple(h, hstar);
}



// RotRot-Laplace: (*d*d)
template <class GridView>
class Laplace
    : public Operator<Laplace<GridView>, GridView, 0, 0>
{
  friend class OperatorAccessor;

  using Super = Operator<Laplace<GridView>, GridView, 0, 0>;

public:

  static constexpr int dim = GridView::dimension;

  /// Constructor
  Laplace(GridView const& gv, double alpha)
    : Super(gv)
    , alpha(alpha)
  {}

  using Super::gridView;

//   auto nzrows_impl() const
//   {
//     return make_vector([this](std::size_t i)
//     {
//       auto const& elem = Dune::ReferenceElements<float_type, 1>::simplex();
//       EntityIndex<dim, dim> e{i};
//       auto const& T = this->grid().template entities<dim-1>(e);
//       return T.size()*(elem.size(1) - 1) + 1;
//     });
//   }

  template <class Inserter, class Entity>
  void assembleRow_impl(Inserter& A, Entity const& v, float_type factor) const
  {
    std::size_t v_i = v.index();
    double dual_volume_v = gridView().dual_volume(v);

    auto edges_ = edges(v);
    if (alpha > 1.e-10) {
      if (edges_.size() == 4)
        factor *= (1.0 - alpha);
      else
        factor *= (1.0 + 2*alpha - sqr(alpha)) / (1.0 + alpha);
    }

//     msg("v(",grid().index(v),"): dual_vol=",dual_volume_v);

    for (auto const& e : edges_) {
//       msg("  |*e|=",grid().dual_volume(e),", |e|=", grid().volume(e),", |*e|/|e|=",grid().dual_volume(e)/grid().volume(e));
      double coeff = factor * gridView().dual_volume(e) / (gridView().volume(e) * dual_volume_v);

      for (auto const& v_ : vertices(e)) {
        std::size_t v_j = v_.index();
        A(v_i,v_j) << (v_j == v_i ? -1 : 1) * coeff;
      }
    } // end for(e)
  }

  double alpha;
};


/// calculate approximation to condition number of matrix A using the LinearSolver solver
template <class Matrix>
inline double max_eigenvalue(const Matrix& A, int m=100)
{
  DOFVector<double> x(A.num_rows()), y(A.num_rows());
  random(x);

  double lambda = 0.0, lambda_old = 0.0, relErr, result1;
  x /= two_norm(x);
  for (int i = 0; i < m; ++i) {
    y = A * x;
    lambda_old = lambda;
    lambda = dot(y,x);
    relErr = std::abs((lambda-lambda_old)/lambda);
    if (relErr < 1.e-5)
      break;
    x = y / two_norm(y) ;
  }
  result1 = std::abs(lambda);
  return result1;
}



int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert_msg( argc > 2, "usage: ./ellipt grid-filename strategy");

  using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  int strategy = 0;
  if (argc > 2)
    strategy = std::atoi(argv[2]);
  std::string filename = "ellipt_convergence_";
  std::ofstream out(filename + std::to_string(strategy) + ".csv", std::ios_base::out);

  int nSteps = 1;
  if (argc > 3)
    nSteps = std::atoi(argv[3]);

  msg("y... exact solution");
  msg("x... numeric solution, x = A^(-1)*b");
  msg("w... expected truncation error, w = factor*y");
  msg("");

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  auto gv = grid.leafGridView();

  for (int i = 0; i < nSteps; ++i) {
    msg("refinement: ",(1+i)*DEC_DIM);
    if (i > 0)
      grid.globalRefine(DEC_DIM, false);



    // move cell-centers in direction of barycenter
//     if (strategy > 0)
//       WeightedTriangulation::apply(grid);
    MusicalIsomorphism<GridView> transform(gv);

// #ifndef NDEBUG
//     msg("elements:");
//     for (auto const& e : elements(grid.gridView()))
//       msg(grid.center(e)[0],", ",grid.center(e)[1]);
//     msg("");
//     msg("edges:");
//     for (auto const& e : edges(grid.gridView()))
//       msg(e.index(), ", ", grid.center(e)[0],", ",grid.center(e)[1]);
//     msg("");
// #endif

    double h, hstar;
    std::tie(h, hstar) = get_grid_width(gv);

    double alpha = hstar/h;
    msg("h=",h,", h*=",hstar,", alpha=",alpha);

    using Matrix = DOFMatrix<double>;
    Matrix A;

    Laplace<GridView> laplace(gv, (strategy > 1 ? alpha : 0.0));
    laplace.build(A, -1.0);
//     msg("A = ",A);

    DOFVector<double> b(gv, 0, "b");
    transform.interpol(b, int_<0>, [](auto const& x) {
      double r2 = unary_dot(x);
      double ux = exp(-10.0 * r2);
      return -(400.0 * r2 - 20.0 * DEC_DOW) * ux;
    });

    DOFVector<double> solution(gv, 0);
    transform.interpol(solution, int_<0>, [](auto const& x) {
      return exp(-10.0 * unary_dot(x));
    });

    auto boundary = get_boundary<Grid::dimension>(gv, [](auto const& x) {
      return x[0] < 1.e-10 || x[0] > 1.0 - 1.e-10 || x[1] < 1.e-10 || x[1] > 1.0 - 1.e-10;
    });
    msg_dbg("#boundary-indices = ",boundary.size());
    for (auto i : boundary) {
      A.clear_dirichlet_row(i);
      b[i] = solution[i];
    }

    Eigen::UmfPackLU<typename Matrix::Matrix> solver;

    solver.analyzePattern(A);
    solver.factorize(A);

    DOFVector<double> x(gv, 0, "x");
    x = solver.solve(b);

    msg("|A*x-b| = ", residuum(A, x, b));

    DOFVector<double> x2(gv, 0, "x2"), w2(gv, 0, 0.0);
    x2 = A*solution;
    x2 -= b;

    for (auto const& v : vertices(gv)) {
      auto edges_ = edges(v);
      std::size_t nEdges = edges_.size();
      if (nEdges == 8)
        w2[v.index()] = (alpha-sqr(alpha))/(1.0+2*alpha-sqr(alpha)) * b[v.index()];
      else if (nEdges == 4)
        w2[v.index()] = (alpha)/(alpha-1.0) * b[v.index()];
    }

    msg("|A*y-b| = ",two_norm(x2));
    msg("|w| = ",two_norm(w2));


    Eigen::saveMarket(A, "matrix_" + std::to_string(i) + ".mtx");
//     msg("|w|/|A*y-b| = ",two_norm(w2) / two_norm(x2));


    DOFVector<double> x3(gv, 0, "x3");
    x3 = A*x;
    x3 -= b;

//     msg("|w|/|A*x-b| = ",two_norm(w2) / two_norm(x3));


    double rho = max_eigenvalue(A, A.num_rows());
    msg("rho(A) = ", rho);

    solution -= x;

    double inf_error = 0.0;
    for (auto const& v : vertices(gv))
      inf_error = std::max(inf_error, std::abs(solution[v.index()]));

    double two_error = two_norm(solution);
    msg("|x - y|_2 = ",two_error);
    msg("|x - y|_inf = ",inf_error);

    out << h << ", " << two_error << ", " << inf_error << ", " << alpha << "\n";
  }
}
