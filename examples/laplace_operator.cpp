#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>


#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/utility/Timer.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>

using namespace Dec;

template <class Matrix>
void clearDirichletRows(Matrix& A, int idx)
{
#if DEC_HAS_EIGEN
  using value_type = typename Matrix::value_type;

  for (int k = 0; k < A.outerSize(); ++k) {
    for (typename Matrix::InnerIterator it(A, k); it; ++it) {
      if (it.index() != idx)
        break;

      it.valueRef() = it.row() == it.col() ? value_type(1) : value_type(0);
    }
  }
#endif
}

int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert( argc > 1 );

  using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  auto gv = grid.leafGridView();

  using Matrix = DOFMatrix<double>;

  Timer t;
  Matrix A;

  LaplaceDeRham<GridView,DEC_DIM> laplace(gv);
  laplace.build(A);
  A.clear_dirichlet_row(0);

  msg("time(assemble) = ",t.elapsed());

  MusicalIsomorphism<GridView> transform(gv);

  DOFVector<double> b(gv, DEC_DIM, 1.0, "b");
  b[0] = 0.0;

  t.reset();
  DOFVector<double> x(gv, DEC_DIM, "x");
  int iter = solver::bcgs(A, x, transform.flat<DEC_DIM>(b));
  msg("time(solve) = ",t.elapsed());
  msg("#iterations:     ", iter);
//   msg("estimated error: ", solver.error());

  t.reset();
  Matrix B;

  LaplaceDeRham<GridView,0> laplace2(gv);
  laplace2.build(B);
  B.clear_dirichlet_row(0);
  msg("time(assemble) = ",t.elapsed());

  t.reset();

  DOFVector<double> c(gv, 0, 1.0, "c");
  c[0] = 0.0;
  DOFVector<double> y(gv, 0, "y");
  int iter2 = solver::bcgs(B,y,transform.flat<0>(c));
  msg("time(solve) = ",t.elapsed());
  msg("#iterations:     ", iter2);
//   msg("estimated error: ", solver.error());

  Dune::VTKWriter<GridView> writer(gv);
  auto solution_x = transform.sharp<DEC_DIM>(x);
  auto solution_y = transform.sharp<0>(y);
  writer.addCellData(solution_x, "two_form");
  writer.addVertexData(solution_y, "zero_form");

  writer.write("laplace.vtu");
//   msg("x = ",x);
}
