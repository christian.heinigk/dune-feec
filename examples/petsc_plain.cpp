#include <petscmat.h>
#include <petscvec.h>

#include <string>

#include <dune/dec/common/Output.hpp>

using namespace Dec;

void matrix_check(bool flag, std::string description)
{
  std::string message = "Matrix is ";
  if (!flag)
    message += "not ";
  message += description + ".";
  msg(message);
}

bool is_assembled(Mat& matrix, bool print = true)
{
  PetscBool result;
  MatAssembled(matrix, &result);
  if(print)
    matrix_check(result, "assembled");
  return result;
}

bool is_valid(Mat& matrix)
{
  bool result = (matrix != nullptr) && is_assembled(matrix, false);
  //bool result = matrix != nullptr;
  matrix_check(result, "valid");
  return result;
}

void set_up(Mat& matrix)
{
  MatCreate(PETSC_COMM_WORLD, &matrix);
  MatSetSizes(matrix, PETSC_DECIDE, PETSC_DECIDE, 2, 2);
  MatSetFromOptions(matrix);
  MatSetUp(matrix);
}

void initialize(Mat& matrix, PetscInt slotsize)
{
  MatType type;
  MatGetType(matrix, &type);

  matrix_check(true, type);

  if (strcmp(type, MATSEQAIJ) == 0) {
    MatSeqAIJSetPreallocation(matrix, slotsize, PETSC_NULL);
  } else if (strcmp(type, MATMPIAIJ) == 0) {
    MatMPIAIJSetPreallocation(matrix, slotsize, PETSC_NULL, slotsize/2, PETSC_NULL);
  } else {
    error_exit("Matrix type ",type," not supported!");
  }
}

void set(PetscInt row, PetscInt col, PetscScalar value, Mat& matrix)
{
  msg("Set matrix[", row, ",", col, "] to ", value, ".");
  MatSetValue(matrix , row, col, value, ADD_VALUES);
}

void assemble(Mat& matrix)
{
  MatAssemblyBegin(matrix, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(matrix, MAT_FINAL_ASSEMBLY);
}

void view(Mat& matrix)
{
  // Must call MatAssemblyBegin/End() before viewing matrix
  if (is_assembled(matrix, false))
    MatView(matrix, PETSC_VIEWER_STDOUT_WORLD);
  else
    matrix_check(false, "assembled, so nothing to view");
}

void duplicate(Mat& source, Mat& destination)
{
  MatDuplicate(source, MAT_COPY_VALUES, &destination);
}

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, NULL, NULL);

  Mat A = nullptr;
  msg();

  msg(">>> A is nullptr.");
  is_valid(A);
  //is_assembled(A);
  msg();

  set_up(A);

  msg(">>> A is set up.");
  is_valid(A);
  //is_assembled(A);
  msg();

  msg(">> A is going to be initialized.");
  MatSetType(A, MATSEQAIJ);
  initialize(A, 2);
  msg();

  msg(">>> A is initialized.");
  is_valid(A);
  is_assembled(A);
  msg();

  msg(">>> A view:");
  view(A);
  msg();

  msg(">>> A set values:");
  set(0, 0, 41.21, A);
  set(1, 1, 5, A);
  is_valid(A);
  is_assembled(A);
  msg();

  msg(">>> A assemble.");
  assemble(A);
  is_valid(A);
  is_assembled(A);
  msg();

  msg(">>> A view:");
  view(A);
  msg();

  Mat B;

  msg(">>> B.");
  is_valid(B);
  //is_assembled(B);
  msg();

  msg(">>> Duplicate A -> B:");
  duplicate(A, B);
  is_valid(B);
  is_assembled(B);
  view(B);
  msg();

  PetscFinalize();

  return 0;
}
