#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <Eigen/IterativeLinearSolvers>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/utility/FunctorVector.hpp>
#include <dune/dec/utility/Timer.hpp>

using namespace Dec;

int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert( argc > 1 );

  static_assert( DEC_DIM == 2, "" );

  using Grid = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<Grid> gridFactory;
  Dune::AlbertaReader<Grid>().readGrid(argv[1], gridFactory);
  std::unique_ptr<Grid> gridBase( gridFactory.createGrid() );

  DecGrid<Grid> grid(*gridBase);

  using Matrix = DOFMatrix<double>;
  using Vector = DOFVector<double>;

  Timer t;

  std::size_t N = grid.size(DEC_DIM-1);
  Matrix A(N,N);
  {
    auto ins = A.inserter();

    for (auto const& e : edges(grid.gridView()))
    {
      std::size_t e_i = e.index();
      ins(e_i,e_i) << 1.0;

      // assemble Laplace^RR operator
      double volume_e = grid.volume(e);
      double dual_volume_e = grid.dual_volume(e);
      for (auto const& T : faces(e)) {
        double volume_T = grid.volume(T);

        double factor = -T.sign(e) * volume_e / std::max(1.e-8, dual_volume_e * volume_T);

        for (auto const& e_ : edges(T)) {
          std::size_t e_j = e_.index();
          ins(e_i,e_j) << e_.sign(T) * factor;
        }
      }

      // assemble Laplace^GD operator
      for (auto const& v : vertices(e)) {
        double dual_volume_v = -v.sign(e) * grid.dual_volume(v);

        for (auto const& e_ : edges(v)) {
          double dual_volume_e_ = grid.dual_volume(e_);
          double volume_e_ = grid.volume(e_);

          std::size_t e_j = e_.index();
          ins(e_i,e_j) << e_.sign(v) * dual_volume_v * dual_volume_e_ / std::max(1.e-8, volume_e_);
        }
      }
    }
  }
  msg("time(assemble) = ",t.elapsed());

  A.clear_dirichlet_row(0);


  Vector b(N, 1.0);
  b[0] = 0.0;

  t.reset();
  Eigen::BiCGSTAB<typename Matrix::Matrix> solver;
  solver.compute(A);
  Vector x = solver.solve(b);
  msg("time(solve) = ",t.elapsed());

  msg("#iterations:     ", solver.iterations());
  msg("estimated error: ", solver.error());

//   msg("x = ",x);
}
