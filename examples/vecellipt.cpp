#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/albertareader.hh>

#include <Eigen/UmfPackSupport>
#include <Eigen/Eigenvalues>

#include <unsupported/Eigen/SparseExtra>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/linear_algebra/iteration/IteratedSolver.hpp>
#include <dune/dec/linear_algebra/iteration/NoisyIteration.hpp>
#include <dune/dec/linear_algebra/block.hpp>
#include <dune/dec/linear_algebra/block/BlockJacobi.hpp>
#include <dune/dec/linear_algebra/smoother/Jacobi.hpp>
#include <dune/dec/operators/Laplace.hpp>

using namespace Dec;

// laplace^RR(p) + p = (-cos(x)*sin(y) + x^2(y^2+x-2), 2*sin(x)*cos(y) + 4*x*y), p = 0 for x = 0
//
// => p = (x^2*(y^2 + x), sin(x)*cos(x))

template <int cd, class GridView, class Predicate>
std::list<std::size_t> get_boundary(GridView const& gv, Predicate pred)
{
  std::list<std::size_t> indices;
  for (auto const& e : entities(gv, Codim_<cd>))
  {
    if (pred(gv.center(e)))
      indices.push_back(e.index());
  }

  return indices;
}

int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert_msg( argc > 1, "usage: ./ellipt grid-filename refinement");

  using GridBase = Dune::AlbertaGrid<DEC_DIM, DEC_DOW>;

  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  int nSteps = 1;
  if (argc > 2)
    nSteps = std::atoi(argv[2]);

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);
  auto gv = grid.leafGridView();
  MusicalIsomorphism<GridView> transform(gv);

  grid.globalRefine(nSteps, false);


  LaplaceBeltrami<GridView> laplace(gv);

  BlockMatrix<double, 2, 2> A;
  laplace.build(A(0,0), -1.0);
  laplace.build(A(1,1), -1.0);

  auto boundary = get_boundary<Grid::dimension>(gv, [](auto const& x) {
    return x[0] < 1.e-10 || x[0] > 1.0 - 1.e-10 || x[1] < 1.e-10 || x[1] > 1.0 - 1.e-10;
  });

  BlockVector<double, 2> b(gv, {0,0});
  BlockVector<double, 2> u(gv, {0,0});
  for (int i = 0; i < 2; ++i) {
    transform.interpol(b[i], int_<0>, [](auto const& x) {
      double r2 = unary_dot(x);
      double ux = exp(-10.0 * r2);
      return -(400.0 * r2 - 20.0 * DEC_DOW) * ux;
    });

    transform.interpol(u[i], int_<0>, [](auto const& x) {
      return exp(-10.0 * unary_dot(x));
    });

    for (std::size_t j : boundary)
      dirichletBC(j, A(i,i), b[i], u[i][j]);
  }


  Jacobi jacobi1, jacobi2;
  auto precon1 = makeIteratedSolver(jacobi1, 10);
  auto precon2 = makeIteratedSolver(jacobi2, 10);

  auto bjacobi = makeBlockJacobi(precon1, precon2);
  bjacobi.compute(A);

  BlockVector<double, 2> x(gv, {0,0});
  x.setZero();

  NoisyIteration<double> iter(100, 1.e-7);
  solver::pcg(A,x,b, bjacobi, iter);

  msg("|A*x-b| = ", residuum(A, x, b));
}
