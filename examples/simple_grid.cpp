#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/albertagrid/albertareader.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>
#include <dune/dec/simplegrid/SimpleGridFactory.hpp>
#include <dune/dec/common/Output.hpp>


int main(int argc, char** argv)
{
  using namespace Dec;
  decpde::init(argc, argv);

  assert( argc > 1 );

  using Grid = Dec::SimpleGrid<DEC_DIM, DEC_DOW>;
  Dune::GridFactory<Grid> gridFactory;
  Dune::AlbertaReader<Grid>().readGrid(argv[1], gridFactory);
  std::unique_ptr<Grid> grid( gridFactory.createGrid() );

  auto const& indexSet = grid->leafIndexSet();
  for (auto const& e : Dune::elements(grid->leafGridView()))
  {
    msg_("cell[", indexSet.index(e), "] = {");

    for (unsigned int i = 0; i < e.subEntities(DEC_DIM); ++i)
      msg_(indexSet.subIndex(e, i, DEC_DIM)," ");
    msg("}");
  }

  msg("------------------------------------");

//   for (auto const& e : Dune::edges(grid->leafGridView()))
//   {
//     msg("edge[", indexSet.index(e), "] = ");
//   }

  msg("------------------------------------");

  for (auto const& v : Dune::vertices(grid->leafGridView()))
  {
    msg("vertex[", indexSet.index(v), "] = ", v.geometry().corner(0));
  }


}
