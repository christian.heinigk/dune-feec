#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <array>
#include <iostream>
#include <memory>
#include <vector>

#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/utility/parmetisgridpartitioner.hh>
#include <dune/grid/uggrid.hh>

#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/plocalindex.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <dune/common/parallel/interface.hh>
#include <dune/common/enumset.hh>

#include <dune/dec/parallel/ParallelDofMapper.hpp>

enum GridFlags {
  owner, overlap, border
};


template <class GridView, int cd>
class Handle
    : public Dune::CommDataHandleIF<Handle<GridView, cd>, std::size_t>
{
  using ParallelIndexSet = Dune::ParallelIndexSet<std::size_t, Dune::ParallelLocalIndex<GridFlags>>;

public:
  Handle(GridView const& gv, Dec::LocalCodimMapper<cd> const& localCodimMapper)
    : indexSet_(gv.indexSet())
    , idSet_(gv.grid().globalIdSet())
    , localCodimMapper_(localCodimMapper)
  {
    parallelIndexSet_.beginResize();
  }

  bool contains(int /*dim*/, int codim) const { return codim == cd; }
  bool fixedSize(int /*dim*/, int /*codim*/) const { return true; }

  template <class Entity>
  std::size_t size(Entity const& /*e*/) const { return 2u; }

  template <class MessageBuffer, class Entity>
  void gather(MessageBuffer& buff, Entity const& e) const
  {
    buff.write(std::size_t(indexSet_.index(e)));
    buff.write(std::size_t(idSet_.id(e)));
  }

  template <class MessageBuffer, class Entity>
  void scatter(MessageBuffer& buff, Entity const& e, std::size_t /*n*/)
  {
    assert( !finished );
    std::size_t local = 0, global = 0;
    buff.read(local);
    buff.read(global);

    GridFlags attribute = localCodimMapper_.owns(local) ? GridFlags::owner : GridFlags::overlap;
    Dune::ParallelLocalIndex<GridFlags> localIndex{local, attribute};
    parallelIndexSet_.add(global, localIndex);
  }

  ParallelIndexSet&& get()
  {
    parallelIndexSet_.endResize();

    finished = true;
    return std::move(parallelIndexSet_);
  }

private:
  typename GridView::IndexSet const& indexSet_;
  typename GridView::Grid::GlobalIdSet const& idSet_;

  Dec::LocalCodimMapper<cd> const& localCodimMapper_;

  ParallelIndexSet parallelIndexSet_;

  bool finished = false;
};


int main(int argc, char** argv)
{
  auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  // Create ug grid from structured grid
  std::array<unsigned int, 2> n = {{2, 2}};
  Dune::FieldVector<double, 2> lower = {{0, 0}};
  Dune::FieldVector<double, 2> upper = {{1, 1}};

  using GridBase = Dune::UGGrid<2>;
  std::shared_ptr<GridBase> gridBase = Dune::StructuredGridFactory<GridBase>::createSimplexGrid(lower, upper, n);

  using GridView = typename GridBase::LeafGridView;
  auto gv = gridBase->leafGridView();

  // Create initial partitioning using ParMETIS
  std::vector<unsigned> part(Dune::ParMetisGridPartitioner<GridView>::partition(gv, mpihelper));

  // Transfer partitioning from ParMETIS to our grid
  gridBase->loadBalance(part, 0);


  Dec::LocalCodimMapper<2> localCodimMapper(gv);
  Handle<GridView, 2> handle{gv, localCodimMapper};
  gv.communicate(handle, Dune::All_All_Interface, Dune::ForwardCommunication);

  auto parallelIndexSet = handle.get();

  Dune::RemoteIndices<decltype(parallelIndexSet)> remoteIndices{parallelIndexSet, parallelIndexSet, gridBase->comm()};
  remoteIndices.rebuild<true>();

  Dune::Interface interface{gridBase->comm()};
  interface.build(remoteIndices, Dune::EnumItem<GridFlags,owner>{}, Dune::EnumItem<GridFlags,overlap>{});

}
