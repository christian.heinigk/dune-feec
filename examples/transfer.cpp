#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/DecGrid.hpp>
#include <dune/dec/MusicalIsomorphism.hpp>


using namespace Dec;

template <class Grid>
std::tuple<double, double> get_grid_width(Grid const& grid)
{
  double h = 0.0, hstar = 0.0;
  for (auto const& e : edges(grid.leafGridView())) {
    if (!e.hasBoundaryIntersections() && grid.volume(e) > h) {
      h = grid.volume(e);
      hstar = grid.dual_volume(e);
    }
  }

  return std::make_tuple(h, hstar);
}


int main(int argc, char** argv)
{
  decpde::init(argc, argv);

  assert_msg( argc > 1, "usage: ./transfer grid-filename");

  using GridBase = Dune::FoamGrid<DEC_DIM, DEC_DOW>;
  Dune::GridFactory<GridBase> gridFactory;
  Dune::AlbertaReader<GridBase>().readGrid(argv[1], gridFactory);
  std::unique_ptr<GridBase> gridBase( gridFactory.createGrid() );

  gridBase->globalRefine(0);

  using Grid = DecGrid<GridBase>;
  using GridView = typename Grid::GridView;
  Grid grid(*gridBase);

  auto leafGridView = grid.leafGridView();
  MusicalIsomorphism<GridView> transform(leafGridView);


  auto const& indexSet = gridBase->leafIndexSet();
  auto const& idSet = gridBase->localIdSet();
  for (auto v : vertices(gridBase->leafGridView()))
    DEC_MSG("v[",idSet.id(v),"] = ",indexSet.index(v));



  DOFVector<double> coarse(leafGridView, 0);
  transform.interpol(coarse, int_<0>, [](auto const& x) {
    return exp(-10.0 * unary_dot(x));
  });

  Dune::VTKWriter<GridView> writer_coarse(grid.levelGridView(gridBase->maxLevel()));
  writer_coarse.addVertexData(coarse, "coarse");
  writer_coarse.write("transfer_coarse");

  grid.globalRefine(1);
  DOFVector<double> fine(leafGridView, 0);
  auto const& transfer = grid.transfer_[0];

//   print(transfer.mat1_);
//   print(transfer.mat2_);

  DEC_MSG("---------------------------");

  for (auto v : vertices(gridBase->leafGridView()))
    DEC_MSG("v[",idSet.id(v),"] = ",indexSet.index(v));

  transfer.prolongate(coarse, fine);

  Dune::VTKWriter<GridView> writer_fine(leafGridView);
  writer_fine.addVertexData(fine, "fine");
  writer_fine.write("transfer_fine");


  transfer.restrict(fine, coarse);
  writer_coarse.write("transfer_coarse2");
}
