#include <dune/dec/DecGrid.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec { namespace test {

  bool wellcentered()
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {0.5, 1.0};

    std::vector<std::vector<int>> cells = { {0, 1, 2} };
    std::vector<WorldVector> coords = {x0, x1, x2};

    using GridBase = Dec::SimpleGrid<2,2,float_type,int>;
    GridBase simplegrid(cells, coords);

    DecGrid<GridBase> grid(simplegrid);

    return grid.check_well_centeredness();
  }

  bool maybe_wellcentered()
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {0.0, 1.0};

    std::vector<std::vector<int>> cells = { {0, 1, 2} };
    std::vector<WorldVector> coords = {x0, x1, x2};

    using GridBase = Dec::SimpleGrid<2,2,float_type,int>;
    GridBase simplegrid(cells, coords);

    DecGrid<GridBase> grid(simplegrid);

    return grid.check_well_centeredness();
  }

  bool not_wellcentered()
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {-0.5, 1.0};

    std::vector<std::vector<int>> cells = { {0, 1, 2} };
    std::vector<WorldVector> coords = {x0, x1, x2};

    using GridBase = Dec::SimpleGrid<2,2,float_type,int>;
    GridBase simplegrid(cells, coords);

    DecGrid<GridBase> grid(simplegrid);

    return !grid.check_well_centeredness();
  }

  bool oriented()
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {0.0, 1.0};
    WorldVector x3 = {-1.0, 0.0};

    std::vector<std::vector<int>> cells = { {0, 1, 2}, {0, 2, 3} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using GridBase = Dec::SimpleGrid<2,2,float_type,int>;
    GridBase simplegrid(cells, coords);

    DecGrid<GridBase> grid(simplegrid);

    return grid.check_orientation();
  }

  bool not_oriented_2d()
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {0.0, 1.0};
    WorldVector x3 = {-1.0, 0.0};

    std::vector<std::vector<int>> cells = { {0, 1, 2}, {0, 3, 2} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using GridBase = Dec::SimpleGrid<2,2,float_type,int>;
    GridBase simplegrid(cells, coords);

    DecGrid<GridBase> grid(simplegrid);

    return !grid.check_orientation();
  }

  bool not_oriented_3d()
  {
    using WorldVector = Dune::FieldVector<float_type, 3>;

    // Möbius strip with 7 triangles

    WorldVector a = {0.0, 0.0, 0.0};
    WorldVector b = {1.0, 0.0, 0.0};
    WorldVector c = {1.0, 1.0, 0.0};
    WorldVector d = {0.0, 1.0, 0.0};
    WorldVector e = {0.0, 0.0, 1.0};
    WorldVector f = {1.0, 0.0, 1.0};
    WorldVector g = {1.0, 1.0, 1.0};

    std::vector<std::vector<int>> cells = { {0, 1, 3}, {1, 2, 3}, {1, 2, 6}, {1, 6, 5}, {4, 5, 6}, {3, 4, 5}, {0, 3, 4} };
    std::vector<WorldVector> coords = {a, b, c, d, e, f, g};

    using GridBase = Dec::SimpleGrid<2,3,float_type,int>;
    GridBase simplegrid(cells, coords);

    DecGrid<GridBase> grid(simplegrid);

    return !grid.check_orientation();
  }

} }


int main()
{
  using namespace Dec;

  DEC_TEST( test::wellcentered() );
  DEC_TEST( test::maybe_wellcentered() );
  DEC_TEST( test::not_wellcentered() );
  DEC_TEST( test::oriented() );
  DEC_TEST( test::not_oriented_2d() );
  DEC_TEST( test::not_oriented_3d() );

  return report_errors();
}
