#include <vector>

#include <dune/dec/LocalView.hpp>
#include <dune/dec/ranges/Enumerate.hpp>
#include <dune/dec/ranges/IndexRange.hpp>
#include <dune/dec/ranges/IteratorRange.hpp>
#include <dune/dec/ranges/Map.hpp>
#include <dune/dec/ranges/FunctorRange.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec { namespace test {

  void map()
  {
    std::vector<double> vec { 1.0, 2.0, 3.0, 4.0, 5.0 };
    DEC_TEST_APPROX( (ranges::map([](auto v) { return 2*v; }, vec)), (std::vector<double>{2.0, 4.0, 6.0, 8.0, 10.0}));
  }

  void enumerate()
  {
    std::vector<double> vec { 1.0, 2.0, 3.0, 4.0, 5.0 };
    DEC_TEST_APPROX( ranges::enumerate(vec),
                     (std::vector<std::pair<std::size_t,double>>{{0,1.0}, {1,2.0}, {2,3.0}, {3,4.0}, {4,5.0}}));
  }

  void iterator_range()
  {
    auto vec = ranges::irange(5);

    DEC_TEST_APPROX( (ranges::make_range(vec.begin(), vec.end(), [](auto& it, auto const /*end*/) { ++it; }, [](auto it) { return *it; })), vec );
    DEC_TEST_APPROX( (ranges::make_cyclic_range(0, 0, [](auto& it) { ++it; it = it%5; }, [](auto it) { return it; })), vec );
  }

  void index_range()
  {
    DEC_TEST_APPROX( ranges::irange(5), (std::vector<int> { 0, 1, 2, 3, 4 }));
  }

  void local_view()
  {
    std::vector<double> vec { 1.0, 2.0, 3.0, 4.0, 5.0 };
    auto idx = ranges::irange(5);

    DEC_TEST_APPROX( (Dec::local_view(vec, idx)), vec );
  }

  void functor()
  {
    DEC_TEST_APPROX( make_range([](int i) { return 2*i; }, 5), (std::vector<int> { 0, 2, 4, 6, 8 }));
  }

} }


int main()
{
  using namespace Dec;

  test::map();
  test::enumerate();
  test::iterator_range();
  test::index_range();
  test::local_view();
  test::functor();

  return report_errors();
}
