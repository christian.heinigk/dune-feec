#include <sstream>
#include <dune/dec/utility/Cast.hpp>
#include <dune/dec/utility/Tests.hpp>

using namespace Dec;

template <class T>
struct Converter
{
  Converter(const char* str)
  {
    std::istringstream ss(str);
    ss >> value;
  }

  T get() const
  {
    return value;
  }

private:
  T value;
};

// ---------------------------------------------------------------------------------------
void test0()
{
  DEC_TEST_EQ( -0.5f, cast<float>("-0.5") );
  DEC_TEST_EQ( -0.5,  cast<double>("-0.5") );
  DEC_TEST_EQ( -0.5l, cast<long double>("-0.5") );

  auto c = cast< Converter<double> >("-0.5");
  DEC_TEST_EQ( -0.5, c.get() );
}

int main()
{
  test0();

  return report_errors();
}
