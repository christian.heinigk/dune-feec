#include <dune/grid/common/rangegenerators.hh>

#include <dune/dec/DecGrid.hpp>
#include <dune/dec/common/FieldMatVec.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  bool simplegrid_0d(double tol)
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.5, 1.5};

    std::vector<std::vector<int>> cells = { {0} };
    std::vector<WorldVector> coords = {x0};

    using GridBase = Dec::SimpleGrid<0,2,float_type,int>;
    GridBase simplegrid(cells, coords);

    DEC_TEST( simplegrid.size(0) == 1 );

//     DecGrid<GridBase> grid(simplegrid);
//
//     for (auto const& v : vertices(grid.leafGridView()))
//     {
//       if (distance(grid.center(v), x0) > tol)
//         return false;
//     }

    return true;
  }


  bool simplegrid_1d(double tol)
  {
    using WorldVector = Dune::FieldVector<float_type, 1>;

    WorldVector x0 = {0.5};
    WorldVector x1 = {1.5};

    std::vector<std::vector<int>> cells = { {0, 1} };
    std::vector<WorldVector> coords = {x0, x1};

    using GridBase = Dec::SimpleGrid<1,1,float_type,int>;
    GridBase simplegrid(cells, coords);

    auto indexSet = simplegrid.leafIndexSet();
    for (auto const& v : vertices(simplegrid.leafGridView()))
    {
      if (distance(v.corner(0), coords[indexSet.index(v)]) > tol)
        return false;
    }

    for (auto const& e : elements(simplegrid.leafGridView()))
    {
      for (int i = 0; i < 2; ++i)
        if (distance(e.corner(i), coords[i]) > tol)
          return false;
    }

//     DecGrid<GridBase> grid(simplegrid);

    return true;
  }

}}


int main()
{
  using namespace Dec;
  double tol = 1.e-10;

  DEC_TEST( test::simplegrid_0d(tol) );
  DEC_TEST( test::simplegrid_1d(tol) );

  return report_errors();
}
