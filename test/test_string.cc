#include <vector>

#include <dune/dec/utility/String.hpp>
#include <dune/dec/utility/Tests.hpp>

using namespace Dec;

// ---------------------------------------------------------------------------------------
void test0()
{
  DEC_TEST_EQ( to_upper("Hallo Welt"), std::string("HALLO WELT") );
  DEC_TEST_EQ( to_lower("Hallo Welt"), std::string("hallo welt") );

  DEC_TEST_EQ( trim_copy("  Hallo Welt  "), std::string("Hallo Welt") );
}

// ---------------------------------------------------------------------------------------
// test the split() function
void test1()
{
  std::vector<std::string> subs;
  std::string text = "Dies ist ein langer Text";
  split(text.begin(), text.end(), ' ', [&subs](auto it0, auto it1) { subs.push_back(std::string(it0, it1)); });
  DEC_TEST_EQ( subs.size(), 5 );
  DEC_TEST_EQ( subs[0], std::string("Dies") );
  
  int n = 0;
  std::string sep = " x";
  split(text.begin(), text.end(), sep.begin(), sep.end(), [&n](auto,auto) { ++n; });
  DEC_TEST_EQ( n, 6 );
  
  n = 0;
  std::string text0 = "";
  split(text0.begin(), text0.end(), ' ', [&n](auto,auto) { ++n; });
  DEC_TEST_EQ( n, 0 );
  
  std::string text1 = "Hallo";
  split(text1.begin(), text1.end(), ' ', [](auto it0, auto it1) 
  { 
    DEC_TEST_EQ( std::string(it0, it1), std::string("Hallo") ); 
  });
}


int main()
{
  test0();
  test1();

  return report_errors();
}
