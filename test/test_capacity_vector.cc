#include <dune/dec/utility/CapacityVector.hpp>
#include <dune/dec/utility/Tests.hpp>

using namespace Dec;

// ---------------------------------------------------------------------------------------
void test0()
{
  using Array = CapacityVector<double, 10>;
  Array a;
  
  DEC_TEST( a.empty() );

  for (size_t i = 0; i < 3; ++i)
    a.push_back( double(i) );

  DEC_TEST( !a.empty() );
  DEC_TEST_EQ( a.size(), size_t(3) );

  size_t i = 0;
  for (auto const& a_i : a) {
    DEC_TEST_EQ( a_i, double(i) );
    ++i;
  }
}


int main()
{
  test0();

  return report_errors();
}
