#include <dune/grid/common/rangegenerators.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/halfedgegrid/Grid.hpp>
#include <dune/dec/linear_algebra/block.hpp>
#include <dune/dec/linear_algebra/krylov/cg.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  void block(double tol)
  {
    static constexpr int dim = 2;
    static constexpr int dow = 2;
    using WorldVector = Dune::FieldVector<float_type, dow>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {0.5, std::sqrt(3.0)/2.0};
    WorldVector x3 = {0.5, -std::sqrt(3.0)/2.0};

    std::vector<std::vector<int>> cells = { {0,1,2}, {1,0,3} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using GridBase = SimpleGrid<dim,dow,float_type,int>;
    GridBase simplegrid(cells, coords);

    using Grid = HalfEdgeGrid<GridBase>;
    using GridView = typename Grid::GridView;
    Grid grid(simplegrid);
    auto gv = grid.leafGridView();

    // build up laplace matrix
    LaplaceBeltrami<GridView> laplace(gv);

    BlockMatrix<double, 2, 2> A;
    laplace.build(A(0,0));
    laplace.build(A(1,1));

    A(0,0).clear_dirichlet_row(0);
    A(1,1).clear_dirichlet_row(0);

    // build rhs vector
    msg("build rhs vector...");
    BlockVector<double, 2> b(grid.leafGridView(), {0,0});
    msg("b.setConstant");
    b.setConstant(1.0);
//     b[1].setConstant(1.0);

    msg("construct x from b...");
    BlockVector<double, 2> x(b, tag::ressource{});

    msg("x.setZero");
    x.setZero();
    msg("bcgs...");
    solver::bcgs(A, x, b);

    auto v1 = unary_dot(b);
    auto v2 = two_norm(b);

    auto v3 = dot(b, x);

  }

}}


int main(int argc, char** argv)
{
  using namespace Dec;
  decpde::init(argc, argv);

  double tol = 1.e-10;

  test::block(tol);

  return report_errors();
}
