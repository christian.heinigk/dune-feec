#include <dune/grid/common/rangegenerators.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/halfedgegrid/Grid.hpp>
#include <dune/dec/linear_algebra/iteration.hpp>
#include <dune/dec/linear_algebra/preconditioner.hpp>
#include <dune/dec/linear_algebra/smoother.hpp>
#include <dune/dec/linear_algebra/krylov/cg.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  void solver(double tol)
  {
    static constexpr int dim = 2;
    static constexpr int dow = 2;
    using WorldVector = Dune::FieldVector<float_type, dow>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {0.5, std::sqrt(3.0)/2.0};
    WorldVector x3 = {0.5, -std::sqrt(3.0)/2.0};

    std::vector<std::vector<int>> cells = { {0,1,2}, {1,0,3} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using GridBase = SimpleGrid<dim,dow,float_type,int>;
    GridBase simplegrid(cells, coords);

    using Grid = HalfEdgeGrid<GridBase>;
    using GridView = typename Grid::GridView;
    Grid grid(simplegrid);
    auto gv = grid.leafGridView();

    // build up laplace matrix
    LaplaceBeltrami<GridView> laplace(gv);
    DOFMatrix<double> A(grid.size(0), grid.size(0));
    laplace.build(A);
    A.clear_dirichlet_row(0);

    // build rhs vector
    DOFVector<double> b(grid.leafGridView(), 0, 1.0);
    DOFVector<double> x(grid.leafGridView(), 0);

    x.setZero();
    solver::bcgs(A, x, b);

//     x.setZero();
    BasicIteration<double> iter0(100);
//     solver::bcgs(A, x, b, iter0); // ERROR

    x.setZero();
    ResidualIteration<double> iter1(100, 1.e-7);
    solver::bcgs(A, x, b, iter1);

    x.setZero();
    GenericIteration<NoisyIteration<double>> iter2{[&A,&x,&b]() { return residuum(A,x,b); }, 100, 1.e-7};
    solver::bcgs(A, x, b, iter2);

    x.setZero();
    NoisyIteration<double> iter3(100, 1.e-7);
    solver::bcgs(A, x, b, iter3);

    x.setZero();
    CyclicIteration<double> iter4(100, 1.e-7, 10);
    solver::bcgs(A, x, b, iter4);

    x.setZero();
    auto solver = GaussSeidel{};
    solver.compute(A).solve(b, x, iter0);
//     solver.compute(A).solveWithGuess(b, x, iter1); // ERROR
    solver.compute(A).solveWithGuess(b, x, iter2);

    x.setZero();
    auto iter_solver = iterated(solver, iter0);
    iter_solver.compute(A).solve(b, x);
    iter_solver.compute(A).solveWithGuess(b, x);

    auto resid = [&A,&x,&b]() { return residuum(A,x,b); };
    GenericIteration<decltype(iter0)> giter0(resid, iter0);
    GenericIteration<decltype(iter1)> giter1(resid, iter1);
    GenericIteration<decltype(iter2)> giter2(resid, iter2);
    GenericIteration<decltype(iter3)> giter3(resid, iter3);
    GenericIteration<decltype(iter4)> giter4(resid, iter4);
  }

}}


int main(int argc, char** argv)
{
  using namespace Dec;
  decpde::init(argc, argv);

  double tol = 1.e-10;

  test::solver(tol);

  return report_errors();
}
