#include <dune/dec/nurbs/Bernstein.hpp>

#include <dune/dec/utility/Tests.hpp>

void plot_curve()
{
  using namespace Dec;
  Coord<2> P0 = {-2.0, 2.0};
  Coord<2> P1 = {-1.0, 3.5};
  Coord<2> P2 = {0.0, 1.0};
  Coord<2> P3 = {1.0, 2.0};
  Coord<2> P4 = {1.5, 4.5};
  Coord<2> P5 = {-3, 0.5};

  std::vector<Coord<2>> points = {P0, P1, P2, P3, P4, P5, P0};
  std::vector<double> weights = {1.0, 1.0, 2.0, 1.0, 3.0, 2.0, 1.0};

  plot(points, "deCasteljau.dat");
  plot(points, weights, "deCasteljau_rational.dat");
}


void plot_surface()
{
  using namespace Dec;

  Coord<3> P00 = {0.0, 0.0, 1.0};
  Coord<3> P10 = {1.0, 0.0, 1.5};
  Coord<3> P20 = {2.0, 0.0, 0.6};

  Coord<3> P01 = {-0.2, 1.0, 0.5};
  Coord<3> P11 = {1.3, 1.0, 1.0};
  Coord<3> P21 = {2.1, 1.0, 0.1};

  Coord<3> P02 = {0.2, 2.0, 0.25};
  Coord<3> P12 = {1.0, 2.0, 0.75};
  Coord<3> P22 = {1.9, 2.0, -0.15};

  Eigen::Matrix<Coord<3>, 3, 3> points;
  points <<  P00, P10, P20,
             P01, P11, P21,
             P02, P12, P22;

  plot2(points, "deCasteljau2.dat");
}

int main()
{
  using namespace Dec;

  double tol = 1.e-10;

  DEC_TEST( test::bernstein_nonnegativity(tol) );
  DEC_TEST( test::bernstein_partition_of_unity(tol) );
//  DEC_TEST( test::bernstein_symmetry(tol) );
//  DEC_TEST( test::bernstein_maximum(tol) );
//  DEC_TEST( test::bernstein_recursive_definition(tol) );

  DEC_TEST( test::point_on_bezier_curve_decasteljau(tol) );
  DEC_TEST( test::decasteljau_line(tol) );
  DEC_TEST( test::decasteljau_rational(tol) );
//  DEC_TEST( test::decasteljau_circle(tol) );

  DEC_TEST( test::decasteljau2_rational(tol) );

  plot_curve();
  plot_surface();

  return report_errors();
}
