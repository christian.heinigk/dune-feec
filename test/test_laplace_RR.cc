#include <dune/grid/common/rangegenerators.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/halfedgegrid/Grid.hpp>
#include <dune/dec/common/FieldMatVec.hpp>
#include <dune/dec/operators/Laplace.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  bool laplaceRR_2d(double tol)
  {
    static constexpr int dim = 2;
    static constexpr int dow = 2;
    using WorldVector = Dune::FieldVector<float_type, dow>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {0.5, std::sqrt(3.0)/2.0};
    WorldVector x3 = {0.5, -std::sqrt(3.0)/2.0};

    std::vector<std::vector<int>> cells = { {0,1,2}, {1,0,3} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using GridBase = SimpleGrid<dim,dow,float_type,int>;
    GridBase simplegrid(cells, coords);

    using Grid = HalfEdgeGrid<GridBase>;
    using GridView = typename Grid::GridView;
    Grid grid(simplegrid);
    auto gv = grid.leafGridView();

    LaplaceRR<GridView,1> laplace(gv);
    DOFMatrix<double> A(grid.size(1), grid.size(1));
    DOFMatrix<double> B(grid.size(1), grid.size(1));
    laplace.build(A);

    {
      auto ins = B.inserter();
      ins(0,0) << -8; ins(0,1) <<  4; ins(0,2) <<  4; ins(0,3) << -4; ins(0,4) << -4;
      ins(1,0) <<  8; ins(1,1) << -8; ins(1,2) << -8;
      ins(2,0) <<  8; ins(2,1) << -8; ins(2,2) << -8;
      ins(3,0) << -8; ins(3,3) << -8; ins(3,4) << -8;
      ins(4,0) << -8; ins(4,3) << -8; ins(4,4) << -8;
    }

#ifdef DEC_HAS_EIGEN
    return (A - B).norm() < tol;
#else
    print(A);
    print(B);

    return true;
#endif
  }

}}


int main(int argc, char** argv)
{
  using namespace Dec;
  decpde::init(argc, argv);

  double tol = 1.e-10;

  DEC_TEST(( test::laplaceRR_2d(tol) ));

  return report_errors();
}
