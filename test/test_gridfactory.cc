#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/grid/albertagrid.hh>

#include <dune/dec/CoordsGridFactory.hpp>
#include <dune/dec/common/FieldMatVec.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>
#include <dune/dec/simplegrid/SimpleGridFactory.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  bool simplegrid(double /*tol*/ = 0)
  {
    using WorldVector = Dune::FieldVector<float_type, 1>;

    WorldVector x0 = {0.0};
    WorldVector x1 = {1.0};

    std::vector<std::vector<unsigned int>> cells = { {0, 1} };
    std::vector<WorldVector> coords = {x0, x1};

    using Grid = Dec::SimpleGrid<1,1,float_type,int>;
    std::unique_ptr<Grid> grid{ Dune::CoordsGridFactory<Grid>::createGrid(cells, coords) };

    DEC_TEST_EQ( grid->size(0), 1 );
    DEC_TEST_EQ( grid->size(1), 2 );

    return true;
  }

  bool albertagrid(double /*tol*/ = 0)
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {1.0, 1.0};
    WorldVector x3 = {0.0, 1.0};

    std::vector<std::vector<unsigned int>> cells = { {2,0,1}, {0,2,3} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using Grid = Dune::AlbertaGrid<2,2>;
    std::unique_ptr<Grid> grid{ Dune::CoordsGridFactory<Grid>::createGrid(cells, coords) };

    DEC_TEST_EQ( grid->size(0), 2 );
    DEC_TEST_EQ( grid->size(1), 5 );
    DEC_TEST_EQ( grid->size(2), 4 );

    return true;
  }

}}


int main()
{
  using namespace Dec;
  double tol = 1.e-10;

  DEC_TEST(( test::simplegrid(tol) ));
  DEC_TEST(( test::albertagrid(tol) ));

  return report_errors();
}
