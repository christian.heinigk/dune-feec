#include <dune/dec/LinearAlgebra.hpp>
#include <dune/dec/linear_algebra/krylov/cg.hpp>
#include <dune/dec/linear_algebra/krylov/bcgs.hpp>
#include <dune/dec/linear_algebra/preconditioner/Diagonal.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  // ---------------------------------------------------------------------------------------
  bool dofvector(double /*tol*/ = 0)
  {
    // create empty vector
    DOFVector<double> v0;
    DEC_TEST_EQ( v0.size(), 0 );

    // create vector with size
    DOFVector<double> v1(10);
    DEC_TEST_EQ( v1.size(), 10 );
    DEC_TEST_EQ( v1.num_rows(), 10 );
    DEC_TEST_EQ( v1.num_cols(), 1 );

    // create vector with size and initial value
    DOFVector<double> v2(10, 2.0);
    DEC_TEST_EQ( v2[0], 2.0 );

    // create a copy of vector
    DOFVector<double> v3(v2);

    // create a vector of same size (do not copy values)
    DOFVector<double> v4(v2, tag::ressource{});

    // fill vector with random values, with mean=0 and amplitude=1
    random(v4, 0.0, 1.0);

    // assign vector
    v2 = v3;
    v2 += v3;
    v2 -= v3;

    DEC_TEST_EQ( v2[0], v3[0] );

    v1.setConstant(1.0);

#ifndef DEC_HAS_PETSC
    // extract a sub-vector
    auto sub_v2 = v2.segment(0, 5);
    DEC_TEST_EQ( sub_v2.size(), 5 );

    DOFVector<double> v5(sub_v2);
    DEC_TEST_EQ( v5.size(), 5 );
    DEC_TEST_EQ( v5[0], v2[0] );
#endif

    auto d23 = dot(v2, v3);
    DEC_TEST_EQ( d23, 40.0 );

    auto d22 = unary_dot(v2);
    DEC_TEST_EQ( d22, 40.0 );

    auto n3 = two_norm(v3);

    DEC_TEST_EQ( n3, std::sqrt(40.0) );

    return true;
  }

  template <class Matrix>
  void fill_matrix(Matrix& A)
  {
    auto ins = A.inserter(3); // maximal 3 nz per row
    for (std::size_t i = 0; i < A.num_rows(); ++i) {
      for (std::size_t j = max(0, int(i)-1);
                       j < min(i+2, A.num_cols());
                     ++j) {
        ins(i,j) << (i == j ? 2.0 : -1.0);
      }
    }
    ins(0,A.num_cols()-1) << -1.0;
    ins(A.num_cols()-1,0) << -1.0;
  }

  // ---------------------------------------------------------------------------------------
  bool dofmatrix(double /*tol*/)
  {
    // create an empty matrix
    DOFMatrix<double> M1;
    DEC_TEST_EQ( M1.size(), 0 );

    // write a matrix with size
    DOFMatrix<double> M2(10, 10);

    DEC_TEST_EQ( M2.num_rows(), 10 );
    DEC_TEST_EQ( M2.num_cols(), 10 );

    // create acopy of the matrix
    DOFMatrix<double> M3(M2);

    DEC_TEST_EQ( M3.size(), M2.size() );

    M3 = M2;
    M3 = 0;

#ifndef DEC_HAS_PETSC
    DEC_TEST_EQ( M3(0,0), 0.0 );
    DEC_TEST_EQ( M2(1,0), 0.0 );
#endif

    fill_matrix(M3);

#ifndef DEC_HAS_PETSC
    DEC_TEST_EQ( M3(0,0), 2.0 );
    DEC_TEST_EQ( M3(0,1), -1.0 );
    DEC_TEST_EQ( M3(1,0), -1.0 );
#endif

    M3.clear_dirichlet_row(0);
#ifndef DEC_HAS_PETSC
    DEC_TEST_EQ( M3(0,0), 1.0 );
    DEC_TEST_EQ( M3(0,1), 0.0 );
#endif

    return true;
  }


  // ---------------------------------------------------------------------------------------
  bool dofmatvec(double /*tol*/)
  {
    DOFMatrix<double> A(10, 10);
    fill_matrix(A);

    DOFVector<double> b(10, 1.0);

    DOFVector<double> x1 = A*b;
    DOFVector<double> x2( A*b );

    DOFVector<double> x3(10);
    x3 = A*b;

    for (std::size_t i = 0; i < x1.size(); ++i)
      DEC_TEST_EQ( x1[i], 0.0 );

    return true;
  }


  // ---------------------------------------------------------------------------------------
  bool krylov(double /*tol*/)
  {
    DOFVector<double> b(10, 1.0);
    DOFMatrix<double> A(10, 10);
    fill_matrix(A);
    A.clear_dirichlet_row(0);
    b[0] = 1.0;

    // use conjugate gradient method
    DOFVector<double> x1(A.num_cols());
    x1[0] = 1.0;
    int iter1 = solver::cg(A, x1, b);
    msg("iter(cg) = ",iter1);
    DEC_TEST( residuum(A, x1, b) < 1.e-8 );

    // use stabilized bi-conjugate method
    DOFVector<double> x2(A.num_cols());
    x2[0] = 1.0;
    int iter2 = solver::bcgs(A, x2, b);
    msg("iter(bcgs) = ",iter2);

    DEC_TEST( residuum(A, x2, b) < 1.e-8 );

#ifndef DEC_HAS_PETSC
    // use preconditioned conjugate gradient method
    DOFVector<double> x3(A.num_cols());
    x3[0] = 1.0;
    auto P = precon::Diagonal{};
    int iter3 = solver::pcg(A, x3, b, P.compute(A));
    msg("iter(pcg) = ",iter3);

    DEC_TEST( residuum(A, x3, b) < 1.e-8 );
    msg("residuum = ",residuum(A, x3, b));
#endif

    return true;
  }

}}


int main(int argc, char** argv)
{
  using namespace Dec;

  decpde::init(argc, argv);
  double tol = 1.e-10;

  test::dofvector(tol);
  test::dofmatrix(tol);
  test::dofmatvec(tol);
  test::krylov(tol);

  return report_errors();
}
