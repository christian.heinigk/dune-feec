#include <dune/grid/common/rangegenerators.hh>

#include <dune/dec/Dec.hpp>
#include <dune/dec/halfedgegrid/Grid.hpp>
#include <dune/dec/common/FieldMatVec.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  bool half_edges_2d(double /*tol*/ = 0)
  {
    static constexpr int dim = 2;
    static constexpr int dow = 2;
    using WorldVector = Dune::FieldVector<float_type, dow>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {1.0, 1.0};
    WorldVector x3 = {0.0, 1.0};

    std::vector<std::vector<int>> cells = { {2,0,1}, {0,2,3} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using GridBase = Dec::SimpleGrid<dim,dow,float_type,int>;
    GridBase simplegrid(cells, coords);

    msg("simplegrid.size(0) = ",simplegrid.size(0));
    msg("simplegrid.size(1) = ",simplegrid.size(1));
    msg("simplegrid.size(2) = ",simplegrid.size(2));

    auto const& indexSet = simplegrid.leafIndexSet();
    for (auto const& elem : elements(simplegrid.leafGridView())) {
      msg("elem[",indexSet.index(elem),"]");

      for (int i = 0; i < 3; ++i)
        msg(" vertex[",indexSet.subIndex(elem, i, dim),"]");

      for (int i = 0; i < 3; ++i)
        msg(" edge[",indexSet.subIndex(elem, i, 1),"]");
    }
    msg("---------------------------");

    using Grid = HalfEdgeGrid<GridBase>;
    Grid grid(simplegrid);
    auto const& is = grid.indexSet();
    msg("vertices_ = [");
    for (std::size_t i = 0; i < is.vertices_.size(); ++i)
      msg(is.vertices_[i]," ");
    msg("]");
    msg("edges_ = [");
    for (std::size_t i = 0; i < is.edges_.size(); ++i)
      msg(is.edges_[i]," ");
    msg("]");
    msg("faces_ = [");
    for (std::size_t i = 0; i < is.faces_.size(); ++i)
      msg(is.faces_[i]," ");
    msg("]");
    msg("half_edges_ = [");
    for (std::size_t i = 0; i < is.half_edges_.size(); ++i)
      msg("{[",i,"]: ",is.halfEdge(i).next_,", ",is.halfEdge(i).previous_,", ",is.halfEdge(i).opposite_,", ",is.halfEdge(i).vertex_,", ",is.halfEdge(i).edge_,", ",is.halfEdge(i).face_,"} ");
    msg("]");




    for (auto const& elem : elements(grid.gridView())) {
      msg("elem [",elem.index(),"]");
      for (auto const& v : vertices(elem)) {
        msg("  vertex ",v.index());
        for (auto const& e : edges(v))
          msg("    edge ",e.index());
      }
      for (auto const& e : edges(elem)) {
        msg("  edge ",e.index());
        for (auto const& v : vertices(e))
          msg("    vertex ",v.index());
      }
      msg("");
    }
    msg("-------------------------------");
    msg("subIndices...");


    for (auto const& elem : elements(grid.gridView())) {
      msg("elem [",elem.index(),"]");
      for (auto const& v : is.vertexIndices(elem)) {
        msg("  vertex ",v);
        for (auto const& e : is.edgeIndices(grid.entity<2>(is.halfEdgeOfIndex<2>(v))))
          msg("    edge ",e);
      }
      for (auto const& e : is.edgeIndices(elem)) {
        msg("  edge ",e);
        for (auto const& v : is.vertexIndices(grid.entity<1>(is.halfEdgeOfIndex<1>(e))))
          msg("    vertex ",v);
        for (auto const& f : is.faceIndices(grid.entity<1>(is.halfEdgeOfIndex<1>(e))))
          msg("    face ",f);
      }
      msg("");
    }
    msg("-------------------------------");


    for (auto const& elem : elements(grid.gridView())) {
      msg("elem [",elem.index(),"]:");
      msg("  center=(",elem.geometry().center()," = ",grid.center(elem),")");
      msg("  volume=(",elem.geometry().volume()," = ",grid.volume(elem),")");
      msg("  dual_volume=(",elem.geometry().dual_volume()," = ",grid.dual_volume(elem),")");
    }
    msg("");

    for (auto const& edge : edges(grid.gridView())) {
      msg("edge [",edge.index(),"]:");
      msg("  center=(",edge.geometry().center()," = ",grid.center(edge),")");
      msg("  volume=(",edge.geometry().volume()," = ",grid.volume(edge),")");
      msg("  dual_volume=(",edge.geometry().dual_volume()," = ",grid.dual_volume(edge),")");
    }
    msg("");

    for (auto const& v : vertices(grid.gridView())) {
      msg("vertex [",v.index(),"]:");
      msg("  center=(",v.geometry().center()," = ",grid.center(v),")");
      msg("  volume=(",v.geometry().volume()," = ",grid.volume(v),")");
      msg("  dual_volume=(",v.geometry().dual_volume()," = ",grid.dual_volume(v),")");
    }
    msg("");

    msg("-------------------------------");


    for (auto const& elem : elements(grid.gridView())) {
      msg("elem[",elem.index(),"]:");
      for (auto const& v : vertices(elem))
        msg("  vertex[",v.index(),"]");
      for (auto const& e : edges(elem))
        msg("  edge[",e.index(),"]");
      for (auto const& f : faces(elem))
        msg("  face[",f.index(),"]");
    }
    msg("");

    for (auto const& edge : edges(grid.gridView())) {
      msg("edge[",edge.index(),"]:");
      for (auto const& v : vertices(edge))
        msg("  vertex[",v.index(),"]");
      for (auto const& e : edges(edge))
        msg("  edge[",e.index(),"]");
      for (auto const& f : faces(edge))
        msg("  face[",f.index(),"]");
    }
    msg("");

    for (auto const& vertex : vertices(grid.gridView())) {
      msg("vertex[",vertex.index(),"]:");
      for (auto const& v : vertices(vertex))
        msg("  vertex[",v.index(),"]");
      for (auto const& e : edges(vertex))
        msg("  edge[",e.index(),"]");
      for (auto const& f : faces(vertex))
        msg("  face[",f.index(),"]");
    }

    return true;
  }

}}


int main(int argc, char** argv)
{
  using namespace Dec;
  decpde::init(argc, argv);

  double tol = 1.e-10;

  DEC_TEST(( test::half_edges_2d(tol) ));

  return report_errors();
}
