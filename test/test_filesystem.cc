#include <dune/dec/utility/Filesystem.hpp>

#include <dune/dec/utility/Tests.hpp>


using namespace Dec;

// ---------------------------------------------------------------------------------------
void test0()
{
  using namespace Dec::filesystem;
  DEC_TEST_EQ( path("a/b/c").parent_path(), path("a/b") );
  DEC_TEST_EQ( path("a/b/c.txt").parent_path(), path("a/b") );
  DEC_TEST_EQ( path("a/b/c.txt").filename(), path("c.txt") );
  DEC_TEST_EQ( path("a/b/c.txt").stem(), path("c") );

  DEC_TEST_EQ( path("a/b/c.txt").extension(), path(".txt") );
  DEC_TEST_EQ( path("a/b/c.").extension(), path(".") );
  DEC_TEST_EQ( path(".txt").extension(), path(".txt") );

  DEC_TEST( !path("a/b/c").is_absolute() );
  DEC_TEST( !path("a\\b\\c").is_absolute() );
#ifdef _WIN32
  DEC_TEST( path("a:\\b\\c").is_absolute() );
  DEC_TEST( !path("a:\\b\\c").is_relative() );
#else
  DEC_TEST( path("/a/b/c").is_absolute() );
  DEC_TEST( !path("/a/b/c").is_relative() );
  DEC_TEST( path("/").is_absolute() );
#endif

  DEC_TEST( path("a/b/c").is_relative() );

  DEC_TEST_EQ( path("a/b/c.txt").remove_filename(), path("a/b") );
  DEC_TEST_EQ( path("a/b/c").string(), "a/b/c" );

  DEC_TEST_EQ( path("a/b/../c").string(), "a/c" );
  DEC_TEST_EQ( path("a/b/./c").string(), "a/b/c" );
  DEC_TEST_EQ( path("../a/b/c").string(), "../a/b/c" );
  DEC_TEST_EQ( path("./a/b/c").string(), "a/b/c" );
}

void test1()
{
  using namespace Dec::filesystem;
  DEC_TEST( exists( path("/tmp") ) );
  DEC_TEST( exists("/etc/fstab") );
}

int main()
{
  test0();
  test1();

  return report_errors();
}
