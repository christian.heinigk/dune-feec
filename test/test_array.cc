#include <dune/dec/common/Array.hpp>
#include <dune/dec/utility/Tests.hpp>

using namespace Dec;

// ---------------------------------------------------------------------------------------
void test0()
{
  using Array = std::array<double, 3>;
  Array a{{1.0, 2.0, 3.0}};
  Array b{{2.0, 3.0, 4.0}};

  DEC_TEST_EQ( a+b, (Array{{3.0, 5.0, 7.0}}) );
  DEC_TEST_EQ( a-b, (Array{{-1.0, -1.0, -1.0}}) );

  DEC_TEST_EQ( a*2.0, (Array{{2.0, 4.0, 6.0}}) );
  DEC_TEST_EQ( 2.0*a, (Array{{2.0, 4.0, 6.0}}) );

  DEC_TEST_EQ( b/2.0, (Array{{1.0, 1.5, 2.0}}) );
}

// ---------------------------------------------------------------------------------------
void test1()
{
  using Array = std::array<double, 3>;
  Array a{{1.0, 2.0, 3.0}};
  Array b{{2.0, 3.0, 4.0}};

  DEC_TEST_EQ( unary_dot(a), 14.0 );
  DEC_TEST_EQ( dot(a, b), 20.0 );

  DEC_TEST_APPROX( two_norm(a), std::sqrt(14.0) );
  DEC_TEST_APPROX( distance(a, b), std::sqrt(3.0) );

  cross(a, b);
}

int main()
{
  test0();
  test1();

  return report_errors();
}
