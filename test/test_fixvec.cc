#include <dune/dec/fixmatvec/FixMat.hpp>
#include <dune/dec/fixmatvec/FixMatOperations.hpp>
#include <dune/dec/fixmatvec/ReductionOperations.hpp>
#include <dune/dec/fixmatvec/Factorization.hpp>

#include <dune/dec/utility/Tests.hpp>

using namespace Dec;

// ---------------------------------------------------------------------------------------
void test0()
{
  using Array = FixVec<double, 3>;
  Array a{1.0, 2.0, 3.0};
  Array b{2.0, 3.0, 4.0};

  DEC_TEST_EQ( a+b, (Array{3.0, 5.0, 7.0}) );
  DEC_TEST_EQ( a-b, (Array{-1.0, -1.0, -1.0}) );

  DEC_TEST_EQ( a*2,   (Array{2.0, 4.0, 6.0}) );
  DEC_TEST_EQ( 2.0*a, (Array{2.0, 4.0, 6.0}) );

  DEC_TEST_EQ( b/2.0, (Array{1.0, 1.5, 2.0}) );

  DEC_TEST_EQ( a + 2*b, (Array{5.0, 8.0, 11.0}) );
}

// ---------------------------------------------------------------------------------------
void test1()
{
  using Array = FixVec<double, 3>;
  Array a{1.0, 2.0, 3.0};
  Array b{2.0, 3.0, 4.0};

  DEC_TEST_EQ( unary_dot(a), 14.0 );
  DEC_TEST_EQ( dot(a, b), 20.0 );
  DEC_TEST_EQ( dot(a, b), dot(b, a) );

  DEC_TEST_APPROX( two_norm(a), std::sqrt(14.0) );
  DEC_TEST_APPROX( distance(a, b), std::sqrt(3.0) );

  DEC_TEST_EQ( cross(a, b), (Array{-1.0, 2.0, -1.0}) );
}

// ---------------------------------------------------------------------------------------
void test2()
{
  using Array = FixVec<double, 3>;
  using Matrix = FixMat<double, 3, 3>;

  Matrix A{ {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
  Matrix B{ {1.0, 1.0, 1.0}, {2.0, 1.0, 0.0}, {3.0, 1.0, -1.0} };
  Matrix C{ {0.5, 2.0, -1.0}, {1.0, -1.0, 1.0}, {-0.25, 0.5, 4.0} };
  Matrix D{ {-0.25, 1.0, -1.0}, {2.0, -2.0, 8.0}, {-0.125, -0.125, 2.0} };

  DEC_TEST_EQ( row(A, 0), (Array{1.0, 0.0, 0.0}) );
  DEC_TEST_EQ( row(B, 0), (Array{1.0, 1.0, 1.0}) );

  DEC_TEST_EQ( A+B,   (Matrix{ {2.0, 1.0, 1.0}, { 2.0, 2.0, 0.0}, { 3.0, 1.0, 0.0} }) );
  DEC_TEST_EQ( A-B,   (Matrix{ {0.0,-1.0,-1.0}, {-2.0, 0.0, 0.0}, {-3.0,-1.0, 2.0} }) );

  DEC_TEST_EQ( A*2,   (Matrix{ {2.0, 0.0, 0.0}, {0.0, 2.0, 0.0}, {0.0, 0.0, 2.0} }) );
  DEC_TEST_EQ( 2.0*A, (Matrix{ {2.0, 0.0, 0.0}, {0.0, 2.0, 0.0}, {0.0, 0.0, 2.0} }) );

  DEC_TEST_EQ( A/2.0, (Matrix{ {0.5, 0.0, 0.0}, {0.0, 0.5, 0.0}, {0.0, 0.0, 0.5} }) );


  Array a{1.0, 2.0, 3.0};
  Array b{2.0, 3.0, 4.0};

  DEC_TEST_EQ( outer(a, b), (Matrix{ {2.0, 3.0, 4.0}, {4.0, 6.0, 8.0}, {6.0, 9.0, 12.0} }) );
  DEC_TEST_EQ( diagonal(a), (Matrix{ {1.0, 0.0, 0.0}, {0.0, 2.0, 0.0}, {0.0, 0.0, 3.0} }) );

  DEC_TEST_EQ( diagonal(A), (Array{1.0, 1.0, 1.0}) );

  // some tests for the determinant calculation

  DEC_TEST_EQ( det(A), 1.0 );
  DEC_TEST_EQ( det(B), 0.0 );

  DEC_TEST_EQ( det(C*D), det(C)*det(D) );
  DEC_TEST_EQ( det(trans(C)), det(C) );
  DEC_TEST_EQ( det(2.0*D), pow<3>(2.0)*det(D) );

  // some tests for the trace

  DEC_TEST_EQ( trace(D), trace(trans(D)) );
  DEC_TEST_EQ( trace(2.0*C + 0.5*D), 2.0*trace(C) + 0.5*trace(D) );
  DEC_TEST_EQ( trace(C*D), trace(D*C) );
  DEC_TEST_EQ( trace(C), sum(diagonal(C)) );

  // some tests for the norm

  DEC_TEST_APPROX( sqr(frobenius_norm(B)), trace(hermitian(B)*B) );
  DEC_TEST( frobenius_norm(C*D) <= frobenius_norm(B)*frobenius_norm(C) );

  // test the inverse

  DEC_TEST_EQ( inv(C), adj(C)/det(C) );
  DEC_TEST_APPROX( frobenius_norm( inv(C)*C ), frobenius_norm(eye<double, 3>()) );

  auto I = eye<double>(index_<3>);
  auto one = ones<double>(index_<4>,index_<5>);
  auto zero = zeros<double>(index_<3>, index_<3>);

  DEC_TEST_EQ( I(1,1), 1 );
  DEC_TEST_EQ( I(1,0), 0 );

  DEC_TEST_EQ( one(1,0), 1 );

  DEC_TEST_EQ( zero(1,1), 0 );
  DEC_TEST_EQ( zero(1,0), 0 );
}

// ---------------------------------------------------------------------------------------
void test3()
{
  FixMat<double, 3, 3> B{ {1.0, 1.0, 1.0}, {2.0, 1.0, 0.0}, {3.0, 1.0, -1.0} };
  FixMat<double, 3, 3> C{ {0.5, 2.0, -1.0}, {1.0, -1.0, 1.0}, {-0.25, 0.5, 4.0} };

  FixMat<double, 2, 2> B2{ {1.0, 1.0}, {2.0, 1.0} };
  FixMat<double, 2, 2> C2{ {-1.0,1.0}, {0.5, 4.0} };

  DEC_TEST_EQ( B(range_<0,2>, range_<0,2>), B2 );
  DEC_TEST_EQ( C(range_<1,3>, range_<1,3>), C2 );

  auto Bsub = B(range_<0,2>, range_<0,2>);

  Bsub += B2;
  DEC_TEST_EQ( B(range_<0,2>, range_<0,2>), 2*B2 );
}

// ---------------------------------------------------------------------------------------
void test4()
{
  FixMat<double, 3, 3> C{ {0.5, 2.0, -1.0}, {1.0, -1.0, 1.0}, {-0.25, 0.5, 4.0} };

//   auto A = qr(C);

  FixVec<double, 3> b{ 1.0, 2.0, 3.0 };

  auto x = qr_solve(C, b);
  auto x_= inv(C) * b;

  DEC_TEST_APPROX( two_norm(x), two_norm(x_) );


  FixMat<double, 4, 3> C2{ {0.5, 2.0, -1.0}, {1.0, -1.0, 1.0}, {-0.25, 0.5, 4.0}, {-1.0,-1.5,-0.5} };
  FixVec<double, 4> b2{ 1.0, 2.0, 3.0, 4.0 };

//   auto A2 = qr(C);
  auto x2 = qr_solve(C2, b2);
  auto x2_= inv(trans(C2)*C2) * (trans(C2)*b2);

  DEC_TEST_APPROX( two_norm(x2), two_norm(x2_) );
}



int main()
{
  test0();
  test1();
  test2();
  test3();
  test4();

  return report_errors();
}
