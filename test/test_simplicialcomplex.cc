#include <dune/grid/common/rangegenerators.hh>

#include <dune/dec/DecGrid.hpp>
#include <dune/dec/common/FieldMatVec.hpp>
#include <dune/dec/simplegrid/SimpleGrid.hpp>

#include <dune/dec/utility/Tests.hpp>

namespace Dec {
namespace test {

  bool simplicial_complex_1d(double /*tol*/ = 0)
  {
    using WorldVector = Dune::FieldVector<float_type, 1>;

    WorldVector x0 = {0.0};
    WorldVector x1 = {1.0};

    std::vector<std::vector<int>> cells = { {0, 1} };
    std::vector<WorldVector> coords = {x0, x1};

    using GridBase = Dune::SimpleGrid<1,1,float_type,int>;
    GridBase simplegrid(cells, coords);

    SimplicialComplex<1> cmplx(simplegrid);

    DEC_TEST_EQ( (cmplx.subIndices<0,1>(0).size()) , 2u );
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(0)[0]) , 0u ); // vertex_0 of edge_0
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(0)[1]) , 1u ); // vertex_1 of edge_0

    return true;
  }

  bool simplicial_complex_2d(double /*tol*/ = 0)
  {
    using WorldVector = Dune::FieldVector<float_type, 2>;

    WorldVector x0 = {0.0, 0.0};
    WorldVector x1 = {1.0, 0.0};
    WorldVector x2 = {1.0, 1.0};
    WorldVector x3 = {0.0, 1.0};

    std::vector<std::vector<int>> cells = { {2,0,1}, {0,2,3} };
    std::vector<WorldVector> coords = {x0, x1, x2, x3};

    using GridBase = Dune::SimpleGrid<2,2,float_type,int>;
    GridBase simplegrid(cells, coords);

    SimplicialComplex<2> cmplx(simplegrid);

    DEC_TEST_EQ( (cmplx.subIndices<0,1>(0).size()) , 3u );
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(0)[0]) , 1u ); // edge_0 of face_0
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(0)[1]) , 3u ); // edge_1 of face_0
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(0)[2]) , 0u ); // edge_2 of face_0
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(1)[0]) , 1u ); // edge_0 of face_1
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(1)[1]) , 2u ); // edge_0 of face_1
    DEC_TEST_EQ( (cmplx.subIndices<0,1>(1)[2]) , 4u ); // edge_0 of face_1

    return true;
  }

}}


int main()
{
  using namespace Dec;
  double tol = 1.e-10;

  DEC_TEST(( test::simplicial_complex_1d(tol) ));
  DEC_TEST(( test::simplicial_complex_2d(tol) ));

  return report_errors();
}
